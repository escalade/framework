# hdf2root converter

The purpose of this program is to convert HDF5 into ROOT file under a quite simple convention.
- It converts HDF5 data set into trees.
- There are two types of leaves: dimensions and data leave
  Leave names are defined as following: "dim[0....N]" for dimensions; data
- Attributes are stored into 'TTree::GetUserInfo()'

```
--Usage: hdf2root [--help, -h] [--sequential] [--verbose] *.[hdf5|h5]
         --sequential  Enables sequential mode.
         --verbose  Enables print out.
         --help, -h  Shows this message.
```

NB: "Sequential mode" is not fully ready yet and "multidimension datasets" haven't been tested yet.
Any contribution in this regard would be very welcome.
