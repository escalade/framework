#include <iostream>
#include <string>
#include <vector>

#include "hdf2root.hh"

#include "H5Cpp.h"

#include <TString.h>
#include <TFile.h>
#include <TParameter.h>
#include <TSystem.h>
// #include "UIUC/HandboxMsg.h"

using namespace std;
using namespace H5;

//
// Marco Meyer <marco.meyer@cern.ch>
// May 29th, 2022

// /!\ SEQUENTIAL HDF5 IS UNTESTED
// I didn't have sequential files..
// So please.. send me an email (marco.meyer@cern.ch) with a sample if this breaks..
void reversePtr(hsize_t* dim, int rank)
{
  hsize_t *begin_ptr, *end_ptr, ch;
 
  begin_ptr = dim;
  end_ptr = dim;
 
  // Move the end_ptr to the last character
  for (int i = 0; i < rank - 1; i++)
    end_ptr++;
 
  // Swap the char from start and end
  // index using begin_ptr and end_ptr
  for (int i = 0; i < rank / 2; i++) {
 
    // swap character
    ch = *end_ptr;
    *end_ptr = *begin_ptr;
    *begin_ptr = ch;
 
    // update pointers positions
    begin_ptr++;
    end_ptr--;
  }
}

char getType(const AbstractDs &d) {

  // Hint: H5 type
  const DataType h5type = d.getDataType();
  if (h5type == PredType::NATIVE_SHORT ) return 'B';
  if (h5type == PredType::NATIVE_USHORT) return 'b';
  if (h5type == PredType::NATIVE_INT   ) return h5type.getSize() == 16 ? 'S' : 'I';
  if (h5type == PredType::NATIVE_UINT  ) return h5type.getSize() == 16 ? 's' : 'I';
  if (h5type == PredType::NATIVE_LONG  ) return 'I';
  if (h5type == PredType::NATIVE_ULONG ) return 'i';
  if (h5type == PredType::NATIVE_FLOAT ) return 'F';
  if (h5type == PredType::NATIVE_DOUBLE) return 'D';
  if (h5type == PredType::NATIVE_CHAR  ) return 'B';
  if (h5type == PredType::NATIVE_SCHAR ) return 'C';
  if (h5type == PredType::NATIVE_HBOOL ) return 'O';

  // Hint: H5 class
  const H5T_class_t h5class = h5type.getClass();
  if(h5class == H5T_STRING ) return 'C';
  if(h5class == H5T_INTEGER) return 'I';
  if(h5class == H5T_FLOAT  ) return 'I';
  // if(h5class == H5T_BITFIELD ) return ""; // Bit field types
  // if(h5class == H5T_OPAQUE   ) return ""; // Opaque types
  // if(h5class == H5T_COMPOUND ) return ""; // Compound types
  // if(h5class == H5T_REFERENCE) return ""; // Reference types
  // if(h5class == H5T_ENUM		  ) return ""; // Enumeration types
  // if(h5class == H5T_VLEN	    ) return ""; // Variable-Length types
  // if(h5class == H5T_ARRAY	  ) return ""; // Array types

  return 0;
}

TObject *createScalar(TString objName, const DataSet &dataSet)
{
    const DataSpace dataSpace = dataSet.getSpace();
    Int_t rank = dataSpace.getSimpleExtentNdims();
    if(rank) return nullptr;

    char type = getType(dataSet);
    switch(type) {

      case 'C':
      {
        string buf;
        dataSet.read(buf, dataSet.getDataType(), dataSpace);
        return new TNamed(objName, buf);
      }

      case 'I':
      {
        void *buf = malloc(sizeof(int));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p =new TParameter<int>(objName, *((int*) buf));
        free(buf);

        return p;
      }

      case 'F':
      {
        void *buf = malloc(sizeof(float));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p =new TParameter<float>(objName, *((float*) buf));
        free(buf);

        return p;
      }

      case 'D':
      {
        void *buf = malloc(sizeof(double));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p =new TParameter<double>(objName, *((double*) buf));
        free(buf);

        return p;
      }

      case 'L':
      {
        void *buf = malloc(sizeof(long long int));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p =new TParameter<long long int>(objName, *((long long int*) buf));
        free(buf);

        return p;
      }

      case 'G':
      {
        void *buf = malloc(sizeof(long int));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p =new TParameter<long int>(objName, *((long int*) buf));
        free(buf);

        return p;
      }

      case 'O':
      {
        void *buf = malloc(sizeof(bool));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p =new TParameter<bool>(objName, *((bool*) buf));
        free(buf);

        return p;
      }

      default:
        return nullptr;
    }
}

TObject *createAttribute(TString objName, const Attribute *attr)
{
    const DataSpace dataSpace = attr->getSpace();

    char type = getType(*attr);
    switch(type) {

      case 'C':
      {
        string buf;
        attr->read(attr->getDataType(), buf);
        return new TNamed(objName, buf);
      }

      case 'I':
      {
        void *buf = malloc(sizeof(int));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<int>(objName, *((int*) buf));
        free(buf);

        return p;
      }

      case 'F':
      {
        void *buf = malloc(sizeof(float));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<float>(objName, *((float*) buf));
        free(buf);

        return p;
      }

      case 'D':
      {
        void *buf = malloc(sizeof(double));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<double>(objName, *((double*) buf));
        free(buf);

        return p;
      }

      case 'L':
      {
        void *buf = malloc(sizeof(long long int));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<long long int>(objName, *((long long int*) buf));
        free(buf);

        return p;
      }

      case 'G':
      {
        void *buf = malloc(sizeof(long int));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<long int>(objName, *((long int*) buf));
        free(buf);

        return p;
      }

      case 'O':
      {
        void *buf = malloc(sizeof(bool));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<bool>(objName, *((bool*) buf));
        free(buf);

        return p;
      }

      default:
        return nullptr;
    }
}

TObject *createObject(TString objName, const DataSet &dataSet, Bool_t bSequential, int verbosity)
{
    //
    // Get object type
    char objType = getType(dataSet);
    if  (objType == 0) {

      cout << " --> Skipping: Unsupported type ("<<dataSet.getDataType().getClass()<<")! " << endl;
      return nullptr;
    }

    if (verbosity) cout << " (type=\"" << objType << "\"";

    //
    // Rank gives the dimensionaly of the object
    DataSpace dataSpace = dataSet.getSpace();
    Int_t rank = dataSpace.getSimpleExtentNdims();

    if (verbosity) cout << ", rank=" << rank;

    if(rank < 1) { // Scalar object

        if (verbosity) cout << ")" << endl;
        return createScalar(objName, dataSet);
    }

    // Find evidences for non sequential dataset
    if(bSequential && rank < 2) {

      bSequential = kFALSE;
      if(verbosity) cout << ", seq=" << bSequential << "(forced)";

    } else if(verbosity) cout << ", seq=" << bSequential;

    //
    // Dimension will store the number of elements in each dims
    hsize_t *dimSize   = new hsize_t[rank];
    dataSpace.getSimpleExtentDims(dimSize);

    //
    // Arrays to read out the data later on
    hsize_t *dimCount  = new hsize_t[rank];
    hsize_t *dimOffset = new hsize_t[rank];

    Int_t entries = 1;
    for (Int_t i = 0; i < rank; i++) {

        dimOffset[i] = 0;

        if (bSequential && (i == 0)) dimCount[i] = 1;
        else dimCount[i] = dimSize[i];

        entries *= dimCount[i];
    }

    if (verbosity) {

        cout << ", entries=" << entries;
        if(rank > 1) {

            cout << " = [";
            for (Int_t i = 0; i < rank; i++) cout << dimSize[i] << (i < rank - 1 ? "x" : "");
            cout << "]";
        }
    }

    //
    // Prepare the description of the TTree branch of this dataset.
    size_t dataSize = dataSet.getDataType().getSize();
    if (verbosity) cout << ", basket=\"" << dataSize << " byte(s)\"";

    //
    // Define the branches
    TTree  *obj = new TTree(objName, objName);
            obj->SetAutoSave(0);

    TString objDesc = "";

    vector<int> dims = vector<int>(rank);
    for (Int_t i = (bSequential ? 1 : 0); i < rank; i++) {

        TString branchName = "dim"+TString::Itoa(i,10);
        obj->Branch(branchName, &dims[i], branchName+"/I");
        objDesc  += branchName+"/I:";
    }

    void *data = malloc(dataSize);
    obj->Branch("data", data, "data/" + TString(objType));
    objDesc +=  "data/" + TString(objType);

    if (verbosity)
        cout << ", branch=\"" << objDesc.Data() << "\")" << endl;

    //
    // Index to dim vector converter
    reversePtr(dimSize, rank); // Date space with multiple dimensions are stored in reversed order
    std::function<int(int N, hsize_t *D)> X = [&](int i, hsize_t *D) { return (i == 0) ? 1 : D[i-1] * X(i-1, D); };

    vector<int> Xn(1,1);
    for(int n = 0; n < rank-1; n++)
        Xn.push_back( X(n+1, dimSize) );

    //
    // Actually read and fill data
    DataSpace *dataMem    = new DataSpace(rank, dimCount);

    void *dataBuffer = malloc(dataSize*entries);
    if(bSequential) {

        std::cerr << "Sequential processing not tested.." << std::endl;

        // /!\ SEQUENTIAL HDF5 IS UNTESTED
        // I didn't have sequential files..
        // So please.. send me an email (marco.meyer@cern.ch) with a sample if this attempt breaks..
        for (int iChunk = 0; iChunk < dimSize[0]; iChunk++) {

            DataSpace dataSpace = dataSet.getSpace();
                      dataSpace.selectHyperslab(H5S_SELECT_SET, dimCount, dimOffset);

            const DataType type = dataSet.getDataType();
            dataSet.read(dataBuffer, type, *(dataMem), dataSpace);

            int N0 = dimOffset[0];
            int Ni = dimOffset[0]+dimCount[0];
            for(int N = N0; N < Ni; N++) {

                // UIUC::HandboxMsg::SetSingleCarriageReturn();
                // UIUC::HandboxMsg::PrintProgressBar("Processing.. ", N+1, entries, 4096);

                for(int k = 0; k < rank; k++) // Index to dim vector conversion
                    dims[(rank-1)-k] = (N / Xn[k]) % dimSize[k];

                memcpy(data, (void*) ((char*)dataBuffer + dataSize*N), dataSize); // Copy data behind data buffer pointer into tree linked variable
                obj->Fill();
            }

            dimOffset[0] = Ni;
        }

    } else {

        DataSpace dataSpace = dataSet.getSpace();
                  dataSpace.selectHyperslab(H5S_SELECT_SET, dimCount, dimOffset);

        const DataType type = dataSet.getDataType();
        dataSet.read(dataBuffer, type, *(dataMem), dataSpace);

        for(int N = 0; N < entries; N++) {

            // UIUC::HandboxMsg::SetSingleCarriageReturn();
            // UIUC::HandboxMsg::PrintProgressBar("Processing.. ", N+1, entries, 4096);

            for(int k = 0; k < rank; k++) // Index to dim vector conversion
                dims[(rank-1)-k] = (N / Xn[k]) % dimSize[k];

            memcpy(data, (void*) ((char*)dataBuffer + dataSize*N), dataSize); // Copy data behind data buffer pointer into tree linked variable
            obj->Fill();
        }
    }

    free(data);
    free(dataBuffer);

    delete dataMem;
    delete[] dimSize;
    delete[] dimCount;
    delete[] dimOffset;

    //
    // Objects and vectors for Attributes readout in the current group
    TList *userInfo = obj->GetUserInfo();
    for (int i = 0; i < dataSet.getNumAttrs(); i++) {

        Attribute *attr = new Attribute(dataSet.openAttribute(i));

        if (verbosity > 1) cout << "\t* H5 Attribute #" << i+1 << ".. " << attr->getName();
        DataSpace attrSpace = attr->getSpace();

        char attrType = getType(*attr);
        if(attrType == 0) {

          if (verbosity > 1) cout << " --> Skipping: Non-supported type! " << endl;
          return nullptr;
        }

        if (verbosity > 1) cout << "(type=\"" << attrType << "\"";

        //
        // Rank gives the dimensionaly of the object
        Int_t rank = attrSpace.getSimpleExtentNdims();
        if (verbosity > 1) cout << ", rank=" << rank << ")";

        TObject *objAttr = createAttribute(objName+"_"+attr->getName(), attr);
        if (verbosity > 1) cout << " : " << objAttr->ClassName() << endl;

        userInfo->Add(objAttr);
    }

    return obj;
}

int help() {

  printf("\n Usage: hdf2root [--help, -h] [--sequential] [--verbose] <inFile.h5|hdf5|hdf> <outFile.root(=inFile.root)>\n");
  printf("\n  %15s  %s ", "--sequential", "Enables sequential mode.");
  printf("\n  %15s  %s ", "--verbose", "Enables print out.");
  printf("\n  %15s  %s ", "--help, -h", "Shows this message.");
  printf("\n\n");

  return 0;
}

void payload(Group *group, Bool_t bSequential, int verbosity)
{
  // Read attributes
  for (int idx = 0; idx < group->getNumAttrs(); idx++) {

    H5::Attribute attr = group->openAttribute(idx);

    TString objName = attr.getName();
    TObject *objAttr = createAttribute(objName, &attr);
    if(objAttr) {

      if (verbosity > 0) std::cout << /*UIUC::HandboxMsg::kOrange <<*/ gDirectory->GetPath() << "/" << objName << " : " << objAttr->ClassName() /*<< UIUC::HandboxMsg::kNoColor*/<< std::endl;
      gDirectory->WriteTObject(objAttr);
    }
  }

  for (int idx = 0; idx < group->getNumObjs(); idx++) {

    TString pwd = gDirectory->GetPath();

    TString objName = group->getObjnameByIdx(idx);
    int objType = group->getObjTypeByIdx(idx);

    switch (objType)
    {
      case H5G_GROUP:
      {
        gDirectory->mkdir(objName);
        gDirectory->cd(objName);

        if(verbosity) cout << /*UIUC::HandboxMsg::kRed <<*/ gDirectory->GetPath() /*<< UIUC::HandboxMsg::kNoColor*/ << endl;
        payload(new Group(group->openGroup(objName)), bSequential, verbosity);

        gDirectory->cd(pwd);
      }
      break;

      case H5G_DATASET:
      {
        if(verbosity) cout << /*UIUC::HandboxMsg::kGreen <<*/ gDirectory->GetPath() << "/" << objName /*<< UIUC::HandboxMsg::kNoColor*/;

        TObject *obj = createObject(objName, group->openDataSet(objName), bSequential, verbosity);
        if(obj) gDirectory->WriteTObject(obj);
      }
      break;

      default:
        throw Exception("Unexpected H5 object type:" + std::to_string(objType));
    }
  }
}

int main(int argc, char *argv[]) {

  if (argc <= 1) return help();

  vector<TString> infile;
  int verbosity     = 0;
  Bool_t bSequential  = kFALSE;

  for (int l = 1; l < argc; l++) {

    TString arg = argv[l];

    if (arg.Contains("--help") || arg.Contains("-h")) return help();
    else if (arg.EndsWith(".h5") || arg.EndsWith(".hdf5") || arg.EndsWith(".hdf")) infile.push_back(arg);
    else if (arg.Contains("-vvv")) verbosity = 3;
    else if (arg.Contains("-vv")) verbosity = 2;
    else if (arg.Contains("-v")) verbosity = 1;
    else if (arg.Contains("--sequential")) bSequential = kTRUE;
  }

  for(int i = 0, N = infile.size(); i < N; i++) {

    TString outfile = TString(gSystem->BaseName(infile[i])).ReplaceAll(".h5", ".root").ReplaceAll(".hdf5", ".root").ReplaceAll(".hdf", ".root");
    if(verbosity) cout << endl;
    cout << /*UIUC::HandboxMsg::kPurple <<*/ "Turning " /*<< UIUC::HandboxMsg::kNoColor*/ << infile[i].Data() << /*UIUC::HandboxMsg::kPurple <<*/ " into " << /*UIUC::HandboxMsg::kNoColor <<*/ outfile.Data() << " " << endl;

    //
    // Open input HDF5 file
    H5File h5 = H5File(infile[i], H5F_ACC_RDONLY);

    //
    // Writting TTrees in output file
    TFile *root = new TFile(outfile, "RECREATE", (TString) "HDF5: " + gSystem->BaseName(infile[i]));

    payload(new Group(h5.openGroup("/")), bSequential, verbosity);
    root->Close();
  }

  return 0;
}
