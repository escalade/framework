#include <Riostream.h>

#include <TROOT.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TH1.h>

int main(int argc, char**argv) {

    // Start ROOT Application
    TApplication app("app", &argc, argv);

    //
    // Main program
    std::cout << std::endl << "Hello World." << std::endl;
    std::cout << "ROOT Version: " << gROOT->GetVersion() << std::endl << std::endl;

    TCanvas *c = new TCanvas("c", ""); // Create canvas to avoid warning
             c->SetGridx(true);
             c->SetGridy(true);
             
    TH1D* h = new TH1D("h","Random Gaussian Test",100,-5,5);
          h->FillRandom("gaus",10000);
          h->Fit("gaus");
          h->Draw();

    //
    // Display available canvas..
    if (gROOT->GetListOfCanvases()->GetSize() > 0) {
            
        TCanvas *c = NULL;
        TIter Next(gROOT->GetListOfCanvases());
        while( (c = ((TCanvas*) Next())) ) {

            c->Update();
            c->Modified();
        }

        std::cout << std::endl << "Wait for graphics to be closed; Use Ctrl+C" << std::endl;
        app.Run();
    }

    std::cout << std::endl << "DOOM BYE!" << std::endl;
    return 0;
}
