# Minimum GCC version required
if (CMAKE_CXX_COMPILER_VERSION STRLESS 4.7)
        message( FATAL_ERROR "GCC has to be at least 4.7" )
endif()

# Minimum ROOT version required
if (ROOT_VERSION STRLESS 5.16/30 OR NOT ROOT_VERSION)
        message( FATAL_ERROR "ROOT has to be at least 5.16/30" )
endif()

# Check if Proof is loaded when use find_library()
if ( NOT ROOT_Proof_LIBRARY )
  message(FATAL_ERROR "Proof is required to compile libUIUC, but not found!")
endif()


