# <b>I. Data management tool</b>

This tool allows you to work effectively on data, using COMPASS software and the batch system in a systematic way.
In addition to this, the tool can be easily tuned, in order to analyse severous set of data, produce data or generate MC.

The framework has been designed in order to process huge amount of data.
This means that you can easily start the analysis of a run, a whole period or a complete year of data taking.
It can be done on the batch system whatever the cluster if the pilots are installed.

This framework is provided with the following features :
- An integrated ROOT library (called Library UIUC)
- A toolbox with some compiled scripts.
- A pilot where each clusters and software and batch system known are configured

# II. How to setup Escalade ?

## 1. Download

First you have first to clone the repository:
```
mkdir escalade && cd escalade
git clone --recursive ssh://git@gitlab.cern.ch:7999/illinois-and-friends-group/escalade-framework.git
```

## 2. Compilation and installation of the framework
This GIT project will download the Escalade Framework and the integrated UIUC library (if access right granted).
In order to install one has to run the bootstrap script :
```
./bootstrap.sh
```

## 3. Configuration of Escalade

After having install you have to source the setup in ./escalade-install/ :
```
source setup.[c]sh
```

If everything is green and if you go though the setup, this mean your environment is correctly setup
Otherwise solve the error messages (Usually basic error messages)
Mainly, this setup configures :
* the PATH and the LD_LIBRARY_PATH for binaries, toolbox binaries and library
* the ESCALADE related variables

In the $ESCALADE/bin/ directory, you mainly have:
* escalade; the main executable
* lib2phast; in order to convert the UIUC library to PHAST library

At this moment, you can configure PHAST, Coral, TGeant or whatever software if the corresponding pilot is installed.
Pilots are stored here : ./pilots/

Many variables can be configured according to your second and the configuration you need.
Here are the variables and have to be tuned depending on your cluster :
```
ESCALADE_CLUSTER=cern # Mandatory before sourcing setup.sh
ESCALADE_EXPERIMENT=compass # Mandatory before sourcing setup.sh
ESCALADE_BATCH=htcondor # Can be set and updated at anytime
ESCALADE_SOFTWARE=phast # Can be set and updated at anytime
```

There are 3 types of pilots: cluster, software, batch. The supported pilots can be listed using the following commands.
```
escalade internal software-list
escalade internal batch-list
escalade internal cluster-list
```

Those variables have to be defined before running the setup script.
In case of mistake you can always use the following command and then source again:
```
unset ESCALADE
```

# III. First use of the tool

## 1. Example of production

The procedure of a production is the following:
- Create the XML configuration file, in order to tell to Escalade which set of data use, which software, ...
- Generate a production directory on disk and store data to process into files
- Execute an example of data processing with the choosen software
- Send on batch.. (and wait some time)
- Checking procedure of logfiles and data output
- Archive your production

This was an example of a classic production, but it's not mandatory to follow it. You can also just process one input file (e.g. 1 run)
On the other hand you can also to mass production of 90k files. The structure of the production directory will be splitted into subdirectory of 1024 files each.

## 2. Creation of an XML file for production

The major idea behind an xmlfile is to be able to recover configurations and variables used for a production. You can also process severous set of data at the same time.
Here is an example of XML file :

```
<escalade>
    <production>
        <step data="$CASTOR_EXPERIMENT/dy15W15t3/microDST" data-pattern=".DST-(.*).root.*">
                <directory>testprod</directory>
                <software>phast</software>
                <output>$CASTOR_HOME/escalade-production/testprod</output>
                <extras>
                        <variable name="PHAST_USEREVENT">11</variable>
                        <variable name="PHAST_OPTIONS">-h hist.root -o mDST.root</variable>
                </extras>
        </step>
    </production>
</escalade>
```

NB: $CASTOR_EXPERIMENT, $CASTOR_HOME and $ESCALADE are variables defined when you sources the setup.[c]sh
In the structure of the configuration file you can find the following tags:
- `<escalade>` which is the top-level of the xmlfile
- `<production>` which is the first level and will loop over each set of data. (cf. shared xml example in ./share/xml-examples)
- `<step>` which is the second level and will loop over each production tag. (cf. shared xml example in ./share/xml-examples)
- `<directory>` is the location of the production directory on disk
- `<software>` will specify the software to use with escalade
- `<output>` is the location where the output data will be stored
- `<extras>` is a special tag where you can define special variables used by your software pilots for example

In the DY case, a good way would be to create one production per period: from W07 to W15
And then you can process one year just with one configuration file..

NB: Some chained production can also be implemented by using `<parent>` tags
## 3. Creation of your first escalade module: a workplace for your analysis (optional)

An escalade module is a directory where you can put all your codes : compiled codes, PHAST userevent, Trafdic options, etc..
According to which software your are using, your module architecture might be different.

Here is an example of structure using PHAST, Coral and user-codes for analysis:
```
./mymodule/
./mymodule/phast-users/
./mymodule/phast-users/UserEventXX.cc
./mymodule/phast-users/UserEventXX.h
./mymodule/phast-users/UserJobEndXX.cc
./mymodules/coral-trafdics/
./mymodules/coral-trafdics/mytrafdic.opt
./mymodules/coral-trafdics/coral-files/ # for mapping, or files used by Coral
./mymodules/
./mymodules/ROOTcodes/
./mymodules/ROOTcodes/Makefile
./mymodules/ROOTcodes/MyCompiledCode.cxx
```

For example, PHAST required a ./phast-users/ directory where you can store your UserEvent.
Second example, Coral doesn't required any structure, just need to specify the location of the option file
NB: This way of working might be easily changed, of course, by changing the pilot for CERN/Phast or CERN/Coral.

## 4. The standalone commands

For each software you should have a standalone command available if defined. They don't need any configuration file and allow you to compile or configure
- For PHAST, it will install the userevents in your phast directory according to the module you specified and recompile
```
ESCALADE_WORKSPACE=./mymodule/ escalade standalone phast
```
- For Coral, it will recompile Coral and Coral in PHAST.
```
ESCALADE_WORKSPACE=./mymodule/ escalade standalone coral
```

## 5. The internal commands
Those commands are listing when you type
```
escalade --help
```

You can get for example directory size, etc.. just by typing :
```
escalade internal getsize
```


# IV. Main commands:

## Main options

If you need help and want to see all the option available you can just type :
The main options are very usefull if you want to add a job submission rate, or add a delay timem, or just run an escalade command on batch directly.
```
escalade --help
```


## Concerning data management:
```
escalade config.xml <software>-data : to get data and store them into ./<production>/input/ according to the specified pattern
escalade config.xml <software>-datastage : to stage input files
escalade config.xml <software>-datasplit : to split input files into smaller files (1 file per line)
escalade config.xml <software>-datamerge : to merge input files
```
NB: Just replace `<software>` by your software name : phast, coral, tgeant..

## About production
```
escalade config.xml <software>-setup : to see the current setup of your software (version, configuration, etc..)
escalade config.xml <software>-exec : to just execute your code on one input file
escalade config.xml <software>-batch : to send on batch all the input files
RUN=XXX escalade config.xml <software>-exec : to select the test file to process
RUN=XXX escalade config.xml <software>-batch : to send on batch only the selected files
```

## About checks before archive your production
```
escalade config.xml <software>-logs : to check the logfiles of your production
escalade config.xml <software>-recover : to recover the files jobs
RUN=XXX escalade config.xml <software>-forcerecover : to recover the specified file (even if there is no error!)
```

NB: The check of the log are based on configuration files which are removing harmless messages from the stderr. (All logfiles are kept safe of course)
In the end if the stderr logfile is empty, it means there is no problem found. It's up to you to update this file
(Because errors will appear for sure and not be removed if they are unknown)

## About archived productions
```
escalade config.xml <software>-archive : to archive a production (erase the production directory and create a tar.gz archive into the <output> directory with all logfiles and userfiles)
escalade config.xml <software>-unarchive : to unarchive a production already archived (if you kept safe the xml file of course !)
```

Those commands are the main commands to know and all software should be developped based on this structure

NB: `<software>` has to be replaced by the software of your choise: e.g. phast-data or coral-exec
NB: For experts, "ESCALADE_DEBUG=1" can be defined in order to check your jobscript codes, when you are debugging your pilots

# <b>V. Need help ?</b>

Please contact : Marco Meyer, marco.meyer@cern.ch
https://gitlab.cern.ch/escalade/framework
