#! /bin/bash

#User defined variables if required
[ -z "$BATCH_ACCOUNT" ] && export ESCALADE_BATCH_ACCOUNT="PHY20003" || export ESCALADE_BATCH_ACCOUNT="$BATCH_ACCOUNT"
[ -z "$BATCH_JOBNAME" ] && export ESCALADE_BATCH_JOBNAME="escalade-$(date | md5sum | awk '{print $1}')" || export ESCALADE_BATCH_JOBNAME="$BATCH_JOBNAME"
[ -z "$BATCH_QUEUE" ] && export ESCALADE_BATCH_QUEUE="normal" || export ESCALADE_BATCH_QUEUE="$BATCH_QUEUE"
[ -z "$BATCH_WALLTIME" ] && export ESCALADE_BATCH_WALLTIME="12:00:00" || export ESCALADE_BATCH_WALLTIME="$BATCH_WALLTIME"
[ -z "$BATCH_PCP_OPTIONS" ] && export ESCALADE_BATCH_PCP_OPTIONS="" || export ESCALADE_BATCH_PCP_OPTIONS="$BATCH_PCP_OPTIONS"
[ -z "$BATCH_PPN" ] && export ESCALADE_BATCH_PPN="56" || export ESCALADE_BATCH_PPN="$BATCH_PPN"
if [ ! -z "$ESCALADE_JOBLIST" ]; then

	NFILES=$(wc -l $ESCALADE_JOBLIST | awk '{print $1}')
        export BATCH_NODES=$($ESCALADE_ROUND "$NFILES/$((ESCALADE_BATCH_PPN - 1))")
fi

[ -z "$BATCH_NODES" ] && export ESCALADE_BATCH_NODES="1" || export ESCALADE_BATCH_NODES="$BATCH_NODES"
[ -z "$BATCH_CRAY" ] && export ESCALADE_BATCH_CRAY="skx" || export ESCALADE_BATCH_CRAY="$BATCH_CRAY"
[ -z "$BATCH_SETUP" ] && export ESCALADE_BATCH_SETUP="" || export ESCALADE_BATCH_SETUP="$BATCH_SETUP"

# You need to have this in a common place to then dispatch the files on /tmp
[ -z "$ESCALADE_BATCH_WORKDIR" ] && export ESCALADE_BATCH_WORKDIR=${SCRATCH}/escalade-batchjobs/$ESCALADE_BATCH_NAME

# If this is a command to submit on batch..
#if [ "$ESCALADE_SOFTWARE" == "escalade" ]; then

#    if [[ "$ESCALADE_SANDBOX_QUEUE" == "large" ]]; then

#        export ESCALADE_BATCH_QUEUE="large"
#        export ESCALADE_BATCH_WALLTIME="2:00:00"

#    else

#        export ESCALADE_BATCH_QUEUE="development"
#        export ESCALADE_BATCH_WALLTIME="02:00:00"
#    fi

#    [ ! -z "$BATCH_WALLTIME" ] && export ESCALADE_BATCH_WALLTIME="$BATCH_WALLTIME"
#fi

# new for SLURM
TOT_THREADS=$((${ESCALADE_BATCH_NODES} * ${ESCALADE_BATCH_PPN}))

# Default variables
export ESCALADE_BATCH_SEPARATOR="+"
export ESCALADE_BATCH_SEDPID="s/.*Submitted batch job \([0-9]*\)[ ]*/\1/p" # String used to check if it has been submitted
export ESCALADE_BATCH_OPTIONS="-D ${ESCALADE_BATCH_WORKDIR} -A ${ESCALADE_BATCH_ACCOUNT}"
export ESCALADE_BATCH_OPTIONS="$ESCALADE_BATCH_OPTIONS -N ${ESCALADE_BATCH_NODES}"
export ESCALADE_BATCH_OPTIONS="$ESCALADE_BATCH_OPTIONS -n ${TOT_THREADS}"
export ESCALADE_BATCH_OPTIONS="$ESCALADE_BATCH_OPTIONS -t ${ESCALADE_BATCH_WALLTIME} -p ${ESCALADE_BATCH_QUEUE}"
export ESCALADE_BATCH_OPTIONS="$ESCALADE_BATCH_OPTIONS $BATCH_EXTRA"

export ESCALADE_QSTAT="squeue -u $USER"
export ESCALADE_QSUB="sbatch"
export ESCALADE_QDEL="scancel"
export ESCALADE_BATCH_JOBLAUNCHER=$ESCALADE_BATCH_DIR/joblauncher.sh
export ESCALADE_BATCH_JOBWRAPPER=$ESCALADE_BATCH_DIR/jobwrapper.sh
