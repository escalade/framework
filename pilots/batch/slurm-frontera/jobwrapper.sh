#!/bin/bash

ulimit -c 0 # remove core.* files

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE_BATCH_DIR/jobwrapper.sh "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#

if [ ! -f "$ESCALADE/bin/pcp" ]; then
    echo -e "--- PCP is not installed.. \"$ESCALADE/bin/pcp\" missing.."
fi

if [ ! -z "$ESCALADE_BATCH_PCP_OPTIONS" ]; then
    echo -e "--- PCP options detected: \"$ESCALADE_BATCH_PCP_OPTIONS\""
fi

export ESCALADE_JOBCMDLIST="$ESCALADE_JOBWORKDIR0/cmdlist.txt"
echo -e "--- Creating the cmdlist file \"$ESCALADE_JOBCMDLIST\".. $ESCALADE_JOBFILE"

i=0
j=0
ESCALADE_BATCH_OFFSET=0

IFILE=1
IFILE_TOT=1
NFILE=$(wc -l $ESCALADE_JOBLIST | awk '{print $1}')

if [ ! -z "$ROOTSYS" ]; then

	pwd_save=`pwd`
	myid=`id -u`
	echo "Broadcasting ROOT ($ROOTSYS) on each compute node to /tmp/sw_root_${myid}"
	date
	cd /tmp ; mkdir -p sw_root_${myid}; cd sw_root_${myid} ; tar -xf $ROOTSYS/root.tar ; cd $pwd_save

	scontrol show hostnames $SLURM_NODELIST > /tmp/sw_root_${myid}/nodelist.txt
	mpiexec.hydra -np $SLURM_NNODES -ppn 1 -f /tmp/sw_root_${myid}/nodelist.txt \
	/work/00410/huang/share/bcast_dir_mpi /tmp/sw_root_${myid} &> /tmp/sw_root_${myid}/log_bcast

	export ROOTSYS=/tmp/sw_root_${myid}
	export LIBPATH=/tmp/sw_root_${myid}/lib
	export JUPYTER_PATH=/tmp/sw_root_${myid}/etc/notebook
	export SHLIB_PATH=/tmp/sw_root_${myid}/lib
	export PATH=/tmp/sw_root_${myid}/local:/tmp/sw_root_${myid}/bin:$PATH
	export LD_LIBRARY_PATH=/tmp/sw_root_${myid}/lib:$LD_LIBRARY_PATH
	export PYTHONPATH=/tmp/sw_root_${myid}/lib:$PYTHONPATH
	export cpp_result_dir="/tmp/sw_root_${myid}/local"
fi

if [[ ! -z "$MYSQL" && -f "$MYSQL" ]]; then

	pwd_save=`pwd`
	myid=`id -u`

	echo "Broadcasting MYSQL ($MYSQL) on each compute node to /tmp/sw_mysql_${myid}"
	date
	cd /tmp ; mkdir -p sw_mysql_${myid}; cd sw_mysql_${myid} ; tar -xf $MYSQL ; cd $pwd_save
	scontrol show hostnames $SLURM_NODELIST > /tmp/sw_mysql_${myid}/nodelist.txt
	mpiexec.hydra -np $SLURM_NNODES -ppn 1 -f /tmp/sw_mysql_${myid}/nodelist.txt \
	/work/00410/huang/share/bcast_dir_mpi /tmp/sw_mysql_${myid} &> /tmp/sw_mysql_${myid}/log_bcast

	export MYSQL=/tmp/sw_mysql_${myid}
fi

export ESCALADE_NODEDIR=$(mktemp -d --suffix -${USER}-escalade-pool)
for ESCALADE_JOBID in $(cat "$ESCALADE_JOBLIST"); do

        source $ESCALADE_JOBINFO $ESCALADE_JOBID
	#export ESCALADE_JOBWORKDIR=$(mktemp -d --suffix -${USER}-escalade-pool)
        export ESCALADE_JOBWORKDIR=${ESCALADE_NODEDIR}/${ESCALADE_JOBID}
	export ESCALADE_JOBCMDLIST=${ESCALADE_JOBWORKDIR0}/cmdlist_${i}.txt

        # Prepare variables
        STDOUT="$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stdout"
        STDERR="$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stderr"
        echo -n "mkdir -p $ESCALADE_JOBWORKDIR && " >> $ESCALADE_JOBCMDLIST
	echo -n "ESCALADE_JOBID=$ESCALADE_JOBID " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBSLOT=$ESCALADE_JOBSLOT " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBWORKDIR=$ESCALADE_JOBWORKDIR " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBFILE=$ESCALADE_JOBFILE " >> $ESCALADE_JOBCMDLIST
        echo "$ESCALADE_GLOBAL_JOBTASK >> $STDOUT 2>> $STDERR" >> $ESCALADE_JOBCMDLIST

        tput el 2> /dev/null
        if [[ $(($IFILE % $(($ESCALADE_BATCH_PPN - 1)))) -eq 0 || $IFILE_TOT == $NFILE ]]; then

	    export ESCALADE_BATCH_SID=$i
            SUBOUT="$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}-${i}.out"
            SUBERR="$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}-${i}.err"

            echo "PCP command list \"$ESCALADE_JOBCMDLIST\" ready.."
	    echo "ibrun command has been called (Processed files: $IFILE_TOT/$NFILE)"
	    echo "Job task working directory: $ESCALADE_JOBWORKDIR"
            #CMD="ibrun -n 56 -o ${ESCALADE_BATCH_OFFSET} task_affinity $ESCALADE/bin/pcp $ESCALADE_BATCH_PCP_OPTIONS -f $ESCALADE_JOBCMDLIST &"
            CMD="ibrun -n 56 -o ${ESCALADE_BATCH_OFFSET} task_affinity $ESCALADE/bin/pcp -s 600 -c $ESCALADE/bin/escalade internal standalone-database -e \"killall mysqld && $ESCALADE/scripts/move_output.sh\"  -f $ESCALADE_JOBCMDLIST &"

	    date
            echo $CMD && eval "$CMD" > $SUBOUT 2> $SUBERR
	    date

            ESCALADE_BATCH_OFFSET=$((${ESCALADE_BATCH_OFFSET}+56))
            IFILE=1
            i=$(($i+1))
	    j=0
        else
            IFILE=$(($IFILE+1))
	    j=$(($j+1))
        fi
        IFILE_TOT=$(($IFILE_TOT+1))
done




wait
exit 0
