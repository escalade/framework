#!/bin/bash

ulimit -c 0 # remove core.* files

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

#
# Environment checks
#
if [ ! -s "$ESCALADE_GLOBAL_JOBWRAPPER" ]; then

	echo "No wrapper found.. $ESCALADE_GLOBAL_JOBWRAPPER" >&2
	exit 1
fi

[ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE} -- DEBUG${NC}"

echo -e "${ORANGE}--> Fake batch system called.. (Uniq ID: $ESCALADE_BATCH_UNIQID)${NC}"

# Need also a special directory
FAKEDIR="$ESCALADE_TMPDIR/$ESCALADE_QJOB_PID"
echo -e "${ORANGE}[DEBUG] Change to temporary directory $FAKEDIR${NC}"
mkdir -p $FAKEDIR
cd $FAKEDIR

env > $ESCALADE_BATCH_ENV
touch $ESCALADE_BATCH_OUT # No need to redirect.. since the processing is done sequentially.. all information is already registered in the STDOUT..

echo "$ESCALADE_GLOBAL_JOBWRAPPER" > $ESCALADE_HISTORY
eval "$ESCALADE_GLOBAL_JOBWRAPPER"
if [ ! $? -eq 0 ]; then

        echo "$ESCALADE_GLOBAL_JOBWRAPPER" >&2
fi

cd - > /dev/null 2> /dev/null

rm -r "$FAKEDIR"
