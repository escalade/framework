#!/bin/bash

ulimit -c 0 # remove core.* files

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE_BATCH_DIR/jobwrapper.sh "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
if [ -z "$ESCALADE_ORIGIN" ]; then
        echo "ESCALADE_ORIGIN is not defined.. checkout your submitter code." >&2
        exit 1
fi

echo "[Error] Job did not finish correctly.." >> $ESCALADE_BATCH_ERR # In case the run don't finish correctly, this will be erased at the end..

# Start the execution
echo -e "${PURPLE}--- Command started : $(date)${NC}"
$ESCALADE/pilots/software/escalade/jobtask.sh
echo -e "${PURPLE}-- Command ended : $(date)${NC}"

# Claim the end of the job (remove the error message)
sed -i "/\[Error\] Job did not finish correctly../d" $ESCALADE_BATCH_ERR # If job is stopped before, the message should stay!
echo "[Batch] Job ended.."
