#! /bin/bash
#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE_BATCH_DIR/joblauncher.sh "" $# 0
[ $? -eq 1 ] && exit 1

ESCALADE_QSUB_PID=$(eval $ESCALADE_QSUB $ESCALADE_BATCH_OPTIONS $ESCALADE_BATCH_DIR/joblauncher.sub 2>&1)
echo "$ESCALADE_QSUB_PID"

exit 0


