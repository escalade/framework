#! /bin/bash

#User defined variables if required
[ -z "$BATCH_QUEUE" ] && export ESCALADE_BATCH_QUEUE="workday" || export ESCALADE_BATCH_QUEUE="$BATCH_QUEUE"
[ -z "$ESCALADE_NAME" ] && export ESCALADE_BATCH_JOBNAME="escalade-$(date | md5sum | awk '{print $1}')" || export ESCALADE_BATCH_JOBNAME="$ESCALADE_NAME"
if [ ! -z "$ESCALADE_JOBLIST" ]; then
        export ESCALADE_BATCH_NCPUS=$(($(wc -l $ESCALADE_JOBLIST | awk '{print $1}') + 1))
else
        export ESCALADE_BATCH_NCPUS=1
fi

# Default variables
export ESCALADE_BATCH_SEPARATOR="/"
export ESCALADE_BATCH_SEDPID="s,.*submitted to cluster \([0-9.]*\).*,\1,p" # String used to check if it has been submitted
export ESCALADE_BATCH_OPTIONS="--batch-name \"$ESCALADE_BATCH_JOBNAME\""
export ESCALADE_BATCH_JOBLAUNCHER=$ESCALADE_BATCH_DIR/joblauncher.sh
export ESCALADE_BATCH_JOBWRAPPER=$ESCALADE_BATCH_DIR/jobwrapper.sh
export ESCALADE_QSTAT="condor_q -nobatch"
export ESCALADE_QSUB="condor_submit"
export ESCALADE_QDEL="condor_rm"
