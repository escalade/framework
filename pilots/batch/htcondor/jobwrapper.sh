#!/bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [[ ! -z "$ESCALADE_BATCH_ENV" && -s "$ESCALADE_BATCH_ENV" ]]; then

	echo "-- Script to setup.sh: $ESCALADE_BATCH_ENV"
	source $ESCALADE_BATCH_ENV
else
	echo "-- No script to setup.sh: ESCALADE_BATCH_ENV=\"$ESCALADE_BATCH_ENV\""
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_BATCH_DIR/jobwrapper.sh "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
echo "Working directory: $ESCALADE_BATCH_WORKDIR"
export ESCALADE_JOBWORKDIR0=$ESCALADE_BATCH_WORKDIR
cd $ESCALADE_JOBWORKDIR0

# Job preparation
export ESCALADE_JOBCMDLIST="$ESCALADE_JOBWORKDIR0/cmdlist.txt"
echo -e "--- Creating the cmdlist file \"$ESCALADE_JOBCMDLIST\".. $ESCALADE_JOBFILE"

IFILE=0
NFILE=$(wc -l $ESCALADE_JOBLIST | awk '{print $1}')
for ESCALADE_JOBID in $(cat "$ESCALADE_JOBLIST"); do

        source $ESCALADE_JOBINFO $ESCALADE_JOBID
	export ESCALADE_JOBWORKDIR=$ESCALADE_JOBWORKDIR0/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/

        # Prepare variables
        STDOUT="$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stdout"
        STDERR="$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stderr"
        echo -n "mkdir -p $ESCALADE_JOBWORKDIR && " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBID=$ESCALADE_JOBID " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBSLOT=$ESCALADE_JOBSLOT " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBWORKDIR=$ESCALADE_JOBWORKDIR " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBFILE=$ESCALADE_JOBFILE " >> $ESCALADE_JOBCMDLIST
        echo "$ESCALADE_GLOBAL_JOBTASK >> $STDOUT 2>> $STDERR" >> $ESCALADE_JOBCMDLIST

        tput el 2> /dev/null
        echo -ne "\tProcessing $ESCALADE_JOBFILE ($((IFILE+1))/$NFILE files)\r"

        IFILE=$((IFILE+1))
done

# Start the execution
echo -e "${PURPLE}--- Command started : $(date)${NC}"

if [ ! -f "$ESCALADE/bin/pcp" ]; then

        echo -e "--- Sequential command processing.."
	eval "$(cat $ESCALADE_JOBCMDLIST)" || true

else
	if [ ! -z "$ESCALADE_BATCH_PCP_OPTIONS" ]; then
	    echo -e "--- PCP options detected: \"$ESCALADE_BATCH_PCP_OPTIONS\""
	fi

        echo -e "--- Parallel command processing.."
	mpiexec -n $ESCALADE_BATCH_NCPUS $ESCALADE/bin/pcp $ESCALADE_BATCH_PCP_OPTIONS -f $ESCALADE_JOBCMDLIST
fi

rm "cmdlist.txt"
rm "$USER.cc"

echo -e "${PURPLE}-- Command ended : $(date)${NC}"
