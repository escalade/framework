#!/bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

echo ".............................................."
if [[ ! -z "$ESCALADE_BATCH_ENV" && -s "$ESCALADE_BATCH_ENV" ]]; then

	echo "-- Script to setup.sh: $ESCALADE_BATCH_ENV"
	source $ESCALADE_BATCH_ENV
else
	echo "-- No script to setup.sh: ESCALADE_BATCH_ENV=\"$ESCALADE_BATCH_ENV\""
fi

if [[ ! -z "$ESCALADE_BATCH_KRB5" && -s "$ESCALADE_BATCH_KRB5" ]]; then

	echo "-- Kerberos token name: $ESCALADE_BATCH_KRB5"
	cp $ESCALADE_BATCH_KRB5 /tmp
else
        echo "-- No krb5 token: ESCALADE_BATCH_KRB5=\"$ESCALADE_BATCH_KRB5\""
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_BATCH_DIR/jobwrapper_escalade.sh "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#

echo "-- Working directory: $PWD"
echo "-- Kerberos token available:"
klist

if [ -z "$ESCALADE_ORIGIN" ]; then

        echo "ESCALADE_ORIGIN is not defined.. checkout your submitter code." >&2
        exit 1
fi

echo "[Error] Job did not finish correctly.." >> $ESCALADE_BATCH_ERR # In case the run don't finish correctly, this will be erased at the end..

# Start the execution
echo -e "${PURPLE}--- Command started : $(date)${NC}"
$ESCALADE/pilots/software/escalade/jobtask.sh
echo -e "${PURPLE}-- Command ended : $(date)${NC}"

# Claim the end of the job (remove the error message)
sed -i "/\[Error\] Job did not finish correctly../d" $ESCALADE_BATCH_ERR # If job is stopped before, the message should stay!
echo "[Batch] Job ended.."
