#! /bin/bash

#User defined variables if required
[ -z "$BATCH_QUEUE" ] && export ESCALADE_BATCH_QUEUE="1nd" || export ESCALADE_BATCH_QUEUE="$BATCH_QUEUE"
[ -z "$BATCH_POOL" ] && export ESCALADE_BATCH_POOL=10000 || export ESCALADE_BATCH_POOL="$BATCH_POOL"

# Default variables
export ESCALADE_BATCH_SEPARATOR="/"
export ESCALADE_BATCH_SEDPID="s,^.*Job <\(.*\)> is submitted to queue.*,\1,p" # String used to check if it has been submitted
export ESCALADE_BATCH_OPTIONS="-R 'pool>$ESCALADE_BATCH_POOL' -q $ESCALADE_BATCH_QUEUE $ESCALADE_BATCH_EXTRA"
export ESCALADE_QSTAT="bjobs -w"
export ESCALADE_QSUB="bsub"
export ESCALADE_QDEL="bkill"
export ESCALADE_BATCH_JOBLAUNCHER=$ESCALADE_BATCH_DIR/joblauncher.sh
export ESCALADE_BATCH_JOBWRAPPER=$ESCALADE_BATCH_DIR/jobwrapper.sh
