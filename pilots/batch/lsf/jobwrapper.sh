#!/bin/bash

ulimit -c 0 # remove core.* files

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE_BATCH_DIR/jobwrapper.sh "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
export ESCALADE_JOBCMDLIST="$ESCALADE_JOBWORKDIR0/cmdlist.txt"
echo -e "--- Creating the cmdlist file \"$ESCALADE_JOBCMDLIST\".. $ESCALADE_JOBFILE"

IFILE=0
NFILE=$(wc -l $ESCALADE_JOBLIST | awk '{print $1}')
for ESCALADE_JOBID in $(cat "$ESCALADE_JOBLIST"); do

        source $ESCALADE_JOBINFO $ESCALADE_JOBID > /dev/null 2> /dev/null

        tput el 2> /dev/null
        echo -ne "\tProcessing $ESCALADE_JOBFILE ($((IFILE+1))/$NFILE files)\r"
        mkdir -p $ESCALADE_JOBWORKDIR

        # Prepare variables
        STDOUT="$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stdout"
        STDERR="$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stderr"
        echo -n "ESCALADE_JOBID=$ESCALADE_JOBID " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBSLOT=$ESCALADE_JOBSLOT " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBWORKDIR=$ESCALADE_JOBWORKDIR " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBFILE=$ESCALADE_JOBFILE " >> $ESCALADE_JOBCMDLIST
        echo "$ESCALADE_GLOBAL_JOBTASK >> $STDOUT 2>> $STDERR" >> $ESCALADE_JOBCMDLIST

        IFILE=$((IFILE+1))

done

# Start the execution
echo -e "${PURPLE}--- Command started : $(date)${NC}"
if [[ -z "$ESCALADE_PCP" ]]; then

        echo -e "Sequential command processing.."

	cat $ESCALADE_JOBCMDLIST
	eval "$(cat $ESCALADE_JOBCMDLIST)"
else
        echo -e "Parallel command processing.."
	mpiexec -n $ESCALADE_PCP $ESCALADE/bin/pcp $ESCALADE_JOBCMDLIST
fi
echo -e "${PURPLE}-- Command ended : $(date)${NC}"
