#! /bin/bash

#User defined variables if required
[ -z "$ESCALADE_BATCH_QUEUE" ] && export ESCALADE_BATCH_QUEUE=long
[ -z "$ESCALADE_BATCH_RAM" ] && export ESCALADE_BATCH_RAM=4G

# Default variables
export ESCALADE_BATCH_SEPARATOR="+"
export ESCALADE_BATCH_SEDPID="s,.*Your job \([0-9]*\) .*,\1,p" # String used to check if it has been submitted
export ESCALADE_BATCH_OPTIONS="-P P_$ESCALADE_EXPERIMENT -l os=sl6 -q $ESCALADE_BATCH_QUEUE -l s_rss=$ESCALADE_BATCH_RAM -l hpss=1 -l sps=1 $ESCALADE_BATCH_EXTRA"
export ESCALADE_QSTAT="qstat -u $USER -xml | tr '\n' ' ' | sed 's#<job_list[^>]*>#\n#g'   | sed 's#<[^>]*>##g'"
export ESCALADE_QSUB="qsub -V"
export ESCALADE_QDEL="qdel"
export ESCALADE_BATCH_JOBLAUNCHER=$ESCALADE_BATCH_DIR/joblauncher.sh
export ESCALADE_BATCH_JOBWRAPPER=$ESCALADE_BATCH_DIR/jobwrapper.sh
