#!/bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

#
# Environment checks
#
[ ! -z "$ESCALADE_BATCH_TITLE" ] && ESCALADE_BATCH_TITLE_STR="-N $ESCALADE_BATCH_TITLE"
[ ! -z "$ESCALADE_BATCH_OUT" ] && ESCALADE_BATCH_OUT_STR="-o $ESCALADE_BATCH_OUT"
[ ! -z "$ESCALADE_BATCH_ERR" ] && ESCALADE_BATCH_ERR_STR="-e $ESCALADE_BATCH_ERR"

# Remove files older than 7 days in the batchjob directory (if not modified)
#find /scratch/sciteam/$USER/escalade-batchjobs -maxdepth 1 -name "escalade-*" -type d -mtime +7 -exec rm -rf {} \; 2> /dev/null

# Create new structure
mkdir -p $ESCALADE_BATCH_WORKDIR
cd $ESCALADE_BATCH_WORKDIR

# Backup environment at the moment of the submission
env > $ESCALADE_BATCH_ENV

# Send the command
echo "$ESCALADE_QSUB $ESCALADE_BATCH_OPTIONS $ESCALADE_BATCH_TITLE_STR $ESCALADE_BATCH_OUT_STR $ESCALADE_BATCH_ERR_STR $ESCALADE_GLOBAL_JOBWRAPPER" > $ESCALADE_HISTORY
eval $ESCALADE_QSUB $ESCALADE_BATCH_OPTIONS $ESCALADE_BATCH_TITLE_STR $ESCALADE_BATCH_OUT_STR $ESCALADE_BATCH_ERR_STR $ESCALADE_GLOBAL_JOBWRAPPER
if [ ! $? -eq 0 ]; then

        echo "$ESCALADE_QSUB $ESCALADE_BATCH_OPTIONS $ESCALADE_BATCH_TITLE_STR $ESCALADE_BATCH_OUT_STR $ESCALADE_BATCH_ERR_STR $ESCALADE_GLOBAL_JOBWRAPPER" >&2
fi

cd - > /dev/null 2> /dev/null
exit 0
