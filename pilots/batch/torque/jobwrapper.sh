#!/bin/bash

ulimit -c 0 # remove core.* files

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE_BATCH_DIR/jobwrapper.sh "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#

if [ ! -f "$ESCALADE/bin/pcp" ]; then
    echo -e "--- PCP is not installed.. \"$ESCALADE/bin/pcp\" missing.."
fi

if [ ! -z "$ESCALADE_BATCH_PCP_OPTIONS" ]; then
    echo -e "--- PCP options detected: \"$ESCALADE_BATCH_PCP_OPTIONS\""
fi

export ESCALADE_JOBCMDLIST="$ESCALADE_JOBWORKDIR0/cmdlist.txt"
echo -e "--- Creating the cmdlist file \"$ESCALADE_JOBCMDLIST\".. $ESCALADE_JOBFILE"

i=0

IFILE=1
IFILE_TOT=1
NFILE=$(wc -l $ESCALADE_JOBLIST | awk '{print $1}')
for ESCALADE_JOBID in $(cat "$ESCALADE_JOBLIST"); do

        source $ESCALADE_JOBINFO $ESCALADE_JOBID
        export ESCALADE_JOBWORKDIR=$(mktemp -d --suffix -${USER}-escalade-pool)
        export ESCALADE_JOBCMDLIST=${ESCALADE_JOBWORKDIR0}/cmdlist_${i}.txt

        # Prepare variables
        STDOUT="$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stdout"
        STDERR="$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stderr"
        echo -n "mkdir -p $ESCALADE_JOBWORKDIR && "
	echo -n "ESCALADE_JOBID=$ESCALADE_JOBID " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBSLOT=$ESCALADE_JOBSLOT " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBWORKDIR=$ESCALADE_JOBWORKDIR " >> $ESCALADE_JOBCMDLIST
        echo -n "ESCALADE_JOBFILE=$ESCALADE_JOBFILE " >> $ESCALADE_JOBCMDLIST
        echo "$ESCALADE_GLOBAL_JOBTASK >> $STDOUT 2>> $STDERR" >> $ESCALADE_JOBCMDLIST

        if [[ $(($IFILE % $(($ESCALADE_BATCH_PPN - 1)))) -eq 0 || $IFILE_TOT == $NFILE ]]; then

            SUBOUT="$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}-${i}.out"
            SUBERR="$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}-${i}.err"

            echo "$ESCALADE_JOBFILE $IFILE_TOT/$NFILE"
            echo "The command list \"cmdlist_${i}.txt\" is ready.. aprun command has been called"
            CMD="aprun -n32 $ESCALADE/bin/pcp $ESCALADE_BATCH_PCP_OPTIONS -f $ESCALADE_JOBCMDLIST &"
	    echo $CMD && eval "$CMD" > $SUBOUT 2> $SUBERR

            IFILE=1
            i=$(($i+1))
        else
            IFILE=$(($IFILE+1))
        fi
        IFILE_TOT=$(($IFILE_TOT+1))
done

wait
exit 0
