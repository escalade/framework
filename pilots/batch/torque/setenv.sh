#! /bin/bash

#User defined variables if required
[ -z "$BATCH_ACCOUNT" ] && export ESCALADE_BATCH_ACCOUNT="baxp" || export ESCALADE_BATCH_ACCOUNT="$BATCH_ACCOUNT"
[ -z "$BATCH_JOBNAME" ] && export ESCALADE_BATCH_JOBNAME="escalade-$(date | md5sum | awk '{print $1}')" || export ESCALADE_BATCH_JOBNAME="$BATCH_JOBNAME"
[ -z "$BATCH_QUEUE" ] && export ESCALADE_BATCH_QUEUE="normal" || export ESCALADE_BATCH_QUEUE="$BATCH_QUEUE"
[ -z "$BATCH_WALLTIME" ] && export ESCALADE_BATCH_WALLTIME="12:00:00" || export ESCALADE_BATCH_WALLTIME="$BATCH_WALLTIME"
[ -z "$BATCH_PPN" ] && export ESCALADE_BATCH_PPN="32" || export ESCALADE_BATCH_PPN="$BATCH_PPN"
[ -z "$BATCH_PCP_OPTIONS" ] && export ESCALADE_BATCH_PCP_OPTIONS="" || export ESCALADE_BATCH_PCP_OPTIONS="$BATCH_PCP_OPTIONS"
if [ ! -z "$ESCALADE_JOBLIST" ]; then

	NFILES=$(wc -l $ESCALADE_JOBLIST | awk '{print $1}')
        export BATCH_NODES=$(echo "$NFILES/$((ESCALADE_BATCH_PPN - 1)) + 1" | bc)
fi

[ -z "$BATCH_NODES" ] && export ESCALADE_BATCH_NODES="1" || export ESCALADE_BATCH_NODES="$BATCH_NODES"
[ -z "$BATCH_CRAY" ] && export ESCALADE_BATCH_CRAY="xe" || export ESCALADE_BATCH_CRAY="$BATCH_CRAY"
[ -z "$BATCH_SETUP" ] && export ESCALADE_BATCH_SETUP="" || export ESCALADE_BATCH_SETUP="$BATCH_SETUP"

[ -z "$ESCALADE_BATCH_WORKDIR" ] && export ESCALADE_BATCH_WORKDIR=/scratch/sciteam/$USER/escalade-batchjobs/$ESCALADE_BATCH_NAME

# If this is a command to submit on batch..
if [ "$ESCALADE_SOFTWARE" == "escalade" ]; then

	if [[ "$ESCALADE_SANDBOX_QUEUE" == "debug" ]]; then

	    export ESCALADE_BATCH_QUEUE="debug"
	    export ESCALADE_BATCH_WALLTIME="0:30:00"

	elif [[ "$ESCALADE_SANDBOX_QUEUE" == "normal" ]]; then

	    export ESCALADE_BATCH_QUEUE="normal"
	    export ESCALADE_BATCH_WALLTIME="2:00:00"

	elif [[ "$ESCALADE_SANDBOX_QUEUE" == "high" ]]; then

	    export ESCALADE_BATCH_QUEUE="high"
	    export ESCALADE_BATCH_WALLTIME="2:00:00"

	elif [[ "$ESCALADE_SANDBOX_QUEUE" == "noalloc" ]]; then

	    export ESCALADE_BATCH_QUEUE="noalloc"
	    export ESCALADE_BATCH_WALLTIME="24:00:00"

	else

	    export ESCALADE_BATCH_QUEUE="normal"
#	    export ESCALADE_BATCH_QUEUE="high"
	    export ESCALADE_BATCH_WALLTIME="02:00:00"
	fi

	[ ! -z "$BATCH_WALLTIME" ] && export ESCALADE_BATCH_WALLTIME="$BATCH_WALLTIME"
fi

# Default variables
export ESCALADE_BATCH_SEPARATOR="+"
export ESCALADE_BATCH_SEDPID="s/.* \([0-9]*\)\.bw.*/\1.bw/p" # String used to check if it has been submitted
export ESCALADE_BATCH_OPTIONS="-d ${ESCALADE_BATCH_WORKDIR} -A ${ESCALADE_BATCH_ACCOUNT}"
export ESCALADE_BATCH_OPTIONS="$ESCALADE_BATCH_OPTIONS -l nodes=${ESCALADE_BATCH_NODES}:ppn=32:${ESCALADE_BATCH_CRAY}"
export ESCALADE_BATCH_OPTIONS="$ESCALADE_BATCH_OPTIONS -l walltime=${ESCALADE_BATCH_WALLTIME} -q ${ESCALADE_BATCH_QUEUE}"
export ESCALADE_BATCH_OPTIONS="$ESCALADE_BATCH_OPTIONS $BATCH_EXTRA"

export ESCALADE_QSTAT="qstat -u $USER"
export ESCALADE_QSUB="qsub -V"
export ESCALADE_QDEL="qdel"
export ESCALADE_BATCH_JOBLAUNCHER=$ESCALADE_BATCH_DIR/joblauncher.sh
export ESCALADE_BATCH_JOBWRAPPER=$ESCALADE_BATCH_DIR/jobwrapper.sh
