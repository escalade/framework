#! /bin/bash

#
# Usage check
#
if [ "$0" == "$BASH_SOURCE" ]; then

        echo "$0 : This script has to be sourced !" >&2
	exit 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$BASH_SOURCE: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

if [[ ! $# -eq 1 && ! $# -eq 0 ]]; then

	echo -n "${ORANGE}[Usage] $BASH_SOURCE ${NC}" >&2
	[ "$BASH_SOURCE" != "$ESCALADE/scripts/setenv.sh" ] && echo -n "${ORANGE}: $ESCALADE/scripts/setenv.sh ${NC}" >&2
	echo "${ORANGE}[<path>]${NC}" >&2
	return 1
fi

#
# Check filesystem to use (for storage system)
#
if [ $# -eq 1 ]; then

	FS_FILE=$(eval echo "$1")

	# If storage system detected
	if [[ "`echo $FS_FILE | cut -d'/' -f2`" = "castor" || ! -z "$(echo $FS_FILE | grep -E "^root://.*.cern.ch//castor")" ]]; then

		ONTAPE=1
		FS="CASTOR"
		FS_LS="nsls -l"
		FS_MKDIR="nsmkdir -p"
		FS_GET="xrdcp --retry 50 -N --force"
                FS_WRITE="xrdcp --retry 50 -N --force"
                [ -z "$ESCALADE_RMDIR" ] && FS_RM="rfrm" || FS_RM="rfrm -r"
		FS_MV="nsrename"
		FS_ADDR="root://$STAGE_HOST.cern.ch/"

	elif [[ "`echo $FS_FILE | cut -d'/' -f2`" = "eos" || ! -z "$(echo $FS_FILE | grep -E "^root://eos.*.cern.ch/")" ]]; then

		ONTAPE=0
		FS="EOS"
		FS_LS="eos ls -l"
		FS_MKDIR="eos mkdir"
                FS_GET="xrdcp --retry 50 -N --force"
                FS_WRITE="xrdcp --retry 50 -N --force" 
		[ -z "$ESCALADE_RMDIR" ] && FS_RM="eos rm" || FS_RM="eos rm -r"
		#FS_MV="eos mv" # It does not exist...
		FS_ADDR=$EOS_MGM_URL
	fi
fi

return 0
