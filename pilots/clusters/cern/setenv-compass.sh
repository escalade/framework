#! /bin/bash

#
# Check if sourced
#
if [ $0 == "$BASH_SOURCE" ]; then

        echo "$_ : This script has to be sourced !"
        exit 1
fi

if [ -z "$ESCALADE_EXPERIMENT" ]; then

    echo "ESCALADE_EXPERIMENT is not defined !"
    exit 1
fi

export COMPASS_FILES=/cvmfs/compass-condb.cern.ch/detector

export STAGE_SVCCLASS=${ESCALADE_EXPERIMENT}user
export CASTOR_COMPASS=/castor/cern.ch/$ESCALADE_EXPERIMENT/generalprod/testcoral

export EOS_COMPASS=/eos/experiment/$ESCALADE_EXPERIMENT/generalprod/testcoral
export EOS_HOME=/eos/experiment/$ESCALADE_EXPERIMENT/user/${USER:0:1}/$USER
export EOS_MGM_URL=root://eos$ESCALADE_EXPERIMENT.cern.ch/
