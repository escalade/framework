#! /bin/bash

#
# Check if sourced
#
if [ $0 == "$BASH_SOURCE" ]; then

    echo "$_ : This script has to be sourced !"
    exit 1
fi

#
# Default config
#
export PATH=$PATH:/usr/lib64/openmpi-1.10/bin # except openmpi.. might be set by default
export MPICC=mpicc

export STAGE_HOST=castorpublic
export CASTOR_URL=root://$STAGE_HOST.cern.ch
export CASTOR_HOME=/castor/cern.ch/user/${USER:0:1}/$USER
export EOS_MGM_URL=root://eosuser.cern.ch/
export EOS_HOME=/eos/user/${USER:0:1}/$USER

#
# Configuration
#
echo -e "${GREEN}--> CASTOR storage system configured${NC}"
echo -e "\t Define STAGE_HOST as $STAGE_HOST"
echo -e "\t Define STAGE_SVCCLASS as $STAGE_SVCCLASS"
echo -e "\t Define CASTOR_HOME as $CASTOR_HOME"

echo -e "${GREEN}--> EOS storage system configured${NC}"
echo -e "\t Define EOS_MGM_URL as $EOS_MGM_URL"
echo -e "\t Define EOS_HOME as $EOS_HOME"
