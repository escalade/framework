#! /bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

if [ -z "$ESCALADE_CLUSTER" ]; then

        echo "$0: ESCALADE_CLUSTER is not defined. " >&2
        exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_CLUSTER_DIR/staging.sh "<file_onstage>" $# 1
[ $? -eq 1 ] && exit 1

FILE=$1

# Check environment
source $ESCALADE_SETFS $FILE > /dev/null
[ $? -eq 1 ] && exit 1

if [[ $ONTAPE == "0" ]]; then

        echo -e "${BLUE}\"$FILE\"${NC} is already on disk !"
	exit 0
fi

if [[ $FS == "CASTOR" ]]; then

        GET=$(stager_get -S $STAGE_SVCCLASS -M ${FILE#$FS_ADDR}) # Send to get the file in your pool

	tput el 2> /dev/null
	STAGER_QRY=$(stager_qry -S $STAGE_SVCCLASS -M ${FILE#$FS_ADDR})
	if [ $? -eq 1 ]; then

		echo -e "${RED}$STAGER_QRY${NC}"
	else
		echo $STAGER_QRY | awk -v GREEN=$GREEN -v RED=$RED -v NC=$NC '{ if ($3 == "STAGED") { print $1" "$2" "GREEN $3 NC } else { print $1" "$2" "RED $3 NC } }'
	fi
else
        echo -e "${RED}[ERROR] Cannot stage the file ${FILE#$FS_ADDR}, unknown tapesystem : $FS${NC}" >&2
fi
