#! /bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

if [ -z "$ESCALADE_CLUSTER" ]; then

        echo "$0: ESCALADE_CLUSTER is not defined. " >&2
        exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_CLUSTER_DIR/staging.sh "<file_onstage>" $# 1
[ $? -eq 1 ] && exit 1

FILE=$1

if [ -f "$FILE" ]; then

	echo -e "${BLUE}\"$FILE\"${NC} is already on disk !"
else
	echo -e "${ORANGE}Please bring \"$FILE\" from the nearline yourself ! Use globus.. ;-D${NC}"
fi

exit 0
