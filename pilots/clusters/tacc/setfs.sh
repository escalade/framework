#! /bin/bash

#
# Usage check
#
if [ "$0" == "$BASH_SOURCE" ]; then

        echo "$0 : This script has to be sourced !" >&2
	exit 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$BASH_SOURCE: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

if [[ ! $# -eq 1 && ! $# -eq 0 ]]; then

	echo -n "${ORANGE}[Usage] $BASH_SOURCE ${NC}" >&2
	[ "$BASH_SOURCE" != "$ESCALADE/scripts/setenv.sh" ] && echo -n "${ORANGE}: $ESCALADE/scripts/setenv.sh ${NC}" >&2
	echo "${ORANGE}[<path>]${NC}" >&2
	return 1
fi

#
# Check filesystem to use (for storage system)
#
if [ $# -eq 1 ]; then

	FS_FILE=$(eval echo "$1")

	# If storage system detected
	# No specific storage system..
fi

return 0
