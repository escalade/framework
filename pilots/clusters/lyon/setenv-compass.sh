#! /bin/bash

#
# Check if sourced
#
if [ $0 == "$BASH_SOURCE" ]; then

        echo "$_ : This script has to be sourced !"
        exit 1
fi

export COMPASS_FILES=/afs/cern.ch/compass/detector

export HPSS_HOME="/hpss/in2p3.fr/group/$ESCALADE_EXPERIMENT/users/$USER"
echo -e "\tSetting HPSS_HOME to $HPSS_HOME"
rfmkdir -p $HPSS_HOME > /dev/null 2> /dev/null

export SPS_HOME=/sps/$ESCALADE_EXPERIMENT/$USER
echo -e "\tSetting SPS_HOME to $SPS_HOME"
mkdir -p $SPS_HOME

export HPSS_COMPASS="/hpss/in2p3.fr/group/compass/data"
echo -e "\tSetting HPSS_COMPASS to $HPSS_COMPASS"


