#! /bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

if [ -z "$ESCALADE_CLUSTER" ]; then

        echo "$0: ESCALADE_CLUSTER is not defined. " >&2
        exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_CLUSTER_DIR/staging.sh "<file>" $# 1
[ $? -eq 1 ] && exit 1

FILE=$1

# Check environment
source $ESCALADE_SETFS $FILE > /dev/null
[ $? -eq 1 ] && exit 1

if [[ $FS == "HPSS" ]]; then

        STAGING_STATUS=$(root-async $FS_ADDR$FILE 2>&1)

        LAST_STAGING=$(rfstat $FILE | grep "Last access" | awk '{printf $4" "$5" "$6" "$7" "$8}')
        TIMESTAMP0=$(date -d "$LAST_STAGING" +'%s')
        TIMESTAMP=$(date +'%s')
        COUNTDOWN_DAYS=$(echo "($TIMESTAMP-$TIMESTAMP0)/3600/24" | bc)
        COUNTDOWN_HOURS=$(echo "scale=1;($TIMESTAMP-$TIMESTAMP0)/3600-$COUNTDOWN_DAYS*24" | bc -l)

        [ $COUNTDOWN_DAYS -le 30 ] && LASTACCESS="${NC}" || LASTACCESS="${ORANGE}"
	LASTACCESS="${LASTACCESS}(${COUNTDOWN_DAYS} day(s) and ${COUNTDOWN_HOURS} hour(s) ago)"
	LASTACCESS="${LASTACCESS}${NC}"

	echo -ne "$FS_ADDR$FILE $LASTACCESS \t: "
        [ $COUNTDOWN_DAYS -le 30 ] && echo -e "${GREEN}STAGED${NC}" || echo -e "${RED}STAGEIN${NC}"

#	if [ ! -z "$(echo $STAGING_STATUS | grep STAGED)" ]; then
#
#		echo -e "${GREEN}$STAGING_STATUS${NC}"
#
#	elif [ ! -z "$(echo $STAGING_STATUS | grep STAGEIN_ONGOING)" ]; then
#
#               echo -e "${WARNING}$STAGING_STATUS${NC}"
#	else
#
#		echo -e "${RED}$STAGING_STATUS${NC}"
#        fi
else

        echo -e "${RED}[ERROR] Unknown filesystem : $FS${NC}" >&2
fi
