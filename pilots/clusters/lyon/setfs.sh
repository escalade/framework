#! /bin/bash

#
# Usage check
#
if [ "$0" == "$BASH_SOURCE" ]; then

        echo "$0 : This script has to be sourced !" >&2
	exit 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$BASH_SOURCE: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

if [[ ! $# -eq 1 && ! $# -eq 0 ]]; then

	echo -n "${ORANGE}[Usage] $BASH_SOURCE ${NC}" >&2
	[ "$BASH_SOURCE" != "$ESCALADE/scripts/setenv.sh" ] && echo -n "${ORANGE}: $ESCALADE/scripts/setenv.sh ${NC}" >&2
	echo "${ORANGE}[<path>]${NC}" >&2
	return 1
fi

#
# Check filesystem to use (for storage system)
#
if [ $# -eq 1 ]; then

        if [[ "`echo $1 | cut -d'/' -f2`" == "hpss" || ! -z "$(echo $1 | grep -E "^root://.*//hpss")" ]]; then

		ONTAPE=1

		FS="HPSS"
		FS_LS="rfdir"
                FS_MKDIR="rfmkdir"
                FS_GET="xrdcp --retry 50 -N --force"
                FS_WRITE="rfcp" 
                [ -z "$ESCALADE_RMDIR" ] && FS_RM="rfrm" || FS_RM="rfrm -r"
		FS_MV="rfrename"
		FS_ADDR="root://ccxroot:1999/"
	fi
fi

return 0
