#! /bin/bash

#
# Check if sourced
#
if [ $0 == "$BASH_SOURCE" ]; then

        echo "$_ : This script has to be sourced !"
        exit 1
fi

#
# Configuration
#
export MPICC=cc

export HPSS_URL="root://ccxroot:1999/"
echo -e "\tHPSS storage system configured: $HPSS_URL${NC}"
