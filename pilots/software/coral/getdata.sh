
#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then

    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then

    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_GETDATA "<first cdr> <nchunk>" $# 0
[ $? -eq 1 ] && exit 1


if [ ! -z "$CORAL_MANYRDS" ]; then

    echo -e "${GREEN}--> Looking for cdr using \"Many RD\" way starting with $ESCALADE_DATA${NC}"

    #
    # Custom METHOD: Many RDS
    #
    CDR_FIRST=$ESCALADE_DATA
    NCHUNK=$CORAL_MANYRDS

    #
    # Environment checks
    #
    source $ESCALADE_SETFS $CDR_FIRST > /dev/null
    [ $? -eq 1 ] && exit 1

    #
    # Getting data
    #
    if [ ! -z "$(ls -A ${ESCALADE_INPUT0} 2> /dev/null)" ]; then
        echo -e "${RED}--> Data input directory ${ESCALADE_INPUT0} already exists.${NC}"
        echo "    Please remove manually if not updated and re-run the previous command"
        exit 1
    fi

    if [ ! -d "$ESCALADE_PRODUCTION" ]; then

        echo -e "\tCreating production directory.."
        mkdir -p $ESCALADE_PRODUCTION
    fi

    #
    # Preparing data
    #
    echo -e "--> Prepare data for $ESCALADE_INPUT0"
    echo -e "--> Getting data ManyRDS-like based on the following file : $CDR_FIRST; Looking for $NCHUNK file(s).."

    #
    # Detect next files
    #
    CDR_RUNNB=$(basename $CDR_FIRST .raw | sed 's/cdr[0-9]*-\([0-9]*\)$/\1/g')
    CDR_FIRSTPC=$(basename $CDR_FIRST .raw | sed 's/cdr\([0-9]*\)-[0-9]*$/\1/g')

    let ESCALADE_JOBSLOT=0
    mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}

    for I in $(seq 1 $NCHUNK); do

        if [[ $(ls ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT} | wc -l) -ge $ESCALADE_SLOTSIZE ]]; then

            echo -e "\tSlot #$ESCALADE_JOBSLOT has been created.. (Slot size: $ESCALADE_SLOTSIZE)"
            export ESCALADE_JOBSLOT=$((ESCALADE_JOBSLOT+1))
            mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}
        fi

        CDR_PC=$(echo $CDR_FIRSTPC | awk -v I="$I" '{print $1 + I - 1}')
        CDR="$(dirname $CDR_FIRST)/cdr${CDR_PC}-${CDR_RUNNB}.raw"
        echo $CDR

        if [ -z "$(eval $FS_LS $CDR 2> /dev/null)" ]; then

            echo -e "${ORANGE}$(dirname $CDR_FIRST)/cdr${CDR_PC}-${CDR_RUNNB}.raw not found.. skip. $(($I-1)) file(s) saved${NC}"
            exit 1
        fi

        echo -ne "$(dirname $CDR_FIRST)/${CDR_PC}-${CDR_RUNNB}.raw found..\r"
        tput el 2> /dev/null

        DATA_ID=$(echo ${CDR_RUNNB}-${CDR_PC} | awk '{print $NF}' | sed 's,\W,_,g')
        echo -e "$CDR" >> ${ESCALADE_INPUT0}/slot-${ESCALADE_JOBSLOT}/$DATA_ID.txt
    done

elif [ ! -z "$CORAL_2DEE" ]; then

    echo -e "${GREEN}--> Looking for input using \"TBNAMES\" method${NC}"

    #
    # Custom METHOD: Sort by TBNAME
    #
    CDRLIST=$ESCALADE_DATA
    TBFILE=$CORAL_2DEE

    #
    # Getting data
    #
    if [ ! -z "$(ls -A ${ESCALADE_INPUT0} 2> /dev/null)" ]; then
        echo -e "${RED}--> Data input directory ${ESCALADE_INPUT0} already exists.${NC}" >&2
        echo "    Please remove manually if not updated and re-run the previous command" >&2
        exit 1
    fi

    #
    # Preparing data
    #
    if [ ! -f $CDRLIST ]; then

        echo -e "${RED}\tCannot find the input file \"$CDRLIST\"${NC}" >&2
        exit 1
    fi

    if [ -z "${ESCALADE_DATAPATTERN}" ]; then

        echo -e "${RED}--> No extraction pattern found.." >&2
        echo -e "Please check your XML file : <step data-pattern='XXX'>" >&2
        exit 1
    fi

    echo -e "\t* $(cat $TBFILE | grep -v -E "^#" | wc -l) tbname(s) found in $TBFILE.."
    echo -e "\t* The CDR list is located in : $CDRLIST"
    rm -rf "${ESCALADE_TMPDIR}/cdrlist"

    #
    # Prepare the list of CDR
    #
    echo -e "${GREEN}--> Looking for data matching with the following pattern \"${ESCALADE_DATAPATTERN}\"${NC}"
    echo -e "\tAll file(s) found (whatever the pattern) : $(cat ${CDRLIST} | wc -l) file(s)"

    let ESCALADE_JOBSLOT=0
    mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}
    IFILE=1
    NFILE=$(cat ${CDRLIST} | grep -E "${ESCALADE_DATAPATTERN}" | wc -l)
    echo -e "\tFile(s) matching with the pattern : $NFILE file(s)"
    [ "$NFILE" == "0" ] && rm -rf "${ESCALADE_TMPDIR}/cdrlist/slot-${ESCALADE_JOBSLOT}"

    for FILE in $(cat ${CDRLIST} | grep -E "${ESCALADE_DATAPATTERN}"); do

        let ESCALADE_JOBSLOT=0
        mkdir -p ${ESCALADE_TMPDIR}/cdrlist/slot-${ESCALADE_JOBSLOT}

        if [[ $(ls ${ESCALADE_TMPDIR}/cdrlist/slot-${ESCALADE_JOBSLOT} | wc -l) -ge $ESCALADE_SLOTSIZE ]]; then

            echo -e "\tSlot #$ESCALADE_JOBSLOT has been created.. (Slot size: $ESCALADE_SLOTSIZE)"
            export ESCALADE_JOBSLOT=$((ESCALADE_JOBSLOT+1))
            mkdir -p ${ESCALADE_TMPDIR}/cdrlist/slot-${ESCALADE_JOBSLOT}
        fi

        FILE=$(echo "$FILE" | sed "s,\./\(.*\),\1,g")
        if [[ $FILE =~ ${ESCALADE_DATAPATTERN} ]]; then

            NCAPTION=$(echo ${BASH_REMATCH[@]} | wc -w)
            NCAPTION=$((NCAPTION-1))

            if [ $NCAPTION -lt 1 ]; then
                DATA_ID="not_defined"
            else
                DATA_ID=$(echo ${BASH_REMATCH[1]} | awk '{print $NF}' | sed 's,\W,_,g')
            fi

	    if [[ ! -z "$ESCALADE_DATARUNLIST" ]]; then

	        IS_VALID=""
	        for RUN in $(cat "$ESCALADE_DATARUNLIST" 2> /dev/null); do

                  if [ ! -z "$(echo $FILE | grep $RUN)" ]; then

                        IS_VALID="OK"
			break;
                  fi
	        done

	        if [ -z "$IS_VALID" ]; then

	                echo -ne "${ORANGE}\tNew entry for ID #${DATA_ID}.. ($IFILE/$NFILE). $(basename $FILE) is matching the pattern.. but not with the runlist..${NC}\r"
	                continue;
	        fi
	    fi

            tput el 2> /dev/null
            echo -ne "\tNew entry for ID #${DATA_ID}.. ($IFILE/$NFILE).  $(basename $FILE) is matching the pattern..\r"

            source $ESCALADE_SETFS $FILE > /dev/null
            [ $? -eq 1 ] && exit 1

            INPUTFILE=${FS_ADDR}$($ESCALADE_READLINK ${FILE#$FS_ADDR})
            echo $INPUTFILE >> ${ESCALADE_TMPDIR}/cdrlist/slot-${ESCALADE_JOBSLOT}/${DATA_ID}.txt
        fi

        IFILE=$(($IFILE+1))
    done

    tput el 2> /dev/null
    RUNLIST=$(find ${ESCALADE_TMPDIR}/cdrlist -name '*.txt' 2> /dev/null)
    echo -e "\t$(echo "$RUNLIST" | wc -l) run(s) found in \"${CDRLIST}\".."

    if [ ! -d "$ESCALADE_PRODUCTION" ]; then

        echo -e "\tCreating production directory.."
        mkdir -p $ESCALADE_PRODUCTION
    fi

    echo -e "${GREEN}--> Prepare data for $ESCALADE_INPUT0${NC}"
    IDET=1
    NDET=$(cat $TBFILE | wc -l)

    let ESCALADE_JOBSLOT=0
    mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}

    while IFS= read -r LINE; do

    [[ $LINE =~ ^#.* ]] && continue

        TBNAME=$(echo "$LINE" | awk '{print $1}')
        NCHUNK=$(echo "$LINE" | awk '{print $2}')

        tput el 2> /dev/null
        echo -ne "\tDetector \"$TBNAME\" found ($IDET/$NDET); Looking for $NCHUNK file(s) per run..\r"

        ICHUNK=0
        IFILE=0
        for RUNFILE in $RUNLIST; do

            for FILE in $(head -n$NCHUNK $RUNFILE); do

                if [[ $(ls ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT} | wc -l) -ge $ESCALADE_SLOTSIZE ]]; then

                    tput el 2> /dev/null
                    echo -ne "\tSlot #$ESCALADE_JOBSLOT has been created.. (Slot size: $ESCALADE_SLOTSIZE)\r"
                    export ESCALADE_JOBSLOT=$((ESCALADE_JOBSLOT+1))
                    mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}
                fi

                echo $FILE >> ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}/${TBNAME}_${IFILE}.txt
                IFILE=$((IFILE+1))
            done
        done

        IDET=$(($IDET+1))

    done < $TBFILE

else

    #
    # Standard METHOD
    #

    esc_echo "${ORANGE}\tNo custom option for getting data found..${NC} calling back the standard method.."
    source $ESCALADE_GETDATA
    return;
fi
