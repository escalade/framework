#! /bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

    echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then

    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE/pilots/cern/coral/job_batch.sh "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
if [ ! -f "$CORAL_TRAFDIC" ]; then

    echo -e "${RED}Trafdic file \"$CORAL_TRAFDIC\" has not been found..${NC}" >&2
    exit 1
fi

if [[ -z "$(grep '$CORAL_RAWDATA' $CORAL_TRAFDIC)" && -z "$(grep '${CORAL_RAWDATA}' $CORAL_TRAFDIC)" ]]; then

    echo -e "${RED}[Fatal error] You need to specify the CORAL_RAWDATA variable, somewhere in your option file, to select the correct data in the trafdic file you are using..${NC}"
    exit 1
fi

[ -z "$CORAL_NEVENTS" ] && export CORAL_NEVENTS=1000000

# Load coral..
CORAL_SETUP=$CORAL/setup.sh
if [ -z "$CORAL" ]; then
    echo "CORAL not found: $CORAL" >&2
    exit 1
fi

if [ -z "$CORAL_SEED" ]; then
    export CORAL_SEED=$RANDOM$RANDOM$RANDOM
    echo "No CORAL_SEED found. Linux RNG will be used as Coral seed : CORAL_SEED=$CORAL_SEED (to be defined in $CORAL_TRAFDIC)"
else
    echo "A CORAL_SEED has been found: CORAL_SEED=$CORAL_SEED (to be defined in $CORAL_TRAFDIC)"
fi

#
# UNTAR MODE CAN BE IMPLEMENTED HERE IF NEEDED
#
#export ESCALADE_JOBFILE=$($ESCALADE_UNTAR_DATA $ESCALADE_JOBFILE $(pwd))
#[ $? -eq 1 ] && exit 1

for CORAL_RAWDATA in $(cat $ESCALADE_JOBFILE); do

    echo "Begin of time: $(date)"
    if [ ! -z "$CORAL_2DEE" ]; then

        export CORAL_TBNAME=$(echo $ESCALADE_JOBID | cut -d'_' -f1)
        echo "Coral 2D efficiency detected.. Processing $CORAL_TBNAME"

	# Exception required when disabling GEM.. Issue with correlation in pattern recognition
	if [ ! -z "$(echo $CORAL_TBNAME | grep '^G[MP]')" ]; then
		echo "GEM correlation disabled.."
		export CORAL_GEMCORR=0
	else
		echo "GEM correlation enabled.."
		export CORAL_GEMCORR=1
	fi
    fi
    CORAL_RAWDATA_BAK="$CORAL_RAWDATA"
    export CORAL_RAWDATA=$($ESCALADE_BRING_ONDISK $CORAL_RAWDATA $(pwd))
    if [[ $? -eq 1 || -z "$CORAL_RAWDATA" ]]; then

	echo "Cannot access \"$CORAL_RAWDATA_BAK\".. skip" >&2
	continue
    fi

    export CORAL_RAWDATA_BASENAME=$(basename $CORAL_RAWDATA)
    echo "Data location: $CORAL_RAWDATA"
    echo "Time: $(date)"

    #
    # CORAL command
    #
    if [ -f "$CORAL/bin/coral.exe" ]; then
	export CORAL_EXE="$CORAL/bin/coral.exe"
    elif [ -f "$PHAST/coral/coral.exe" ]; then
	export CORAL_EXE="$PHAST/coral/coral.exe"
    else
	echo "Coral executable not found.." >&2
	echo "$PHAST/coral/coral.exe" >&2
	echo "$CORAL/bin/coral.exe" >&2
    fi

    [ -z "$GDB" ] && CORAL_CMD="$CORAL_EXE $CORAL_TRAFDIC" || CORAL_CMD="gdb --args $CORAL_EXE $CORAL_TRAFDIC"
    echo $CORAL_CMD # Display the cmd, use just to write the command in the logfiles

    if [ "$GDB" == 2 ]; then
      eval "$CORAL_CMD" << EOF
run
bt
quit
Y
EOF
    else
	eval "$CORAL_CMD"
    fi

    echo "End of time: $(date)"
done
