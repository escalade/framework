#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then

        echo "$0 : This script cannot be sourced !" >&2
        return 1
fi


if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_SOFTWARE_DIR/jobtask.sh "" $# 0
[ $? -eq 1 ] && exit 1

# PHAST command
PHAST_CMD="$PHAST/phast -r0 -u$PHAST_USEREVENT $PHAST_OPTIONS -l $ESCALADE_JOBFILE"
echo $PHAST_CMD # Display the cmd, use just to write the command in the logfiles
eval $PHAST_CMD
exit $?
