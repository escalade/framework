#
# Makefile commands dedicated to PHAST
#
ifndef PHAST

     phast: FORCE
	@echo -e "$(ORANGE)PHAST is not defined.$(NC)"
     phast%: phast
	@echo -ne ""

     FORCE:


else ifdef ESCALADE_STANDALONE

     ifeq ($(shell ls $(shell root-config --libdir) | grep RFIO),) # In case RFIO is not found
          NO_RFIO="/"
          export NO_RFIO
     endif

     # Mandatory variables (should not be defined in standalone mode)
     ifndef ESCALADE_WORKSPACE
     $(error ESCALADE_WORKSPACE is not defined. )
     endif

     # Compilation of phast
     phast: FORCE
	@rsync -au --include='User*' --exclude='*' $(ESCALADE_WORKSPACE)/phast-user/ $(PHAST)/user
	@echo -e "$(GREEN)--> Recompile PHAST: $(PHAST)$(NC)"
	$(MAKE) -sC $(PHAST) -j$(shell echo "$(shell nproc --all)/4+1" | bc)

     FORCE:

else

#
# Mandatory variables needed
#

ifndef PHAST_USEREVENT
$(error PHAST_USEREVENT is not defined. )
endif

# Commands
phast-setup:
	@echo -e "$(GREEN)--> Configuration selected :$(NC)"
	@echo -e "\tMakefile config : $(ESCALADE)/modules/$(MODULE)/phast-user/UserEvent$(PHAST_USEREVENT).mk"
	@echo -e "\tMakefile config alias: $(ESCALADE)/UserEvent.mk"
	@$(foreach DATA,$(ESCALADE_DATA), echo -e "\tData path : $(DATA)";)
	@echo -e "\tPattern of files : \"$(ESCALADE_DATAPATTERN)\""
	@echo -e "\tPHAST : $(PHAST)"
ifdef ESCALADE_WORKSPACE
	@echo -e "\tESCALADE Module : $(ESCALADE_WORKSPACE)"
      ifeq ($(diff -qr $(ESCALADE_WORKSPACE)/phast-user/ $PHAST/user/ | grep -e "UserEvent$(PHAST_USEREVENT).[h|cc]" -e "UserJobEnd$(PHAST_USEREVENT).[h|cc]"),)
	@echo -e "\tPHAST UserEvent$(PHAST_USEREVENT) : Already up-to-date"
      else
	@echo -e "$(ORANGE)\tPHAST UserEvent$(PHAST_USEREVENT): Not up-to-date..$(NC)"
      endif
else
	@echo -e "\tPHAST UserEvent$(PHAST_USEREVENT) selected"
endif
	@echo -e "\tPHAST Arguments : $(PHAST_OPTIONS)"
	@echo -e "\tPHAST Input : $(ESCALADE_INPUT0)"
	@echo -e "\tPHAST Output : $(ESCALADE_OUTPUT0)"
#	@echo -e "\tPHAST Info : $(shell $(PHAST)/phast  | grep "version")"

phast-cleantmp:
	@$(ESCALADE_CLEANTEMP)

phast-distclean:
	@$(ESCALADE_DISTCLEAN)

phast-status:
	@$(ESCALADE_CHECKSTATUS)

phast-data:
	@$(ESCALADE_GLOBAL_GETDATA)

phast-datasync:
	@$(ESCALADE_CHECKSYNCDATA)

phast-dataupdate:
	@$(ESCALADE_UPDATEDATA)

phast-datamerge:
	@$(ESCALADE_MERGEDATA) $(ESCALADE_INPUT0) $(ESCALADE_INPUT0)

phast-datasplit:
     ifdef NLINE
	@$(ESCALADE_SPLITDATA) $(ESCALADE_INPUT0) $(ESCALADE_INPUT0) $(NLINE)
     else
	@$(ESCALADE_SPLITDATA) $(ESCALADE_INPUT0) $(ESCALADE_INPUT0) 1
     endif

phast-resetkey:
	@$(ESCALADE_RESETKEY) $(ESCALADE_PRODUCTION) $(ESCALADE_OUTPUT0)

phast-datastage:
	@$(ESCALADE_STAGEDATA) $(ESCALADE_INPUT0) '$(RUN)'

phast-logs:
	@$(ESCALADE_CHECKLOG)

phast-info:
	@$(ESCALADE_GETINFO)

phast-recover:
  ifneq ($(RUN),.*)
	@echo "This \"$(ESCALADE_SOFTWARE)-recover\" command has not been made to be used with RUN=\"$(RUN)\""
	@echo "Please use $(ESCALADE_SOFTWARE)-forcerecover and define RUN='#regex'"
  else
	@$(ESCALADE_RECOVERDATA)
  endif

phast-forcerecover:
      ifeq ($(RUN),.*)
	@echo "This command has not been made to be used with RUN=\".*\""
      else
	@echo -e "$(GREEN)--> Removing data/logfiles concerning with the pattern \"$(RUN)\"$(NC)"
	@$(ESCALADE_FORCERECOVER) '$(RUN)' 1
      endif

phast-archive:
	@$(ESCALADE_ARCHIVELOG)

phast-unarchive:
	@$(ESCALADE_UNARCHIVELOG)

phast-batch: phast-setup
	@$(ESCALADE_GLOBAL_JOBLAUNCHER) '$(RUN)'

phast-exec: phast-setup
	@$(ESCALADE_INTERACTIVETASK) '$(RUN)'
endif
