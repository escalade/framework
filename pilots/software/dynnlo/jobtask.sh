#! /bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE/pilots/cern/coral/job_batch.sh "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
if [ ! -f "$DYNNLO_INFILE" ]; then

        echo -e "${RED}Infile \"$DYNNLO_INFILE\" not found..${NC}" >&2
        exit 1
fi

# Load coral..
if [ -z "$DYNNLO" ]; then

	echo "DYNNLO not found: $DYNNLO" >&2
	exit 1
fi

if [ -z "$DYNNLO_SEED" ]; then

    export DYNNLO_SEED="$RANDOM"
fi

if [[ -z "$(grep '$DYNNLO_SEED' $DYNNLO_INFILE)" && -z "$(grep '${DYNNLO_SEED}' $DYNNLO_INFILE)" ]]; then

	echo -e "${RED}[Fatal error] You need to specify the DYNNLO_SEED variable, somewhere in your option file, to select the correct data in the trafdic file you are using..${NC}"
	exit 1
fi

if [[ -z "$(grep '$DYNNLO_LHAPDF1' $DYNNLO_INFILE)" && -z "$(grep '${DYNNLO_LHAPDF1}' $DYNNLO_INFILE)" ]]; then

	echo -e "${RED}[Fatal error] You need to specify the DYNNLO_LHAPDF1 variable, somewhere in your option file, to select the correct data in the trafdic file you are using..${NC}"
	exit 1
fi

if [[ -z "$(grep '$DYNNLO_LHAPDF2' $DYNNLO_INFILE)" && -z "$(grep '${DYNNLO_LHAPDF2}' $DYNNLO_INFILE)" ]]; then

	echo -e "${RED}[Fatal error] You need to specify the DYNNLO_LHAPDF2 variable, somewhere in your option file, to select the correct data in the trafdic file you are using..${NC}"
	exit 1
fi

# command
echo "DYNNLO seed: \"$DYNNLO_SEED\""
export DYNNLO_LHAPDF1=$(cat $ESCALADE_JOBFILE | awk -F';' '{print $1}')
echo "LHAPDF Dataset#1 to be used: \"$DYNNLO_LHAPDF1\""
export DYNNLO_LHAPDF2=$(cat $ESCALADE_JOBFILE | awk -F';' '{print $2}')
echo "LHAPDF Dataset#2 to be used: \"$DYNNLO_LHAPDF2\""
export DYNNLO_CUTS_BINNING=$(cat $ESCALADE_JOBFILE | awk -F';' '{print $3}')
[ ! -z "$DYNNLO_CUTS_BINNING" ] && echo "Binning type: \"$DYNNLO_CUTS_BINNING\""
export DYNNLO_CUTS=$(cat $ESCALADE_JOBFILE | awk -F';' '{print $4}')
[ ! -z "$DYNNLO_CUTS" ] && echo "Cuts to be used: \"$DYNNLO_CUTS\""

envsubst < $DYNNLO_INFILE > infile
cat infile

#cat infile
DYNNLO_CMD="$DYNNLO/bin/dy* < infile"

echo $DYNNLO_CMD # Display the cmd, use just to write the command in the logfiles
eval $DYNNLO_CMD

top2root *.top
exit $?
