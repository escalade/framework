#! /bin/bash
# This getdata is useful for TGEANT, check if it might be remove later ! @meyerma

#
# Preparing data
#
LHAPDF_VERSION=$(lhapdf-config --version)
if [ "${LHAPDF_VERSION:0:1}" != "6" ]; then
	esc_echo "${ORANGE}Only LHAPDF 6 is expected..${NC}"
	exit 1
fi

LHAPDF_DATADIR=$(lhapdf-config --datadir)
if [ -z "$LHAPDF_DATADIR" ]; then

	esc_echo "${RED}Data directory of LHAPDF not found..${NC}"
	exit 1
fi

export DYNNLO_LHAPDF1=$(print $ESCALADE_XMLDIR/@beam $ESCALADE_CONFIG)
if [ -z "$DYNNLO_LHAPDF1" ]; then

	esc_echo "${RED}No variable beam=\"...\" found.. in \"$ESCALADE_CONFIG\"${NC}"
	exit 1
fi

export DYNNLO_LHAPDF2=$(print $ESCALADE_XMLDIR/@target $ESCALADE_CONFIG)
if [ -z "$DYNNLO_LHAPDF2" ]; then

	esc_echo "${RED}No variable target=\"...\" found.. in \"$ESCALADE_CONFIG\"${NC}"
	exit 1
fi

if [ ! -d "$LHAPDF_DATADIR/$DYNNLO_LHAPDF1" ]; then

	esc_echo "${ORANGE}$DYNNLO_LHAPDF1 not found in the data directory $LHAPDF_DATADIR${NC}"
	exit 1
fi

if [ ! -d "$LHAPDF_DATADIR/$DYNNLO_LHAPDF2" ]; then

	esc_echo "${ORANGE}$DYNNLO_LHAPDF2 not found in the data directory $LHAPDF_DATADIR${NC}"
	exit 1
fi

DYNNLO_NMEMBER1=$(ls $LHAPDF_DATADIR/${DYNNLO_LHAPDF1}/*.dat | wc -l)
DYNNLO_NMEMBER2=$(ls $LHAPDF_DATADIR/${DYNNLO_LHAPDF2}/*.dat | wc -l)
esc_echo "\t* LHAPDF Dataset found :"
esc_echo "\t  - Beam, $DYNNLO_LHAPDF1 with $DYNNLO_NMEMBER1 member(s)"
esc_echo "\t  - Target, $DYNNLO_LHAPDF2 with $DYNNLO_NMEMBER2 member(s)"

function expand_cuts
{
        local CUTS0=$1

        if [ -z "$CUTS0" ]; then

                [ ! -z "$2" ] && echo $2
                return;
        fi

        local CUT=$(echo $CUTS0 | awk '{print $1}')
        local REST=$(echo $CUTS0 | cut -f2- -d ' ')
        [ "$REST" == "$CUTS0" ] && REST=""


        local IFIRST=$(echo $CUT | sed -ne "s/\([0-9]*\)\.\.\([0-9]*\)/\1/p")
        local ILAST=$(echo $CUT | sed -ne "s/\([0-9]*\)\.\.\([0-9]*\)/\2/p")

        if [[ ! -z "$IFIRST" && ! -z "$ILAST" && "$IFIRST" != "$ILAST" ]]; then

                for ITH in $(seq $IFIRST $ILAST); do

                        expand_cuts "$REST" "$2 $ITH"
                done
        else
                expand_cuts "$REST" "$2 $CUT"
        fi
}


export DYNNLO_CUTS="$(print $ESCALADE_XMLDIR/@cuts $ESCALADE_CONFIG)"
if [ ! -z "$DYNNLO_CUTS" ]; then

	export DYNNLO_CUTS_BINNING="$(print $ESCALADE_XMLDIR/@cuts-binning $ESCALADE_CONFIG)"
	if [ -z "$DYNNLO_CUTS_BINNING" ]; then

	        esc_echo "${RED}No variable cuts-binning=\"...\" found.. in \"$ESCALADE_CONFIG\"${NC}"
	        exit 1
	fi

	export DYNNLO_CUTS_PATTERN="$(print $ESCALADE_XMLDIR/@cuts-pattern $ESCALADE_CONFIG)"
	if [ -z "$DYNNLO_CUTS_PATTERN" ]; then

	        esc_echo "${RED}No variable cuts-pattern=\"...\" found.. in \"$ESCALADE_CONFIG\"${NC}"
	        exit 1
	fi

	export DYNNLO_CUTS=$(expand_cuts "$DYNNLO_CUTS" "")
	DYNNLO_NCUTS=$(echo "$DYNNLO_CUTS" | wc -l)

	esc_echo "\t* Looking for cuts:"
        esc_echo "\t  - Pattern, \"$DYNNLO_CUTS_PATTERN\" settings"
        esc_echo "\t  - Binning, \"$DYNNLO_CUTS_BINNING\" settings"
	esc_echo "\t  - Cuts, $DYNNLO_NCUTS combination(s) found"
else
        esc_echo "\t* No cut option found.."
fi

echo -e "${GREEN}--> Combining all PDF set and cuts configurations..; Please wait..${NC}"
mkdir -p ${ESCALADE_TMPDIR}/input

export ESCALADE_JOBSLOT=0
mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}


IFS=$'\n'
I=0
for CUTS in $DYNNLO_CUTS; do

	unset IFS
	J=1
	PATTERN="$DYNNLO_CUTS_PATTERN"
	for CUT in $CUTS; do
		PATTERN=$(echo $PATTERN | sed "s/%$J/$CUT/g")
		J=$((J+1))
	done
	[ ! -z "$PATTERN" ] && PATTERN="-$PATTERN"

	for MEMBER1 in $(seq 0 $((${DYNNLO_NMEMBER1}-1))); do
			for MEMBER2 in $(seq 0 $((${DYNNLO_NMEMBER2}-1))); do

			if [[ $(ls ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT} | wc -l) -ge $ESCALADE_SLOTSIZE ]]; then

				echo -e "\tSlot #$ESCALADE_JOBSLOT is full.. New slot created (Slot size: $ESCALADE_SLOTSIZE)"
				export ESCALADE_JOBSLOT=$((ESCALADE_JOBSLOT+1))
				mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}
			fi

			if [ -f "${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}/beam$MEMBER1-target$MEMBER2$PATTERN.txt" ]; then

				esc_echo "${RED}--> ID is not uniq.. File \"${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}/beam$MEMBER1-target$MEMBER2$PATTERN.txt\" already exists${NC}"
				rm -r "${ESCALADE_TMPDIR}/input/"
				exit 1
			fi

			echo "$DYNNLO_LHAPDF1 $MEMBER1;$DYNNLO_LHAPDF2 $MEMBER2;$DYNNLO_CUTS_BINNING;$CUTS" >> ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}/beam$MEMBER1-target$MEMBER2$PATTERN.txt
		done
	done
	IFS=$'\n'
done
