#! /bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_SOFTWARE_DIR/job_batch.sh "" $# 0
[ $? -eq 1 ] && exit 1

#Remove potential conflict with the BATCH options variables
source $ESCALADE_BATCH_DIR/unsetenv.sh
[ $? -eq 1 ] && exit 1

# Local variables
unset BATCH_ACCOUNT
unset BATCH_JOBNAME
unset BATCH_QUEUE
unset BATCH_WALLTIME
unset BATCH_PPN
unset BATCH_NODES
unset BATCH_CRAY
unset BATCH_SETUP

echo $ESCALADE_ORIGIN # Display the cmd, use just to write the command in the logfiles
eval $ESCALADE_ORIGIN
exit $?
