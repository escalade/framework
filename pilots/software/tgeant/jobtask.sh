#! /bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE/pilots/cern/shell/job_batch.sh "" $# 0
[ $? -eq 1 ] && exit 1

# TGEANT command
TGEANT_CMD="$TGEANT/bin/TGEANT $TGEANT_SETTINGS"

echo $TGEANT_CMD # Display the cmd, use just to write the command in the logfiles
eval $TGEANT_CMD
exit $?
