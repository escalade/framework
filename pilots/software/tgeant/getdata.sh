#! /bin/bash
# This getdata is useful for TGEANT, check if it might be remove later ! @meyerma

#
# Preparing data
#
export ESCALADE_NDATA=$(print $ESCALADE_XMLDIR/@ndata $ESCALADE_CONFIG)
if [ -z "$ESCALADE_NDATA" ]; then

	esc_echo "${RED}No variable ndata=\"...\" found.. in \"$ESCALADE_CONFIG\"${NC}"
	exit 1
fi

echo -e "${GREEN}--> Generating ${ESCALADE_NDATA} dummy file(s) as input; Please wait..${NC}"
for DUMMY_ID in $(seq 1 ${ESCALADE_NDATA}); do

	echo "dummy-$DUMMY_ID" >> ${ESCALADE_TMPDIR}/input.txt
done

#
# Looking for matching
#
echo -e "${GREEN}--> Looking for data matching with the following pattern \"${ESCALADE_DATAPATTERN}\"${NC}"

let ESCALADE_JOBSLOT=0
mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}

ESCALADE_DATAPATTERN="dummy-(.*)"
for FILE in $(cat ${ESCALADE_TMPDIR}/input.txt | grep -E "${ESCALADE_DATAPATTERN}"); do

        if [[ $(ls ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT} | wc -l) -ge $ESCALADE_SLOTSIZE ]]; then

                echo -e "\tSlot #$ESCALADE_JOBSLOT is full.. New slot created (Slot size: $ESCALADE_SLOTSIZE)"
                export ESCALADE_JOBSLOT=$((ESCALADE_JOBSLOT+1))
                mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}
        fi

	if [[ $FILE =~ ${ESCALADE_DATAPATTERN} ]]; then

	        DATA_ID=$(echo ${BASH_REMATCH[@]} | awk '{print $NF}' | sed 's/-/_/g' | sed 's/+/_/g' | sed 's/:/_/g')

		echo -ne "\tFile ID #${DATA_ID} found..\r"
		echo ${FILE} >> ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}/${DATA_ID}.txt
	fi
done
