#! /bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

    echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then

    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE/pilots/cern/coral/job_batch.sh "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
if [ ! -z "$TGEANT_SETTINGS" ]; then

	if [ ! -f "$TGEANT_SETTINGS" ]; then
	    echo -e "${ORANGE}TGEANT settings file \"$TGEANT_SETTINGS\" has not been found..${NC}"
	    exit 1
	fi

	TGEANT_CMD="$TGEANT/bin/TGEANT $TGEANT_SETTINGS"
else
	echo "${ORANGE} TGEANT_SETTING not specified.. skip step.."
fi


if [ ! -z "$CORAL_TRAFDIC" ]; then

	if [ ! -f "$CORAL_TRAFDIC" ]; then
	    echo -e "${ORANGE}Trafdic file \"$CORAL_TRAFDIC\" has not been found..${NC}"
	    exit 1
	fi

	[ -z "$CORAL_NEVENTS" ] && export CORAL_NEVENTS=500000
	if [[ -z "$(grep '$CORAL_RAWDATA' $CORAL_TRAFDIC)" && -z "$(grep '${CORAL_RAWDATA}' $CORAL_TRAFDIC)" ]]; then

	    echo -e "${RED}[Fatal error] You need to specify the CORAL_RAWDATA variable, somewhere in your option file, to select the correct data in the trafdic file you are using..${NC}"
	    exit 1
	fi

	if [ -z "$CORAL_SEED" ]; then
	    export CORAL_SEED=$RANDOM$RANDOM$RANDOM
	    echo "No CORAL_SEED found. Linux RNG will be used as Coral seed : CORAL_SEED=$CORAL_SEED (to be defined in $CORAL_TRAFDIC)"
	else
	    echo "A CORAL_SEED has been found: CORAL_SEED=$CORAL_SEED (to be defined in $CORAL_TRAFDIC)"
	fi

	# Load coral..
	CORAL_SETUP=$CORAL/setup.sh
	if [ -z "$CORAL" ]; then
	    echo "CORAL not found: $CORAL" >&2
	    exit 1
	fi

    	if [ -f "$CORAL/bin/coral.exe" ]; then
		export CORAL_EXE="$CORAL/bin/coral.exe"
    	elif [ -f "$PHAST/coral/coral.exe" ]; then
		export CORAL_EXE="$PHAST/coral/coral.exe"
	else
		echo "Coral executable not found.." >&2
		echo "$PHAST/coral/coral.exe" >&2
		echo "$CORAL/bin/coral.exe" >&2
	fi

    [ -z "$GDB" ] && CORAL_CMD="$CORAL_EXE $CORAL_TRAFDIC" || CORAL_CMD="gdb --args $CORAL_EXE $CORAL_TRAFDIC"

else
	echo "${ORANGE} CORAL_TRAFDIC not specified.. skip step.."
fi

if [ ! -z "$PHAST_USEREVENT" ]; then

    PHAST_CMD="$PHAST/phast -r0 -u$PHAST_USEREVENT $PHAST_OPTIONS -l phast.txt"
else
	echo "${ORANGE} PHAST_USEREVENT not specified.. skip step.."
fi

echo "--> Command to be executed:"
[ ! -z "$TGEANT_CMD" ] && echo $TGEANT_CMD  # Display the cmd, use just to write the command in the logfiles
[ ! -z "$CORAL_CMD" ] && echo $CORAL_CMD  # Display the cmd, use just to write the command in the logfiles
[ ! -z "$PHAST_CMD" ] && echo $PHAST_CMD  # Display the cmd, use just to write the command in the logfiles

#
# UNTAR MODE CAN BE IMPLEMENTED HERE IF NEEDED
#
#export ESCALADE_JOBFILE=$($ESCALADE_UNTAR_DATA $ESCALADE_JOBFILE $(pwd))
#[ $? -eq 1 ] && exit 1
if [ ! -z "$TGEANT_CMD" ]; then

	echo $TGEANT_CMD # Display the cmd, use just to write the command in the logfiles
	eval "$TGEANT_CMD"

	ESCALADE_JOBFILE=$(mktemp)
	find -name '*.tgeant' > $ESCALADE_JOBFILE # If not use the default ESCALADE_JOBFILE
fi

for CORAL_RAWDATA in $(cat $ESCALADE_JOBFILE); do

    echo "Begin of time: $(date)"
    export CORAL_RAWDATA=$($ESCALADE_BRING_ONDISK $CORAL_RAWDATA $(pwd))
    [ $? -eq 1 ] && exit 1

    export CORAL_RAWDATA_BASENAME=$(basename $CORAL_RAWDATA)
    echo "Data location: $CORAL_RAWDATA"
    echo "Time: $(date)"

    #
    # CORAL command
    #
    echo "$CORAL_CMD"
    if [ "$GDB" == 2 ]; then
      eval "$CORAL_CMD" << EOF
run
bt
quit
Y
EOF
    else
	eval "$CORAL_CMD"
    fi

    ls *DST.root > phast.txt

    # PHAST command
    echo $PHAST_CMD # Display the cmd, use just to write the command in the logfiles
    eval $PHAST_CMD

    echo "End of time: $(date)"
done
