#! /bin/bash

#
# Usage checks
#
if [ $0 != "$BASH_SOURCE" ]; then

        echo "$(eval $ESCALADE_READLINK $BASH_SOURCE) : This script cannot be sourced !" >&2
        return 1
fi

if [ -z "$ESCALADE" ]; then

        echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
        exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE/pilots/cern/shell/jobtask.sh "" $# 0
[ $? -eq 1 ] && exit 1

function count {

        XMLELEMENT=$1
        XMLFILE=$2

        [ ! -f $XMLFILE ] && echo 0
        $ESCALADE/bin/xmllint --xpath "count(${XMLELEMENT})" $XMLFILE
}

function print {

        XMLELEMENT=$1
        XMLFILE=$2

        [ ! -f $XMLFILE ] && exit 1
        XMLRESULT=$($ESCALADE/bin/xmllint --xpath "string(${XMLELEMENT})" $XMLFILE 2> /dev/null)

        if [[ -z "$3" || "$3" == "1" ]]; then

                XMLRESULT=$(echo "$XMLRESULT" | sed "s/&amp;/\&amp;/g") # replace specials chars : &amp; = &
                XMLRESULT=$(echo "$XMLRESULT" | sed "s/&lt;/\&lt;/g")
                XMLRESULT=$(echo "$XMLRESULT" | sed "s/&gt;/\&gt;/g")
                XMLRESULT=$(eval echo '$XMLRESULT')

                XMLRESULT=$(echo "$XMLRESULT" | sed "s/\&amp;/&/g") # replace specials chars : &amp; = &
                XMLRESULT=$(echo "$XMLRESULT" | sed "s/\&lt;/</g")
                XMLRESULT=$(echo "$XMLRESULT" | sed "s/\&gt;/>/g")
        fi

        echo "$XMLRESULT"
}

NEXTRAS=$(count $ESCALADE_XMLDIR/shell/command $ESCALADE_CONFIG)
if [ $NEXTRAS -eq 0 ]; then

	echo -e "${RED} No shell command found in $ESCALADE_CONFIG for production #$ESCALADE_ID" >&2
else

   echo -e "${GREEN}Shell command found.. ($NEXTRAS element(s) found)${NC}"
   for j in $(seq 1 $NEXTRAS); do

        NAME=$(print $ESCALADE_XMLDIR/shell/command[$j]/@name $ESCALADE_CONFIG 0)

	NEXTRAS=$(count $ESCALADE_XMLDIR/shell/command[$j]/variable $ESCALADE_CONFIG)
        if [ "$NEXTRAS" != "0" ]; then

               echo -e "${GREEN}Extra variables defined ($NEXTRAS element(s) found)${NC}"
               [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}>>> Define extra variable : \"$NAME\"${NC}"
               for k in $(seq 1 $NEXTRAS); do

                       NAME=$(print $ESCALADE_XMLDIR/shell/command[$j]/variable[$k]/@name $ESCALADE_CONFIG 0)
                       if [[ -z "$NAME" ]]; then

                               echo -e "${RED}[Fatal error] No name found for variable #$k.. abort${NC}" >&2
                               exit 1
                       else

                               COUNT_ITEM=$(count $ESCALADE_XMLDIR/shell/command[$j]/variable[$k]/item $ESCALADE_CONFIG)
                               if [ $COUNT_ITEM -eq 0 ]; then

                                       # Simple variable
                                       VALUE=$(print $ESCALADE_XMLDIR/shell/command[$j]/variable[$k] $ESCALADE_CONFIG)

                                           VALUE_INT=$(printf "%d" $VALUE 2> /dev/null) # Variables might be hexacode..
                                           [ $? -eq 0 ] && VALUE=$VALUE_INT
                               else
                                     # For an array
                                       SEPARATOR=$(print $ESCALADE_XMLDIR/shell/command[$j]/variable[$k]/@separator $ESCALADE_CONFIG)
                                       [ -z "$SEPARATOR" ] && SEPARATOR=""

                                       for m in $(seq 1 $COUNT_ITEM); do


                                               ITEM=$(print $ESCALADE_XMLDIR/shell/command[$j]/variable[$k]/item[$m] $ESCALADE_CONFIG)
                                               ITEM_INT=$(printf "%d" $ITEM 2> /dev/null) # Variables might be hexacode..
                                               if [ $? -eq 0 ]; then

							[ $n -eq 1 ] && VALUE="${ITEM_INT}" || VALUE="$VALUE ${SEPARATOR} ${ITEM_INT}"
						else
							[ $n -eq 1 ] && VALUE="${ITEM}" || VALUE="$VALUE ${SEPARATOR} ${ITEM}"
					       fi
                                       done
                               fi

                               # Set the variable
			       [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}\texport $NAME=\"$VALUE\"${NC}"
                               eval "export $NAME=\"$VALUE\""
                       fi

                        unset NAME
                        unset VALUE
                done
        fi

	unset SHELL_CMD
        COUNT_VAR=$(count $ESCALADE_XMLDIR/shell/command[$j]/variable $ESCALADE_CONFIG)
	COUNT_ITEM=$(count $ESCALADE_XMLDIR/shell/command[$j]/item $ESCALADE_CONFIG)

        SHELL_WORKDIR=$(eval "echo \"$(print $ESCALADE_XMLDIR/shell/@workdir $ESCALADE_CONFIG)\"")
	if [[ $COUNT_ITEM -eq 0 && $COUNT_VAR -eq 0 ]]; then

		   # Simple variable
		   SHELL_CMD=$(print $ESCALADE_XMLDIR/shell/command[$j] $ESCALADE_CONFIG)
		   SHELL_CMD_STR=$(eval "echo \"$(print $ESCALADE_XMLDIR/shell/command[$j] $ESCALADE_CONFIG | sed 's,",\\\",g')\"")
	else
                   # For an array
                   SEPARATOR=$(print $ESCALADE_XMLDIR/shell/command[$j]/@separator $ESCALADE_CONFIG)
                   [ -z "$SEPARATOR" ] && SEPARATOR=""


		      for k in $(seq 1 $COUNT_ITEM); do
			ITEM=$(print $ESCALADE_XMLDIR/shell/command[$j]/item[$k] $ESCALADE_CONFIG)
			ITEM_STR=$(eval "echo \"$(print $ESCALADE_XMLDIR/shell/command[$j]/item[$k] $ESCALADE_CONFIG | sed 's,",\\\",g')\"")
			[ $k -eq 1 ] && SHELL_CMD="$ITEM" || SHELL_CMD="$SHELL_CMD ${SEPARATOR} ${ITEM}"
			[ $k -eq 1 ] && SHELL_CMD_STR="$ITEM_STR" || SHELL_CMD_STR="$SHELL_CMD_STR ${SEPARATOR} ${ITEM_STR}"
		      done
	fi

	PARENT_DIR=$(pwd)
	echo -e "${BLUE}--- New command detected.. : $(date)${NC}"
	echo -e "--- Command : ${PURPLE}$(echo $SHELL_CMD_STR)${NC}"

        SHELL_WORKDIR=$(eval "echo \"$(print $ESCALADE_XMLDIR/shell/@workdir $ESCALADE_CONFIG)\"")
	[ ! -z "$SHELL_WORKDIR" ] && cd "$SHELL_WORKDIR"

	SHELL_CMD_WORKDIR=$(eval "echo \"$(print $ESCALADE_XMLDIR/shell/command[$j]/@workdir $ESCALADE_CONFIG)\"")
	[ ! -z "$SHELL_CMD_WORKDIR" ] && cd "$SHELL_CMD_WORKDIR"

	echo -e "--- Sent from : \"$(pwd)\" (Please wait..)"
	eval "$SHELL_CMD"

    	echo -e "--- $(date)"
	cd "$PARENT_DIR"

   done

   echo -e "--- Waiting for background jobs.. $(date)"
   wait
   echo -e "--- Finished.. $(date)"
fi
