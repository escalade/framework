#include <TApplication.h>
#include <Riostream.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TLegend.h>
#include <TFile.h>
#include <TEllipse.h>
#include <TKey.h>
#include <TH1.h>
#include <TH2.h>
#include <TH2D.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TEntryList.h>
#include <TROOT.h>
#include <TClass.h>
#include <TProfile.h>
#include <TGaxis.h>
#include <TPaveText.h>
#include <TPaveStats.h>

#include <TFactory.h>
#include <Handbox.h>
#include <TCli.h>
#include <TPrint.h>
#include <TFileReader.h>

using namespace std;

class MyHandbox: public Handbox
{
protected:

        const static int nspill = 200;
public:

        bool bDisplayFullname;
        bool bSkipDisplay;

        int nFiles = 0;
        vector<TFileReader*> vDataList;

        TString fPatternStr;
        TString sObjname;
        TString hFormula, hFormulaErr;
        TString name;

        int first_run, last_run;
        vector<int> vRunSeparator;
        bool bRunByRun;
        bool bSpillBySpill;
        bool bSingle;

        int buffersize;
        bool bSimpleAverage;
        bool bWeightedAverage;
        bool bDisregardTH3;
        bool bNoTree;

        bool bChain1D, bChain2D, bChain3D;
        MyHandbox() : Handbox(2, 0) {
        }

        ~MyHandbox() {

                vPath.clear();
                vList.clear();
                vMergedObject.clear();

                this->DeleteDataList();
        }

        void DeleteDataList() {

                for(auto i = 0; i < vDataList.size(); i++) {

                        if(vDataList[i] != NULL) delete vDataList[i];
                        vDataList[i] = NULL;
                }
                vDataList.clear();
        }

        bool Init()
        {
                gPrint->Message(__METHOD_NAME__, "Initialization of the outgoing histograms");

                //gain time, do not add the objects in the list in memory
                TH1::AddDirectory(kFALSE);

                if(fIdentifiers.size()) {

                        int min = std::distance(fIdentifiers.begin(), std::min_element(fIdentifiers.begin(), fIdentifiers.end()));
                        int max = std::distance(fIdentifiers.begin(), std::max_element(fIdentifiers.begin(), fIdentifiers.end()));
                        
                        if(fIdentifiers[min].size()) {

                                first_run = fIdentifiers[min][0].Atoi();
                                gPrint->Message(__METHOD_NAME__, "First run found: " + TString::Itoa(first_run,10));
                        }

                        if(fIdentifiers[max].size()) {

                                last_run  = fIdentifiers[std::distance(fIdentifiers.begin(), std::max_element(fIdentifiers.begin(), fIdentifiers.end()))][0].Atoi();
                                gPrint->Message(__METHOD_NAME__, "Last run found: " + TString::Itoa(last_run,10));
                        }
                }

                if(buffersize < 2) buffersize = -1;
                if(buffersize > 0)
			gPrint->Message(__METHOD_NAME__, "Buffer size: " + TString::Itoa(buffersize,10));

                if(bRunSort) vRun = GetRuns();
                TString eMessage = "You specified formula.. Please use the option \"--hist-name\" for the name of the outgoing histogram";
                if(gPrint->WarningIf(name.EqualTo("") && (!hFormula.EqualTo("") || !hFormulaErr.EqualTo("")), __METHOD_NAME__, eMessage)) return 1;

                return 0;
        }

        vector<int> GetRuns() {

                vector<int> runlist;
                for(auto i = 0; i < fIdentifiers.size(); i++) runlist.push_back( fIdentifiers[i][0].Atoi() );

                return runlist;
        }

        int Process(TObject *input, int i)
        {
                //TString eMessage = this->SubstituteIdentifier("Initialization of the outgoing histograms for \"{}\"", i);
                //gPrint->Message(__METHOD_NAME__, eMessage);

                TFileReader *fData = new TFileReader("data-" + TString::Itoa(i, 10), "f", input->GetName() + this->sObjname);
                if(!fData->IsValid()) {

                        delete fData;
                        fData = NULL;
                        return 1;
                }
                vDataList.push_back(fData);

                TString output_str = UIUC::TFileReader::FileName(fOutputPattern);
                TString eMessage = Form("Outgoing rootfile \"%s\" is same as one of the input.. abort", output_str.Data());
                if( gPrint->WarningIf(fData->Find(output_str.Data()), __METHOD_NAME__, eMessage) ) return 1;

                if(bSingle) {

                        if((int) vMergedObject.size() == 0) {

                                vPath.resize(1);
                                vList.resize(1);
                                vMergedObject.resize(1);

                                TString url0 = UIUC::TFileReader::Url(gSystem->Getenv("ESCALADE_HOUTPUT"));
                                TString obj0 = UIUC::TFileReader::Obj(gSystem->Getenv("ESCALADE_HOUTPUT"));
                                this->SetOutputPattern(url0 + ":/");
                                
                                if(obj0.EqualTo("") || obj0.EqualTo("/")) {

                                        gPrint->Error(__METHOD_NAME__, "You used the --single option, so you should also provide an output name..");
                                        return 2;
                                }

                                vPath[0] = obj0;
                                vMergedObject[0] = 0;
                        }

                        gPrint->ProgressBar("Processing.. ", i, this->GetN());
                        for(auto j = 0; j < fData->GetFqfn().size(); j++) {

                                if(j == 0) nFiles++;

                                // To do apply badlist..
                                // vector<TString> runspill = UIUC::TFileReader::GetIdentifier(fData->At(j), fPatternStr);
                                // //gPrint->Debug(0, __METHOD_NAME__, runspill);

                                // if(runspill.size() != 2 && UIUC::EventSelection::IsUsingBadSpillList()) {

                                //         gPrint->Warning(__METHOD_NAME__, "Badspill list found.. but double pattern {} required..");

                                // } else if(runspill.size() == 2) {

                                //         if(UIUC::EventSelection::IsBadSpill(runspill[0].Atoi(), runspill[1].Atoi())) continue;
                                // }

                                vList[0].push_back(fData->At(j));
                        }

                } else {

                        gPrint->ProgressBar("Sequential processing.. ", i, this->GetN());
                        for(auto j = 0; j < fData->GetFqfn().size(); j++) {

                                if(j == 0) nFiles++;

                                // Information about the current object
                                TString fqfn = fData->At(j);

                                TString url = UIUC::TFileReader::Url(fqfn);
                                TString obj = UIUC::TFileReader::Obj(fqfn);
                                TString objdir = UIUC::TFileReader::ObjDirName(obj);
                                TString objname = UIUC::TFileReader::ObjBaseName(obj);

                                // Create substructure
                                unsigned int index = find(vPath.begin(), vPath.end(), obj) - vPath.begin();
                                if(index == vPath.size()) {

                                        vPath.push_back(obj);
                                        vList.push_back(vector<TString>());
                                        vMergedObject.push_back(NULL);
                                }

                                vList[index].push_back(fqfn);
                        }
                }

                // BUFFER SIZE REACHE
                if(i%(buffersize-1) == 0 && i != 0 && buffersize != -1) {

                        if(gPrint->IsQuiet()) {

                                gPrint->CarriageReturn(1);
                                cout << Form("Buffer limit, %d files opened => Pre-merging ongoing..", buffersize) << endl;
                        }

                        // Standard merging procedure
                        int N = vPath.size();
                        for(auto j = 0; j < N; j++) {

                                TString objname = vPath[j];
                                if(gPrint->IsQuiet()) {

                                        TString output_filename = UIUC::TFileReader::FileName(this->fOutputPattern);
                                        if(!bDisplayFullname) output_filename = UIUC::TFileReader::BaseName(output_filename);

                                        TString output_obj = UIUC::TFileReader::ApplyCorrectionObj(UIUC::TFileReader::Obj(this->fOutputPattern) + objname);

                                        gPrint->CarriageReturn(1);
                                        if(!bSkipDisplay) cout << gPrint->MakeItShorter(Form("%s:%s (%d/%d)", output_filename.Data(), output_obj.Data(), j+1, (int) N)) << '\r';
                                }

                                if( !this->MergingProcess(vPath[j]) ) {

                                        gPrint->Error(__METHOD_NAME__, "Failed to merge.. " + vPath[j]);
                                }

                                vList[j].clear();
                        }

                        gPrint->CarriageReturn(1);
                        this->DeleteDataList();
                }
                return 0;
        }

        bool bRunSort;
        vector<int> vRun;
        vector<TString> vPath;
        vector<TObject*> vMergedObject;
        vector< vector<TString>> vList;

        bool MergingProcess(TString obj)
        {
                int index = find(vPath.begin(), vPath.end(), obj) - vPath.begin();
                if(!vList[index].size()) return 1;
                if(vList[index].size() == 1 && vMergedObject[index] == NULL) {

                        vMergedObject[index] = UIUC::TFileReader::ReadObj(vList[index][0]);
                        return 1;
                }

                TString objdir = UIUC::TFileReader::ObjDirName(obj);
                TString objname = UIUC::TFileReader::ObjBaseName(obj);

                if(bRunSort) std::transform(fIdentifiers.begin(), fIdentifiers.end(), vRun.begin(), [] (vector<TString> c)->int { return c[0].Atoi(); });

                TObject *obj0 = UIUC::TFileReader::ReadObj(vList[index][0]);
                if(obj0->InheritsFrom("TH1") && !obj0->InheritsFrom("TProfile")) {

                        TH1 *myhist = (TH1*) obj0;
                        TString xlabel = myhist->GetXaxis()->GetTitle();
                        if(obj0->InheritsFrom("TH3") && bDisregardTH3) {

                                gPrint->Warning(__METHOD_NAME__, "Histogram \"" + TString(obj0->GetName()) + "\" has been disregarded.. (TH3D)");
                                if(gPrint->IsQuiet()) cout << gPrint->kOrange << Form("TH3 disregarded..") << gPrint->kNoColor  << endl;

                                delete obj0;
                                obj0 = NULL;

                                return 1;
                        }

                        TString eMessage = "Please consider to use --buffer option, circular buffer is enabled and size of the provided list is bigger..";
                        gPrint->WarningIf(vList[index].size() > UIUC::TFileReader::kOpenLimit, __METHOD_NAME__, eMessage);

                        TList *lHist = new TList;
                        lHist->SetOwner();

                        if(vMergedObject[index]) lHist->Add(vMergedObject[index]);
                        lHist->Add(obj0->Clone());

                        int N = vList[index].size();
                        for(auto j = 1; j < N; j++) {

                                if(N > 512) gPrint->ProgressBar("Loading histos into memory.. ", j+1, N);
                                TH1 *buf = (TH1*) UIUC::TFileReader::ReadObj(vList[index][j]);
                                lHist->Add(buf);
                        }

                        if(bSpillBySpill) {

                                cout << "Spill by spill merging enabled.." << endl;
                                if(!xlabel.EqualTo("spill")) cout << "no valid x-label found.. skip" << endl;
                                else {

                                        cout << "x-label named \"spill\" found" << endl;
                                        if(obj0->InheritsFrom("TH3")) vMergedObject[index] = UIUC::TFactory::CreateTH3chain(obj0->GetName(), obj0->GetTitle(), lHist);
                                        else if(obj0->InheritsFrom("TH2")) vMergedObject[index] = UIUC::TFactory::CreateTH2chain(obj0->GetName(), obj0->GetTitle(), lHist);
                                        else if(obj0->InheritsFrom("TH1")) vMergedObject[index] = UIUC::TFactory::CreateTH1chain(obj0->GetName(), obj0->GetTitle(), lHist);
                                        ((TH1*) vMergedObject[index])->GetXaxis()->SetTitle("spill");
                                        ((TH1*) vMergedObject[index])->SetTitle(obj0->GetTitle());
                                        ((TH1*) vMergedObject[index])->SetName(objname.Data() + TString(":spill-by-spill"));
                                }

                        } else if(bRunByRun) {

                                cout << "Run by run merging enabled.." << endl;
                                if(xlabel.EqualTo("run")) {

                                        cout << "x-label named \"run\" found" << endl;

                                        if(obj0->InheritsFrom("TH3")) vMergedObject[index] = UIUC::TFactory::CreateTH3chain(obj0->GetName(), obj0->GetTitle(), lHist, vRun);
                                        else if(obj0->InheritsFrom("TH2")) vMergedObject[index] = UIUC::TFactory::CreateTH2chain(obj0->GetName(), obj0->GetTitle(), lHist, vRun);
                                        else if(obj0->InheritsFrom("TH1")) vMergedObject[index] = UIUC::TFactory::CreateTH1chain(obj0->GetName(), obj0->GetTitle(), lHist);
                                        ((TH1*) vMergedObject[index])->GetXaxis()->SetTitle("run");
                                        ((TH1*) vMergedObject[index])->SetTitle(obj0->GetTitle());
                                        ((TH1*) vMergedObject[index])->SetName(objname.Data() + TString(":run-by-run"));

                                } else if(xlabel.EqualTo("spill")) {

                                        cout << "x-label named \"run\" found" << endl;
                                        if(obj0->InheritsFrom("TH3")) {

                                                TList *l0 = UIUC::TFileReader::ReadObj(lHist);
                                                TList *mylist = new TList;
                                                for(auto i = 0; i < l0->GetSize(); i++) {

                                                        TH2* myhist;
                                                        if(bSimpleAverage) {

                                                                gPrint->Debug(__METHOD_NAME__, "Histogram simple average ongoing..");
                                                                myhist = (TH2*) UIUC::TFactory::AverageBinContent((TH3*) obj0, "x");

                                                        } else if(bWeightedAverage) {

                                                                gPrint->Debug(__METHOD_NAME__, "Histogram weighted average ongoing..");
                                                                myhist = (TH2*) UIUC::TFactory::AverageBinContent((TH3*) obj0, "x", "rc");

                                                        } else {

                                                                gPrint->Debug(__METHOD_NAME__, "Histogram sum ongoing..");
                                                                myhist = (TH2*) UIUC::TFactory::Projection((TH3*) obj0, "x");
                                                        }
                                                        mylist->Add(myhist);
                                                }


                                                if(vMergedObject[index]) {
                                                        vMergedObject[index] = UIUC::TFactory::CreateTH3chain(obj0->GetName(), obj0->GetTitle(), mylist, vRun);
                                                }
                                                l0->Clear();
                                                delete l0;
                                                mylist->Clear();
                                                delete mylist;

                                        } else if(obj0->InheritsFrom("TH2")) {

                                                TList *l0 = UIUC::TFileReader::ReadObj(lHist);
                                                TList *mylist = new TList;
                                                for(auto i = 0; i < l0->GetSize(); i++) {

                                                        TH1* myhist;
                                                        if(bSimpleAverage) {

                                                                gPrint->Debug(__METHOD_NAME__, "Histogram simple average ongoing..");
                                                                myhist = (TH1*) UIUC::TFactory::AverageBinContent((TH2*) obj0, "x");

                                                        } else if(bWeightedAverage) {

                                                                gPrint->Debug(__METHOD_NAME__, "Histogram weighted average ongoing..");
                                                                myhist = (TH1*) UIUC::TFactory::AverageBinContent((TH2*) obj0, "x", "rc");

                                                        } else {

                                                                gPrint->Debug(__METHOD_NAME__, "Histogram sum ongoing..");
                                                                myhist = (TH1*) UIUC::TFactory::Projection((TH2*) obj0, "x");
                                                        }
                                                        mylist->Add(myhist);
                                                }

                                                if(vMergedObject[index]) {
                                                        vMergedObject[index] = UIUC::TFactory::CreateTH2chain(obj0->GetName(), obj0->GetTitle(), mylist, vRun);
                                                }

                                                l0->Clear();
                                                delete l0;
                                                mylist->Clear();
                                                delete mylist;

                                        } else {

                                                TH2* myhist = (TH2*) UIUC::TFactory::CreateTH2chain(obj0->GetName(), obj0->GetTitle(), lHist, vRun);
                                                if(bSimpleAverage) {

                                                        gPrint->Debug(__METHOD_NAME__, "Histogram simple average ongoing..");
                                                        vMergedObject[index] = (TObject*) UIUC::TFactory::AverageBinContent(myhist, "x");

                                                } else if(bWeightedAverage) {

                                                        gPrint->Debug(__METHOD_NAME__, "Histogram weighted average ongoing..");
                                                        vMergedObject[index] = (TObject*) UIUC::TFactory::AverageBinContent(myhist, "x", "rc");

                                                } else {

                                                        gPrint->Debug(__METHOD_NAME__, "Histogram sum ongoing..");
                                                        vMergedObject[index] = (TObject*) UIUC::TFactory::Projection(myhist, "x");
                                                }
                                        }

                                        if(vMergedObject[index]) {
                                                ((TH1*) vMergedObject[index])->GetXaxis()->SetTitle("run");
                                                ((TH1*) vMergedObject[index])->SetTitle(obj0->GetTitle());
                                                ((TH1*) vMergedObject[index])->SetName(objname.Data() + TString(":run-by-run"));
                                        }

                                } else {

                                        if(gPrint->WarningIf(obj0->InheritsFrom("TH3"), __METHOD_NAME__, "TH3 cannot be merged run by run.. (not implementeed) skip..") ) {

                                                delete obj0;
                                                obj0 = NULL;

                                                return false;

                                        } else if(obj0->InheritsFrom("TH2")) {

                                                if(bDisregardTH3) {

                                                        gPrint->Warning(__METHOD_NAME__, "Histogram \"" + TString(obj0->GetName()) + "\" has been disregarded.. because outgoing histogram is a TH3");
                                                        if(gPrint->IsQuiet()) cout << gPrint->kOrange << Form("Histogram disregarded.. outgoing histogram is TH3") << gPrint->kNoColor  << endl;

                                                        delete obj0;
                                                        obj0 = NULL;

                                                        return false;
                                                }

                                                vMergedObject[index] = UIUC::TFactory::CreateTH3chain(obj0->GetName(), obj0->GetTitle(), lHist, vRun);

                                        } else if(obj0->InheritsFrom("TH1")) vMergedObject[index] = UIUC::TFactory::CreateTH2chain(obj0->GetName(), obj0->GetTitle(), lHist, vRun);

                                        if(vMergedObject[index]) {
                                                ((TH1*) vMergedObject[index])->GetXaxis()->SetTitle("run");
                                                ((TH1*) vMergedObject[index])->SetTitle(obj0->GetTitle());
                                                ((TH1*) vMergedObject[index])->SetName(objname.Data() + TString(":run-by-run"));
                                        }
                                }

                        } else {

                                gPrint->Debug(__METHOD_NAME__, "Default merging enabled..");
                                if(bSimpleAverage) {

                                        gPrint->Debug(__METHOD_NAME__, "Histogram simple average ongoing..");
                                        vMergedObject[index] = UIUC::TFactory::AverageBinContent(lHist);

                                } else if(bWeightedAverage) {

                                        gPrint->Debug(__METHOD_NAME__, "Histogram weighted average ongoing..");
                                        vMergedObject[index] = UIUC::TFactory::AverageBinContent(lHist, "rc");

                                } else if(bChain1D) {

                                        gPrint->Debug(__METHOD_NAME__, "1D chain the selected histograms..");
                                        vMergedObject[index] = UIUC::TFactory::CreateTH1chain((TString) lHist->First()->GetName()+":chain-1d", "", lHist);

                                } else if(bChain2D) {

                                        gPrint->Debug(__METHOD_NAME__, "2D chain the selected histograms..");
                                        vMergedObject[index] = UIUC::TFactory::CreateTH2chain((TString) lHist->First()->GetName()+":chain-2d", "", lHist);

                                } else if(bChain3D) {

                                        gPrint->Debug(__METHOD_NAME__, "3D chain the selected histograms..");
                                        vMergedObject[index] = UIUC::TFactory::CreateTH3chain((TString) lHist->First()->GetName()+":chain-3d", "", lHist);

                                } else {

                                        gPrint->Debug(__METHOD_NAME__, "Histogram sum ongoing..");
                                        vMergedObject[index] = UIUC::TFactory::Sum(lHist);
                                }

                                if(vMergedObject[index]) {

                                        ((TH1*) vMergedObject[index])->SetTitle(obj0->GetTitle());
                                        ((TH1*) vMergedObject[index])->SetName(objname.Data());
                                }
                        }

                        delete obj0;
                        obj0 = NULL;

                        lHist->Clear();

                } else if (obj0->InheritsFrom("TTree")) {

                        if(bNoTree) {

                                gPrint->Message(__METHOD_NAME__, "Skipping tree named \""+TString(obj0->GetName())+"\"");
                                delete obj0;
                                obj0 = NULL;

                                return true;
                        }

                        TString eMessage = "Please consider to use --buffer option, circular buffer is enabled and size of the provided list is bigger..";
                        gPrint->WarningIf(vList[index].size() > UIUC::TFileReader::kOpenLimit, __METHOD_NAME__, eMessage);

                        gPrint->Debug(__METHOD_NAME__, "Tree merging enabled..");
                        TList *lTree = new TList;
                        lTree->SetOwner();

                        if(vMergedObject[index]) lTree->Add(vMergedObject[index]);
                        lTree->Add(obj0);

                        TTree *mytree = NULL;
                        
                        int N = vList[index].size();
                        if(lTree->GetEntries() == 1 && N == 1) mytree = (TTree*) obj0;
                        else {

                                for(auto j = 1; j < N; j++) {

                                        if(N > 512) gPrint->ProgressBar("Loading trees into memory.. ", j+1, N);
                                        TTree *buf = (TTree*) UIUC::TFileReader::ReadObj(vList[index][j]);
                                        lTree->Add(buf);
                                }

                                mytree = TTree::MergeTrees(lTree);
                        }

                        mytree->SetName(objname.Data());
                        gDirectory->Add(mytree);

                        vMergedObject[index] = mytree->CloneTree();
                        lTree->Delete();

                } else if (obj0->InheritsFrom("TVirtualPad")) {

                        gPrint->Debug(__METHOD_NAME__, "TCanvas merging enabled..");
                        TCanvas *canvas = (TCanvas*) obj0->Clone(objname.Data() + TString("-0"));
                        gDirectory->Add(canvas);

                        TList *l0 = canvas->GetListOfPrimitives();
                        TIter Next0(l0);

                        int k = 0;
                        const int color[10] = {kBlue+2, kRed+2, kGreen+2, kOrange+5, kPink-7, kViolet+10, kMagenta+2, kTeal+2, kSpring+9, kBlack};
                        const int fillcolor[10] = {38, 50, kGreen+2, kOrange+5, kPink-7, kViolet+10, kMagenta+2, kTeal+2, kSpring+9, kBlack};
                        const double fillalpha[10] = {0.35, 0.35, 0.35, 0.35};

                        TObject *obj = NULL;
                        vector<TVirtualPad*> vpad0;

                        for(auto i = 0; (obj = Next0()); i++)
                        {
                                if(!obj->InheritsFrom("TVirtualPad")) continue;

                                vpad0.push_back((TVirtualPad*) obj);

                                TList *l02 = vpad0.back()->GetListOfPrimitives();
                                TIter Next02(l02);

                                TObject *obj2 = NULL;
                                for(auto j = 0; (obj2 = Next02()); j++) {

                                        if(obj2->InheritsFrom("TH1D")) {

                                                TH1D *h = (TH1D*) obj2->Clone();
                                                h->SetLineColor(color[k]);
                                                h->SetMarkerColor(color[k]);
                                                h->SetFillColorAlpha(fillcolor[k], fillalpha[k]);
                                        }
                                }
                        }
                        k++;

                        vector<TVirtualPad*> vpad;
                        int N = vList[index].size();
                        for(auto j = 1; j < N; j++) {

                                if(N > 512) gPrint->ProgressBar("Loading canvases into memory.. ", j+1, N);

                                TCanvas *buf = (TCanvas*) UIUC::TFileReader::ReadObj(vList[index][j]);

                                TList *l = (TList*) buf->GetListOfPrimitives();
                                TIter Next(l);

                                vector<TVirtualPad*> vpad;
                                for(auto i = 0; (obj = Next()); i++)
                                {
                                        if(!obj->InheritsFrom("TVirtualPad")) continue;
                                        vpad.push_back((TVirtualPad*) obj);

                                        if(gPrint->WarningIf(vpad0.size() < vpad.size(), __METHOD_NAME__, "Too many subpad.. ")) return 1;
                                        else vpad0[vpad.size()-1]->cd();

                                        TList *l2 = (TList*) vpad.back()->GetListOfPrimitives();

                                        TIter Next2(l2);
                                        TObject *obj2 = NULL;
                                        for(auto jj = 0; (obj2 = Next2()); jj++) {

                                                if(obj2->InheritsFrom("TH1D")) {

                                                        TH1D *h = (TH1D*) obj2;
                                                        if(TString(h->GetName()).EqualTo("Statbox")) continue;

                                                        h->SetLineColor(color[k]);
                                                        h->SetMarkerColor(color[k]);
                                                        h->SetFillColorAlpha(fillcolor[k], fillalpha[k]);
                                                        h->Draw("SAME");
                                                }
                                        }
                                }

                                vpad.clear();
                                //  delete buf;
                                //  buf = NULL;

                                k++;
                        }

                        vMergedObject[index] = canvas;
                        //  delete obj0;
                        //  obj0 = NULL;

                } else {

                        gPrint->Warning(__METHOD_NAME__, (TString) "Unknown merging process for \"" + obj + "\" ("+TString(obj0->IsA()->GetName())+")..");
                }


                return true;
        }

        int Finalize()
        {
                gPrint->SetSingleSkipIncrementTab();
                gPrint->Info(__METHOD_NAME__, Form("%d object(s) found over %d file(s) to merge..", (int) vPath.size(), nFiles));
                if(!vPath.size()) {

                        cerr << "Nothing to merge.." << endl;
                        return 0;
                }

                int N = vPath.size();
                for(auto i = 0; i < N; i++) {

                        TString objname = vPath[i];

                        TString output_filename = UIUC::TFileReader::FileName(this->fOutputPattern);
                        if(!bDisplayFullname) output_filename = UIUC::TFileReader::BaseName(output_filename);

                        TString output_obj = UIUC::TFileReader::ApplyCorrectionObj(UIUC::TFileReader::Obj(this->fOutputPattern) + objname);

                        this->MergingProcess(objname);
                        if(vMergedObject[i] == NULL) {

                                if(!bSkipDisplay)
                                        if(gPrint->IsQuiet()) cout << Form(gPrint->kRed + "%s:%s (%d/%d)" + gPrint->kNoColor + " [No outgoing object saved..]", output_filename.Data(), output_obj.Data(), i+1, (int) N) << endl;
                                continue;
                        }

                        if(!bSkipDisplay)
                                if(gPrint->IsQuiet()) cout << Form(gPrint->kGreen + "%s:%s (%d/%d)" + gPrint->kNoColor, output_filename.Data(), output_obj.Data(), i+1, (int) N) << endl;
                        if(vMergedObject[i]->InheritsFrom("TH1")) InstanceUsage().ApplyHistosOptions((TH1*) vMergedObject[i]);

                        this->AddOutput(vMergedObject[i], UIUC::TFileReader::ObjDirName(vPath[i]));
                        /*if(hFormula.EqualTo("") && hFormulaErr.EqualTo("") && vMergedObject[i] != NULL) {

                            this->Save();
                            this->CleanOutput();
                           }*/
                }

                if(!hFormula.EqualTo("") || !hFormulaErr.EqualTo("")) {

                        cout << "Applying formula.. Outgoing histogram named \"" << name  << "\""<< endl;
                        vector<TH1*> vMergedHist(vMergedObject.size());

                        int N = vPath.size();
                        for(auto i = 0; i < N; i++)
                                vMergedHist[i] = (TH1*) vMergedObject[i];

                        TH1 *h = TVarexp(hFormula).GetHistogram(name, vMergedHist, hFormulaErr);
                        this->InstanceStyle().ApplyStyle(h);
                        this->AddOutput(h);
                }

                UIUC::HandboxUsage::bCallHistos = true;
                return 0;
        }
};

int main(int argc, char **argv)
{
        HandboxUsage &cmdline = UIUC::HandboxUsage::Instance(&argc, argv, "basics histos trees");

        TString output_str;
        vector<TString> vInput, vFlux;
        TFileReader *fRootList = new TFileReader("rootfile", "f");

        MyHandbox handbox;
        cmdline.AddMessage("This script is used to combine either root files or text files together..");
        cmdline.AddOption("--display-fqfn", "", &handbox.bDisplayFullname);
        cmdline.AddOption("--skip-display", "", &handbox.bSkipDisplay);
        cmdline.AddOption("--badspill-pattern", "Pattern to recognize run and spill number", &handbox.fPatternStr, ".*-{}.root:.*spill{}");
        cmdline.AddOption("--search", "h*", &handbox.sObjname);
        cmdline.AddOption("--run-by-run", "(will create N+1 dimension histogram)", &handbox.bRunByRun);
        cmdline.AddOption("--run-sort", "(make weighted average, instead of sum)", &handbox.bRunSort);
        cmdline.AddOption("--run-separator", "(red line on a given runnb)", handbox.vRunSeparator);
        cmdline.AddOption("--spill-by-spill", "(will look for spill label and create N+1 dimension histogram)", &handbox.bSpillBySpill);
        cmdline.AddOption("--single|--single-file", "Will try to merge all objects found into a single one..", &handbox.bSingle);
        cmdline.AddOption("--chain-1d", "", &handbox.bChain1D);
        cmdline.AddOption("--chain-2d", "", &handbox.bChain2D);
        cmdline.AddOption("--chain-3d", "", &handbox.bChain3D);
        cmdline.AddOption("--hist-name", "", &handbox.name);
        cmdline.AddOption("--hist-average", "(make weighted average, instead of sum)", &handbox.bWeightedAverage);
        cmdline.AddOption("--hist-simple-average", "(make simple average, instead of sum)", &handbox.bSimpleAverage);
        cmdline.AddOption("--hist-formula", "(e.g. 2*sqrt([c0]*[c1]); formula used to combine multiple histograms bin by bin)", &handbox.hFormula);
        cmdline.AddOption("--hist-formula-err", "(e.g. [c0]*[c1]*sqrt(1/[e0]+1/[e1]); formula used to combine multiple histograms bin by bin)", &handbox.hFormulaErr);
        cmdline.AddOption("--disregard-th3", "(remove th3 from analysis.. operations are time consuming)", &handbox.bDisregardTH3);
        cmdline.AddOption("--disregard-tree|-T", "(skip tree/tchain merging)", &handbox.bNoTree);
        cmdline.AddOption("--buffer", "(buffer size before merging.. step by step.. to be used when too many files)", &handbox.buffersize, -1);
        cmdline.AddArgument(fRootList, "input-{}.[txt|root] (use --search to use a pattern inside and only look for object names)");

        if(!cmdline.Usage() ) return 1;

        // Main core
        if(!fRootList->IsValid()) return 1;
        fRootList->Print();

        handbox.AddInput(fRootList->GetFqfn());
        delete fRootList;
        fRootList = NULL;

        if( gPrint->ErrorIf(handbox.GetOutputPattern().EqualTo(""), __METHOD_NAME__, "No output specified.. Please use --output option") ) return 1;

        // Run the Handbox
        return handbox.Run("0", TObjString::Class());
}
