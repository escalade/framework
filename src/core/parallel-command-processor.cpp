/* Copyright 2008 Ohio Supercomputer Center */
/* Copyright 2015 University of Tennessee */
/* Copyright 2020 University of Illinois */

/* Distribution of this program is governed by the GNU GPL.  See
   ../COPYING for details. */

/*
   Usage:  mpiexec [-c special-master-command] [-n #] [arg] parallel-command-processor cfgfile
   OR:  mpiexec [-c special-master-command] [-n #] [arg] parallel-command-processor << EOF
   cmd1
   cmd2
   ...
   cmdM
   EOF
 */

#include <limits.h>
#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <mpi.h>

#define REQUEST_TAG 0
#define STATUS_TAG  1
#define DATA_TAG    2

using namespace std;

int exec(string command) {

    pid_t pid = fork();

    if (pid < 0) return 0;
    else if (pid > 0) return pid;
    else {

 	char buffer[128];
 	string result = "";

	// Open pipe to file
   	FILE* pipe = popen(command.c_str(), "r");
   	if (!pipe) return 1;

   	// read till end of process:
   	while (!feof(pipe)) {

   	   // use buffer to read and add to result
   	   if (fgets(buffer, 128, pipe) != NULL)
   	      cout << buffer << flush;
   	}

   	pclose(pipe);
   	exit(0);
    }
}

int master_sleep = 0; // was set to 120 at first.. tbc

string master_cmd = "";
int master_cmd_pid= 0;

string master_cmd_end = "";

void remove_EOL(char *string) {
    for (int i = 0, len = strlen(string); i<len; i++)
        if (string[i]=='\n') string[i]='\0';
}

int MasterMind(int argc, char *argv[], int nminions) {
    int i = 0;
    int cont = 1;
    int stop = 0;
    int ncmds = 0;
    char cmd[LINE_MAX + 1];
    double tstart, tend;

    tstart = MPI_Wtime();

    /*
        Usage command
     */
    bool usage = true;
    string flag = "";
    FILE *input = NULL;

    cout << "[MASTER] " << argv[0] << ": " << (nminions + 1) << " core(s) available!" << endl;

    for (int i = 1; i<argc; i++) {
        if (strcmp(argv[i], "-c")==0) flag = "c";
        else if (strcmp(argv[i], "-e")==0) flag = "e";
        else if (strcmp(argv[i], "-f")==0) flag = "f";
        else if (strcmp(argv[i], "-s")==0) flag = "s";
        else if (flag=="c") master_cmd += string(argv[i]) + " ";
        else if (flag=="e") master_cmd_end += string(argv[i]) + " ";
        else if (flag=="s") master_sleep = atoi(argv[i]);
        else if (flag=="f" || flag.empty()) {
            if (input==NULL) {
                cout << "[MASTER] " << argv[0] << ": Input file found.. \"" << argv[i] << "\"" << endl;
                input = fopen(argv[i], "r");
                if (input==NULL) {
                    cerr << "[MASTER] " << argv[0] << ": Failed to load \"" << argv[i] << "\"" << endl;
                    usage = false;
                }
            } else {
                cerr << "[MASTER] " << argv[0] << ": Only a single file can be provided.." << endl;
                usage = false;
            }
        } else {
            cerr << "[MASTER] " << argv[0] << ": Unknown option provided.. \"" << argv[i] << "\"" << endl;
            usage = false;
        }
    }
    if (!usage) {
        cout << "[MASTER] " << argv[0] << " [-c <master command>] [-s <time>] [-f cmdlist.txt] (if no input file, stdin will be used)" << endl;
        return 1;
    }
    if (input==NULL) input = stdin;

    /*
        special master command
     */
    if (master_cmd.empty()) cout << "[MASTER] No master command to execute" << endl;
    else {

        master_cmd += " &";
        cout << "[MASTER] Command to execute: \"" << master_cmd << "\"" << endl;

        master_cmd_pid = exec(master_cmd);
        cout << "[MASTER] --- Child PID returned \"" << master_cmd_pid << "\"" << endl;

        if (master_sleep!=0) {
            cout << "[MASTER] --- Sleeping request for " << master_sleep << " second(s)" << endl;
            sleep(master_sleep);
        }
    }

    if (nminions<1) {
        cerr << "[MASTER] At least 2 MPI processes are required to continue!" << endl;
        return 1;
    }

    /*
        initial distribution
     */
    MPI_Barrier(MPI_COMM_WORLD);
    cout << "[MASTER] All minions are ready. Let's start.." << endl;

    /*
        input file processing
     */
    while (ncmds<nminions && !feof(input) ) {
        memset(cmd, '\0', (size_t)LINE_MAX);
        fgets(cmd, LINE_MAX, input);

        while ((strlen(cmd)==0 || cmd[0]=='#') && !feof(input)) {
            memset(cmd, '\0', (size_t)LINE_MAX);
            fgets(cmd, LINE_MAX, input);
            if (strlen(cmd)>0) remove_EOL(cmd);
        }
        if (strlen(cmd)>0) MPI_Send(cmd, strlen(cmd) - 1, MPI_CHAR, ++ncmds, DATA_TAG, MPI_COMM_WORLD);
    }

    /* main loop */
    cout << "[MASTER] Main loop.. calling workers" << endl;

    while (!feof(input) ) {
        int next;
        MPI_Status mystat;

        int retcode;
        MPI_Recv(&retcode, 1, MPI_INT, MPI_ANY_SOURCE, REQUEST_TAG, MPI_COMM_WORLD, &mystat);
        next = mystat.MPI_SOURCE;

        #ifdef DEBUG
        cout << "[MASTER] Rank " << next << " returned code " << retcode << endl;
        #endif /* DEBUG */

        MPI_Send(&cont, 1, MPI_INT, next, STATUS_TAG, MPI_COMM_WORLD);
        memset(cmd, '\0', (size_t)LINE_MAX);
        fgets(cmd, LINE_MAX, input);

        while ((strlen(cmd)==0 || cmd[0]=='#') && !feof(input)) {
            memset(cmd, '\0', (size_t)LINE_MAX);
            fgets(cmd, LINE_MAX, input);
            if (strlen(cmd)>0) remove_EOL(cmd);
        }
        if (strlen(cmd)>0) MPI_Send(cmd, strlen(cmd) - 1, MPI_CHAR, next, DATA_TAG, MPI_COMM_WORLD);
    }

    /* cleanup */
    cout << "[MASTER] Waiting for workers to finish.." << endl;
    MPI_Request *req = (MPI_Request *)calloc((size_t)(2 * nminions), sizeof(MPI_Request));
    MPI_Status *reqstat = (MPI_Status *)calloc((size_t)(2 * nminions), sizeof(MPI_Status));

    for (int i = 0; i<nminions; i++) {
        char exitcmd[] = "exit";
        MPI_Isend(exitcmd, strlen(exitcmd), MPI_CHAR, i + 1, DATA_TAG, MPI_COMM_WORLD, &req[2 * i]);
        MPI_Isend(&stop, 1, MPI_INT, i + 1, STATUS_TAG, MPI_COMM_WORLD, &req[2 * i + 1]);
    }

    MPI_Waitall(2 * nminions, req, reqstat);
    MPI_Barrier(MPI_COMM_WORLD);
    ncmds = 0;

    MPI_Reduce(&stop, &ncmds, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    tend = MPI_Wtime();

    cout << "[MASTER] Executed " << (ncmds - 1) << " commands in " << (tend - tstart) << " seconds on " << nminions << " minions" << endl;
    free(req);
    free(reqstat);

    if(master_cmd_pid != 0) kill(master_cmd_pid, SIGKILL);

    if (master_cmd_end.empty()) cout << "[MASTER] No master command to execute at end" << endl;
    else {
        cout << "[MASTER] Command to execute (at end): \"" << master_cmd_end << "\"" << endl;
        int retcode = system(master_cmd_end.c_str());
        cout << "[MASTER] --- Returned code: \"" << retcode << "\"" << endl;
    }

    return 0;
}

#define UNUSED(expr) do { (void)(expr); } while (0)
int Minion(int rank) {
    char cmd[LINE_MAX + 1];
    int cont = 1;

    MPI_Status mystat;
    int ncmds = 0;

    /* initial distribution */
    #ifdef DEBUG
    cout << " MINION-" << rank << ".. ";
    #else
    UNUSED(rank);
    #endif
    MPI_Barrier(MPI_COMM_WORLD);

    /* main loop */
    int retcode = 0;

    while (cont==1) {
        int nbytes;
        memset(cmd, '\0', (size_t)LINE_MAX);
        MPI_Recv(cmd, LINE_MAX, MPI_CHAR, 0, DATA_TAG, MPI_COMM_WORLD, &mystat);
        /* do your thing */
        if (strlen(cmd)>0) {
            #ifdef DEBUG
            cout << "[MINION-" << rank << "] Executing \"" << cmd << "\"" << endl;
            #endif /* DEBUG */

            retcode = system(cmd);
            ncmds++;
        }

        MPI_Send(&retcode, 1, MPI_INT, 0, REQUEST_TAG, MPI_COMM_WORLD);
        MPI_Recv(&cont, 1, MPI_INT, 0, STATUS_TAG, MPI_COMM_WORLD, &mystat);
    }

    /* cleanup */
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Reduce(&ncmds, NULL, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    return retcode;
}

int main(int argc, char *argv[]) {

    int nproc;
    int rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Split master and minions
    int retcode;
    if (rank==0) retcode = MasterMind(argc, argv, nproc - 1);
    else retcode = Minion(rank);

    // Finalize execution
    MPI_Finalize();
    return retcode;
}
