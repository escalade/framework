#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <limits>

#include <dirent.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

using namespace std;

bool Usage(int argc, char **argv)
{
        if(argc < 2) {

                cerr << "[Usage] " << argv[0] << " <directory or file>" << endl;
                return 0;
        }

        return 1;
}

/*
I had need in something like this not so long ago (my difference is I
needed recursive scan) so I added only some comments... Sorry for recursion
but I was short of time and this was only part of internal one-time tool.
*/

int nfile = 0;

double size_file = 0;
int count_file = 0, count_dir = 0;

/* Print all the dirs starting from <path> [maybe recursive]. */
int print_dirs(const char *path, int recursive)
{
    struct dirent *direntp = NULL;
    DIR *dirp = NULL;
    size_t path_len;

    /* Check input parameters. */
    if (!path)
        return -1;
    path_len = strlen(path);

    if (!path || !path_len || (path_len > _POSIX_PATH_MAX))
        return -1;

    /* Open directory */
    dirp = opendir(path);
    if (dirp == NULL)
        return -1;

    while ((direntp = readdir(dirp)) != NULL)
    {
        /* For every directory entry... */
        struct stat fstat;
        char full_name[_POSIX_PATH_MAX + 1];

        /* Calculate full name, check we are in file length limts */
        if ((path_len + strlen(direntp->d_name) + 1) > _POSIX_PATH_MAX)
            continue;

        strcpy(full_name, path);
        if (full_name[path_len - 1] != '/')
            strcat(full_name, "/");
        strcat(full_name, direntp->d_name);

        /* Ignore special directories. */
        if ((strcmp(direntp->d_name, ".") == 0) ||
            (strcmp(direntp->d_name, "..") == 0))
            continue;

        /* Print only if it is really directory. */
        if (stat(full_name, &fstat) < 0)
            continue;
        if (S_ISDIR(fstat.st_mode))
        {
    	    count_dir++;
            if (recursive)
                print_dirs(full_name, 1);
        } else {

			if(count_file%1024 == 0 && count_file > 0) {

				cout << count_file << " file(s) processed.." << '\r' << flush;
			}

		FILE *f = fopen(full_name, "r");
		if (f == NULL) {

			cerr << "Failed to open: " << full_name << endl;
			return 1;
		}

		unsigned char byte;
		byte = std::fgetc(f);
		fclose(f);

		size_file += fstat.st_size;
		count_file++;
	}
    }

    /* Finalize resources. */
    (void)closedir(dirp);
    return 0;
}

/* We are taking first argument as initial path name. */
int main(int argc, const char* argv[])
{
    if (argc < 2)
        return -1;

    print_dirs(argv[1], 1);
	cout << count_dir << " folder(s) found" <<"; " << count_file << " file(s) refreshed: "<< std::setprecision(2) << (size_file/1024/1024/1024) << " GB" << endl;
    return 0;
}
