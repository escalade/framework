#
# Compilation of sources in ./src
find_package(MPI)
if(MPI_FOUND)

        message(STATUS "Parallel command process enabled..")

        add_executable(pcp ${CMAKE_CURRENT_SOURCE_DIR}/parallel-command-processor.cpp)
        target_link_package(pcp PRIVATE MPI)

        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/pcp DESTINATION bin
                PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
endif()

find_package(ROOT)
if(ROOT_FOUND)

        find_package(HDF5)
        if(HDF5_FOUND)

                message(STATUS "HDF5 support enabled..")

                add_executable(hdf2root ${CMAKE_CURRENT_SOURCE_DIR}/hdf2root.cc)
                target_link_package(hdf2root PRIVATE ROOT)
                target_link_package(hdf2root PRIVATE HDF5)

                install(FILES ${CMAKE_CURRENT_BINARY_DIR}/hdf2root DESTINATION bin
                        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)

        endif()

        add_executable(root-async ${CMAKE_CURRENT_SOURCE_DIR}/root-async.C)
        target_link_libraries(root-async ${ROOT_LIBRARIES})

        add_executable(root-accesstry ${CMAKE_CURRENT_SOURCE_DIR}/root-accesstry.C)
        target_link_libraries(root-accesstry ${ROOT_LIBRARIES})
        
        install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/top2root DESTINATION bin
                PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/root-async DESTINATION bin
                PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
        install(FILES ${CMAKE_CURRENT_BINARY_DIR}/root-accesstry DESTINATION bin
                PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)

endif()

add_executable(badd ${CMAKE_CURRENT_SOURCE_DIR}/binary-merging.cxx)
add_executable(pokedir ${CMAKE_CURRENT_SOURCE_DIR}/pokedir.cxx)

install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/escalade DESTINATION bin
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/pokedir DESTINATION bin
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/badd DESTINATION bin
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)

include(ProcessorCount)
ProcessorCount(N)

message(STATUS "${N} CPU(s) available for compilation..")