#include <iostream>
#include <fstream>

#include <string.h>
#include <map>
#include <vector>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

using namespace std;

double get_filesize(const char* fname){

    std::fstream file(fname, file.in | file.binary);
    if (!file )  return 0;

    file.seekg(0, file.end);
    auto size = file.tellg();

    file.close();

    return size;
}

bool Usage(int argc, char **argv)
{
        if(argc < 3) {

                cerr << "[Usage] " << argv[0] << " <target.root> <source1.root> [.. <sourceN.root>] [--max|-m <bytes>]" << endl;
                return 0;
        }

        return 1;
}

int main(int argc, char **argv)
{
        if( !Usage(argc, argv) ) return 1;

    // READ ARGUMENTS
    string target = argv[1];

    double maxsize = 0;
    vector<string> source;
        for (int i = 2; i < argc; i++) {

        if(strcmp(argv[i], "-m") == 0 || strcmp(argv[i], "--max") == 0) {

            if(i == argc) {

                cerr << "Wrong usage.." << endl;
                return 1;
            }

            maxsize = atof(argv[++i]);

        } else source.push_back(argv[i]);
    }

    // PREPARE I/O VECTORS
    vector<string> input;
    map<int,vector<string>> output;

    double fsize = 0;
    for(int i = 0, j = 0, k = 0, I = source.size(); i < I; i++) {

        fsize += get_filesize(source[i].c_str());
        if(fsize > maxsize && maxsize > 0) {

            k = 0;
            fsize = 0;
        }

        if(k == 0) {

            output.insert( pair<int,vector<string>>(j, vector<string>()) );

            if(j == 0) input.push_back(target);
            else if(j < 10) input.push_back(target + ".00" + to_string(j));
            else if(j < 100) input.push_back(target + ".0" + to_string(j));
            else if(j == 1000) {

                cerr << "Too many target files.. (>999) cannot process!" << endl;
                return 1;
            }

            j++;
        }

        output[j-1].push_back(source[i]);
        k++;
    }

    // PROCESS..
    for(int i = 0, I = input.size(); i < I; i++) {

            ofstream fTarget(input[i], ios::binary);
            if(!fTarget || !fTarget.is_open()) {

                    cerr << "Failed to open " << argv[0] << endl;
            continue;
            }

        cout << "TARGET #" << i << ": "<< input[i] << endl;
        for(int j = 0, J = output[i].size(); j < J; j++) {

                    ifstream fSource(output[i][j], ios::binary);
                    if(!fSource || !fSource.is_open()) {

                            cerr << "\tSOURCE #"<< i << "." << j << ": " << output[i][j] << " (FAILED)" << endl;
                            continue;
                       }

                    if ( fSource.peek() == ifstream::traits_type::eof() ) {

                            cerr << "\tSOURCE #"<< i << "." << j << ": " << output[i][j] << " (EMPTY)" << endl;
                            fSource.close();
                            continue;
                    }

                    copy(istreambuf_iterator<char>(fSource),
                         istreambuf_iterator<char>( ),
                         ostreambuf_iterator<char>(fTarget)
                    );

                    cout << "\tSOURCE #"<< j << ": " << output[i][j] << " (SUCCEEDED)" << endl;
                    fSource.close();
        }

            fTarget.close();
        }

        return 0;
}
