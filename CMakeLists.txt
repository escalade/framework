
# Version used during development
cmake_minimum_required(VERSION 3.20)

# Project information
set(PROJECT_TITLE "Bulk Data Management Framework")
project(esc
    VERSION 0.1
    DESCRIPTION "Send job on batch, monitor and archive."
    HOMEPAGE_URL "https://gitlab.cern.ch/escalade/framework"
)

set(LIBRARY ${PROJECT_NAME})
set(LIBRARY_TITLE ${PROJECT_TITLE})

# Additional cmake features
add_compile_options(-Wextra -Wno-sign-compare -Wimplicit-fallthrough)
set(CMAKE_INSTALL_MESSAGE "LAZY")

# Check if git submodule are initialized in case you use custom cmake modules.
file(GLOB CMAKE_MODULE_PATHS "${CMAKE_CURRENT_SOURCE_DIR}/cmake/*")
foreach(MODULE_PATH ${CMAKE_MODULE_PATHS})
    if(IS_DIRECTORY ${MODULE_PATH})
        list(PREPEND CMAKE_MODULE_PATH ${MODULE_PATH})
    endif()
endforeach()

#
# Prepare project architecture
include(ProjectArchitecture)
include(FindPackageStandard)
include(BuildOptions)
include(DisplayMotd)

DISPLAY_MOTD()
MESSAGE_TITLE("${PROJECT_TITLE}")

update_build_options()
check_build_option(BUILD_IMAGE "Docker container won't be built.")
check_build_option(BUILD_DOCUMENTATION "Library documentation won't be built.")
check_build_option(BUILD_TESTS "Tests for this library won't be running.")
check_build_option(BUILD_LIBRARY "Library `${PROJECT_NAME}` will not be built.")
check_build_option(BUILD_EXAMPLES "Examples how-to-use `${PROJECT_NAME}` won't be computed.")
check_build_option(BUILD_TOOLS "Additional binary tools won't be computed.")

#
# Build project
if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmake/.env.in AND NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/.env)
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/.env.in ${CMAKE_CURRENT_SOURCE_DIR}/.env @ONLY)
endif()
if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/build AND NOT CMAKE_CURRENT_BINARY_DIR STREQUAL ${CMAKE_CURRENT_SOURCE_DIR}/build)
     FILE(CREATE_LINK ${CMAKE_BINARY_DIR_RELATIVE} ${CMAKE_CURRENT_SOURCE_DIR}/build SYMBOLIC)
endif()

CONFIGURE_FILE(./cmake/setenv.sh.in ${CMAKE_BINARY_DIR}/setenv.sh @ONLY)
CONFIGURE_FILE(./cmake/setup.sh.in ${CMAKE_BINARY_DIR}/setup.sh @ONLY)
FILE(COPY ${CMAKE_CURRENT_SOURCE_DIR}/pilots   DESTINATION ${CMAKE_BINARY_DIR})
FILE(COPY ${CMAKE_CURRENT_SOURCE_DIR}/scripts  DESTINATION ${CMAKE_BINARY_DIR})

execute_process(COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/scripts/detect_cluster.sh OUTPUT_VARIABLE CLUSTER OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "Cluster detected: \"${CLUSTER}\"")

#
# Create docker image (used in GitLab CI/CD for running tests)
if(BUILD_IMAGE AND EXISTS Dockerfile)
    include(DockerContainer)
endif()

#
# Additional restriction/warning feature
include(DisableInSourceBuild)
include(GitBranchWarning)

# Project overview
DUMP_PROJECT()

#
# Create a library
if(BUILD_LIBRARY)

    #
    # Load compiler
    include(StandardCompilerC)
    include(StandardCompilerCXX)

    #
    # Library dependencies
    include(RootFramework)
    find_package(ROOT+)

    #
    # Prepare library including ROOT    
    FILE_SOURCES(SOURCES "src" EXCLUDE "src/core")
    FILE_HEADERS(HEADERS "include" RECURSE)
    ROOT_ADD_LIBRARY(${LIBRARY}
        SOURCES ${SOURCES}
        HEADERS ${HEADERS}
        PACKAGES PUBLIC ROOT+
    )

    #
    # Compilation of sources in ./src
    add_subdirectory(src/core)

    if(CLUSTER AND NOT DISABLE_CLUSTER_SOURCE)
        if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/pilots/clusters/${CLUSTER}/src/CMakeLists.txt)
            message(STATUS "Prepare ./pilots/clusters/${CLUSTER}/src/ directory")
            add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/pilots/clusters/${CLUSTER}/src)
        endif()
    endif()

    # Install framework
    ROOT_INSTALL_LIBRARY(${LIBRARY})
    if(${CMAKE_MAINPROJECT})
        
        add_custom_target(lib DEPENDS ${LIBRARY} COMMENT "Compiling library")
        
        if(BUILD_TESTS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tests)
            add_subdirectory("tests")
        endif()

        if(BUILD_EXAMPLES AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/examples)
            add_subdirectory("examples")
        endif()

        if(BUILD_TOOLS AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tools)
            add_subdirectory("tools")
        endif()
    endif()
endif()

#
# Prepare scripts
install(FILES ${CMAKE_BINARY_DIR}/setup.sh DESTINATION ${CMAKE_INSTALL_PREFIX} RENAME setup.sh
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
install(FILES ${CMAKE_BINARY_DIR}/setenv.sh DESTINATION . RENAME setenv.sh
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/share   DESTINATION . USE_SOURCE_PERMISSIONS)
install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/scripts DESTINATION . USE_SOURCE_PERMISSIONS)
install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/pilots  DESTINATION . USE_SOURCE_PERMISSIONS)

#
# Prepare assets for documentation
if(BUILD_DOCUMENTATION AND EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/share/doxygen)
    add_subdirectory("share/doxygen")
endif()

#
# Hook Scripts
if(CMAKE_MAINPROJECT)

    # Make sure cmake includes are available
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/cmake 
            DESTINATION share 
            PATTERN "*.in" EXCLUDE 
            PATTERN ".*" EXCLUDE
            PATTERN "README*" EXCLUDE
    )

    # Post install message
    find_file(CMAKE_POST_CONFIG PostInstallConfig.cmake PATHS ${CMAKE_MODULE_PATHS})
    if(CMAKE_POST_CONFIG)
        install(SCRIPT "${CMAKE_POST_CONFIG}")
    endif()

    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        check_target_dependencies()
    endif()

    # Post cmake message
    include(PostCMakeConfig)

endif()