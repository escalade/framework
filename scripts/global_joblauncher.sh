#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_GLOBAL_JOBLAUNCHER "<job idpattern>" $# 1
[ $? -eq 1 ] && exit 1

#
# Global variables
#
export ESCALADE_JOBPATTERN=$1

set --

source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && exit 1

if [ -z "$ESCALADE_BATCH" ]; then
    
    echo -e "${RED}--> No batch system loaded.. ESCALADE_BATCH is not defined${NC}"
    echo -e "${RED}    So you cannot send this command ! ${NC}"
    exit 1
fi

source $ESCALADE_PREPENV > /dev/null # This script create the production structure and the batch uniqID
[ $? -eq 1 ] && exit 1

# Check if already archived
$ESCALADE_ISARCHIVED $ESCALADE_OUTPUT0
[ $? -eq 1 ] && exit 1

# Check input files
if [ -z "$(ls -A $ESCALADE_INPUT0)" ]; then
    echo -e "${RED}--> There is no source directory ($ESCALADE_INPUT0) as expected from where to take input txtfiles${NC}"
    echo -e "${RED}    Please run first \`${ESCALADE_SOFTWARE}-data\`${NC}"
    exit 1
fi

#
# Authenticity keys
#
source $ESCALADE_CHECKKEY ${ESCALADE_OUTPUT0} ${ESCALADE_PRODUCTION} # If created, it will check keys
[ $? -eq 1 ] &&	exit 1

# Add new key
echo -ne "\t"
source $ESCALADE_ADDKEY ${ESCALADE_INPUT0} ${ESCALADE_OUTPUT0} ${ESCALADE_PRODUCTION} # Create a pair of key if not found
[ $? -eq 1 ] && exit 1

#
# Prepare jobs request
#
source $ESCALADE_LSDIR "1111" "0000" "1112"
[ $? -eq 1 ] && exit 1

[[ -z "$RUN" || "$RUN" == "*" ]] && RUN=.*
[ -z "$LS_INPUT0_ID" ] && LS_INPUT0_ID=$(cat "$LS_INPUT0" | grep '.txt' | grep -E "$RUN" | xargs -P$(nproc --all) -I % basename % .txt 2> /dev/null | sort -V | uniq)
[ -z "$LS_LOGDIR0_ID" ] && LS_LOGDIR0_ID=$(cat "$LS_LOGDIR0" | grep -v -E "/\." | grep -E "$RUN" | xargs -P$(nproc --all) -I % basename % 2> /dev/null | sort -V | uniq)
[ -z "$LS_LOGDIR0_TMP_ID" ] && LS_LOGDIR0_TMP_ID=$(cat "$LS_LOGDIR0_TMP" | grep -v -E "/\." | grep -E "$RUN" | xargs -P$(nproc --all) -I % basename % 2> /dev/null | sort -V | uniq)
[ -z "$LS_OUTPUT0_ID" ] && LS_OUTPUT0_ID=$(cat "$LS_OUTPUT0" | grep -v -E "/\." | grep -E "$RUN" | xargs -P$(nproc --all) -I % basename % 2> /dev/null | sort -V | uniq)

esc_echo -e "${GREEN}--> Looking for the PID file located in $ESCALADE_LOGDIR0${NC}"
LS_PIDFILE=$(cat $ESCALADE_LOGDIR0/.pid 2> /dev/null | grep -v "#" | awk '{print $4}' | sort -V | uniq)

echo -e "--> Check files to be send on grid.."
ESCALADE_NINPUT=$(echo "$LS_INPUT0_ID)" | wc -w)
ESCALADE_JOBACCEPTED_ID=$(diff <(echo "$LS_INPUT0_ID" | sort -V | uniq) <(echo -e "\n$LS_LOGDIR0_ID\n$LS_LOGDIR0_TMP_ID\n$LS_OUTPUT0_ID\n$LS_PIDFILE" | sort -V | uniq) | awk '$1 == "<" {print $2}')
ESCALADE_JOBACCEPTED_ID=$(echo "$ESCALADE_JOBACCEPTED_ID" | sort -V | uniq)
ESCALADE_NFILES=$(echo $ESCALADE_JOBACCEPTED_ID | wc -w)


ESCALADE_JOBREJECTED_ID=$(diff <(echo "$LS_INPUT0_ID" | uniq | sort -V) <(echo "$ESCALADE_JOBACCEPTED_ID") | awk '$1 == "<" {print $2}')
ESCALADE_NREJECTED=$(echo $ESCALADE_JOBREJECTED_ID | wc -w)
if [ ! -z "$ESCALADE_SUBMIT_NJOBS" ]; then
    if [[ ! -z "$ESCALADE_DELAY" ]]; then
        echo -e "${PURPLE}\tSubmission rate limitation is enabled..${NC} Only $ESCALADE_SUBMIT_NJOBS batch job(s) will be submitted every ${ESCALADE_DELAY}s.."
    else
        echo -e "${PURPLE}\tBatch job rate limitation is enabled..${NC} Only $ESCALADE_SUBMIT_NJOBS batch job(s) of this production will be submitted.."
    fi
fi

if [ ! -z "$ESCALADE_PENDING_JOBS" ]; then
    echo -e "${PURPLE}\tSubmission rate limitation is enabled..${NC} Only $ESCALADE_PENDING_JOBS batch job(s) will be allowed in the queue submitted every.."
fi

unset LS_INPUT0_ID
unset LS_LOGDIR0_ID
unset LS_LOGDIR0_TMP_ID
unset LS_OUTPUT0_ID

if [[ ! -z "$ESCALADE_NTRY" && $(echo "$ESCALADE_NFILES - $ESCALADE_NTRY" | bc) -gt 0 ]]; then
    
    ESCALADE_JOBREJECTED_EXTRA_ID=$(echo "$ESCALADE_JOBACCEPTED_ID" | tail -n+$(($ESCALADE_NTRY+1)))
    ESCALADE_NREJECTED_EXTRA=$(echo $ESCALADE_JOBREJECTED_EXTRA_ID | wc -w)
    ESCALADE_JOBACCEPTED_ID=$(echo "$ESCALADE_JOBACCEPTED_ID" | head -n$(($ESCALADE_NTRY)))
    
    echo -e "${PURPLE}\tSubmission limitation is enabled..${NC} $(echo "$ESCALADE_NFILES - $ESCALADE_NTRY" | bc) file(s) have been disregarded.."
    ESCALADE_NFILES=$(echo $ESCALADE_JOBACCEPTED_ID | wc -w)
fi

if [ ${ESCALADE_NFILES} -eq 0 ]; then
    echo -e "${ORANGE}\tSubmission summary:${NC} ${ESCALADE_NFILES}/${ESCALADE_NINPUT} file(s) have been accepted.."
    exit 0
else
    echo -e "${GREEN}\tSubmission summary:${NC} ${ESCALADE_NFILES}/${ESCALADE_NINPUT} file(s) have been accepted.."
fi

IFILE_DISPLAY=0
NFILE_DISPLAY=20
for ACCEPTEDID in $ESCALADE_JOBACCEPTED_ID; do
    
    if [ $IFILE_DISPLAY -eq $NFILE_DISPLAY ]; then
        
        echo -e "\t${CYAN}[..]${NC} (+ Some $(($ESCALADE_NFILES-$IFILE_DISPLAY)) other file(s) accepted)"
        let IFILE_DISPLAY=$(($IFILE_DISPLAY+1))
        break;
    fi
    
    tput el 2> /dev/null
    echo -e "\t${CYAN}Job #$ACCEPTEDID is valid and has been accepted..${NC}"
    
    let IFILE_DISPLAY=$(($IFILE_DISPLAY+1))
done

IFILE_DISPLAY=0
NFILE_DISPLAY=20
for REJECTEDID in $ESCALADE_JOBREJECTED_ID; do
    
    if [ $IFILE_DISPLAY -eq $NFILE_DISPLAY ]; then
        
        echo -e "\t${ORANGE}[..]${NC} (+ Some $(($ESCALADE_NREJECTED-$IFILE_DISPLAY)) other file(s) rejected)"
        let IFILE_DISPLAY=$(($IFILE_DISPLAY+1))
        break;
    fi
    
    tput el 2> /dev/null
    echo -e "\t${ORANGE}Job #$REJECTEDID is not valid..${NC} Some file(s) related to this run found in the production directory.."
    
    let IFILE_DISPLAY=$(($IFILE_DISPLAY+1))
done

IFILE_DISPLAY=0
NFILE_DISPLAY=20
for REJECTEDID in $ESCALADE_JOBREJECTED_EXTRA_ID; do
    
    if [ $IFILE_DISPLAY -eq $NFILE_DISPLAY ]; then
        
        echo -e "\t${PURPLE}[..]${NC} (+ Some $(($ESCALADE_NREJECTED_EXTRA-$IFILE_DISPLAY)) other file(s) rejected)"
        let IFILE_DISPLAY=$(($IFILE_DISPLAY+1))
        break;
    fi
    
    tput el 2> /dev/null
    echo -e "\t${PURPLE}Job #$REJECTEDID has been disregarded..${NC} You reach the file processing limit.."
    
    let IFILE_DISPLAY=$(($IFILE_DISPLAY+1))
done

#
# Loop over JOBLIST according to the value of ESCALADE_JOBSIZE
#
IFILE=0
NFILE=$(echo $ESCALADE_JOBACCEPTED_ID | wc -w)

MODULO=$ESCALADE_JOBSIZE
[[ ! -z "$ESCALADE_SUBMIT_NJOBS" && ! -z "$ESCALADE_DELAY" && $MODULO -gt $ESCALADE_SUBMIT_NJOBS ]] && MODULO=$ESCALADE_SUBMIT_NJOBS

IJOB=0
IJOBMAX=$(echo "(${NFILE}+${MODULO}-1)/${MODULO}" | bc)
if [ $NFILE -eq 0 ]; then
    echo -e "${RED}--> There is no job to submit..${NC} ($NFILE file(s) to be send)"
    exit 1
else
    echo -e "${GREEN}--> Prepare job requests..${NC} ($NFILE file(s) to be send)"
fi

unset ESCALADE_JOBREJECTED_ID
unset ESCALADE_JOBREJECTED_EXTRA_ID

export ESCALADE_JOBLEFT=$(mktemp --suffix -jobleft --tmpdir=$ESCALADE_TMPDIR)
echo "$ESCALADE_JOBACCEPTED_ID" | tr ' ' '\n' > $ESCALADE_JOBLEFT

# If --delay option is enabled and no --jobs option => Wait at the beginning of the job submittion
if [[ -s "$ESCALADE_JOBLEFT" && -z "$ESCALADE_SUBMIT_NJOBS" && ! -z "$ESCALADE_DELAY" ]]; then
    
    for TIMECOUNT in $(seq 1 $ESCALADE_DELAY); do
        
        # Check the safethread lock
        source $ESCALADE_SAFETHREAD > /dev/null
        [ $? -eq 1 ] && exit 1
        
        ESCALADE_REMAININGTIME=$(echo "$ESCALADE_DELAY-$TIMECOUNT" | bc)
        echo -ne "\t${PURPLE}* Time delay requested before starting productions.. please wait ${ESCALADE_REMAININGTIME}s${NC}\r"
        
        sleep 1
        tput el 2> /dev/null
    done
fi

#
# There is an ambiguity when using fakebatch and using a batch job launcher from software..
# This should not be needed.. Job wrapper should be modified
#
#if [[ -f $ESCALADE_SOFTWARE_DIR/joblauncher.sh && $ESCALADE_BATCH != "fakebatch" && -z "$ESCALADE_DEBUG" ]]; then
#
#        echo -e "\t* \"$ESCALADE_SOFTWARE\" software is using a specific batch job launcher available in the software pilot directory"
#	export ESCALADE_BATCH_JOBLAUNCHER=$ESCALADE_SOFTWARE_DIR/joblauncher.sh
#fi

while [ -s "$ESCALADE_JOBLEFT" ]; do
    
    export ESCALADE_JOBLIST=$(mktemp --suffix -joblist --tmpdir=$ESCALADE_TMPDIR)
    head -n$ESCALADE_JOBSIZE $ESCALADE_JOBLEFT > $ESCALADE_JOBLIST
    
    if [ ! -z "$ESCALADE_SUBMIT_NJOBS" ]; then
        
        # If --delay option is enabled and --jobs option => Wait in between X job submission
        if [[ ! -z "$ESCALADE_DELAY" ]]; then
            
            #head -n$ESCALADE_JOBSIZE $ESCALADE_JOBLIST > $ESCALADE_JOBLIST.0
            #mv $ESCALADE_JOBLIST.0 $ESCALADE_JOBLIST
            if [[ $(($IJOB%$ESCALADE_SUBMIT_NJOBS)) == 0 && $IJOB != 0 && ! -z "$ESCALADE_SUBMIT_NJOBS" ]]; then
                
                [ ! $IJOB -eq 0 ] && echo ""
                for TIMECOUNT in $(seq 1 $ESCALADE_DELAY); do
                    
                    # Check the safethread lock
                    source $ESCALADE_SAFETHREAD > /dev/null
                    [ $? -eq 1 ] && exit 1
                    
                    ESCALADE_REMAININGTIME=$(echo "$ESCALADE_DELAY-$TIMECOUNT" | bc)
                    echo -ne "\t${PURPLE}* Time delay requested before starting next jobs.. please wait ${ESCALADE_REMAININGTIME}s${NC}\r"
                    
                    sleep 1
                    tput el 2> /dev/null
                done
            fi
        else
            if [[ $(($IJOB%$ESCALADE_SUBMIT_NJOBS)) == 0 && $IJOB != 0 && ! -z "$ESCALADE_SUBMIT_NJOBS" ]]; then
                
                # quit the job submission
                [ ! $IJOB -eq 0 ] && echo ""
                
                echo -e "\t${PURPLE}* Job submission limit triggered.. skip${NC}"
                rm "$ESCALADE_JOBLEFT"
                
                tput el 2> /dev/null
                if [ $IJOB -eq $IJOBMAX ]; then
                    echo -e "\t* Summary : ${GREEN}$IJOB/$IJOBMAX job successfully processed !${NC}"
                else
                    echo -e "${ORANGE}\t* Summary : ${ORANGE}$IJOB/$IJOBMAX job successfully processed !${NC}"
                fi
                exit 0
            fi
        fi
        
        if [ ! -z "$ESCALADE_PENDING_JOBS" ]; then
            
            #Wait until the number of running job is decreasing below the limit !!
            ESCALADE_IJOBS=$($ESCALADE_GLOBALQSTAT $ESCALADE_LOGDIR0/.pid 2> /dev/null | wc -l)
            
            [ ! $IJOB -eq 0 ] && echo ""
            while [[ $ESCALADE_IJOBS -ge $ESCALADE_PENDING_JOBS && $ESCALADE_PENDING_JOBS -gt 0 ]]; do
                
                echo -ne "\t${PURPLE}* Job #$(($IJOB+1))/${IJOBMAX} : job limit triggered, please wait until \"batch jobs < $ESCALADE_PENDING_JOBS\".. (check every minute)${NC}\r"
                ESCALADE_IJOBS=$($ESCALADE_GLOBALQSTAT $ESCALADE_LOGDIR0/.pid 2> /dev/null | wc -l)
                
                sleep 60
                echo -ne "\a"
                tput el 2> /dev/null
            done
        fi
    fi
    
    diff "$ESCALADE_JOBLEFT" "$ESCALADE_JOBLIST" | awk '$1 == "<" {print $2}' > "$ESCALADE_JOBLEFT.0"
    mv "$ESCALADE_JOBLEFT.0" "$ESCALADE_JOBLEFT"
    
    ESCALADE_JOBLIST_SIZE=$(wc -l "$ESCALADE_JOBLIST" | awk '{print $1}')
    source $ESCALADE_PREPENV > /dev/null # This script create the production structure and the batch uniqID
    [ $? -eq 1 ] && exit 1
    
    #[[ -f $ESCALADE_SOFTWARE_DIR/joblauncher.sh ]] && export ESCALADE_BATCH_JOBLAUNCHER=$ESCALADE_SOFTWARE_DIR/joblauncher.sh
    
    # Check the safethread lock
    source $ESCALADE_SAFETHREAD > /dev/null
    [ $? -eq 1 ] && exit 1
    
    tput el 2> /dev/null
    echo -ne "\t* Job #$(($IJOB+1))/${IJOBMAX} sent.. ($ESCALADE_JOBLIST_SIZE file(s) included) : Waiting for batch answer.."
    
    # Job submittion
    if [[ ! -z "$ESCALADE_DEBUG" && "$ESCALADE_BATCH" != "fakebatch" ]]; then
        
        unset ESCALADE_QSUB
        unset ESCALADE_BATCH_OPTIONS
        unset ESCALADE_BATCH_TITLE_STR
        unset ESCALADE_BATCH_OUT_STR
        unset ESCALADE_BATCH_ERR_STR
    fi
    
    if [ "$ESCALADE_BATCH" == "fakebatch" ]; then
        
        export ESCALADE_QJOB_PID="fakeID.$(date | md5sum | head -c6)"
        sleep 1
        
        echo ""
        $ESCALADE_BATCH_JOBLAUNCHER 2>&1
    else
        ESCALADE_QJOB=$(eval "$ESCALADE_BATCH_JOBLAUNCHER 2>&1")
        [ ! -z "$ESCALADE_QJOB" ] && export ESCALADE_QJOB_PID=$(echo "$ESCALADE_QJOB" | tr '\n' ' ' | sed -ne "$ESCALADE_BATCH_SEDPID")
    fi
    
    if [ ! -z "$ESCALADE_QJOB_PID" ]; then
        
        # Create PID file
        JOBCOUNT=0
        while IFS= read -r JOBFILE; do
            
            source $ESCALADE_JOBINFO $JOBFILE #> /dev/null 2> /dev/null
            
            if [ $JOBCOUNT != 0 ]; then
                
                tput el 2> /dev/null
                echo -ne "\t* Job #$(($IJOB+1))/${IJOBMAX} sent.. ($ESCALADE_JOBLIST_SIZE file(s) included)"
            fi
            
            if [[ -z "$ESCALADE_JOBSLOT" || -z "$ESCALADE_JOBID" ]]; then
                
                tput el 2> /dev/null
                echo -e " : ${RED}Job ID ($ESCALADE_QJOB_PID) failed to register into PID file..${NC} ($(($JOBCOUNT+1))/$ESCALADE_JOBLIST_SIZE)"
                continue;
            fi
            
            echo -ne " : Job ID ($ESCALADE_QJOB_PID) registered into PID file.. Please wait ($(($JOBCOUNT+1))/$ESCALADE_JOBLIST_SIZE)\r"
            [ ! -s "$ESCALADE_LOGDIR0/.pid" ] && echo -e "#Batch PID\tUniq ID\t\t\t\t\tSlot\tJob ID\tOutput\tBatch System\tDate\tJoblist" > $ESCALADE_LOGDIR0/.pid
            
            echo -e "$ESCALADE_QJOB_PID\t$ESCALADE_BATCH_UNIQID\t$ESCALADE_JOBSLOT\t$ESCALADE_JOBID\t$ESCALADE_OUTPUT0\t$ESCALADE_BATCH\t$(date '+%Y%m%d-%H%M%S')\t$ESCALADE_JOBLIST" >> $ESCALADE_LOGDIR0/.pid
            JOBCOUNT=$(($JOBCOUNT+1))
            
        done < "$ESCALADE_JOBLIST"
        unset ESCALADE_QJOB_PID
        
        let IJOB=$((IJOB+1))
    else
        echo -e " : [Error] Job not sent (no PID found..)\a" >&2
        echo -e "\t${ORANGE}Command returned: \"$ESCALADE_QJOB\" ; Pattern used: \"$ESCALADE_BATCH_SEDPID\"${NC}" >&2
    fi
    
done
echo ""

#
# Summary of the jobs sent
#
if [ $IJOBMAX -eq 0 ]; then
    
    echo -e "${RED}--> There is no file accepted to process with the specified pattern \"$ESCALADE_JOBPATTERN\" (already processed, or running?)${NC}"
    exit 1
fi

rm "$ESCALADE_JOBLEFT"

tput el 2> /dev/null
if [ $IJOB -eq $IJOBMAX ]; then
    echo -e "\t* Summary : ${GREEN}$IJOB/$IJOBMAX job successfully processed !${NC}"
else
    echo -e "${ORANGE}\t* Summary : ${RED}$IJOB/$IJOBMAX job successfully processed !${NC}"
fi

exit 0
