#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_LOGFOOTER "" $# 0
[ $? -eq 1 ] && return 1

echo -e "\n--- ESCALADE------ ---" >> $ESCALADE_HISTORY
echo -e "--- End of the call." >> $ESCALADE_HISTORY
echo -e "--- Date: $(date)" >> $ESCALADE_HISTORY
echo -e "--- -------------- ---\n" >> $ESCALADE_HISTORY

return 0
