#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_ARCHIVELOG "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && exit 1

# Check if production is already archived or not
$ESCALADE_ISARCHIVED $ESCALADE_OUTPUT0
[ $? -eq 1 ] && exit 1

# Check the safethread lock
source $ESCALADE_SAFETHREAD > /dev/null
[ $? -eq 1 ] && exit 1

source $ESCALADE_LSDIR "1101" "0000" "2200" #> /dev/null 2> /dev/null
[ $? -eq 1 ] && exit 1

# Authenticity keys
source $ESCALADE_CHECKKEY ${ESCALADE_OUTPUT0} ${ESCALADE_PRODUCTION} # If created, it will check keys
[ $? -eq 1 ] && exit 1

LS_QSTAT=$(mktemp --suffix -ls-qstat)
$ESCALADE_GLOBALQSTAT $ESCALADE_LOGDIR0/.pid 2> /dev/null > $LS_QSTAT

[ -s "$LS_QSTAT" ] && ESCALADE_IJOBS=$(cat "$LS_QSTAT" | sort -V | uniq | wc -l) || ESCALADE_IJOBS=0
[ ! $ESCALADE_IJOBS -eq 0 ] && echo -e "\t${ORANGE}Some jobs ($ESCALADE_IJOBS) are still running, please wait until the end..${NC}"

#
# If jobs are running or/and no error have been found.
#
if [ ! $ESCALADE_IJOBS -eq 0 ]; then
    
    echo -e "${ORANGE}--> This job cannot be archived.. no all jobs are finished${NC}"
    exit 1
fi

if [[ ! -s "$LS_INPUT0" || -z "$(cat "$LS_OUTPUT0" | grep -v escalade-key)" ]]; then
    
    echo -e "${ORANGE}--> There is nothing to archive..${NC}" >&2
    exit 1
fi

ESCALADE_KEY=$(basename $(cat "$LS_OUTPUT0" | grep "escalade-key") 2> /dev/null)
if [ -z "$ESCALADE_KEY" ]; then
    
    echo -e "${RED}No escalade key found.. You cannot archive files${NC}"
    exit 1
fi

if [[ ! -z "$ESCALADE_FORCEARCHIVE" && "$ESCALADE_FORCEARCHIVE" != "0" ]]; then
    
    echo -e "${ORANGE}--> Force to archive option is enabled.. You are archiving without checking the logfiles !${NC}"
    
else
    # Checking all files were produced
    NB_INPUT0=$(cat "$LS_INPUT0" | grep -E "txt$" | wc -l)
    NB_PROCESSED_INPUT0=$(cat "$LS_LOGDIR0" | grep -E "txt$" | wc -l)
    
    if [[ $NB_INPUT0 > $NB_PROCESSED_INPUT0 ]]; then
        echo -e "${RED}--> Input files have not ${REDBKG}ALL${RED} been processed /!\ ${NC} Please use --force-archive to avoid this check"
        exit 1
    fi
    
    # Looking for non-zero size stderr logfiles
    FATAL=$(find $ESCALADE_TMPDIR/logs -type f -name "*.stderr" ! -size 0 2> /dev/null)
    if [[ ! $(echo "$FATAL" | wc -l) -eq 0 && ! -z "$FATAL" ]]; then
        
        # Overview of the fatal errors detected
        if [ ! $ESCALADE_IJOBS -eq 0 ]; then
            echo -e "${ORANGE}--> $(echo "$FATAL" | wc -l) log(s) detected with potentially fatal errors among the completed jobs${NC}"
        else
            echo -e "${ORANGE}--> $(echo "$FATAL" | wc -l) log(s) detected with potentially fatal errors${NC}"
        fi
        
        for FILE in $FATAL; do
            
            [[ ! -z "$RUN" && -z "$(echo "$FILE" | grep -E "$RUN")" ]] && continue;
            
            unset SHOWUP
            if [ -z "$(cat "$LS_LOGDIR0" | grep $(basename $FILE))" ]; then
                
                SHOWUP=$(echo $FILE | sed "s,$ESCALADE_TMPDIR/logs,$ESCALADE_LOGDIR0,g")
            fi
            
            if [ -f "$SHOWUP" ]; then
                esc_echo  "\t${BLUE}Error(s) detected in: $FILE${NC} ($(ls -lh $FILE | awk '{print $5}')B) - BUT it showed up after starting the parsing procedure${NC}"
            else
                esc_echo "\t${ORANGE}Error(s) detected in: $FILE${NC} ($(ls -lh $FILE | awk '{print $5}')B)${NC}"
            fi
        done
        [ ! -z "$ESCALADE_TMPDIR" ] && echo -e "\t${BLUE}ESCALADE_TMPDIR=${NC}$ESCALADE_TMPDIR"
        
        echo -e "${REDBKG}--> Manual action required..${NC}"
        
        # To prevent mistakes, ask for manual check
        echo -e "${ORANGE}\tPlease use : \"--force-archive\" option in your escalade command to pass by the detected error(s) while archiving..${NC}"
        exit 1
    fi
fi

#
# MD5 checksum of output file (not working on CASTOR..)
# To do : Find a solution to improve check of the integrity.. ?
#
#if [ $FS == "DISK" ]; then
#	echo -e "${GREEN}--> Creating the checksum file in ESCALADE_LOGDIR0, please wait..${NC}"
#	find $ESCALADE_OUTPUT0 -type f -exec md5sum {} \; > $ESCALADE_PRODUCTION/md5sum.txt
#else
#        echo -e "${ORANGE}--> Outgoing data is not on disk.. please implement the xrd checksum..${NC}"
#fi

#
# Create an archive to store the original logfiles (can be reuse to re-check data)
#
cd $ESCALADE_PRODUCTION

esc_echo "${GREEN}--> Creating an archive to store the original logfiles and inputfiles processed (in $ESCALADE_LOGDIR0/escalade-logfiles.tar.gz)${NC}"
[ -z "$ESCALADE_DEBUG" ] && mkdir -p ./input ./logs ./.tmp
[ -z "$ESCALADE_DEBUG" ] && tar -czf $ESCALADE_PRODUCTION/escalade-logfiles.tar.gz ./input ./logs ./.tmp $ESCALADE_KEY
[ $? -eq 1 ] && exit 1

[ ! -z "$ESCALADE_DEBUG" ] && echo -e "\t   ${ORANGE}(NB:Nothing is archived.. debug mode is enabled!!)${NC}"
cd - > /dev/null 2> /dev/null

if [ ! "$ESCALADE_OUTPUT0" -ef "$ESCALADE_PRODUCTION" ]; then
    
    #
    # If storage system used: copy the archive and the userfiles on tape, to keep safe all information about theproduction
    #
    esc_echo "${GREEN}--> Backup logfiles archive from ${NC}$ESCALADE_LOGDIR0/escalade-logfiles.tar.gz ${GREEN}to${NC} $ESCALADE_OUTPUT0/escalade-logfiles.tar.gz"
    
    if [ ! -z "$(echo $FS_WRITE | grep -E ^xrdcp)" ]; then
        
        echo ${FS_WRITE} $ESCALADE_PRODUCTION/escalade-logfiles.tar.gz ${FS_ADDR}${ESCALADE_OUTPUT0#$FS_ADDR}/escalade-logfiles.tar.gz > /dev/null
        [ -z "$ESCALADE_DEBUG" ] && ${FS_WRITE} $ESCALADE_PRODUCTION/escalade-logfiles.tar.gz ${FS_ADDR}${ESCALADE_OUTPUT0#FS_ADDR}/escalade-logfiles.tar.gz > /dev/null
        [ ! -z "$ESCALADE_DEBUG" ] && echo -e "\t   ${ORANGE}(NB:Nothing is archived.. debug mode is enabled!!)${NC}"
    else
        echo ${FS_WRITE} $ESCALADE_PRODUCTION/escalade-logfiles.tar.gz ${FS_ADDR}${ESCALADE_OUTPUT0#$FS_ADDR}/escalade-logfiles.tar.gz > /dev/null
        [ -z "$ESCALADE_DEBUG" ] && ${FS_WRITE} $ESCALADE_PRODUCTION/escalade-logfiles.tar.gz ${ESCALADE_OUTPUT0#$FS_ADDR}/escalade-logfiles.tar.gz > /dev/null
        [ ! -z "$ESCALADE_DEBUG" ] && echo -e "\t   ${ORANGE}(NB:Nothing is archived.. debug mode is enabled!!)${NC}"
    fi
    [ $? -eq 1 ] && exit 1
fi

#
# Finalize the checks and remove the temporary directory used to analyze logfiles and keep the originals safe
#
if [[ ! -z "$ESCALADE_DEBUG" || ! -z "$(${FS_LS} ${ESCALADE_OUTPUT0#$FS_ADDR}/escalade-logfiles.tar.gz)" ]]; then
    
    echo -e "${GREEN}--> Escalade archive correctly created.. Congratulations !${NC}"
    echo -e "\t${GREEN}- Archive stored here : ${NC}$ESCALADE_OUTPUT0/escalade-logfiles.tar.gz"
    echo -e "\t${GREEN}- Production stored here : ${NC}$ESCALADE_OUTPUT0"
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "\t   ${ORANGE}(NB:Nothing is archived.. debug mode is enabled!!)${NC}"
    
    [ -z "$ESCALADE_DEBUG" ] && echo -e "${GREEN}==> You can now delete the production directory yourself!${NC}"
    [ -z "$ESCALADE_DEBUG" ] && echo "$ESCALADE_OUTPUT0" > $ESCALADE_PRODUCTION/archive-report.log
    exit 0
else
    echo -e "${RED}--> No archive detected.. A problem has most probably occured !${NC}"
    echo -e "${RED}    Please check the output directory : $ESCALADE_OUTPUT0${NC}"
    echo -e "${RED}    The associated production directory is still here : $ESCALADE_PRODUCTION${NC}"
    exit 1
fi
