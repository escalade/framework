#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_TESTSUPPR "<suppression file> <log file>" $# 2
[ $? -eq 1 ] && return 1

SUPPRFILE=$1
LOGFILE=$2

#
# Pipe error messages from STDERR to STDERR
#
rm -f "/tmp/testsuppr-*"
function suppress
{
    local FILELIST=$1
    local LOGFILE_ORIGINAL=$2
    
    local LOGFILE=""
    for STDERR in $LOGFILE_ORIGINAL; do
        
        STDFILE_TMP=/tmp/testsuppr-$(basename $STDERR)-$RANDOM$RANDOM
        echo "Copy to \"$STDFILE_TMP\""
        cp $STDERR $STDFILE_TMP
        
        LOGFILE="$LOGFILE $STDFILE_TMP"
    done
    
    for FILE in $FILELIST; do
        
        local SUPPRFILE=$FILE
        
        if [ ! -f "$SUPPRFILE" ]; then
            
            esc_echo "${ORANGE}--> Suppression file not found.. skip${NC} ($SUPPRFILE)" >&2
            continue;
        fi
        
        esc_echo "${GREEN}--> Suppress harmless errors using the following file..${NC} $SUPPRFILE"
        local TOTAL_NCODES=0
        while read CODE
        do
            CODE="$(echo "$CODE" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$CODE" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            
            TOTAL_NCODES=$(($TOTAL_NCODES+1))
            
        done < "$SUPPRFILE"
        
        [ $TOTAL_NCODES -eq 0 ] && continue;
        
        local ICODE=1
        while read CODE
        do
            CODE="$(echo "$CODE" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$CODE" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            
            echo -e "\t- Removing \"$CODE\" (sed,regex) warning messages.. ($ICODE out $TOTAL_NCODES)"
            local SEDCODE="-e \"$CODE\" $SEDCODE 2> /dev/null"
            ICODE=$(($ICODE+1))
            
            # In order to process quicker if there is no CPU usage restriction from the cluster team
            ESCALADE_NCODES=20
            if [[ $(( $ICODE % $((ESCALADE_NCODES+1)) )) -eq 0 || $ICODE == $((TOTAL_NCODES+1)) ]]; then
                
                IRUN=1
                NRUN=$(echo $LOGFILE | wc -w)
                for STDERR in $LOGFILE; do
                    
                    tput el 2> /dev/null
                    echo -ne "\t  Processing file $(basename $STDERR).. ($IRUN/$NRUN)\r"
                    IRUN=$(($IRUN+1))
                    
                    eval "sed $SEDCODE -i \"$STDERR\" 2> /dev/null"
                    if [ ! $? -eq 0 ]; then
                        echo -e "${ORANGE}\t Error returned.. Something might be wrong with the SED command you wrote${NC}"
                        exit 1
                    fi
                done
                
                SEDCODE=""
            fi
            
        done < "$SUPPRFILE"
        
        if [ ! -z "$SEDCODE" ]; then
            
            IRUN=1
            NRUN=$(echo $LOGFILE | wc -w)
            for STDERR in $LOGFILE; do
                
                tput el 2> /dev/null
                echo -ne "\t  Processing file $(basename $STDERR).. ($IRUN/$NRUN)\r"
                IRUN=$(($IRUN+1))
                
                eval "sed $SEDCODE -i \"$STDERR\" 2> /dev/null"
                if [ ! $? -eq 0 ]; then
                    
                    echo -e "${ORANGE}\t Error returned.. Something might be wrong with the SED command you wrote${NC}"
                    exit 1
                fi
            done
            
            SEDCODE=""
        fi
    done
    
    for STDERR in $LOGFILE; do
        
        echo -e "${RED}--> Summary error report for \"$STDERR\"${NC}"
        echo -e "${RED}    There is/are $(cat "$STDERR" | wc -l) error line(s)${NC}"
        if [ -z "$STDERR" ]; then
            
            echo -e "   ${ORANGE}No error detected inside !${NC}"
        else
            cat $STDERR
        fi
        
    done
}

echo -e "${GREEN}--> Start analyzing.. \"$LOGFILE\"${NC}"
suppress "$SUPPRFILE" "$LOGFILE"
echo -e "${GREEN}--> Finished.. :-)${NC}"
exit 0
