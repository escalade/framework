#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_SPLITDATA "<inputfile/inputdir> <outputdir> <nb volumes per file>" $# 3
[ $? -eq 1 ] && exit 1

#
# Global variables
#
INPUT0=$1
OUTPUT0=$2
NLINE=$3
set --

#
# Environment checks
#
# Check if there is a auth key, that mean that jobs have already been produced..
source $ESCALADE_CHECKKEY $(dirname $INPUT0) $(dirname $OUTPUT0) > /dev/null 2> /dev/null
if [ ! $? -eq 2 ]; then
    echo -e "${RED}--> Key already detected, you cannot change anything about the input files at this moment..${NC}"
    echo -e "${RED}    $(dirname $INPUT0)${NC}"
    exit 1
else
    echo -e "${ORANGE}--> No key found, that's ok. You can still modify the input files at this moment..${NC}"
fi

# Some other checks
if [ -z "$(ls -A $INPUT0 2> /dev/null)" ]; then
    echo -e "${RED}--> Input directory not found..${NC} ($INPUT0)"
    exit 1
fi
if [ $NLINE -eq 0 ]; then
    echo -e "${GREEN}--> Nothing to do, NLINE < 1${NC}"
    exit 0
fi

#
# Splitting method
#

if [ "$INPUT0" == "$OUTPUT0" ]; then
    
    echo -e "${ORANGE} Same output and input directory.. Input will be removed after the procedure..${NC}"
fi

WORKDIR=$(mktemp -d)

let SLOT=0
mkdir -p $OUTPUT0 ${WORKDIR}/slot-$SLOT/

echo -e "${GREEN}--> Splitting input files here: $INPUT0${NC}"
for TXTFILE in $(find $INPUT0 -type f -name '*.txt' 2> /dev/null); do
    
    tput el 2> /dev/null
    echo -ne "\t- Splitting file ID $(basename $TXTFILE .txt)\r"
    
    RUN=$(basename $TXTFILE .txt)
    let i=1
    let j=1
    
    while read LINE; do
        
        if [ $(ls ${WORKDIR}/slot-${SLOT} | wc -l) -gt $ESCALADE_SLOTSIZE ]; then
            
            SLOT=$((SLOT+1))
            mkdir -p ${WORKDIR}/slot-$SLOT/
            
            tput el 2> /dev/null
            echo -ne "[INFO] New slot created.. (slot:$SLOT)\r"
        fi
        
        echo "$LINE" >> ${WORKDIR}/slot-$SLOT/${RUN}_${i}.txt
        j=$(($j%$NLINE))
        
        [ $j -eq 0 ] && let i=i+1
        let j=j+1
        
    done < $TXTFILE
    
done

# Move splitted files
[ "$INPUT0" == "$OUTPUT0" ] && rm -rf $INPUT0/*
mv ${WORKDIR}/* $OUTPUT0/

# Remove empty files and remove old txtfiles
find $OUTPUT0 -size 0 -exec rm {} \;

rm -r "${WORKDIR}"
tput el 2> /dev/null 2> /dev/null
exit 0
