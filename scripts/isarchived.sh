#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_ISARCHIVED "<outputdir>" $# 1
[ $? -eq 1 ] && exit 1

DIR=$1

source $ESCALADE_SETFS $DIR > /dev/null
[ $? -eq 1 ] && exit 1

if [ "$ESCALADE_OUTPUT0" == "$DIR" ]; then
    
    source $ESCALADE_LSDIR "0001" "0000" "0000" > /dev/null
    [ $? -eq 1 ] && exit 1
    
    LS_DIR="$LS_OUTPUT0"
    
else
    
    # Get list of output files
    esc_echo "${GREEN}--> Looking for data stored on $FS, please wait.. ($DIR)${NC}"
    LS_DIR=$(mktemp --suffix -ls-logdir0)
    $ESCALADE_GETLIST $DIR 0 2> /dev/null > $LS_DIR
fi

#
# Check if already archived
#
if [ ! -z "$(cat "$LS_DIR" | grep escalade-logfiles.tar.gz 2> /dev/null)" ]; then
    
    esc_echo "--> ${GREENBKG}Logfiles already archived here : $DIR/escalade-logfiles.tar.gz${NC}"
    exit 1
fi

exit 0
