#! /bin/bash

#
# Usage checks
#
[[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]] && SOURCED=1

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

# Global variables
source $ESCALADE_USAGE $0 $ESCALADE_CLEANTEMP "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
$ESCALADE_ISARCHIVED $ESCALADE_OUTPUT0
[ $? -eq 1 ] && exit 1

#
# Check if jobs are still running
#
if [ ! $ESCALADE_IJOBS -eq 0 ]; then
    
    echo -e "${ORANGE}\tYou cannot clean your production temporary directory at this moment..${NC}"
    echo -e "${ORANGE}\tSome jobs ($ESCALADE_IJOBS) are still running ! ${NC}"
    exit 1
fi

#
# Start removing files found
#
if [ -z "$ESCALADE_FORCEACTION" ]; then
    
    echo -e "\t${REDBKG}Please use the option '--force-action' to confirm this deletion..${NC}"
    sleep 2
    exit 1
fi

ESCALADE_KEY=$(ls $ESCALADE_PRODUCTION | grep escalade-key)
if [ ! -z "$(ls $ESCALADE_PRODUCTION | grep escalade-key)" ]; then
    
    echo -e "${ORANGE}--> Escalade-key found.. start to remove the production temporary directory:${NC}"
    echo -e "\t$ESCALADE_PRODUCTION/.tmp/"
    rm -rf "$ESCALADE_PRODUCTION/.tmp/"
else
    
    echo -e "${RED}--> Escalade key not found.. skip $ESCALADE_PRODUCTION directory"
fi
