#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_GLOBALQSTAT "<pid file>" $# 1
[ $? -eq 1 ] && exit 1

#
# Environment checks
#

PIDLIST="$1"
touch $PIDLIST 2> /dev/null

ALL_QSTAT=""
for BATCH in $(cat "$ESCALADE_CLUSTER_DIR/valid_batch"); do
    
    source "$ESCALADE/pilots/batch/$BATCH/setenv.sh"
    [ $? -eq 1 ] && exit 1
    
    BATCH_QSTAT="$(eval "$ESCALADE_QSTAT" 2> /dev/null | grep $USER)"
    [ ! -z "$BATCH_QSTAT" ] && ALL_QSTAT="$BATCH_QSTAT $ALL_QSTAT"
done

for JOBID in $(cat "$PIDLIST" | awk '{print $1}' | sort -V | uniq); do
    
    echo "$ALL_QSTAT" | grep "$JOBID"
done

exit 0
