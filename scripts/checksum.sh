#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

CHECKSUM="$1"
if [ "$CHECKSUM" == "control" ]; then
    source $ESCALADE_USAGE $0 $ESCALADE_CHECKSUM "<control> <input> <input2>" $# 3
    [ $? -eq 1 ] && exit 1
else
    source $ESCALADE_USAGE $0 $ESCALADE_CHECKSUM "<md5|adler32|crc32|control> <input> [<input2> (in case control is used)]" $# 2
    [ $? -eq 1 ] && exit 1
fi

#
# Global variables
#
DATA="$2"
DATA2="$3"
set --

if [ "$CHECKSUM" == "control" ]; then
    
    echo -e "${GREEN}--> Checksum control of the following files:${NC}"
    if [ ! -f "$DATA" ]; then
        
        echo -e "${ORANGE}\t- File \"$DATA\" not found..${NC}"
        exit 1
    fi
    CONTROL_METHOD1=$(cat $DATA | head -n1 | cut -d' ' -f1)
    echo -e "\t- File \"$DATA\" is using a checksum \"$CONTROL_METHOD1\""
    
    if [ ! -f "$DATA2" ]; then
        
        echo -e "${ORANGE}\t- File \"$DATA2\" not found..${NC}"
        exit 1
    fi
    CONTROL_METHOD2=$(cat $DATA2 | head -n1 | cut -d' ' -f1)
    echo -e "\t- File \"$DATA2\" is using a checksum \"$CONTROL_METHOD2\""
    
    if [ $CONTROL_METHOD1 != $CONTROL_METHOD2 ]; then
        
        echo -e "${RED}\t  This is problematic..${NC}" >&2
        exit 1
    fi
    
    echo -e "${GREEN}--> Verification of the integrity ongoing.. please wait..${NC}"
    echo -e "\t- File \"$DATA\" preparation ongoing.."
    LS_DATA1="$(cat $DATA | tail -n+2 | sort -V | sed '/^$/d')"
    LS_CODE1="$(cat "$LS_DATA1" | awk '{print $1}')"
    LS_FULLFILE1="$(cat "$LS_DATA1" | awk '{print $2}')"
    LS_FILE1="$(cat "$LS_FULLFILE1" | xargs -L1 basename)"
    LS_DATA1_SORTED=$(paste <(cat "$LS_CODE1") <(cat "$LS_FILE1"))
    
    echo -e "\t- File \"$DATA\", preparation ongoing.."
    LS_DATA2="$(cat $DATA2 | tail -n+2 | sort -V | sed '/^$/d')"
    LS_CODE2="$(cat "$LS_DATA2" | awk '{print $1}')"
    LS_FULLFILE2="$(cat "$LS_DATA2" | awk '{print $2}')"
    LS_FILE2="$(cat "$LS_FULLFILE2" | xargs -L1 basename)"
    LS_DATA2_SORTED=$(paste <(cat "$LS_CODE2") <(cat "$LS_FILE2"))
    
    echo -e "\t- Comparison ongoing.."
    REPORT="$(diff -y <(cat "$LS_DATA1_SORTED") <(cat "$LS_DATA2_SORTED") | cat -n)"
    NFILES="$(echo "$REPORT" | wc -l)"
    DIFF="$(echo "$REPORT" | grep '|' | awk '{print $1}')"
    MISSING1="$(echo "$REPORT" | grep '>' | awk '{print $1}')"
    MISSING2="$(echo "$REPORT" | grep '<' | awk '{print $1}')"
    NDIFF=$(echo "$(echo "$MISSING1" | wc -l)+$(echo "$MISSING2" | wc -l)+$(echo "$DIFF" | wc -l)" | bc)
    
    [ -z "$DIFF" ] && NDIFF=$(($NDIFF-1))
    [ -z "$MISSING1" ] && NDIFF=$(($NDIFF-1))
    [ -z "$MISSING2" ] && NDIFF=$(($NDIFF-1))
    
    OK=$(echo "$REPORT" | grep -v "|" | grep -v '<' | grep -v '>')
    NOK=$(echo "$OK" | wc -l)
    
    echo -e "${GREEN}--> Summary report :${NC}"
    echo -e "\tTotal file(s): $NFILES file(s) found"
    echo -e "\tControl passed: $NOK file(s) found"
    echo -e "\tControl failed: $NDIFF file(s) found"
    
    if [ ! $NDIFF -eq 0 ]; then
        
        echo -e "${RED}--> Detailed report for problematic files :${NC}"
        COUNTER=0
        for I in $DIFF; do
            
            COUNTER=$(($COUNTER+1))
            echo "(Issue #$COUNTER)"
            FILE1=$(cat "$LS_FULLFILE1" | head -n $I | tail -1)
            FILE2=$(cat "$LS_FULLFILE2" | head -n $I | tail -1)
            echo -e "\t${DATA} : ${ORANGE}$FILE1${NC}"
            echo -e "\t${DATA2} : ${ORANGE}$FILE2${NC}"
        done
        for I in $MISSING1; do
            
            COUNTER=$(($COUNTER+1))
            echo "(Issue #$COUNTER)"
            FILE2=$(cat "$LS_FULLFILE2" | head -n $I | tail -1)
            echo -e "\t${DATA} : Missing file"
            echo -e "\t${DATA2} : ${ORANGE}$FILE2${NC}"
        done
        for I in $MISSING2; do
            
            COUNTER=$(($COUNTER+1))
            echo "(Issue #$COUNTER)"
            FILE1=$(cat "$LS_FULLFILE1" | head -n $I | tail -1)
            FILE2=$(cat "$LS_FULLFILE2" | head -n $I | tail -1)
            echo -e "\t${DATA} : ${ORANGE}$FILE1${NC}"
            echo -e "\t${DATA2} : Missing file"
        done
    fi
    
    exit 0
fi

#
# Environment checks
#
source $ESCALADE_SETFS $DATA > /dev/null
[ $? -eq 1 ] && exit 1

if [ "$CHECKSUM" == "md5" ]; then
    
    if [[ "$FS" != "DISK" && -z "$(which xrdfs 2> /dev/null)" ]]; then
        
        echo "\"xrdfs\" not found.. Is XROOTD installed and properly configured ?" >&2
        exit 0
    fi
    
    echo "md5                               file"
    if [ "$FS" == "DISK" ]; then
        
        find $($ESCALADE_READLINK $DATA) -type f -exec md5sum {} \;
    else
        
        echo "$FS_ADDR${DATA#$FS_ADDR} is on tape.. \"md5\" is not yet implemented on tape"
        exit 1
        #LS_DIR=$($ESCALADE_GETLIST $FS_ADDR${DATA#$FS_ADDR})
        #for FILE in $LS_DIR; do
        # put the command here for XROOTD protocol..
        #done
    fi
    exit 0
fi

if [[ "$CHECKSUM" == "crc32" ]]; then
    
    if [[ -z "$(which crc32 2> /dev/null)" ]]; then
        
        echo "\"crc32\" not found.. skip" >&2
        exit 1
    fi
    
    echo "crc32     file"
    if [ "$FS" == "DISK" ]; then
        
        for FILE in $(find $($ESCALADE_READLINK $DATA) -type f); do
            
            echo -ne "$(crc32 $FILE) "
            echo $FILE
        done
    else
        
        echo "$FS_ADDR${DATA#$FS_ADDR} is on tape.. \"crc32\" is not yet implemented on tape"
        exit 1
        
        #LS_DIR=$($ESCALADE_GETLIST $FS_ADDR${DATA#$FS_ADDR})
        #for FILE in $LS_DIR; do
        # put the command here for XROOTD protocol..
        #done
    fi
    exit 0
fi

if [ "$CHECKSUM" == "adler32" ]; then
    
    # check if xrdadler32 is found
    if [ -z "$(which xrdadler32 2> /dev/null)" ]; then
        
        echo "\"xrdadler32\" not found.. Is XROOTD installed and properly configured ?" >&2
        exit 0
    fi
    
    echo "adler32  file"
    if [ "$FS" == "DISK" ]; then
        
        find $($ESCALADE_READLINK $DATA) -type f -exec xrdadler32 {} \;
    else
        LS_DIR=$($ESCALADE_GETLIST $FS_ADDR${DATA#$FS_ADDR} 0)
        for FILE in $LS_DIR; do
            xrdadler32 $FS_ADDR${FILE#$FS_ADDR}
        done
    fi
    exit 0
fi

echo "Unknown checksum test.." >&2
exit 1

