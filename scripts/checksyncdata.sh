#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_CHECKSYNCDATA "" $# 0
[ $? -eq 1 ] && exit 1

#
# Global variables
#

#
# Get list #1
#
source $ESCALADE_SETFS "$ESCALADE_DATA"
[ $? -eq 1 ] && exit 1

if [[ -f "$ESCALADE_DATA" || ! $(echo $ESCALADE_DATA | wc -w) -eq 1 ]]; then
    
    echo -e "--> Getting data from file $ESCALADE_DATA"
    echo -e "    using \"$ESCALADE_DATAPATTERN\"; Please wait.."
    LIST1=$(cat $ESCALADE_DATA | grep -E "$ESCALADE_DATAPATTERN" | sort -V)
else
    echo -e "--> Getting data from $ESCALADE_DATA"
    echo -e "    using \"$ESCALADE_DATAPATTERN\"; Please wait.."
    LIST1=$(eval $FS_LS $ESCALADE_DATA/ | grep -E "$ESCALADE_DATAPATTERN" | awk -v INPUT0="$ESCALADE_DATA/" '{ if($9!="") print INPUT0$9 }' | sort -V)
fi

#
# Get list #2
#
if [[ -f $ESCALADE_INPUT0 || ! $(echo $ESCALADE_INPUT0 | wc -w) -eq 1 ]]; then
    
    echo -e "--> Getting data from file $ESCALADE_INPUT0"
    echo -e "    using \"$ESCALADE_DATAPATTERN\"; Please wait.."
    LIST2=$(cat $ESCALADE_INPUT0 | grep -E "$ESCALADE_DATAPATTERN" | sort -V)
else
    echo -e "--> Getting data from $ESCALADE_INPUT0"
    echo -e "    using \"$ESCALADE_DATAPATTERN\"; Please wait.."
    LIST2=$(cat $(find $ESCALADE_INPUT0 -type f) | grep -E "$ESCALADE_DATAPATTERN" | sort -V)
fi

#
# Looking for matching
#
echo -e "${GREEN}--> Looking for mismatching between $ESCALADE_DATA${NC}"
echo -e "${GREEN}    and $ESCALADE_INPUT0${NC}"
DIFF=$(diff  <(echo "$LIST1" ) <(echo "$LIST2"))
DIFF12=$(echo "$DIFF" | grep -E "<" | awk '{print $2}')
DIFF21=$(echo "$DIFF" | grep -E ">" | awk '{print $2}')

#
# Cleaning temporary files and empty files
#
echo -e "${GREEN}--> Total: $(echo "$LIST1" | wc -l) line found from $ESCALADE_DATA${NC}"
echo -e "${GREEN}--> Total: $(echo "$LIST2" | wc -l) line found from $ESCALADE_INPUT0${NC}"

#echo "$DIFF" | grep -E "<|>" | sort -V | uniq | wc -l
if [ -z "${DIFF12}${DIFF21}" ]; then
    
    echo -e "${GREEN}--> No difference found.. OK${NC}"
else
    echo -e "${RED}--> Directories are not synchronized..${NC}"
    
    echo -e "${RED}--> $(echo "$DIFF12" | wc -l) line(s) is/are in $ESCALADE_DATA${NC}"
    echo -e "${RED}    and not in $(dirname $ESCALADE_INPUT0))${NC}"
    
    echo -e "${RED}--> $(echo "$DIFF21" | wc -l) line(s) is/are in $ESCALADE_INPUT0${NC}"
    echo -e "${RED}    and not in $(dirname $ESCALADE_DATA))${NC}"
    echo -e "${RED}--> Write the missing files here : $ESCALADE_TMPDIR/syncdata.log${NC}"
    
    rm "$ESCALADE_TMPDIR/syncdata.log" 2> /dev/null
    echo "$DIFF" > $ESCALADE_TMPDIR/syncdata.log
fi

exit 0
