#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_GLOBAL_JOBTASK "" $# 0
[ $? -eq 1 ] && exit 1

#
# Pre-action
#

$ESCALADE_PREPJOB ## CHECK ALL VARIABLES REQUIRED FOR JOBTASK ARE DEFINED
[ $? -eq 1 ] && exit 1

mkdir -p $ESCALADE_JOBWORKDIR
source $ESCALADE_JOBINFO "$ESCALADE_JOBFILE" > /dev/null

if [ ! -z "$ESCALADE_BATCH_UNIQID" ]; then
    export ESCALADE_BATCH_OUT="$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}.out"
    export ESCALADE_BATCH_ERR="$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}.err"
    
    if [ ! -z "$ESCALADE_BATCH_SID" ]; then
        export ESCALADE_BATCH_SUBOUT="$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}-$ESCALADE_BATCH_SID.out"
        export ESCALADE_BATCH_SUBERR="$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}-$ESCALADE_BATCH_SID.err"
    fi
fi

[ ! -z "$ESCALADE_BATCH_OUT" ] && echo "[ESCALADE] Include $(basename $ESCALADE_BATCH_OUT)" >> $ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stdout
[ ! -z "$ESCALADE_BATCH_ERR" ] && echo "[ESCALADE] Include $(basename $ESCALADE_BATCH_ERR)" >> $ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stderr
[ ! -z "$ESCALADE_BATCH_SUBOUT" ] && echo "[ESCALADE] Include $(basename $ESCALADE_BATCH_SUBOUT)" >> $ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stdout
[ ! -z "$ESCALADE_BATCH_SUBERR" ] && echo "[ESCALADE] Include $(basename $ESCALADE_BATCH_SUBERR)" >> $ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stderr

echo "--> Job task working directory: $ESCALADE_JOBWORKDIR"
cd $ESCALADE_JOBWORKDIR

echo "----- Processing $ESCALADE_JOBFILE : $(date)"

function backup_logs
{
    # Backup files
    echo "--> Backup standard files here : $ESCALADE_LOGDIR/$ESCALADE_JOBID"
    mv $ESCALADE_JOBWORKDIR/escalade-$(basename $ESCALADE_CONFIG .xml).xml $ESCALADE_LOGDIR/$ESCALADE_JOBID/ # XML config file
    [ -f "$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stdout" ] && mv $ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stdout $ESCALADE_LOGDIR/$ESCALADE_JOBID/
    [ -f "$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stderr" ] && mv $ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.stderr $ESCALADE_LOGDIR/$ESCALADE_JOBID/
    [ -f "$ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.log" ] && mv $ESCALADE_JOBWORKDIR/${ESCALADE_JOBID}.log $ESCALADE_LOGDIR/$ESCALADE_JOBID/
}

# Backup files
echo "--> Keep input files safe here : $ESCALADE_LOGDIR/$ESCALADE_JOBID"
cp $ESCALADE_INPUT/$ESCALADE_JOBID.txt $ESCALADE_JOBWORKDIR/input.txt # Input file
cp $ESCALADE_CONFIG $ESCALADE_JOBWORKDIR/escalade-$(basename $ESCALADE_CONFIG .xml).xml # XML config file (try to remove the .xml, in case someone na$

mkdir -p $ESCALADE_LOGDIR/$ESCALADE_JOBID
mv $ESCALADE_JOBWORKDIR/input.txt $ESCALADE_LOGDIR/$ESCALADE_JOBID/ # Input file

$ESCALADE_PREPJOB #Check if job environment is ready..
if [ $? -eq 1 ]; then
    backup_logs
    exit 1
fi

#
# Call the job task
#
$ESCALADE_STAGEDATA $ESCALADE_JOBFILE '*' 0
eval $ESCALADE_SOFTWARE_DIR/jobtask.sh

#
# Post action (in case file was on tape, and were transferred.. we need now to delete it)
#
[[ ! -z "$ESCALADE_TARPATH" && ! -z "$IS_TARGZ" ]] && rm -f "$ESCALADE_JOBFILE"

#
# Check file integrity
#
echo -e "${GREEN}--> Computing full checksum set on $ESCALADE_JOBWORKDIR..${NC}"

echo -e "\t- \"md5\" checksum on going.. output stored here : $ESCALADE_JOBWORKDIR/checksum.md5"
$ESCALADE_CHECKSUM md5 $ESCALADE_JOBWORKDIR > $ESCALADE_JOBWORKDIR/checksum.md5
[ ! -s $ESCALADE_JOBWORKDIR/checksum.md5 ] && cat $ESCALADE_JOBWORKDIR/checksum.md5 || rm "$ESCALADE_JOBWORKDIR/checksum.md5"

echo -e "\t- \"adler32\" checksum on going.. output stored here : $ESCALADE_JOBWORKDIR/checksum.adler32"
$ESCALADE_CHECKSUM adler32 $ESCALADE_JOBWORKDIR > $ESCALADE_JOBWORKDIR/checksum.adler32
[ ! -s $ESCALADE_JOBWORKDIR/checksum.adler32 ] && cat $ESCALADE_JOBWORKDIR/checksum.adler32 || rm "$ESCALADE_JOBWORKDIR/checksum.md5"


echo -e "\t- \"crc32\" checksum on going.. output stored here : $ESCALADE_JOBWORKDIR/checksum.crc32"
$ESCALADE_CHECKSUM crc32 $ESCALADE_JOBWORKDIR > $ESCALADE_JOBWORKDIR/checksum.crc32
[ ! -s $ESCALADE_JOBWORKDIR/checksum.crc32 ] && cat $ESCALADE_JOBWORKDIR/checksum.crc32 || rm "$ESCALADE_JOBWORKDIR/checksum.md5"

# Start copying output files
$ESCALADE_COPYFROMTO $ESCALADE_JOBWORKDIR $ESCALADE_OUTPUT/$ESCALADE_JOBID $ESCALADE_JOBID
if [ $? -eq 0 ]; then
    
    backup_logs
    
    cd $ESCALADE_JOBWORKDIR0
    rm -r "$ESCALADE_JOBWORKDIR"
    exit 0
fi

backup_logs
exit 1
