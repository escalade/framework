#! /bin/bash

#
# Usage checks
#

if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_STANDALONE_DATABASE "" $# 0
[ $? -eq 1 ] && exit 1

if [ -z "$MYSQL" ]; then
    
    echo "MYSQL variable not defined.."
    exit 1
fi

if [ ! -z "$(ps aux | grep -v grep | grep -v killall | grep -v mysqldump | grep 'mysqld ')" ]; then
    echo "A database seems to be already running on this computer.."
else
    rm -rf /tmp/*${USER}-standalone-mysql*
    
    export MYSQL_BASEDIR=$(mktemp -d --suffix -${USER}-standalone-mysql)
    if [ ! -z "$(echo $MYSQL | grep -E .*\.tar\.gz$)" ]; then
        echo "--> Untar data from $MYSQL into $MYSQL_BASEDIR"
        tar -xzf $MYSQL -C $MYSQL_BASEDIR
        elif [ ! -z "$(echo $MYSQL | grep -E .*\.tar$)" ]; then
        echo "--> Untar data from $MYSQL into $MYSQL_BASEDIR"
        tar -xf $MYSQL -C $MYSQL_BASEDIR
    else
        echo "--> Use data from $MYSQL"
        export MYSQL_BASEDIR=$MYSQL
    fi
    
    export MYSQL=$MYSQL_BASEDIR
    
    echo "--> Starting database.."
    MYSQL_LOGFILE=mysql-$RANDOM.log
    $MYSQL_BASEDIR/bin/mysqld --defaults-file=$MYSQL_BASEDIR/data/auto.cnf \
    --basedir=$MYSQL_BASEDIR --datadir=$MYSQL_BASEDIR/data \
    --socket $MYSQL_BASEDIR/mysql.sock --pid-file=$MYSQL_BASEDIR/mysql.pid \
    --table_open_cache=431 --open_files_limit=1024 --server-id=2 >> $MYSQL_LOGFILE 2>> $MYSQL_LOGFILE &
    
    [ $? -eq 1 ] && exit 1
fi

if [ ! -z "$(ps aux | grep -v grep | grep -v killall | grep -v mysqldump | grep 'mysqld ')" ]; then
    echo "Database ready. Use \"killall mysqld\" if you want to stop it !"
    echo "MySQL PID \"$(ps aux | grep -v grep | grep -v killall | grep 'mysqld ' | awk '{print $2}')\""
else
    echo "Database seems to not have started for some reason.. (tbc by looking at the log files) Maybe the directory \"$MYSQL_BASEDIR\" is corrupted."
    echo "You should remove it and relaunch the command."
fi

exit 0
