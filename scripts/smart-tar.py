#! /usr/bin/env python

import os
from os.path import join, getsize
from hurry.filesize import size

import argparse
parser = argparse.ArgumentParser(description='Convert a directory into tar file..')
parser.add_argument('size', default="100GB", help='Tar size')
parser.add_argument('name', metavar='Input directory', type=str, help='Input directory')
args = parser.parse_args()

# Convert string to bytes
def s2b(size_str):
    multipliers = {'kilobyte':  1024,'megabyte':  1024 ** 2,'gigabyte':  1024 ** 3,'terabyte':  1024 ** 4,'petabyte':  1024 ** 5,'exabyte':   1024 ** 6,'zetabyte':  1024 ** 7,'yottabyte': 1024 ** 8,'kb': 1024,'mb': 1024**2,'gb': 1024**3,'tb': 1024**4,'pb': 1024**5,'eb': 1024**6,'zb': 1024**7,'yb': 1024**8,}
    for suffix in multipliers:
        size_str = size_str.lower().strip().strip('s')
        if size_str.lower().endswith(suffix):
            return int(float(size_str[0:-len(suffix)]) * multipliers[suffix])
    else:
        if size_str.endswith('b'):
            size_str = size_str[0:-1]
        elif size_str.endswith('byte'):
            size_str = size_str[0:-4]
    return int(size_str)

# Trace back the last file in the tar list..
def lastfile():
    iter0 = 0
    if(not os.path.exists(base + '.0.info')):
       return ["",0]

    while(os.path.exists(base + '.' + str(iter0) + '.info')):
        iter0 += 1

    fname = base + '.' + str(iter0-1) + '.info'
    with open(fname, "r") as f1:
            last_line = f1.readlines()[-1].rstrip('\n')

    return [last_line, iter0]

print(args)

tar_limit = s2b(args.size)
size = 0
filelist = []

mydir = args.name
base = os.path.basename(mydir)
if(len(base) == 0):
    print("Use subdirectories, not \"%s\"", mydir)
    exit(1)

lastfile = lastfile()
fname_last = lastfile[0]
iter = lastfile[1]
mutex = 0
if(fname_last):
    mutex = 1
    print("Please wait.. rollback..")

count = 0
for root, dirs, files in os.walk(mydir, topdown = False):

    if(not mutex and count > 10):
        print(base + '.' + str(iter) + '.info: ' + str(round(100*size/tar_limit,2)) + "%", end='\r', flush=True)
        count = 0

    if(os.path.islink(root)):
        continue

    for name in files:
        fname = join(root, name)

        # Resume..
        if(mutex and fname != fname_last): continue
        elif(mutex):
            print("Resume after: ", fname)
            mutex = 0
            continue

        if(not os.path.islink(fname)):
            size += getsize(fname)
            filelist.append(fname)
            if(size > tar_limit):
                print("\n==> TAR limit called: %s.%d.info > %s" % (base,iter,args.size))
                with open(base + '.' + str(iter) + '.info', 'w') as f:
                    for item in filelist:
                        f.write("%s\n" % item)
                iter += 1
                filelist = []
                size = 0
    count += 1

if(size != 0):
    print("\n==> TAR called: %s.%d.info" % (base,iter))
    with open(base + '.' + str(iter) + '.info', 'w') as f:
        for item in filelist:
            f.write("%s\n" % item)
    iter += 1
else:
    print("\nNo more file to save..")

with open("pcplist.txt", 'w') as f:
    for i in range(iter):
        f.write("tar -cf %s.%d.tar -T %s.%d.info\n" % (base,i,base,i) )

print("\nDONE..")
