#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_JOBINFO "<INPUTFILE>" $# 1
[ $? -eq 1 ] && return 1

INPUTFILE=$1

set --

[ -z "$ESCALADE_PREVIOUS_SLOT" ] && export ESCALADE_PREVIOUS_SLOT=-1
[ ! -z "$ESCALADE_JOBSLOT" ] && export ESCALADE_PREVIOUS_SLOT=$ESCALADE_JOBSLOT || export ESCALADE_PREVIOUS_SLOT=-1

if [ ! -z "$(echo $INPUTFILE | grep -E ^[^/]*$)" ]; then
    
    RUNID=$INPUTFILE
    INPUTFILE=$(find $ESCALADE_INPUT0 -maxdepth 2 -name "$RUNID.txt" -print -quit)
    
    if [ -z "$INPUTFILE" ]; then
        
        echo -e "${ORANGE}[Warning] No information found about run ID #$RUNID..${NC}" >&2
        return 1
    fi
fi

INPUTFILE_JOBINFO=$(echo $INPUTFILE | sed 's,.*/slot-\([0-9]*\)/\([^/]*\).*,\1 \2,g')
export ESCALADE_JOBSLOT=$(echo $INPUTFILE_JOBINFO | awk '{print $1}')
export ESCALADE_JOBID=$(basename $(echo $INPUTFILE_JOBINFO | awk '{print $2}') | cut -d'.' -f1)

if [ -z "$INPUTFILE_JOBINFO" ]; then
    
    echo "[Warning] Unknown type of path for determination of the jobslot and jobid" >&2
    echo "          (INPUTFILE=$INPUTFILE)" >&2
    return 1
fi

[ ! -z "$ESCALADE_INPUT0" ] && export ESCALADE_JOBFILE=$ESCALADE_INPUT0/slot-$ESCALADE_JOBSLOT/$ESCALADE_JOBID.txt

if [ ! -z "$ESCALADE_JOBSLOT" ]; then
    
    [ ! -z "$ESCALADE_LOGDIR0" ] && export ESCALADE_LOGDIR=$ESCALADE_LOGDIR0/slot-$ESCALADE_JOBSLOT
    [ ! -z "$ESCALADE_INPUT0" ] && export ESCALADE_INPUT=$ESCALADE_INPUT0/slot-$ESCALADE_JOBSLOT
    [ ! -z "$ESCALADE_OUTPUT0" ] && export ESCALADE_OUTPUT=$ESCALADE_OUTPUT0/slot-$ESCALADE_JOBSLOT
fi

return 0
