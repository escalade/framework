#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_ROUND "<math operation>" $# 1
[ $? -eq 1 ] && exit 1

INPUT=$1

set --

INT=$(echo "$INPUT" | bc)
FLOAT=$(echo "$INPUT" | bc -l)


IS_NULL=$(echo "$FLOAT == 0" | bc)
IS_INT=$(echo "($FLOAT-$INT) == 0" | bc)
if [ "$IS_NULL" == "1" ]; then
    
    echo -ne 0
    exit 0
    
    elif [ "$IS_INT" == "1" ]; then
    
    echo -ne $INT
    exit 0
fi

echo -ne $((INT+1))
exit 0
