#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

if [[ ! $# -eq 2 ]]; then
    
    source $ESCALADE_USAGE $0 $ESCALADE_GETLIST "<input> [<depth>]" $# 1
    [ $? -eq 1 ] && exit 1
fi

SCRIPT_GETLIST=$0
INPUT=$1

[ $# -ge 2 ] && DEPTH=$2 || DEPTH=-1

set --

#
# Read content of directory
#

source $ESCALADE_SETFS $INPUT > /dev/null
[ $? -eq 1 ] && exit 1

if [ "$FS" == "DISK" ]; then
    
    if [[ ! -d "$INPUT" && ! -f "$INPUT" ]]; then
        
        echo -e "${ORANGE}\t$INPUT not found${NC}" >&2
        exit 1
    fi
fi

LS_STATUS=0
LS="$(eval $FS_LS ${INPUT#$FS_ADDR} 2> /dev/null)"
if [ -z "$LS" ]; then
    
    LS_STATUS=1
    LS="$(eval $FS_LS ${INPUT#$FS_ADDR} 2> /dev/null)"
    if [ -z "$LS" ]; then
        
        LS_STATUS=2
        LS_REGEXDIR="$(eval $FS_LS $(dirname ${INPUT#$FS_ADDR}) 2> /dev/null)"
        LS="$(echo "$LS_REGEXDIR" | grep "$(basename ${INPUT#$FS_ADDR} | sed 's/*/.*/g')")"
    fi
fi

[ "$LS_STATUS" -eq 0 ] && DIRPATH="$(dirname $INPUT)/$(basename ${INPUT})"
[ "$LS_STATUS" -eq 1 ] && DIRPATH="$(dirname $INPUT)/$(basename ${INPUT})"
[ "$LS_STATUS" -eq 2 ] && DIRPATH="$(dirname $INPUT)"

if [ ! -z "$(echo \"$LS\" | grep -i 'connection closed')" ]; then
    
    echo "$LS" >&2 | grep -i 'connection closed'
    exit 1
fi

if [ $FS == "DISK" ]; then
    
    if [ $DEPTH != -1 ]; then
        find $INPUT -maxdepth $(($DEPTH))
        find $INPUT -mindepth $(($DEPTH+1)) -maxdepth $(($DEPTH+1))
    else
        find $INPUT
    fi
    unset LS
    exit 0
    
else
    FILELIST=$(echo "$LS" | awk '{if (NF>=4 && $(NF-4)!="0" && $NF!="." && $NF!="..") print $NF}')
    DIRLIST=$(echo "$LS" | awk '{if (NF>=4 && $(NF-4)=="0" && $NF!="." && $NF!="..") print $NF}')
    unset LS
fi

if [ $DEPTH -eq 0 ]; then
    
    for FILE in $FILELIST; do
        echo "$FS_ADDR${DIRPATH#$FS_ADDR}/$(basename $FILE)"
    done
    for DIR in $DIRLIST; do
        echo "$FS_ADDR${DIRPATH#$FS_ADDR}/$(basename $DIR)"
    done
    
else
    
    for FILE in $FILELIST; do
        echo "$FS_ADDR${DIRPATH#$FS_ADDR}/$(basename $FILE)"
    done
    for DIR in $DIRLIST; do
        $SCRIPT_GETLIST $FS_ADDR${DIRPATH#$FS_ADDR}/$(basename $DIR)/ $((DEPTH-1))
    done
fi

exit 0
