#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_SPLITPROD "<inputdir> <outputdir> <nslot>" $# 3
[ $? -eq 1 ] && exit 1

#
# Global variables
#

INPUT0=$(eval "$ESCALADE_READLINK $1")
OUTPUT0=$(eval "$ESCALADE_READLINK $2")
MAXSLOT=$3

set --

if [ ! -s $INPUT0 ]; then
    
    echo -e "${RED}--> $INPUT0 not found..${NC}" >&2
    exit 0
fi

#
# Splitting method
#
WORKDIR=$OUTPUT0
echo -e "${GREEN}--> Temporary working directory used: $WORKDIR${NC}"
mkdir -p $WORKDIR

if [[ ! -d "$INPUT0/input" || ! -f "$INPUT0/logs/.pid" ]]; then
    echo -e "${ORANGE}--> This directory seems to not be an escalade production directory${NC}" >&2
    exit 1
fi
if [[ -f "$INPUT0/escalade-key*" ]]; then
    echo -e "${ORANGE}--> This directory seems to have an existing escalade-key.. Operation not permitted..${NC}" >&2
    exit 1
fi

echo -e "\tLooking for input file in \"${INPUT0}/input\"${NC}"
INPUTSLOT=$(ls $INPUT0/input)
NSLOT=$(echo "$INPUTSLOT" | wc -w)
NSUBSET=$($ESCALADE_ROUND "$NSLOT/$MAXSLOT")
if [ $NSUBSET -le 1 ]; then
    echo -e "\t$NSLOT slot(s) to shrink into $NSUBSET subset(s)\"${NC}"
    exit 1
fi

echo -e "\t$NSLOT slot(s) to shrink into $NSUBSET subset(s)\""
ISLOT=0
OSLOT=0
SUBSET=0

mkdir -p $WORKDIR/subset-$SUBSET/input/
echo -ne "\tNew subset #$SUBSET has been created: $ISLOT/$NSLOT slots processed\r"
for SLOT in $INPUTSLOT; do

    cp -R $INPUT0/input/slot-$ISLOT $WORKDIR/subset-${SUBSET}/input/slot-${OSLOT}
    if [[ $((OSLOT+1)) -ge $MAXSLOT ]]; then
        tput el 2> /dev/null
        SUBSET=$((SUBSET+1))
        echo -ne "\tNew subset #$SUBSET has been created: $((ISLOT+1))/$NSLOT slots processed\r"
        mkdir -p $WORKDIR/subset-${SUBSET}/input
        OSLOT=0
    else
        OSLOT=$((OSLOT+1))
    fi

    ISLOT=$((ISLOT+1))
done

echo -e "${GREEN}Production directory splitted into${NC} \"${OUTPUT0}/subset-X/\""
exit 0
