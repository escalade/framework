#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_PREPSET "<expended set> <depth> <index> <separator>" $# 4
[ $? -eq 1 ] && return 1

# ERASE VARIABLE
EXPENDED_SET=$1
DEPTH=$2
IPROD=$3
SEPARATOR="$4"

export ESCALADE_NSET=$(echo "$EXPENDED_SET" | head -n1 | awk '{print $1}')
[ -z "$ESCALADE_NSET" ] && export ESCALADE_NSET=0

export ESCALADE_NARGS=$(echo "$EXPENDED_SET" | head -n1 | awk '{print $2}')
[ -z "$ESCALADE_NARGS" ] && export ESCALADE_NARGS=0

# First erase subdepth..
i0=$DEPTH
j0=0
while [ ! -z "$(eval echo \${ESCALADE_SET${i0}})" ]; do
    
    unset ESCALADE_SET${i0}
    while [ ! -z "$(eval echo \${ESCALADE_SET${i0}_ARG${j0}})" ]; do
        
        unset ESCALADE_SET${i0}_ARG${j0}
        j0=$((j0+1))
    done
    i0=$((i0+1))
done

i0=$DEPTH
# Select the i-th production line
if [ ! -z "$IPROD" ]; then
    
    PROD_STR=$(echo "$EXPENDED_SET" | head -n$((IPROD+1)) | tail -n1)
    
    export ESCALADE_SET${i0}=$(echo "$PROD_STR" | awk -F"$SEPARATOR" '{print $1}')
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}$(eval echo ESCALADE_SET${i0}=\${ESCALADE_SET${i0}})${NC}"
    
    for j0 in $(seq 0 $((ESCALADE_NARGS-1))); do
        
        col='$'"$((j0+2))"
        export ESCALADE_SET${i0}_ARG${j0}=$(echo "$PROD_STR" | awk -F"$SEPARATOR" "{print $col}")
        [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}$(eval echo ESCALADE_SET${i0}_ARG${j0}=\${ESCALADE_SET${i0}_ARG${j0}})${NC}"
    done
fi

return 0
