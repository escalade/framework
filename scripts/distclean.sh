#! /bin/bash

#
# Usage checks
#
[[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]] && SOURCED=1

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

# Global variables
source $ESCALADE_USAGE $0 $ESCALADE_DISTCLEAN "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
$ESCALADE_ISARCHIVED $ESCALADE_OUTPUT0
[ $? -eq 1 ] && exit 1

#
# Check if jobs are still running
#
if [ ! $ESCALADE_IJOBS -eq 0 ]; then
    
    echo -e "${ORANGE}\tYou cannot distclean your production directory at this moment..${NC}"
    echo -e "${ORANGE}\tSome jobs ($ESCALADE_IJOBS) are still running ! ${NC}"
    exit 1
fi

source $ESCALADE_LSDIR "0001" "0000" "0002"
[ $? -eq 1 ] && exit 1

#
# Start removing files found
#

if [ -z "$ESCALADE_FORCEACTION" ]; then
    
    echo -e "\t${REDBKG}Please use the option '--force-action' to confirm this deletion..${NC}"
    sleep 2
    exit 1
fi

ESCALADE_KEY1=$(cat "$LS_OUTPUT0" | grep escalade-key)
if [ ! -z "$ESCALADE_KEY1" ]; then
    
    echo -e "${ORANGE}--> Escalade-key found.. Start to delete output directory :${NC}"
    
    echo -e "\t$ESCALADE_OUTPUT0"
    for FILE in $(cat "$LS_OUTPUT0" | grep -v escalade-key); do
        eval $FS_RM "${FILE#$FS_ADDR}"
    done
else
    
    echo -e "${RED}--> Escalade key not found.. skip $ESCALADE_OUTPUT0 directory${NC}"
fi

ESCALADE_KEY2=$(ls $ESCALADE_PRODUCTION | grep escalade-key)
if [ ! -z "$ESCALADE_KEY2" ]; then
    
    echo -e "${ORANGE}--> Escalade-key found.. Start to delete production directory:${NC}"
    echo -e "\t$ESCALADE_PRODUCTION/.tmp/"
    rm -rf "$ESCALADE_PRODUCTION/.tmp/"
    
    echo -e "\t$ESCALADE_PRODUCTION/logs/"
    rm -rf "$ESCALADE_PRODUCTION/logs/"
    
    echo -e "\t$ESCALADE_PRODUCTION/README"
    rm -f "$ESCALADE_PRODUCTION/README"
    
    echo -e "\t$ESCALADE_HISTORY"
    rm -f "$ESCALADE_HISTORY"
    
else
    
    echo -e "${RED}--> Escalade key not found.. skip $ESCALADE_PRODUCTION directory"
fi

if [ ! -z "$ESCALADE_KEY1" ]; then
    echo -e "\t$ESCALADE_OUTPUT0/$(basename $ESCALADE_KEY1)"
    eval $FS_RM "${ESCALADE_OUTPUT0#$FS_ADDR}/$(basename $ESCALADE_KEY1)"
fi
if [ ! -z "$ESCALADE_KEY2" ]; then
    echo -e "\t$ESCALADE_PRODUCTION/$(basename $ESCALADE_KEY2)"
    rm -f "$ESCALADE_PRODUCTION/$(basename $ESCALADE_KEY2)"
fi

