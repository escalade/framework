#!/bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0: This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

if [ $# != 5 ]; then
    
    echo -e "${ORANGE}[Usage] $ESCALADE_USAGE <source> <file> <args> <nargs excepted> <nargs>${NC}" >&2 # source = file, if not sourced
    echo -e "${RED}--> The usage script is misused.. Located somewhere in the code that you just started${NC}" >&2
    return 1
fi

#
# Usage checks for the "motherscript"
#
if [ $4 != $5 ]; then
    
    echo -ne "${ORANGE}[Usage] $(eval $ESCALADE_READLINK $1) ${NC}" >&2
    [ "$(eval $ESCALADE_READLINK $1)" != "$(eval $ESCALADE_READLINK $2)" ] && echo -ne ": $(eval $ESCALADE_READLINK $2) ${NC}" >&2
    echo -e "${ORANGE}$3${NC}" >&2
    return 1
fi

function esc_echo
{
    OPT=${@:1:$(($#-1))}
    STR="${@: -1}"
    
    if [[ -z "$ESCALADE_DEBUG" && -z "$ESCALADE_FASTMODE" ]]; then
        
        [ ! -z "$ESCALADE_INTERNAL_DIR" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_INTERNAL_DIR,\\\033\[34m\$ESCALADE_INTERNAL_DIR\\\033\[0m,g")
        [ ! -z "$ESCALADE_SANDBOX0" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_SANDBOX0,\\\033\[34m\$ESCALADE_SANDBOX0\\\033\[0m,g")
        [ ! -z "$ESCALADE_INPUT0" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_INPUT0,\\\033\[34m\$ESCALADE_INPUT0\\\033\[0m,g")
        [ ! -z "$ESCALADE_LOGDIR0" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_LOGDIR0,\\\033\[34m\$ESCALADE_LOGDIR0\\\033\[0m,g")
        [ ! -z "$ESCALADE_TMPDIR" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_TMPDIR,\\\033\[34m\$ESCALADE_TMPDIR\\\033\[0m,g")
        [ ! -z "$ESCALADE_OUTPUT0" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_OUTPUT0,\\\033\[34m\$ESCALADE_OUTPUT0\\\033\[0m,g")
        [ ! -z "$ESCALADE_PRODUCTION" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_PRODUCTION,\\\033\[34m\$ESCALADE_PRODUCTION\\\033\[0m,g")
        [ ! -z "$ESCALADE" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE,\\\033\[34m\$ESCALADE\\\033\[0m,g")
        [ ! -z "$ESCALADE_PWD" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_PWD,\\\033\[34m\$ESCALADE_PWD\\\033\[0m,g")
        [ ! -z "$PWD" ] && STR=$($(which echo) "$STR" | sed "s,$PWD,\\\033\[34m\$PWD\\\033\[0m,g")
    fi
    
    $(which echo) -e $OPT "$STR"
}


# Useful functions
function count {
    
    local XMLELEMENT=$1
    local XMLFILE=$2
    
    [ ! -f $XMLFILE ] && echo 0
    $ESCALADE/bin/xmllint --xpath "count($XMLELEMENT)" $XMLFILE
}

function print {
    
    local XMLELEMENT=$1
    local XMLFILE=$2
    
    [ ! -f $XMLFILE ] && exit 1
    local XMLRESULT=$($ESCALADE/bin/xmllint --xpath "string(${XMLELEMENT})" $XMLFILE 2> /dev/null)
    
    eval echo "$XMLRESULT" 2> /dev/null
    [ $? -eq 1 ] && eval echo '$XMLRESULT'
}

return 0
