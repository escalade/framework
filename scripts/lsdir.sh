#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_LSDIR "<lsmask> <forcemask> <depthmask>" $# 3
[ $? -eq 1 ] && return 1

MASK0=$1
FORCEMASK=$2
DEPTHMASK=$3

set --

if [ ! $(echo $MASK0 | wc -m) -eq 5 ]; then
    
    echo -e "${RED}Wrong LsMask specified${NC}"
    return 1
fi

if [ ! $(echo $FORCEMASK | wc -m) -eq 5 ]; then
    
    echo -e "${RED}Wrong ForceMask specified${NC}"
    return 1
fi

if [ ! $(echo $DEPTHMASK | wc -m) -eq 5 ]; then
    
    echo -e "${RED}Wrong DepthMask specified${NC}"
    return 1
fi

#
# Looking for data + basic checks
#
[[ -z "$LS_INPUT0" || ! -s "$LS_INPUT0" ]] && export ESCALADE_INPUT0_PREVIOUSDEPTHMASK=-1
[ -z "$ESCALADE_INPUT0_PREVIOUSDEPTHMASK" ] && export ESCALADE_INPUT0_PREVIOUSDEPTHMASK=-1
ESCALADE_INPUT0_DEPTHMASK=${DEPTHMASK:0:1}

[ -z "$ESCALADE_INPUT0_DEPTHMASK" ] && ESCALADE_INPUT0_DEPTHMASK=0
if [[ "${FORCEMASK:0:1}" != "0" || ("${MASK0:0:1}" != "0" && $ESCALADE_INPUT0_DEPTHMASK -gt $ESCALADE_INPUT0_PREVIOUSDEPTHMASK) ]]; then
    
    if [ -z "$ESCALADE_INPUT0" ]; then
        
        echo -e "${RED}Input directory variable not defined..${NC}" >&2
        return 1
    fi
    
    DEPTH_STR=""
    for I in $(seq 1 $ESCALADE_INPUT0_DEPTHMASK); do
        DEPTH_STR="/**$DEPTH_STR"
    done
    
    [ "${FORCEMASK:0:1}" != "0" ] && STATUS="Forcing to look" || STATUS="Looking"
    esc_echo -e "${GREEN}--> $STATUS for input in $ESCALADE_INPUT0$DEPTH_STR${NC}"
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}\tprevious depth: $ESCALADE_INPUT0_PREVIOUSDEPTHMASK${NC}; "
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}depth: $ESCALADE_INPUT0_DEPTHMASK${NC}; "
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}force: ${FORCEMASK:0:1}${NC}; "
    
    [ -z "$LS_INPUT0" ] && export LS_INPUT0=$(mktemp --suffix -ls-input0)
    $ESCALADE_GETLIST $ESCALADE_INPUT0 ${DEPTHMASK:0:1} > $LS_INPUT0
    export ESCALADE_INPUT0_PREVIOUSDEPTHMASK=$ESCALADE_INPUT0_DEPTHMASK
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}nfiles: $(cat $LS_INPUT0 | wc -l )${NC}"
fi

















[[ -z "$LS_LOGDIR0" || ! -s "$LS_LOGDIR0" ]] && export ESCALADE_LOGDIR0_PREVIOUSDEPTHMASK=-1
[ -z "$ESCALADE_LOGDIR0_PREVIOUSDEPTHMASK" ] && export ESCALADE_LOGDIR0_PREVIOUSDEPTHMASK=-1
ESCALADE_LOGDIR0_DEPTHMASK=${DEPTHMASK:1:1}

[ -z "$ESCALADE_LOGDIR0_DEPTHMASK" ] && ESCALADE_LOGDIR0_DEPTHMASK=0
if [[ "${FORCEMASK:1:1}" != "0" || ("${MASK0:1:1}" != "0" && $ESCALADE_LOGDIR0_DEPTHMASK -gt $ESCALADE_LOGDIR0_PREVIOUSDEPTHMASK) ]]; then
    
    if [ -z "$ESCALADE_LOGDIR0" ]; then
        
        echo -e "${RED}Logdirectory variable not defined..${NC}" >&2
        return 1
    fi
    
    DEPTH_STR=""
    for I in $(seq 1 $ESCALADE_LOGDIR0_DEPTHMASK); do
        DEPTH_STR="/**$DEPTH_STR"
    done
    
    [ "${FORCEMASK:1:1}" != "0" ] && STATUS="Forcing to look" || STATUS="Looking"
    esc_echo -e "${GREEN}--> $STATUS for logfiles in $ESCALADE_LOGDIR0$DEPTH_STR${NC}"
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}\tprevious depth: $ESCALADE_LOGDIR0_PREVIOUSDEPTHMASK${NC}; "
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}depth: $ESCALADE_LOGDIR0_DEPTHMASK${NC}; "
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}force: ${FORCEMASK:1:1}${NC}; "
    
    [ -z "$LS_LOGDIR0" ] && export LS_LOGDIR0=$(mktemp --suffix -ls-logdir0)
    $ESCALADE_GETLIST $ESCALADE_LOGDIR0 ${DEPTHMASK:1:1} > $LS_LOGDIR0
    export ESCALADE_LOGDIR0_PREVIOUSDEPTHMASK=$ESCALADE_LOGDIR0_DEPTHMASK
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}nfiles: $(cat $LS_LOGDIR0 | wc -l )${NC}"
fi














[[ -z "$LS_LOGDIR0_TMP" || ! -s "$LS_LOGDIR0_TMP" ]] && export ESCALADE_LOGDIR0_TMP_PREVIOUSDEPTHMASK=-1
[ -z "$ESCALADE_LOGDIR0_TMP_PREVIOUSDEPTHMASK" ] && export ESCALADE_LOGDIR0_TMP_PREVIOUSDEPTHMASK=-1
ESCALADE_LOGDIR0_TMP_DEPTHMASK=${DEPTHMASK:2:1}

[ -z "$ESCALADE_LOGDIR0_TMP_DEPTHMASK" ] && ESCALADE_LOGDIR0_TMP_DEPTHMASK=0
if [[ "${FORCEMASK:2:1}" != "0" || ("${MASK0:2:1}" != "0" && $ESCALADE_LOGDIR0_TMP_DEPTHMASK -gt $ESCALADE_LOGDIR0_TMP_PREVIOUSDEPTHMASK) ]]; then
    
    if [ -z "$ESCALADE_TMPDIR" ]; then
        
        echo -e "${RED}Temporary logdirectory variable not defined..${NC}" >&2
        return 1
    fi
    
    DEPTH_STR=""
    for I in $(seq 1 $ESCALADE_LOGDIR0_TMP_DEPTHMASK); do
        DEPTH_STR="/**$DEPTH_STR"
    done
    
    [ "${FORCEMASK:2:1}" != "0" ] && STATUS="Forcing to look" || STATUS="Looking"
    esc_echo -e "${GREEN}--> $STATUS for temporary logfiles in $ESCALADE_TMPDIR/logs$DEPTH_STR${NC}"
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}\tprevious depth: $ESCALADE_LOGDIR0_TMP_PREVIOUSDEPTHMASK${NC}; "
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}depth: $ESCALADE_LOGDIR0_TMP_DEPTHMASK${NC}; "
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}force: ${FORCEMASK:2:1}${NC}; "
    
    [ -z "$LS_LOGDIR0_TMP" ] && export LS_LOGDIR0_TMP=$(mktemp --suffix -ls-logdir0-tmp)
    $ESCALADE_GETLIST $ESCALADE_TMPDIR/logs ${DEPTHMASK:2:1} > $LS_LOGDIR0_TMP
    export ESCALADE_LOGDIR0_TMP_PREVIOUSDEPTHMASK=$ESCALADE_LOGDIR0_TMP_DEPTHMASK
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}nfiles: $(cat $LS_LOGDIR0_TMP | wc -l )${NC}"
fi


















source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && return 1

[[ -z "$LS_OUTPUT0" || ! -s "$LS_OUTPUT0" ]] && export ESCALADE_OUTPUT0_PREVIOUSDEPTHMASK=-1
[ -z "$ESCALADE_OUTPUT0_PREVIOUSDEPTHMASK" ] && export ESCALADE_OUTPUT0_PREVIOUSDEPTHMASK=-1
ESCALADE_OUTPUT0_DEPTHMASK=${DEPTHMASK:3:1}

[ -z "$ESCALADE_OUTPUT0_DEPTHMASK" ] && ESCALADE_OUTPUT0_DEPTHMASK=0

if [[ "${FORCEMASK:3:1}" != "0" || ("${MASK0:3:1}" != "0" && $ESCALADE_OUTPUT0_DEPTHMASK -gt $ESCALADE_OUTPUT0_PREVIOUSDEPTHMASK) ]]; then
    
    if [ -z "$ESCALADE_OUTPUT0" ]; then
        
        echo -e "${RED}Output directory variable not defined..${NC}" >&2
        return 1
    fi
    
    DEPTH_STR=""
    for I in $(seq 1 $ESCALADE_OUTPUT0_DEPTHMASK); do
        DEPTH_STR="/**$DEPTH_STR"
    done
    
    [ "${FORCEMASK:3:1}" != "0" ] && STATUS="Forcing to look" || STATUS="Looking"
    esc_echo -e "${GREEN}--> $STATUS for data in $ESCALADE_OUTPUT0$DEPTH_STR${NC} stored on $FS, please wait.."
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}\tprevious depth: $ESCALADE_OUTPUT0_PREVIOUSDEPTHMASK${NC}; "
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}depth: $ESCALADE_OUTPUT0_DEPTHMASK${NC}; "
    [ ! -z "$ESCALADE_DEBUG" ] && echo -ne "${ORANGE}force: ${FORCEMASK:3:1}${NC}; "
    
    [ -z "$LS_OUTPUT0" ] && export LS_OUTPUT0=$(mktemp --suffix -ls-output0)
    $ESCALADE_GETLIST $ESCALADE_OUTPUT0 ${DEPTHMASK:3:1} > $LS_OUTPUT0
    export ESCALADE_OUTPUT0_PREVIOUSDEPTHMASK=$ESCALADE_OUTPUT0_DEPTHMASK
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}nfiles: $(cat $LS_OUTPUT0 | wc -l )${NC}"
fi

return 0
