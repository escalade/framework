#! /bin/bash

#
# Usage checks
#
[[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]] && SOURCED=1

if [ -z "$ESCALADE" ]; then

  echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
  exit 1
fi

# Global variables
source $ESCALADE_USAGE $0 $ESCALADE_FORCERECOVER "<idpattern> <confirm>" $# 2
[ $? -eq 1 ] && exit 1

ESCALADE_JOBPATTERNID=$1
if [ -z "$ESCALADE_JOBPATTERNID" ]; then

  echo "ESCALADE_JOBPATTERNID not defined.."
  return 1
fi

set --

# Check if production is already archived or not
$ESCALADE_ISARCHIVED $ESCALADE_OUTPUT0
[ $? -eq 1 ] && exit 1

# Check the safethread lock
source $ESCALADE_SAFETHREAD > /dev/null
[ $? -eq 1 ] && exit 1

#
# Environment checks
#

source $ESCALADE_LSDIR "0111" "0000" "0222"
[ $? -eq 1 ] && exit 1

source $ESCALADE_CHECKKEY $ESCALADE_OUTPUT0 $(dirname $ESCALADE_LOGDIR0)
[ ! $? -eq 0 ] && exit 1

#
# Check if jobs are still running
#
if [ "$ESCALADE_FORCERECOVER" != "$0" ]; then

  $ESCALADE_UPDATEPID
  [ $? -eq 1 ] && exit 1
fi

[ "$ESCALADE_JOBPATTERNID" == "*" ] && ESCALADE_JOBPATTERNID=".*"
echo -e "${GREEN}--> Force to recover using the following pattern: ${NC}\"${RED}$ESCALADE_JOBPATTERNID${NC}\""

#
# Start removing files found
#
if [ ! -z "$ESCALADE_DEBUG" ]; then

  echo -e "${ORANGE}--> Debug mode is enabled..${NC}"
  echo -e "${ORANGE}    Therefore file deletion is disabled!${NC}"
fi

# Remove output logfiles (in permanent directory and temporary)
echo -e "${GREEN}--> Logfiles removed from the temporary logdirectory ($ESCALADE_TMPDIR/logs)${NC}"
echo -ne "\t - Nothing to do..\r"

ESCALADE_PREVIOUS_JOBID=-1

for FILE in $(cat "$LS_LOGDIR0_TMP" | grep -E "$ESCALADE_TMPDIR/logs/slot\-[0-9]*/(${ESCALADE_JOBPATTERNID})$"); do

  [[ "$(basename $FILE)" =~ ^\..* ]] && continue; # If hidden file..

  tput el 2> /dev/null
  source $ESCALADE_JOBINFO $FILE

  [ "$ESCALADE_PREVIOUS_JOBID" == "$ESCALADE_JOBID" ] && continue;

  ESCALADE_BATCH_ID=$(cat $ESCALADE_LOGDIR0/.pid | grep -E $'\t'"$ESCALADE_JOBID"$'\t' | awk '{print $1}' | uniq)
  if [[ -z "$ESCALADE_BATCH_ID" || -z "$(cat "$LS_QSTAT" | grep "$ESCALADE_BATCH_ID")" ]]; then

    # Check the safethread lock
    source $ESCALADE_SAFETHREAD > /dev/null
    [ $? -eq 1 ] && exit 1

    [ -z "$ESCALADE_DEBUG" ] && rm -rf "$ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/$ESCALADE_JOBID/"

    tput el 2> /dev/null
    echo -e "\t - Deleting job ID #$ESCALADE_JOBID.."
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "\t   ${ORANGE}(NB:Nothing is deleted.. debug mode is enabled!!)${NC}"
  else
    tput el 2> /dev/null
    echo -e "${ORANGE}\t - Job ID #$ESCALADE_JOBID is still running.. cannot delete it${NC}"
  fi

  ESCALADE_PREVIOUS_JOBID=$ESCALADE_JOBID
done
echo ""

echo -e "${GREEN}--> Logfiles removed from the logdirectory ($ESCALADE_LOGDIR0)${NC}"
echo -ne "\t - Nothing to do..\r"

ESCALADE_PREVIOUS_JOBID=-1
for FILE in $(cat "$LS_LOGDIR0" | grep -E "$ESCALADE_LOGDIR0/slot\-[0-9]*/(${ESCALADE_JOBPATTERNID})$"); do

  [[ "$(basename $FILE)" =~ ^\..* ]] && continue; # If hidden file..

  tput el 2> /dev/null
  source $ESCALADE_JOBINFO $FILE
  [ "$ESCALADE_PREVIOUS_JOBID" == "$ESCALADE_JOBID" ] && continue;

  ESCALADE_BATCH_ID=$(cat $ESCALADE_LOGDIR0/.pid | grep -E $'\t'"$ESCALADE_JOBID"$'\t' | awk '{print $1}' | uniq)
  if [[ -z "$ESCALADE_BATCH_ID" || -z "$(cat "$LS_QSTAT" | grep "$ESCALADE_BATCH_ID")" ]]; then

    # Check the safethread lock
    source $ESCALADE_SAFETHREAD > /dev/null
    [ $? -eq 1 ] && exit 1

    [ -z "$ESCALADE_DEBUG" ] && rm -rf "$ESCALADE_LOGDIR0/slot-$ESCALADE_JOBSLOT/$ESCALADE_JOBID/"

    tput el 2> /dev/null
    echo -e "\t - Deleting job ID #$ESCALADE_JOBID.."
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "\t   ${ORANGE}(NB:Nothing is deleted.. debug mode is enabled!!)${NC}"
  else
    tput el 2> /dev/null
    echo -e "${ORANGE}\t - Job ID #$ESCALADE_JOBID is still running.. cannot delete it${NC}"
  fi

  ESCALADE_PREVIOUS_JOBID=$ESCALADE_JOBID
done
echo ""

# Remove output files/hist files
echo -e "${GREEN}--> Files removed from the directory $ESCALADE_OUTPUT0${NC}"
echo -ne "\t - Nothing to do..\r"

ESCALADE_PREVIOUS_JOBID=-1
for FILE in $(cat "$LS_OUTPUT0" | grep -v "escalade-key" | grep -E "$ESCALADE_OUTPUT0/slot\-[0-9]*/(${ESCALADE_JOBPATTERNID})$"); do

  tput el 2> /dev/null
  source $ESCALADE_JOBINFO $FILE
  [ "$ESCALADE_PREVIOUS_JOBID" == "$ESCALADE_JOBID" ] && continue;

  ESCALADE_BATCH_ID=$(cat $ESCALADE_LOGDIR0/.pid | grep -E $'\t'"$ESCALADE_JOBID"$'\t' | awk '{print $1}' | uniq)
  if [[ -z "$ESCALADE_BATCH_ID" || -z "$(cat "$LS_QSTAT" | grep "$ESCALADE_BATCH_ID")" ]]; then

    # Check the safethread lock
    source $ESCALADE_SAFETHREAD > /dev/null
    [ $? -eq 1 ] && exit 1

    if [ -z "$ESCALADE_DEBUG" ]; then
	eval $FS_RM "$FILE"
	    if [ $? -eq 1 ]; then

	      tput el 2> /dev/null
	      echo -e "${RED}\t - Error during removing on $FS..${NC}"
	      exit 1
	    fi
    fi
    tput el 2> /dev/null
    echo -e "\t - Delete files related to #$(basename $FILE)"
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "\t   ${ORANGE}(NB: Nothing is deleted.. debug mode is enabled!!)${NC}"
  else
    tput el 2> /dev/null
    echo -e "${ORANGE}\t - Job ID #$ESCALADE_JOBID is still running.. cannot delete \"$(basename $FILE)\"${NC}"
  fi
done

echo -e "${GREEN}--> Looking at PID information from $ESCALADE_LOGDIR0${NC}"
echo -ne "\t - Nothing to do..\r"

while read LINE ; do

  [ -z "$LINE" ] && continue;

  RUN=$(echo $LINE | awk '{print $1}')
  MD5SUM=$(echo $LINE | awk '{print $2}')
  ESCALADE_BATCH_ID=$(echo $LINE | awk '{print $3}')
  TMP_JOBLIST=$(echo $LINE | awk '{print $4}')

  if [[ -z "$ESCALADE_BATCH_ID" || -z "$(cat "$LS_QSTAT" | grep "$ESCALADE_BATCH_ID")" ]]; then

   if [ -z "$ESCALADE_DEBUG" ]; then
	sed -e "s/$ESCALADE_BATCH_ID[ \t]*$MD5SUM[ \t]*[0-9]*[ \t]*$RUN[ \t]*\(.*\)//g" -e "/^$/d" -i $ESCALADE_LOGDIR0/.pid
	if [[ -z "$(cat $ESCALADE_LOGDIR0/.pid | grep $MD5SUM | uniq)" && -f "$TMP_JOBLIST" ]]; then
		echo -e "\t - Last entry for PID $MD5SUM"
		echo -e "\t   Delete job submission file: $TMP_JOBLIST"
		rm -f "$TMP_JOBLIST"
        fi
   fi
    tput el 2> /dev/null
    echo -e "\t - Delete entry line for run \"$RUN\" in PID file"
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "\t   ${ORANGE}(NB:Nothing is deleted.. debug mode is enabled!!)${NC}"
  else
    tput el 2> /dev/null
    echo -e "${ORANGE}\t - Job ID #$ESCALADE_JOBID is still running.. cannot delete files related to \"$MD5SUM\"${NC}"
  fi
done <<< "$(cat $ESCALADE_LOGDIR0/.pid | grep -v '^#' | awk '{print $4" "$2" "$1" "$8}' | grep -E "^($ESCALADE_JOBPATTERNID) " | uniq)"

echo -e "${GREEN}--> Reference files without PID information from the directory $ESCALADE_LOGDIR0${NC}"
echo -ne "\t - Nothing to do..\r"

STDFILE_LIST=$(cat "$LS_LOGDIR0" | grep -v "^#" | grep -v ".pid" | grep "\.err\|\.out\|\.env" | xargs -n1 basename 2> /dev/null | cut -d'.' -f2 | cut -d'-' -f1 | uniq)
for MD5SUM in $STDFILE_LIST; do

  [ -z $MD5SUM ] && continue;
  if [ -z "$(cat $ESCALADE_LOGDIR0/.pid | grep $MD5SUM | uniq)" ]; then

    echo -e "\t - Delete single files related to \"$MD5SUM\" (This file is a lonely file)"
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "\t   ${ORANGE}(NB:Nothing is deleted.. debug mode is enabled!!)${NC}"

    [ -z "$ESCALADE_DEBUG" ] && find $ESCALADE_LOGDIR0 -maxdepth 1 -name ".${MD5SUM}*.out" -exec rm "{}" \;
    [ -z "$ESCALADE_DEBUG" ] && find $ESCALADE_LOGDIR0 -maxdepth 1 -name ".${MD5SUM}*.err" -exec rm "{}" \;
    [ -z "$ESCALADE_DEBUG" ] && find $ESCALADE_LOGDIR0 -maxdepth 1 -name ".${MD5SUM}*.env" -exec rm "{}" \;
    [ -z "$ESCALADE_DEBUG" ] && find $ESCALADE_TMPDIR/logs -maxdepth 1 -name ".${MD5SUM}*.out" -exec rm "{}" \;
    [ -z "$ESCALADE_DEBUG" ] && find $ESCALADE_TMPDIR/logs -maxdepth 1 -name ".${MD5SUM}*.err" -exec rm "{}" \;
    [ -z "$ESCALADE_DEBUG" ] && find $ESCALADE_TMPDIR/logs -maxdepth 1 -name ".${MD5SUM}*.env" -exec rm "{}" \;
  fi
done

echo ""
