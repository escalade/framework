#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_HADD "<datadir> <pattern> <target> <nfirst> <nfiles> <nlimit>" $# 6
[ $? -eq 1 ] && exit 1

#
# Global variables
#
DATA_OUTPUT0=$1
PATTERN=$2
TARGET=$3
NFIRST=$4
NFILES=$5
NLIMIT=$6

#
# Environment checks
#
echo -e "${GREEN}--> Merging files in process..${NC}"
source $ESCALADE_SETFS $DATA_OUTPUT0 > /dev/null
[ $? -eq 1 ] && exit 1

source $ESCALADE_ISARCHIVED $DATA_OUTPUT0
[ $? -eq 1 ] && exit 1

#
# Files to merge
#
[ "$NFILES" == "all" ] && NFILES=$(cat "$LS_OUTPUT0" 2> /dev/null | grep -E $PATTERN | wc -l)
NLAST=$((NFIRST+NFILES-1))
DIVIDEJOBS=$((NLAST/NLIMIT+1))

echo -e "${GREEN}--> There are $NFILES to merge, we will do this in $DIVIDEJOBS time(s)${NC}"

#
# Target
#
source $ESCALADE_SETFS $TARGET > /dev/null
[ $? -eq 1 ] && exit 1

for i in $(seq 0 $((DIVIDEJOBS-1))); do
    
    FILES=$(cat "$LS_OUTPUT0" 2> /dev/null | grep -E $PATTERN | sed -n "$((i*NLIMIT+1)),$(( (i+1)*NLIMIT )) p" | sed "s,^,${FS_ADDR},g")
    echo -e "\t* There are $(echo "$FILES" | wc -l) file(s) selected to be merged from $DATA_OUTPUT0"
    
    TARGET="${FS_ADDR}$3.$i"
    echo -e "\t Target file will be here: $TARGET"
    
    #
    # Merging
    #
    
    echo -e "\t Starting to merge files.."
    $ROOTSYS/bin/hadd -f5 $TARGET $FILES
    if [ $? -eq 1 ]; then
        
        echo -e "\t Failed to merge files.. an error occured for $TARGET"
        exit 1
    fi
    echo -e "\t File successfully merged : $TARGET"
    
    FINAL="$FINAL $TARGET"
done

TARGET=${FS_ADDR}$3
FINAL=($FINAL)

if [ ! ${#FINAL[@]} -eq 1 ]; then
    
    echo -e "${GREEN}--> Final merging (${#FINAL[@]} files) into $TARGET${NC}"
    $ROOTSYS/bin/hadd -f5 $TARGET ${FINAL[@]}
    [ $? -eq 1 ] && exit 1
    
    eval $FS_RM ${FINAL[@]}
else
    echo -e "${GREEN}--> Just rename $FINAL${NC}"
    echo -e " to $TARGET"
fi

echo -e "${GREEN}--> Succesfully merging of rootfiles into $3${NC}"
exit 0
