#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_UPDATEPID "" $# 0
[ $? -eq 1 ] && exit 1

# Check the safethread lock
source $ESCALADE_SAFETHREAD > /dev/null
[ $? -eq 1 ] && exit 1


#
# Global variables
#
source $ESCALADE_LSDIR "1111" "0000" "2333"
[ $? -eq 1 ] && exit 1

touch $ESCALADE_LOGDIR0/.pid

PIDLIST=$(cat $ESCALADE_LOGDIR0/.pid 2> /dev/null)
LS_QSTAT=$(mktemp --suffix -ls-qstat)
$ESCALADE_GLOBALQSTAT $ESCALADE_LOGDIR0/.pid 2> /dev/null > $LS_QSTAT

# Check if there are files which exist and are not found in .pid !! (That cannot append, because then the file .out cannot be linked back to it job.. /!\
echo -e "${GREEN}--> Update PID list and job wrapper files${NC}"
if [ ! -z "$ESCALADE_DISABLE_PID" ]; then
    
    echo -e "\t${RED}* Skip PID wrapping procedure.. on request (--disable-pid)${NC}"
    sleep 2
    exit 0
fi

# Check remaining PID files..
let IFILE_DISPLAY=1
let NFILE_DISPLAY=10

mkdir -p "$ESCALADE_TMPDIR/logs"

for MD5SUM in $(echo "$PIDLIST" | grep -v "^#" | awk '{print $2}' | uniq ); do
    
    ESCALADE_BATCH_PID=$(echo "$PIDLIST" | awk -v MD5SUM=$MD5SUM '$2 == MD5SUM { print $1 }' | uniq)
    STDERR="$ESCALADE_TMPDIR/logs/.${MD5SUM}.err"
    SUBSTDERR="$ESCALADE_TMPDIR/logs/.${MD5SUM}-*.err"
    STDOUT="$ESCALADE_TMPDIR/logs/.${MD5SUM}.out"
    SUBSTDOUT="$ESCALADE_TMPDIR/logs/.${MD5SUM}-*.out"
    STDENV="$ESCALADE_TMPDIR/logs/.${MD5SUM}.env"
    
    ISJOBOVER=$(cat "$LS_QSTAT" | grep $ESCALADE_BATCH_PID)
    if [ ! -z "$ISJOBOVER" ]; then
        
        tput el 2> /dev/null
        echo -ne "\tJob #$ESCALADE_BATCH_PID is still running, global stderr/stdout will be copied when it finishes\r"
        continue; # Check if the job is not over.. skip
    fi
    
    if [[ ! -f "$STDERR" ]]; then
        
        tput el 2> /dev/null
        echo -e "${ORANGE}\t* Missing job wrapper logfile for the job ID #${ESCALADE_BATCH_PID}.. (Error line added into .$MD5SUM.err)${NC}" >&2
        esc_echo -e "${ORANGE}\t  $STDERR is not found..${NC}" >&2
        
        echo "[ESCALADE] Missing $MD5SUM stderr wrapper, but entry in PID file.. $ESCALADE_BATCH_PID" >> $STDERR
        continue;
    fi
    if [[ ! -f "$STDOUT" ]]; then
        
        tput el 2> /dev/null
        echo -e "${ORANGE}\t* Missing job wrapper logfile for the job ID #${ESCALADE_BATCH_PID}.. (Error line added into .$MD5SUM.err)${NC}" >&2
        esc_echo -e "${ORANGE}\t  $STDOUT is not found..${NC}" >&2
        
        echo "[ESCALADE] Missing $MD5SUM stdout wrapper, but entry found in PID file.. $ESCALADE_BATCH_PID" >> $STDERR
        continue;
    fi
    
    SKIP=1
    for STD in $SUBSTDOUT $STDERR $STDOUT $SUBSTDERR; do
        if [[ -f "$STD" && -s "$STD" ]]; then
            SKIP=0
            break;
        fi
    done
    [ $SKIP -eq 1 ] && continue;
    
    PIDLIST_READY="$PIDLIST_READY $MD5SUM"
    tput el 2> /dev/null
    echo -ne "\tJob #$ESCALADE_BATCH_PID finished and found in PID list.. Going to be processed..\r"
done

tput el 2> /dev/null

IPID=1
NPID=$(echo "$PIDLIST_READY" | wc -w)
if [ $NPID -eq 0 ]; then
    
    echo -e "\t- Up-to-date and processed.."
    exit 0
fi

#
# Environment checks
#

ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE_ARGUMENT $ESCALADE_PIPEFILE0"
ESCALADE_PIPEFILE_AUTOFOUND=$(find $PWD -maxdepth 1 -name '*.pipe' 2> /dev/null| tr '\n' ' ')
ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE $ESCALADE_PIPEFILE_AUTOFOUND"
ESCALADE_PIPEFILE_AUTOFOUND=$(find $ESCALADE/pilots/software/escalade -maxdepth 1 -name '*.pipe' 2> /dev/null | tr '\n' ' ')
ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE $ESCALADE_PIPEFILE_AUTOFOUND"
if [ "$ESCALADE_CLUSTER" != "unknown" ]; then
    ESCALADE_PIPEFILE_AUTOFOUND=$(find $ESCALADE/pilots/clusters/$ESCALADE_CLUSTER -maxdepth 1 -name '*.pipe' 2> /dev/null | tr '\n' ' ')
    ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE $ESCALADE_PIPEFILE_AUTOFOUND"
fi
if [ "$ESCALADE_BATCH" != "fakebatch" ]; then
    ESCALADE_PIPEFILE_AUTOFOUND=$(find $ESCALADE/pilots/batch/$ESCALADE_BATCH -maxdepth 1 -name '*.pipe' 2> /dev/null | tr '\n' ' ')
    ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE $ESCALADE_PIPEFILE_AUTOFOUND"
fi

tput el 2> /dev/null
echo -e "${GREEN}--> Looking for the pipe files..${NC} $(echo $ESCALADE_PIPEFILE | wc -w) found(s)"

source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && return 1

#
# Pipe error messages from stdout to stderr
#
function pipe_pid
{
    local FILELIST=$1
    
    local IPIPE=0
    local NPIPE=20
    local IFILE=0
    local NFILE=$(echo "$FILELIST" | wc -w)
    
    local FINAL_GREP=""
    for FILE in $FILELIST; do
        
        IFILE=$((IFILE+1))
        
        local PIPEFILE=$FILE
        if [ ! -f "$PIPEFILE" ]; then
            
            esc_echo "${ORANGE}--> Pipe file not found..${NC} ($PIPEFILE)" >&2
            continue
        fi
        
        esc_echo "${GREEN}--> Reading pipe file..${NC} $PIPEFILE"
        IPID=0
        NLINE=0
        while read GREP
        do
            GREP="$(echo "$GREP" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$GREP" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            
            NLINE=$((NLINE+1))
        done < "$PIPEFILE"
        
        [ $NLINE -eq 0 ] && continue;
        
        ILINE=0
        while read GREP
        do
            GREP="$(echo "$GREP" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$GREP" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            
            IPIPE=$((IPIPE+1))
            ILINE=$((ILINE+1))
            FINAL_GREP="-e \"$GREP\" $FINAL_GREP"
            
            tput el 2> /dev/null
            echo -e "\t- Piping \"$GREP\" from PID .out to PID .err.. ($ILINE out of $NLINE)"
            
            if [[ $(( $IPIPE % $NPIPE )) -eq 0 || ($IFILE -eq $NFILE && $NLINE -eq $ILINE) || ! -z "$ESCALADE_DEBUG" ]]; then
                
                tput el 2> /dev/null
                echo -e "${ORANGE}\t\t* Processing buffer..${NC} Please wait.."
                
                # Apply the piping informations
                local IRUN=1
                local NRUN=$(cat "$LS_LOGDIR0_TMP" | grep -E "\.out$" | wc -l)
                
                IPID=1
                MATCHING_LINES=0
                for MD5SUM in $PIDLIST_READY; do
                    
                    STDERR="$ESCALADE_TMPDIR/logs/.${MD5SUM}.err"
                    SUBSTDERR="$ESCALADE_TMPDIR/logs/.${MD5SUM}-*.err"
                    STDOUT="$ESCALADE_TMPDIR/logs/.${MD5SUM}.out"
                    SUBSTDOUT="$ESCALADE_TMPDIR/logs/.${MD5SUM}-*.out"
                    STDENV="$ESCALADE_TMPDIR/logs/.${MD5SUM}.env"
                    
                    tput el 2> /dev/null
                    echo -ne "${ORANGE}\t\t  Reading PID entry #$MD5SUM..${NC} ($IPID/$NPID)\r"
                    for STDOUT0 in $STDOUT $SUBSTDOUT; do
                        
                        IRUN=$(($IRUN+1))
                        [ ! -f "$STDOUT0" ] && continue;
                        
                        rm -f "$ESCALADE_TMPDIR/logs/.$MD5SUM.tmp"
                        MATCH=$(eval "LC_ALL=C fgrep --binary-files=text -hi  $FINAL_GREP $STDOUT0")
                        [ ! -z "$MATCH" ] && MATCHING_LINES=$(($MATCHING_LINES+$(echo "$MATCH" | wc -l)))
                        
                        echo "$MATCH" >> $ESCALADE_TMPDIR/logs/$(basename $STDOUT0 .out).err
                        eval "LC_ALL=C fgrep --binary-files=text -hiv $FINAL_GREP $STDOUT0" >> $ESCALADE_TMPDIR/logs/$(basename $STDOUT0 .out).tmp
                        mv $ESCALADE_TMPDIR/logs/$(basename $STDOUT0 .out).tmp $STDOUT0
                    done
                    
                    if [[ ! -z "$ESCALADE_DEBUG" && $IRUN != 1 ]]; then
                        tput el 2> /dev/null
                        echo -e "${ORANGE}\t\t* $MATCHING_LINES line(s) matching within $((IRUN-1))/$NRUN file(s)${NC}"
                    fi
                    
                    IPID=$(($IPID+1))
                done
            fi
        done < "$PIPEFILE"
    done
}

if [ -z "$ESCALADE_DISABLE_PID_PIPEFILE" ]; then
    pipe_pid "$ESCALADE_PIPEFILE"
else
    echo -e "\t${RED}* Skip PID piping procedure.. on request (--disable-pid-pipe)${NC}"
    sleep 2
fi




ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE_ARGUMENT $ESCALADE_SUPPRFILE0"
ESCALADE_SUPPRFILE_AUTOFOUND=$(find $PWD -maxdepth 1 -name '*.suppr' 2> /dev/null| tr '\n' ' ')
ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE $ESCALADE_SUPPRFILE_AUTOFOUND"
ESCALADE_SUPPRFILE_AUTOFOUND=$(find $ESCALADE/pilots/software/escalade -maxdepth 1 -name '*.suppr' 2> /dev/null | tr '\n' ' ')
ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE $ESCALADE_SUPPRFILE_AUTOFOUND"
if [ "$ESCALADE_CLUSTER" != "unknown" ]; then
    ESCALADE_SUPPRFILE_AUTOFOUND=$(find $ESCALADE/pilots/clusters/$ESCALADE_CLUSTER -maxdepth 1 -name '*.suppr' 2> /dev/null | tr '\n' ' ')
    ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE $ESCALADE_SUPPRFILE_AUTOFOUND"
fi
if [ "$ESCALADE_BATCH" != "fakebatch" ]; then
    ESCALADE_SUPPRFILE_AUTOFOUND=$(find $ESCALADE/pilots/batch/$ESCALADE_BATCH -maxdepth 1 -name '*.suppr' 2> /dev/null | tr '\n' ' ')
    ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE $ESCALADE_SUPPRFILE_AUTOFOUND"
fi

tput el 2> /dev/null
echo -e "${GREEN}--> Looking for suppression files..${NC} $(echo $ESCALADE_SUPPRFILE | wc -w) found(s)"

function suppress_pid
{
    tput el 2> /dev/null
    echo -ne "\t\t${ORANGE}* Fetching non-zero temporary logfiles (PID error only).. ${NC}"
    LS_TMP_SUPPR=$(mktemp --suffix -ls-suppr)
    find $ESCALADE_TMPDIR/logs -type f -name "*.err" ! -size 0 > "$LS_TMP_SUPPR"
    
    local IRUN=1
    local NRUN=$(cat "$LS_TMP_SUPPR" | grep -E "\.err$" | wc -l)
    echo -ne "$NRUN file(s) found..\r"
    
    [ -z "$(cat "$LS_TMP_SUPPR" | grep -E \.err$ 2> /dev/null)" ] && return 0
    
    local FILELIST=$1
    
    local ISUPPR=0
    local NSUPPR=20
    local IFILE=0
    local NFILE=$(echo "$FILELIST" | wc -w)
    
    for FILE in $FILELIST; do
        
        IFILE=$((IFILE+1))
        local SUPPRFILE=$FILE
        if [ ! -f "$SUPPRFILE" ]; then
            esc_echo "${ORANGE}--> Suppression file not found..${NC} ($SUPPRFILE)" >&2
            continue;
        fi
        
        tput el 2> /dev/null
        esc_echo "${GREEN}--> Suppress harmless PID errors using..${NC} $SUPPRFILE"
        NLINE=0
        while read CODE
        do
            CODE="$(echo "$CODE" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$CODE" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            NLINE=$((NLINE+1))
        done < "$SUPPRFILE"
        [ $NLINE -eq 0 ] && continue;
        
        local ILINE=0
        while read CODE
        do
            
            CODE="$(echo "$CODE" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$CODE" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            
            ILINE=$(($ILINE+1))
            ISUPPR=$(($ISUPPR+1))
            
            echo -e "\t- Removing \"$CODE\" (sed,regex) warning messages.. ($ILINE out $NLINE)"
            local SEDCODE="-e \"$CODE\" $SEDCODE"
            
            if [[ $(( $ISUPPR % $NSUPPR )) -eq 0 || ($IFILE -eq $NFILE && $NLINE -eq $ILINE) || ! -z "$ESCALADE_DEBUG" ]]; then
                
                tput el 2> /dev/null
                echo -e "${ORANGE}\t\t* Processing buffer..${NC} Please wait.."
                
                IPID=1
                
                # Apply the piping informations
                for MD5SUM in $PIDLIST_READY; do
                    
                    echo -ne "\t\t${ORANGE}  Reading PID entry #$MD5SUM..${ORANGE} ($IRUN/$NRUN)\r"
                    for STDERR0 in $(cat "$LS_TMP_SUPPR"); do
                        
                        # Check the safethread lock
                        source $ESCALADE_SAFETHREAD > /dev/null
                        [ $? -eq 1 ] && exit 1
                        
                        IRUN=$(($IRUN+1))
                        
                        if [ -s "$STDERR0" ]; then
                            eval "sed $SEDCODE -i $STDERR0"
                        fi
                    done
                    
                    IPID=$(($IPID+1))
                done
                SEDCODE=""
                
                tput el 2> /dev/null
                echo -ne "\t\t${ORANGE}* Fetching non-zero temporary logfiles (PID error only).. ${NC}"
                find $ESCALADE_TMPDIR/logs -type f -name "*.err" ! -size 0 > "$LS_TMP_SUPPR"
                
                local IRUN=1
                local NRUN=$(cat "$LS_TMP_SUPPR" | grep -E "\.err$" | wc -l)
                echo -e "$NRUN file(s) found.."
                
            fi
        done < "$SUPPRFILE"
    done
    
    rm -f "$LS_TMP_SUPPR"
    unset LS_TMP_SUPPR
}

if [ -z "$ESCALADE_DISABLE_PID_SUPPRFILE" ]; then
    suppress_pid "$ESCALADE_SUPPRFILE"
else
    echo -e "\t${RED}* Skip PID suppression procedure.. on request (--disable-pid-suppr)${NC}"
    sleep 2
fi





IPID=0
for MD5SUM in $PIDLIST_READY; do
    
    ESCALADE_BATCH_PID=$(echo "$PIDLIST" | awk -v MD5SUM=$MD5SUM '$2 == MD5SUM { print $1 }' | uniq)
    
    STDERR="$ESCALADE_TMPDIR/logs/.${MD5SUM}.err"
    SUBSTDERR="$ESCALADE_TMPDIR/logs/.${MD5SUM}-*.err"
    STDOUT="$ESCALADE_TMPDIR/logs/.${MD5SUM}.out"
    SUBSTDOUT="$ESCALADE_TMPDIR/logs/.${MD5SUM}-*.out"
    STDENV="$ESCALADE_TMPDIR/logs/.${MD5SUM}.env"
    
    IPID=$(($IPID+1))
    
    # If environnement file are already empty, this means that you already recopied it into the stdout and stderr
    SKIP=1
    for STD in $SUBSTDOUT $STDERR $STDOUT $SUBSTDERR; do
        if [[ -f "$STD" && -s "$STD" ]]; then
            SKIP=0
            break;
        fi
    done
    [ $SKIP -eq 1 ] && continue;
    
    ESCALADE_JOBLIST=$(echo "$PIDLIST" | awk -v MD5SUM=$MD5SUM '$2 == MD5SUM { print $3";"$4 }')
    
    IRUN=1
    NRUN=$(echo "$ESCALADE_JOBLIST" | wc -l)
    for ESCALADE_JOB in $(echo "$ESCALADE_JOBLIST"); do # In case several runs are processed by one job (e.g. using OpenMPI)
        
        ESCALADE_JOBID=$(echo "$ESCALADE_JOB" | cut -d ';' -f2)
        ESCALADE_JOBSLOT=$(echo "$ESCALADE_JOB" | cut -d ';' -f1)
        
        mkdir -p $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}
        
        # Find lonely files (it happend when an internal issue occurs with the batch system..)
        if [[ ! -f "$STDOUT" || ! -f "$STDERR" ]]; then
            
            # Check the safethread lock
            source $ESCALADE_SAFETHREAD > /dev/null
            [ $? -eq 1 ] && exit 1
            
            if [ $IFILE_DISPLAY -eq $NFILE_DISPLAY ]; then
                
                tput el 2> /dev/null
                echo -e "${ORANGE}\t [..]${NC}"
            fi
            
            if [ $IFILE_DISPLAY -lt $NFILE_DISPLAY ]; then
                
                tput el 2> /dev/null
                echo -e "${ORANGE}\t- Lonely PID found (Error line added into ${ESCALADE_JOBID}.stderr; it happend when killing the job before it started)${NC}"
                echo -e "${ORANGE}\t No pair $MD5SUM corresponding to job #${ESCALADE_BATCH_PID}${NC}"
            else
                tput el 2> /dev/null
                echo -ne "${ORANGE}\t- Lonely PID found (Error line added in ${ESCALADE_JOBID}.stderr)${NC}\r"
            fi
            
            echo "[Error] Job didn't finished correctly.. no job wrapper logfile but PID entry found.." > $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr # Store the information in each final logfiles
            
            let IFILE_DISPLAY=$(($IFILE_DISPLAY+1))
            continue;
        fi
        
        tput el 2> /dev/null
        echo -ne "${GREEN}--> Job #$ESCALADE_BATCH_PID found in PID list ($IPID/$NPID)..${NC} Append #$MD5SUM into run #$ESCALADE_JOBID ($IRUN/$NRUN) log files\r"
        IRUN=$(($IRUN+1))
        
        STDID=$(basename $STDOUT .out)
        
        #INCLUDE_STD=$(head -n 10 $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout 2> /dev/null | fgrep -m 1 "[ESCALADE] Include $STDID.out")
        #if [ ! -z "$INCLUDE_STD" ]; then
        
        echo "" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout
        echo "--- Job wrapper details (BEGIN:$STDID) ---" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout
        cat $STDOUT >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout
        echo "--- Job wrapper details (END:$STDID) ---" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout
        echo "" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout
        #fi
        
        #INCLUDE_STD=$(head -n 10 $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr 2> /dev/null | fgrep -m 1 "[ESCALADE] Include $STDID.err")
        #if [ ! -z "$INCLUDE_STD" ]; then
        
        echo ""	>> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr
        echo "--- Job wrapper details (BEGIN:$STDID) ---" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr
        cat $STDERR >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr
        echo "--- Job wrapper details (END:$STDID) ---" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr
        echo "" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr
        #fi
        
        # Merge additional sub-stdout
        INCLUDE_STD=$(head -n 10 $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout 2> /dev/null | sed -ne "s/.*ESCALADE. Include ${STDID}\-\([0-9]*\)\.out.*/\\1/p")
        if [ ! -z "$INCLUDE_STD" ]; then
            
            SID=$INCLUDE_STD
            SUBSTDOUT0="$ESCALADE_TMPDIR/logs/${STDID}-${SID}.out"
            
            echo "" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout
            echo "--- Job subwrapper details (BEGIN: $(basename $SUBSTDOUT0)) ---" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout
            cat $SUBSTDOUT0 >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout
            echo "--- Job subwrapper details (END: $(basename $SUBSTDOUT0)) ---" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout
            echo "" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stdout
        fi
        
        # Merge additional sub-stdout
        INCLUDE_STD=$(head -n 10 $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr 2> /dev/null | sed -ne "s/.*ESCALADE. Include ${STDID}\-\([0-9]*\)\.err.*/\\1/p")
        if [ ! -z "$INCLUDE_STD" ]; then
            
            SID=$INCLUDE_STD
            SUBSTDERR0="$ESCALADE_TMPDIR/logs/${STDID}-${SID}.err"
            
            echo "" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr
            echo "--- Job subwrapper details (BEGIN: $(basename $SUBSTDERR0))  ---" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr
            cat $SUBSTDERR0 >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr
            echo "--- Job subwrapper details (END: $(basename $SUBSTDERR0)) ---" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr
            echo "" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/${ESCALADE_JOBID}/${ESCALADE_JOBID}.stderr
        fi
        
    done
    
    # Empty the reference files!!
    # I do not touch STDENV, because some variables might be piped into the STDERR if it contains some keywords..
    
    for SUBSTDERR0 in $SUBSTDERR; do
        rm -f "$SUBSTDERR0"
        touch "$SUBSTDERR0"
    done
    
    for SUBSTDOUT0 in $SUBSTDOUT; do
        rm -f "$SUBSTDOUT0"
        touch "$SUBSTDOUT0"
    done
    
    rm -f "$STDOUT" "$STDERR"
    touch "$STDOUT" "$STDERR"
done

echo ""
tput el 2> /dev/null

exit 0
