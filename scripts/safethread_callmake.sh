#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
  echo "$0 : This script cannot be sourced !" >&2
  return 1
fi

if [ -z "$ESCALADE" ]; then
  echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
  return 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_SAFETHREAD_CALLMAKE "[<make command>]" $# 1
[ $? -eq 1 ] && return 1

ESCALADE_CMD=$1
set --

        if [ -z "$ESCALADE_FORCEACTION" ]; then
                source $ESCALADE_SAFETHREAD $ESCALADE_TAKEOVER
                [ $? -eq 1 ] && exit 1
        else
                echo -e "${ORANGE}--> Safethread disabled because you used --force-action. Hope you know what you are doing :)${NC}"
        fi

	echo ""
	echo -ne "${PURPLE}--> List of the requested command(s).. \"$ESCALADE_CMD\"${NC} ($(date))"

	for ESCALADE_CMD0 in $ESCALADE_CMD; do

            echo ""
            echo -e "${PURPLE}*** Start of the command.. \"$ESCALADE_CMD0\"${NC} ($(date))"

            MAKEFILE_CMD=$(make -s -q -i -f $ESCALADE_SOFTWARE_DIR/Makefile $ESCALADE_CMD0)
            if [ $? -eq 2 ]; then

                echo -e "${ORANGE}\tCommand not found.. !${NC}"
                [ ! -z "$MAKEFILE_CMD" ] && echo $MAKEFILE_CMD

                echo -e "${ORANGE}\tAvailable commands are the following:"
                unset USAGE_STR
                USAGE_NCMD=0

                for USAGE in $(cat $ESCALADE_SOFTWARE_DIR/Makefile | sed -ne "s,^\([^:\"]*\):.*,\\1,p" | grep -v "%" | tr -d ' ' | sort -V | uniq); do

                    [[ $(($USAGE_NCMD % 5)) -eq 0 && ! $USAGE_NCMD -eq 0 ]] && USAGE_STR="${USAGE_STR}\n"
                    [[ -z "$USAGE_STR" || ($(($USAGE_NCMD % 5)) -eq 0 && ! $USAGE_NCMD -eq 0) ]] && USAGE_STR="$USAGE_STR\t* $USAGE" || USAGE_STR="${USAGE_STR}, $USAGE"
                    USAGE_NCMD=$(($USAGE_NCMD+1))
                done

                [ -z "$USAGE_STR" ] && echo -e "\t${ORANGE}No command found for this pilot..${NC}" || echo -e "$USAGE_STR"
            else

		make -s -i -f $ESCALADE_SOFTWARE_DIR/Makefile $ESCALADE_CMD0
                wait
            fi
            echo -e "${PURPLE}*** End of the command.. \"$ESCALADE_CMD0\"${NC} ($(date))"
	done

        if [ -z "$ESCALADE_FORCEACTION" ]; then
               source $ESCALADE_SAFETHREAD_TERMINATE
               [ $? -eq 1 ] && exit 1
        fi

exit 0
