#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE/scripts/interactivejob.sh "" $# 1
[ $? -eq 1 ] && exit 1

RUN=$1

#
# Pre-action
#
if [[ ! -z "$ESCALADE_FORCEACTION" && "$ESCALADE_FORCEACTION" != "0" ]]; then
    
    echo -e "${ORANGE}--> Force action option is enabled.. You are allowed to run the interactive task !${NC}"
else
    
    # Check if production is already archived or not
    $ESCALADE_ISARCHIVED $ESCALADE_OUTPUT0
    [ $? -eq 1 ] && exit 1
fi

#
# Environment checks
#

if [ -z "$(ls -A ${ESCALADE_INPUT0} 2> /dev/null)" ]; then
    
    echo -e "${RED}--> Data input directory ${ESCALADE_INPUT0} do not exist yet.${NC}"
    echo -e "\tPlease run phast-data command first !"
    exit 1
fi

[ -z "$ESCALADE_TRY" ] && ESCALADE_TRY=1
LS_INPUT0=$($ESCALADE_GETLIST $ESCALADE_INPUT0 1)
export ESCALADE_JOBFILE=$(echo "$LS_INPUT0" | grep -E "${RUN}.txt" | head -n$ESCALADE_TRY)

if [ ! -f "$ESCALADE_JOBFILE" ]; then
    echo -e "\tInput file \"$RUN.txt\" not found.." 2> /dev/null
    exit 1
fi

esc_echo "${GREEN}--> Processing job..${NC} ${ESCALADE_JOBFILE} : $(date)"
source $ESCALADE_JOBINFO $ESCALADE_JOBFILE > /dev/null 2> /dev/null
export ESCALADE_JOBWORKDIR0=$PWD
export ESCALADE_JOBWORKDIR=$ESCALADE_JOBWORKDIR0/escalade-try-$(date '+%Y%m%d-%H%M%S')
export ESCALADE_OUTPUT=$ESCALADE_JOBWORKDIR
export ESCALADE_OUTPUT0=$ESCALADE_JOBWORKDIR

mkdir -p $ESCALADE_JOBWORKDIR
cd $ESCALADE_JOBWORKDIR

# Replace jobfile by the input to be more flexible when using interactive actions..
# Job ID still valid..
cat $ESCALADE_JOBFILE > $ESCALADE_JOBWORKDIR/input.txt
export ESCALADE_JOBFILE=$ESCALADE_JOBWORKDIR/input.txt

$ESCALADE_PREPJOB
[ $? -eq 1 ] && exit 1

#
# Call the job task
#
$ESCALADE_STAGEDATA $ESCALADE_JOBFILE '*' 0
eval $ESCALADE_SOFTWARE_DIR/jobtask.sh

#
# Post action
#
[[ ! -z "$ESCALADE_TARPATH" && ! -z "$IS_TARGZ" ]] && rm -f "$ESCALADE_JOBWORKDIR/input.txt"

cd $ESCALADE_JOBWORKDIR0
echo -e "${GREEN}----- Interactive task performed stored:${NC} $ESCALADE_JOBWORKDIR"
echo -e "${GREEN}----- DONE${NC} : $(date)"
exit 0
