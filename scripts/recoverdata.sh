#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_RECOVERDATA "" $# 0
[ $? -eq 1 ] && exit 1

set --

#
# Environment checks
#
# Check if production is already archived or not
$ESCALADE_ISARCHIVED $ESCALADE_OUTPUT0
[ $? -eq 1 ] && exit 1

source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && exit 1

source $ESCALADE_CHECKKEY $ESCALADE_OUTPUT0 $ESCALADE_PRODUCTION
[ ! $? -eq 0 ] && exit 1

# Update PID file
$ESCALADE_UPDATEPID
[ $? -eq 1 ] && exit 1

source $ESCALADE_LSDIR "0111" "0000" "0222"
[ $? -eq 1 ] && exit 1

#
# Check if fatal error found and recover them
#
FATAL=$(find $ESCALADE_TMPDIR/logs -type f -name "*.stderr" ! -size 0 2> /dev/null)
if [[ ! $(echo "$FATAL" | wc -l) -eq 0 && ! -z "$FATAL" ]]; then
    
    echo -e "${RED}--> $(echo "$FATAL" | wc -l) log(s) detected with potentially fatal errors"
    echo -e "    Start recovering.. (removing output produced related to logfiles with errors)${NC}"
    
    i=0
    for STDERR in $FATAL; do
        
        #echo $STDERR
        [ -z "$RUNID" ] && RUNID="$(basename $STDERR .stderr)" || RUNID="$(basename $STDERR .stderr) $RUNID"
        let i=i+1
    done
    
    RUN_STR=$(echo "$RUNID" | sed 's, ,|,g')
    #  echo -e "${RED}--> Removing data/logfiles related to the pattern \"$RUN_STR\"${NC}"
    $ESCALADE_FORCERECOVER "$RUN_STR" 1
    unset RUN
    
    echo -e "${RED}--> $i run(s) have been deleted${NC}"
    echo -e "${RED}--> Please restart manually the deleted output if mentionned upper.${NC}"
    echo -e "${ORANGE}    You can use \`escalade $ESCALADE_CONFIG ${ESCALADE_SOFTWARE}-batch\` or${NC}"
    echo -e "${ORANGE}    \`RUN='#runnb-with-regex' escalade $ESCALADE_CONFIG ${ESCALADE_SOFTWARE}-batch\` if you just want to restart some runs.${NC}"
    echo -e "${ORANGE}    /!\ Errors will not be detected anymore since all problematic runs have been deleted, please be careful at this state..${NC}"
    
else
    echo -e "${GREEN}--> No error detected.. Nothing to do!${NC}"
fi

exit 0
