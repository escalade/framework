#! /usr/bin/env python

import os
import sys
import re
import glob

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-r", "--runlist", default="", help="<runlist>")
parser.add_argument("filelist", help="<files>")
parser.add_argument('pattern', help='<pattern>')
parser.add_argument("output", help="<output>")
args = parser.parse_args()

if(not args.pattern):
    print(-1)
    exit(1)
if(not args.output):
    print(-4)
    exit(1)

def natural_sort(l): 

    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 

    lsort = sorted(l, key = alphanum_key)
    return lsort

def expand(fname, pattern, runlist = ""):

	fvalid = ""

	f = open(fname, 'r') 
	if(not f):
		print(-2)
		exit(1)
	filelist = f.readlines()

	l1 = []
	n = 0
	N = len(filelist)
	for file in filelist:

		file = file.replace('\n', '')
		if(not file): continue

		m = re.search(pattern, file)
		if(not m): continue
		n += 1

		id = ""
		i = 1
		try:
			while m.group(i):
				if(id): id += "-"
				id += m.group(i)
				i += 1
		except IndexError: pass

		id = id.replace('[^a-zA-Z0-9]','_')
	
		print("\tNew entry for ID #"+id+".. ("+str(n)+"/"+str(N)+").  \""+os.path.basename(file)+"\" is matching the pattern..", end="\r")
		sys.stdout.write("\033[K")

		if(not id): id="noid"
		l1.append([file, id])

	l2 = []
	if(runlist):

		r = open(runlist, 'r')
		if(not r):
			print(-3)
			exit(1)

		rlist = r.readlines()
		for r in rlist:
			r = r.replace('\n', '')
			if(r!=''):
                            l2.append(r)

	return fetch_common_id(l1,l2)

def fetch_common_id(x,y):
    l = []
    if(not y):
        for i in x:
              if(str(i[0]).startswith("root://")): data = i[0]
              else: data = os.path.abspath(i[0])
              l.append(data +"\t"+i[1])
        return l

    for pattern in y:
        r = re.compile('.*'+pattern+'.*')
        for i in x:
            if r.match(i[1]):
                if(str(i[0]).startswith("root://")): data = i[0]
                else: data = os.path.abspath(i[0])
                l.append(data +"\t"+i[1])

    return l

if __name__ == "__main__":

    outF = open(args.output, "w")
    if(not outF):
        print(-4)
        exit(1)

    output = expand(args.filelist, args.pattern, args.runlist)
    for line in output:
        outF.write(line)
        outF.write("\n")

    outF.close()
    exit(0)
