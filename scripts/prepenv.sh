#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_PREPENV "[<inputfile>]" $# 0
[ $? -eq 1 ] && return 1
set --

echo -e "${GREEN}--> Preparing environment and main components..${NC}"
unset ESCALADE_BATCH_TITLE
unset ESCALADE_BATCH_OUT
unset ESCALADE_BATCH_ERR
unset ESCALADE_BATCH_ENV
unset ESCALADE_BATCH_UNIQID
unset ESCALADE_BATCH_SUBOUT
unset ESCALADE_BATCH_SUBERR

#
# Loading main escalade directory
#
[ -z "$ESCALADE_CLUSTER" ] && export ESCALADE_CLUSTER="unknown"

[ -z "$ESCALADE_INTERNAL_DIR" ] && echo -e "${RED}ESCALADE_INTERNAL_DIR not defined.. environment not ready${NC}" >&2
[ -z "$ESCALADE_INTERNAL_DIR" ] && return 1

mkdir -p $ESCALADE_INTERNAL_DIR
[ $? -eq 1 ] && return 1

#
# Definition of the main production variables
#
if [ ! -z "$(echo $ESCALADE_PRODUCTION | grep -E $ESCALADE_INTERNAL_DIR)" ]; then
    
    export ESCALADE_BATCH_UNIQID="escalade"
    export ESCALADE_INPUT0=$ESCALADE_PRODUCTION
    export ESCALADE_LOGDIR0=$ESCALADE_PRODUCTION
    export ESCALADE_TMPDIR=$ESCALADE_PRODUCTION
    export ESCALADE_OUTPUT0=$ESCALADE_PRODUCTION
    
    export ESCALADE_SANDBOX0=$ESCALADE_PRODUCTION/sandbox
    mkdir -p $ESCALADE_SANDBOX0
    [ -z "$ESCALADE_BATCH_NAME" ] && export ESCALADE_BATCH_NAME="escalade-$(date '+%Y%m%d-%H%M%S')-$(echo $RANDOM$RANDOM | md5sum | awk '{print $1}')"
    if [ -z "$ESCALADE_SANDBOX" ]; then
        
        export ESCALADE_SANDBOX=$ESCALADE_SANDBOX0/$ESCALADE_BATCH_NAME
    fi
    
    mkdir -p $ESCALADE_SANDBOX
    export ESCALADE_BATCH_ERR=$ESCALADE_SANDBOX/$ESCALADE_BATCH_NAME.err
    export ESCALADE_BATCH_OUT=$ESCALADE_SANDBOX/$ESCALADE_BATCH_NAME.out
    export ESCALADE_BATCH_ENV=$ESCALADE_SANDBOX/$ESCALADE_BATCH_NAME.env
    
else
    [ -z "$ESCALADE_PRODUCTION" ] && echo -e "\t${RED}Error <directory> is not defined in XML file.${NC}" >&2
    [ -z "$ESCALADE_PRODUCTION" ] && return 1
    
    echo -e "\tStandard production directory created.."
    export ESCALADE_INPUT0=$ESCALADE_PRODUCTION/input
    export ESCALADE_LOGDIR0=$ESCALADE_PRODUCTION/logs
    export ESCALADE_TMPDIR=$ESCALADE_PRODUCTION/.tmp
    
    [ ! -z "$ESCALADE_ID" ] && export ESCALADE_HISTORY=$ESCALADE_PRODUCTION/history.$ESCALADE_ID
    
    [[ -z "$ESCALADE_OUTPUT0" || "$ESCALADE_OUTPUT0" == "$ESCALADE_PRODUCTION/output" ]] && esc_echo "\t${ORANGE}Outgoing directory <output> is not defined. Set by default to ${ESCALADE_PRODUCTION}/output)${NC}"
    
    export ESCALADE_BATCH_NAME="escalade-$(date '+%Y%m%d-%H%M%S')-$(echo $RANDOM$RANDOM | md5sum | awk '{print $1}')"
    export ESCALADE_BATCH_UNIQID=$(echo "$ESCALADE_OUTPUT0 $ESCALADE_JOBLIST $RANDOM$RANDOM $(date)" | md5sum | awk '{print $1}')
    export ESCALADE_UNIQID=$(echo "$ESCALADE_OUTPUT0" | md5sum | awk '{print $1}')
fi

[[ -z "$ESCALADE_ID" && -z "$ESCALADE_HISTORY" ]] && export ESCALADE_HISTORY=$ESCALADE_PRODUCTION/history.0
[ -z "$ESCALADE_UNIQID" ] && export ESCALADE_UNIQID=$RANDOM$RANDOM$RANDOM


# To be redefined.. prepenv is called within sourced scripts.. function are not passed thought it when calling "escalade"
function esc_echo0
{
    OPT=${@:1:$(($#-1))}
    STR="${@: -1}"
    
    [ ! -z "$PWD" ] && STR=$($(which echo) "$STR" | sed "s,$PWD,\\\033\[35m\$PWD\\\033\[0m,g")
    [ ! -z "$HOME" ] && STR=$($(which echo) "$STR" | sed "s,$HOME,\\\033\[35m\$HOME\\\033\[0m,g")
    
    $(which echo) -e $OPT "$STR"
}

function esc_echo
{
    OPT=${@:1:$(($#-1))}
    STR="${@: -1}"
    
    if [[ -z "$ESCALADE_DEBUG" && -z "$ESCALADE_FASTMODE" ]]; then
        
        [ ! -z "$ESCALADE_INTERNAL_DIR" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_INTERNAL_DIR,\\\033\[34m\$ESCALADE_INTERNAL_DIR\\\033\[0m,g")
        [ ! -z "$ESCALADE_SANDBOX0" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_SANDBOX0,\\\033\[34m\$ESCALADE_SANDBOX0\\\033\[0m,g")
        [ ! -z "$ESCALADE_INPUT0" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_INPUT0,\\\033\[34m\$ESCALADE_INPUT0\\\033\[0m,g")
        [ ! -z "$ESCALADE_LOGDIR0" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_LOGDIR0,\\\033\[34m\$ESCALADE_LOGDIR0\\\033\[0m,g")
        [ ! -z "$ESCALADE_TMPDIR" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_TMPDIR,\\\033\[34m\$ESCALADE_TMPDIR\\\033\[0m,g")
        [ ! -z "$ESCALADE_OUTPUT0" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_OUTPUT0,\\\033\[34m\$ESCALADE_OUTPUT0\\\033\[0m,g")
        [ ! -z "$ESCALADE_PRODUCTION" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_PRODUCTION,\\\033\[34m\$ESCALADE_PRODUCTION\\\033\[0m,g")
        [ ! -z "$ESCALADE" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE,\\\033\[34m\$ESCALADE\\\033\[0m,g")
        [ ! -z "$ESCALADE_PWD" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_PWD,\\\033\[34m\$ESCALADE_PWD\\\033\[0m,g")
        [ ! -z "$ESCALADE_XMLCONFIG" ] && STR=$($(which echo) "$STR" | sed "s,$ESCALADE_XMLCONFIG,\\\033\[34m\$ESCALADE_XMLCONFIG\\\033\[0m,g")
    fi
    
    STR="$(esc_echo0 $OPT $STR)"
    
    $(which echo) -e $OPT "$STR"
}

# Useful XML functions
function count {
    
    local XMLELEMENT=$1
    local XMLFILE=$2
    
    [ ! -f $XMLFILE ] && echo 0
    $ESCALADE/bin/xmllint --xpath "count($XMLELEMENT)" $XMLFILE
}

function print {
    
    local XMLELEMENT=$1
    local XMLFILE=$2
    
    [ ! -f $XMLFILE ] && exit 1
    local XMLRESULT=$($ESCALADE/bin/xmllint --xpath "string(${XMLELEMENT})" $XMLFILE 2> /dev/null)
    
    eval echo "$XMLRESULT" 2> /dev/null
    [ $? -eq 1 ] && eval echo '$XMLRESULT'
}
#
# Replacing absolute path
#
[ ! -z "$ESCALADE_FASTMODE" ] && echo -e "\t${BLUE}Fast mode is enabled !${NC} (skip variable replacement while echoing,..)"
if [[ -z "$ESCALADE_DEBUG" && -z "$ESCALADE_FASTMODE" ]]; then
    
    echo -e "\t${BLUE}Replacing absolute path by variable.. (let's make messages shorter) !${NC}"
    [ ! -z "$ESCALADE" ] && esc_echo0 "\t${BLUE}ESCALADE=${NC}$ESCALADE"
    [ ! -z "$ESCALADE_PWD" ] && echo -e "\t${BLUE}ESCALADE_PWD=${NC}$ESCALADE_PWD"
    [ ! -z "$ESCALADE_INTERNAL_DIR" ] && esc_echo0 "\t${BLUE}ESCALADE_INTERNAL_DIR=${NC}$ESCALADE_INTERNAL_DIR"
    [ ! -z "$ESCALADE_SANDBOX0" ] && esc_echo0 "\t${BLUE}ESCALADE_SANDBOX0=${NC}$ESCALADE_SANDBOX0"
    [ ! -z "$ESCALADE_PRODUCTION" ] && esc_echo0 "\t${BLUE}ESCALADE_PRODUCTION=${NC}$ESCALADE_PRODUCTION"
    [ ! -z "$ESCALADE_INPUT0" ] && esc_echo0 "\t${BLUE}ESCALADE_INPUT0=${NC}$ESCALADE_INPUT0"
    [ ! -z "$ESCALADE_OUTPUT0" ] && esc_echo0 "\t${BLUE}ESCALADE_OUTPUT0=${NC}$ESCALADE_OUTPUT0"
    [ ! -z "$ESCALADE_LOGDIR0" ] && esc_echo0 "\t${BLUE}ESCALADE_LOGDIR0=${NC}$ESCALADE_LOGDIR0"
    [ ! -z "$ESCALADE_TMPDIR" ] && esc_echo0 "\t${BLUE}ESCALADE_TMPDIR=${NC}$ESCALADE_TMPDIR"
fi

# #####
# Preparing escalade pilots
# #####

#
# Check software definition
#
[ -z "$ESCALADE_SOFTWARE" ] && esc_echo "\t${RED}Error <software> is not defined in XML file.${NC}" >&2
[ -z "$ESCALADE_SOFTWARE" ] && return 1

export ESCALADE_SOFTWARE_DIR=$ESCALADE/pilots/software/$ESCALADE_SOFTWARE
esc_echo "\tLoading software configuration from here: $ESCALADE_SOFTWARE_DIR"

if [ -z "$(cat $ESCALADE_SOFTWARE_DIR/valid_clusters 2> /dev/null | grep -e $ESCALADE_CLUSTER -e ^all$)" ]; then
    
    esc_echo "${RED}\t[Warning] \"$ESCALADE_SOFTWARE\" software cannot be run on \"$ESCALADE_CLUSTER\" cluster.${NC}" >&2
    esc_echo "${RED}\t          You need to do some tests and add it in the list of valid cluster" >&2
    esc_echo "${RED}\t          ($ESCALADE_SOFTWARE_DIR/valid_clusters)${NC}" >&2
    exit 1
fi

#
# Batch definition
#

if [[ ! -z "$ESCALADE_DEBUG" ]]; then
    
    if [[ -z "$ESCALADE_BATCHMODE" || "$ESCALADE_BATCHMODE" == 0 ]]; then
        
        export ESCALADE_BATCH="fakebatch"
    fi
fi

if [ -z "$ESCALADE_BATCH" ]; then
    
    esc_echo "${ORANGE}\tNo batch system found.. \"fakebatch\" will be used by default${NC}"
    esc_echo "${ORANGE}\tPlease define \"ESCALADE_BATCH\" using one of the known batch pilot${NC} (escalade internal batch-list)"
    export ESCALADE_BATCH="fakebatch"
fi

if [[ -z "$(cat $ESCALADE_CLUSTER_DIR/valid_batch 2> /dev/null | grep $ESCALADE_BATCH)" && "$ESCALADE_CLUSTER" != "unknown" && "$ESCALADE_BATCH" != "fakebatch" ]]; then
    
    esc_echo "${RED}\t[Warning] Unvalid batch system.. you cannot use \"$ESCALADE_BATCH\" on \"$ESCALADE_CLUSTER\" cluster${NC}" >&2
    exit 1
fi

export ESCALADE_BATCH_DIR=$ESCALADE/pilots/batch/$ESCALADE_BATCH
esc_echo "\tLoading batch configuration from here: $ESCALADE_BATCH_DIR"

source $ESCALADE_BATCH_DIR/unsetenv.sh
source ${ESCALADE_BATCH_DIR}/setenv.sh
[ $? -eq 1 ] && return 1

[ -z "$ESCALADE_BATCH_JOBLAUNCHER" ] && esc_echo "\t${RED}ESCALADE_BATCH_JOBLAUNCHER not defined.. environment not ready${NC}" >&2
[ -z "$ESCALADE_BATCH_JOBLAUNCHER" ] && return 1
[ -z "$ESCALADE_BATCH_JOBWRAPPER" ] && esc_echo "\t${RED}ESCALADE_BATCH_JOBWRAPPER not defined.. environment not ready${NC}" >&2
[ -z "$ESCALADE_BATCH_JOBWRAPPER" ] && return 1

# Preparation of batch job options
if [ $ESCALADE_BATCH_UNIQID != "escalade" ]; then
    
    export ESCALADE_BATCH_TITLE="esc${ESCALADE_BATCH_SEPARATOR}$ESCALADE_NAME"
    export ESCALADE_BATCH_OUT=$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}.out
    export ESCALADE_BATCH_ERR=$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}.err
    export ESCALADE_BATCH_ENV=$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}.env
    
    if [ ! -z "$ESCALADE_BATCH_SID" ]; then
        
        export ESCALADE_BATCH_SUBOUT=$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}-${ESCALADE_BATCH_SID}.out
        export ESCALADE_BATCH_SUBERR=$ESCALADE_LOGDIR0/.${ESCALADE_BATCH_UNIQID}-${ESCALADE_BATCH_SID}.err
    fi
    
else
    export ESCALADE_BATCH_TITLE="esc${ESCALADE_BATCH_SEPARATOR}sandbox"
fi

#
# Prepare production dir
#
source $ESCALADE_SETFS $ESCALADE_OUTPUT0 #> /dev/null
[ $? -eq 1 ] && return 1
mkdir -p $ESCALADE_PRODUCTION 2> /dev/null
if [ $? -eq 1 ]; then
    
    echo -e "\t${RED}Cannot create production directory \"$ESCALADE_PRODUCTION\"${NC}" >&2
    return 1
fi

if [ -d "$ESCALADE_PRODUCTION" ]; then
    
    mkdir -p $ESCALADE_TMPDIR 2> /dev/null
    if [ $? -eq 1 ]; then
        
        echo -e "\t${RED}Cannot create temporary directory \"$ESCALADE_TMPDIR\"${NC}" >&2
        return 1
    fi
    
    mkdir -p $ESCALADE_LOGDIR0 2> /dev/null
    if [ $? -eq 1 ]; then
        
        echo -e "\t${RED}Cannot create log root directory \"$ESCALADE_LOGDIR0\"${NC}" >&2
        return 1
    fi
    
    if [ ! -z "$ESCALADE_LOGDIR" ]; then
        
        mkdir -p $ESCALADE_LOGDIR 2> /dev/null
        if [ $? -eq 1 ]; then
            
            echo -e "\t${RED}Cannot create log directory \"$ESCALADE_LOGDIR\"${NC}" >&2
            return 1
        fi
    fi
    
    mkdir -p $ESCALADE_INPUT0 2> /dev/null
    if [ $? -eq 1 ]; then
        
        echo -e "\t${RED}Cannot create input root directory \"$ESCALADE_INPUT0\"${NC}" >&2
        return 1
    fi
    if [ ! -z "$ESCALADE_INPUT" ]; then
        
        mkdir -p $ESCALADE_INPUT 2> /dev/null
        if [ $? -eq 1 ]; then
            
            echo -e "\t${RED}Cannot create input directory \"$ESCALADE_INPUT\"${NC}" >&2
            return 1
        fi
    fi
    
    if [ ! -z "$(echo $FS_MKDIR | grep -E ^xrdcp)" ]; then
        
        if [ -z "$($FS_LS $FS_ADDR${ESCALADE_OUTPUT#$FS_ADDR} 2> /dev/null)" ]; then
            
            $FS_MKDIR $FS_ADDR${ESCALADE_OUTPUT0#$FS_ADDR} 2> /dev/null
            if [ $? -eq 1 ]; then
                
                echo -e "\t${RED}Cannot create output directory \"$ESCALADE_OUTPUT0\" (xrdcp protocol detected)${NC}" >&2
                return 1
            fi
        fi
        
        if [[ ! -z "$ESCALADE_OUTPUT" && -z "$($FS_LS $FS_ADDR${ESCALADE_OUTPUT#$FS_ADDR} 2> /dev/null)" ]]; then
            
            $FS_MKDIR $FS_ADDR${ESCALADE_OUTPUT#$FS_ADDR} 2> /dev/null
            if [ $? -eq 1 ]; then
                
                echo -e "\t${RED}Cannot create output directory \"$ESCALADE_OUTPUT\" (xrdcp protocol detected)${NC}" >&2
                return 1
            fi
        fi
    else
        if [ -z "$($FS_LS ${ESCALADE_OUTPUT0#$FS_ADDR} 2> /dev/null)" ]; then
            
            $FS_MKDIR ${ESCALADE_OUTPUT0#$FS_ADDR} 2> /dev/null
            if [ $? -eq 1 ]; then
                
                echo -e "\t${RED}Cannot create output directory \"${ESCALADE_OUTPUT0#$FS_ADDR}\"${NC}" >&2
                return 1
            fi
        fi
        
        if [[ ! -z "$ESCALADE_OUTPUT" && -z "$($FS_LS ${ESCALADE_OUTPUT#$FS_ADDR} 2> /dev/null)" ]]; then
            
            $FS_MKDIR ${ESCALADE_OUTPUT#$FS_ADDR} 2> /dev/null
            if [ $? -eq 1 ]; then
                
                echo -e "\t${RED}Cannot create output directory \"${ESCALADE_OUTPUT#$FS_ADDR}\"${NC}" >&2
                return 1
            fi
        fi
    fi
    
    [ "$ESCALADE_BATCH_UNIQID" != "escalade" ] && touch $ESCALADE_LOGDIR0/.pid
fi

echo $ESCALADE_PRODUCTION > $ESCALADE_INTERNAL_DIR/last-sessions

# Check jobs running
if [ -z "$ESCALADE_IJOBS" ]; then
    
    $ESCALADE_BATCHSTATUS
    [ $? -eq 1 ] && return 1
    
    export LS_QSTAT=$(mktemp --suffix -ls-qstat)
    $ESCALADE_GLOBALQSTAT $ESCALADE_LOGDIR0/.pid 2> /dev/null > $LS_QSTAT
    if [ -s "$LS_QSTAT" ]; then
        export ESCALADE_IJOBS=$(cat "$LS_QSTAT" | sort -V | uniq | wc -l)
    else
        export ESCALADE_IJOBS=0
    fi
fi

if [ ! $ESCALADE_IJOBS -eq 0 ]; then
    
    esc_echo "${ORANGE}\tSome jobs ($ESCALADE_IJOBS) are still running, please wait until the end..${NC}"
fi

return 0
