#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then

  echo "$0 : This script has to be sourced !" >&2
  exit 1
fi

if [ -z "$ESCALADE" ]; then

  echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
  return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_MISMATCHING "" $# 0
[ $? -eq 1 ] && return 1

#
# Environment checks
#
source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && return 1

#
# Looking for data + basic checks
#
source $ESCALADE_LSDIR "1111" "0000" "2332"
[ $? -eq 1 ] && exit 1

if [ -z "$LS_QSTAT" ]; then

        LS_QSTAT=$(mktemp --suffix -ls-qstat)
        $ESCALADE_GLOBALQSTAT $ESCALADE_LOGDIR0/.pid 2> /dev/null > $LS_QSTAT
fi

function check_mismatch() {

  if [ ! $# -eq 7 ]; then

    echo "[Usage] check_mismatch <title1> <ls_dir1> <sed1> <title2> <ls_dir2> <sed2> <mandatory>" >&2
    return 1
  fi

  TITLE1="$1"
  TITLE2="$4"

  MANDATORY=$7

  RUNLIST1=$(cat "$2" | sed -n "s,$3,\1,p" | sort -V)
  RUNLIST2=$(cat "$5" | sed -n "s,$6,\1,p" | sort -V)

  [ -z "$RUNLIST1" ] && NRUNLIST1=0 || NRUNLIST1=$(echo "$RUNLIST1" | wc -l)
  [ -z "$RUNLIST2" ] && NRUNLIST2=0 || NRUNLIST2=$(echo "$RUNLIST2" | wc -l)

  echo -ne "\t- Looking for mismatching between \"$TITLE1\", $NRUNLIST1 file(s), and \"$TITLE2\", $NRUNLIST2 file(s) "
  if [ $NRUNLIST1 -eq 0 ]; then
    echo -e "${GREEN}/!\ Empty set..${NC}"
    return 0
  elif [ $NRUNLIST1 -eq $NRUNLIST2 ]; then
	   if [ -z "$ESCALADE_FORCEACTION" ]; then
	    echo -e "${GREEN}/!\ Same numbers.. skip${NC}"
	    return 0
	   else
  	    echo -e "${GREEN}/!\ Same numbers.. ${ORANGE}(--force-action detected)${NC}"
           fi
  else
   echo ""
  fi

  DIFF=$(diff <(echo "$RUNLIST1") <(echo "$RUNLIST2") | grep "<" | awk '{print $2}')
  if [ ! -z "$DIFF" ]; then

    if [[ ("$MANDATORY" != "0" && ! -z "$MANDATORY") || ! $NB_FILES -eq 0 ]]; then

      RUNLIST=$(echo $DIFF | sed 's/ /|/g')
      RUNFILES=$(cat "$2" | grep "$3" 2> /dev/null | grep -E "$RUNLIST" 2> /dev/null)
      for RUNID in $DIFF; do

        source $ESCALADE_JOBINFO "$RUNID"
        if [ $? -eq 1 ]; then

          echo -e "\t${RED}  Error: Auto-detection of the path failed.. aborted${NC}"
          return 1
        fi

        mkdir -p $ESCALADE_TMPDIR/logs/slot-$ESCALADE_JOBSLOT/$ESCALADE_JOBID
        if [[ ("$MANDATORY" != "0" && ! -z "$MANDATORY") ]]; then

	  PATTERN="[ESCALADE] Mismatching detected between \"$TITLE1\" and \"$TITLE2\" .."
	  RETURN=$(LC_ALL=C fgrep "$PATTERN" $ESCALADE_TMPDIR/logs/slot-$ESCALADE_JOBSLOT/$ESCALADE_JOBID/$ESCALADE_JOBID.stderr 2> /dev/null)
	  if [ -z "$RETURN" ]; then
            echo "$PATTERN" >> $ESCALADE_TMPDIR/logs/slot-$ESCALADE_JOBSLOT/$ESCALADE_JOBID/$ESCALADE_JOBID.stderr
          fi
        fi
      done
    fi

    let MISMATCH=1

    return 1
  fi

  return 0
}

#
# Check mismatching config
#
[ -z "$ESCALADE_EXPECTED_FILES" ] && ESCALADE_EXPECTED_FILES=$ESCALADE/pilots/software/$ESCALADE_SOFTWARE/expected_files
[ -z "$ESCALADE_MISMATCHING_CONF" ] && ESCALADE_MISMATCHING_CONF=$ESCALADE/pilots/software/$ESCALADE_SOFTWARE/mismatching.conf

if [ ! -s "$ESCALADE_EXPECTED_FILES" ]; then

  echo -e "${RED}\tEntry script used for mismatching not found.. ($ESCALADE_EXPECTED_FILES)${NC}" >&2
  exit 1
fi

if [ ! -s "$ESCALADE_MISMATCHING_CONF" ]; then

  echo -e "${RED}\tConfiguration script for mismatching not found.. ($ESCALADE_MISMATCHING_CONF)${NC}" >&2
  exit 1
fi

echo -e "${GREEN}--> Mismatching procedure on going..${NC}"
if [ ! -z "$ESCALADE_DISABLE_MISMATCHING" ]; then

        echo -e "\t${RED}* Skip mismatching procedure.. on request (--disable-mismatching)${NC}"
	sleep 2
        return 0
fi

#
# Check mismatching
#
if [ $ESCALADE_IJOBS -eq 0 ]; then

  GLOBALMISMATCH=0

  while read CONF
  do
    CONF=$(echo $CONF | cut -d'#' -f1)
    [ -z "$CONF" ] && continue

    TITLE1=$(echo $CONF | cut -d ':' -f1 | sed 's/^ *//g' | sed 's/ *$//g')
    TITLE2=$(echo $CONF | cut -d ':' -f2 | sed 's/^ *//g' | sed 's/ *$//g')
    MANDATORY=$(echo $CONF | cut -d ':' -f3 | sed 's/^ *//g' | sed 's/ *$//g')

    DEF1=$(cat $ESCALADE_EXPECTED_FILES | grep -E "^[\t[:space:]]*$TITLE1[\t[:space:]]*:" | cut -d'#' -f1) # To ensure no substr: [\t[:space:]]*
    if [ -z "$DEF1" ]; then

      echo -e "${ORANGE}\t  [Warning] Bad configuration; Title \"$TITLE1\" not found.. skip${NC}"
      continue
    fi
    DIR1=$(echo $DEF1 | cut -d ':' -f2 | sed 's/^ *//g' | sed 's/ *$//g')
    LS_DIR1=$(echo $DEF1 | cut -d ':' -f3 | sed 's/^ *//g' | sed 's/ *$//g')
    SED1=$(echo "$DEF1" | cut -d ':' -f4 | sed 's/^ *//g' | sed 's/ *$//g')

    DEF2=$(cat $ESCALADE_EXPECTED_FILES | grep -E "^[\t[:space:]]*$TITLE2[\t[:space:]]*:" | cut -d'#' -f1)
    if [ -z "$DEF2" ]; then

      echo -e "${ORANGE}\t  [Warning] Bad configuration; Title \"$TITLE2\" not found.. skip${NC}"
      continue
    fi
    DIR2=$(echo $DEF2 | cut -d ':' -f2 | sed 's/^ *//g' | sed 's/ *$//g')
    LS_DIR2=$(echo $DEF2 | cut -d ':' -f3 | sed 's/^ *//g' | sed 's/ *$//g')
    SED2=$(echo "$DEF2" | cut -d ':' -f4 | sed 's/^ *//g' | sed 's/ *$//g')

    check_mismatch "$TITLE1" "${!LS_DIR1}" $SED1 "$TITLE2" "${!LS_DIR2}" $SED2 "$MANDATORY"

    if [[ "$MANDATORY" != 0 && "$MISMATCH" == 1 ]]; then

      echo -e "${RED}\t  Mismatching between \"$TITLE1\"/ wrt. \"$TITLE2\"${NC}"
      [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}\t $DEF1"
      [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}\t $DEF2"
      [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}\t $LS_DID1 $LS_DIR2"
      [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}\t $SED1 $SED2"
      [ ! -z "$ESCALADE_DEBUG" ] && echo -e "${ORANGE}\t $(echo "${!LS_DIR1}" | head -n1)  $(echo "${!LS_DIR2}" | head -n1)"

      GLOBALMISMATCH=1;
    fi

    if [[ "$MANDATORY" == 0 && "$MISMATCH" == 1 ]]; then

      echo -e "${ORANGE}\t  Mismatch detected, but it might be fine to have missing \"$TITLE2\"/ wrt. \"$TITLE1\"${NC}"
      echo -e "${ORANGE}\t  (e.g. if input is empty.. no output)${NC}"
    fi
    unset MISMATCH # Reset mismatch variable set in check_mismatch

  done < $ESCALADE_MISMATCHING_CONF
fi

#
# Mismatching with PID file..
#

IPID=1
NPID=$(cat $ESCALADE_LOGDIR0/.pid 2> /dev/null | grep -v "^#" | wc -l)

IRUN=0
INPUT="$(cat $LS_LOGDIR0 2> /dev/null | grep 'input.txt')"
[ -z "$INPUT" ] && NRUN=0 || NRUN=$(echo "$INPUT" | wc -l)

esc_echo -ne "\t- Looking for mismatching between PID file, $NPID line(s), and the $NRUN processed files${NC} "
CHECK_PID=1
if [[ $NPID -eq 0 ]]; then
    echo -e "${GREEN}/!\ No PID entry..${NC}"
    CHECK_PID=0
elif [ $NPID -eq $NRUN ]; then
   if [ -z "$ESCALADE_FORCEACTION" ]; then
 	echo -e "${GREEN}/!\ Same numbers.. skip${NC}"
	CHECK_PID=0
   else
   	echo -e "${GREEN}/!\ Same numbers.. ${ORANGE}(--force-action detected)${NC}"
   fi
else
   echo ""
fi

if [ $CHECK_PID -eq 1 ]; then

IFATAL=0
while read LINE ; do

  [ -z "$LINE" ] && continue;

  ESCALADE_BATCH_PID=$(echo "$LINE" | awk '{ print $1 }')
  MD5SUM=$(echo "$LINE" | awk '{ print $2 }')
  JOBSLOT=$(echo "$LINE" | awk '{ print $3 }')
  RUNID=$(echo "$LINE" | awk '{ print $4 }')

  IPID=$((IPID+1))

  ISJOBOVER=$(cat "$LS_QSTAT" | grep $ESCALADE_BATCH_PID)
  tput el 2> /dev/null
  if [ ! -z "$ISJOBOVER" ]; then

	echo -ne "${ORANGE}\t\t* Processing PID entry #$IPID/$NPID.. Job $ESCALADE_BATCH_PID not finished yet${NC}\r"
	continue;
  else
	echo -ne "\t\t* Processing PID entry #$IPID/$NPID..\r"
	[[ -f "$ESCALADE_LOGDIR0/slot-${JOBSLOT}/$RUNID/input.txt" && -d "$ESCALADE_OUTPUT0/slot-${JOBSLOT}/$RUNID" ]] && continue;

	tput el 2> /dev/null

	    [ -z "$ESCALADE_DEBUG" ] && IFATAL=$(($IFATAL+1))
	    [ $IFATAL -lt 5 ] && esc_echo -e "${RED}\t\t  Mismatching between PID file and run \"$RUNID\" (error added to temporary logfiles)${NC}\r"
	    [ $IFATAL -eq 5 ] && esc_echo -e "\t\t${RED}  [..]${NC}"

	mkdir -p "$ESCALADE_TMPDIR/logs/slot-${JOBSLOT}/$RUNID"

	PATTERN="[ESCALADE] Mismatching between information found in PID file ($MD5SUM), but no logs or output for run \"$RUNID\".."
	RETURN=$(LC_ALL=C fgrep "$PATTERN"  $ESCALADE_TMPDIR/logs/slot-${JOBSLOT}/$RUNID/$RUNID.stderr 2> /dev/null)
	if [ -z "$RETURN" ]; then
	  echo "$PATTERN" >> $ESCALADE_TMPDIR/logs/slot-${JOBSLOT}/$RUNID/$RUNID.stderr
        fi
	GLOBALMISMATCH=1
  fi
done <<< "$(cat $ESCALADE_LOGDIR0/.pid 2> /dev/null | grep -v '^#')"

fi

esc_echo -ne "\t- Looking for mismatching between the $NRUN processed files and PID file, $NPID line(s)${NC} "

CHECK_PID=1
if [[ $NRUN -eq 0 ]]; then
    echo -e "${GREEN}/!\ No processed entry..${NC} "
    CHECK_PID=0
elif [ $NPID -eq $NRUN ]; then
   if [ -z "$ESCALADE_FORCEACTION" ]; then
        echo -e "${GREEN}/!\ Same numbers.. skip${NC}"
        CHECK_PID=0
   else
       	echo -e "${GREEN}/!\ Same numbers.. ${ORANGE}(--force-action detected)${NC}"
   fi
else
   echo ""
fi

if [ $CHECK_PID -eq 1 ]; then
IFATAL=0
IRUN=0
while read LINE ; do

  [ -z "$LINE" ] && continue;

  source $ESCALADE_JOBINFO "$LINE"
  IRUN=$((IRUN+1))

  tput el 2> /dev/null
  echo -ne "\t\t* Processing ID #$ESCALADE_JOBID (#$IRUN/$NRUN)..\r"

  if [ -z "$(cat $ESCALADE_LOGDIR0/.pid | grep "$(printf '\t')${ESCALADE_JOBID}$(printf '\t')")" ]; then

	tput el 2> /dev/null
	[ -z "$ESCALADE_DEBUG" ] && IFATAL=$(($IFATAL+1))
	[ $IFATAL -lt 5 ] && esc_echo -e "${RED}\t\t  Mismatching between PID file and run \"$ESCALADE_JOBID\" (error added to temporary logfiles)${NC}\r"
	[ $IFATAL -eq 5 ] && esc_echo -e "\t\t${RED}  [..]${NC}"

	mkdir -p "$ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/$ESCALADE_JOBID"

	PATTERN="[MISMATCHING] Information found in processed input (run \"$ESCALADE_JOBID\"), but not in the PID file"
	RETURN=$(LC_ALL=C fgrep "$PATTERN" $ESCALADE_TMPDIR/logs/slot-${ESCALDAE_JOBSLOT}/$ESCALADE_JOBID/$ESCALADE_JOBID.stderr 2> /dev/null)
        if [ -z "$RETURN" ]; then
	  echo "$PATTERN" >> $ESCALADE_TMPDIR/logs/slot-${ESCALADE_JOBSLOT}/$ESCALADE_JOBID/$ESCALADE_JOBID.stderr
        fi
	GLOBALMISMATCH=1
  fi
done <<< "$(echo "$INPUT")"
fi

if [[ "$GLOBALMISMATCH" == "1" ]]; then
  echo ""
  echo -e "${RED}--> Manual action required.. Mismatching detected..${NC}"
  echo "    Errors during the production may have occurred !"
  return 1
fi

return 0

