#! /bin/bash

#
# Usage checks
#
if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_GETINFO "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && exit 1

#
# Looking for data + basic checks
#
source $ESCALADE_LSDIR "1111" "0000" "2333"
[ $? -eq 1 ] && exit 1

#
# Check config
#
[ -z "$ESCALADE_EXPECTED_FILES" ] && ESCALADE_EXPECTED_FILES=$ESCALADE/pilots/software/$ESCALADE_SOFTWARE/expected_files
if [ ! -s "$ESCALADE_EXPECTED_FILES" ]; then
    
    echo -e "${RED}\tEntry script to find the expected files not found.. ($ESCALADE_EXPECTED_FILES)${NC}" >&2
    exit 1
fi


# Exception : Warning in case of not all files have been processed..
NB_INPUT0=$(cat "$LS_INPUT0" | grep -E "txt$" | wc -l)
NB_PROCESSED_INPUT0=$(cat "$LS_LOGDIR0" | grep -E "txt$" | wc -l)
[[ $NB_INPUT0 > $NB_PROCESSED_INPUT0 ]] && echo -e "${ORANGE}\tNB: Input files have not all been processed /!\ ${NC}"

if [ "$NB_INPUT0" == "0" ]; then
    
    esc_echo "${ORANGE}\tProduction #$ESCALADE_ID is ready to be started, but has not yet been done..${NC}"
    exit 0
fi

#
# Get some numbers of the production (inputfiles, processed inputfiles, histograms, etc..)
# Exception : Warning in case of not all files have been processed..
#
esc_echo "${GREEN}--> Some numbers for $ESCALADE_PRODUCTION${NC} ($(date))"

while read DEF
do
    DEF=$(echo "$DEF" | cut -d'#' -f1)
    [ -z "$DEF" ] && continue
    
    TITLE=$(echo $DEF | cut -d ':' -f1 | sed 's/^ *//g' | sed 's/ *$//g') # sed used to trim str
    DIR=$(echo $DEF | cut -d ':' -f2 | sed 's/^ *//g' | sed 's/ *$//g')
    LS_DIR=$(echo $DEF | cut -d ':' -f3 | sed 's/^ *//g' | sed 's/ *$//g')
    SED=$(echo $DEF | cut -d ':' -f4 | sed 's/^ *//g' | sed 's/ *$//g')
    if [ -z "$TITLE" ]; then
        
        echo -e "${RED}\t- One of the title is empty in the configuration file..${NC}" >&2
        exit 1
    fi
    
    if [[ -z "${!DIR}" ]]; then
        
        echo -e "${RED}\t- Error in configuration file.. for \"$TITLE\"${NC}" >&2
        exit 1
    fi
    
    [ -s "${!LS_DIR}" ] && NB_FILES=$(cat "${!LS_DIR}" | grep -E "$SED" 2> /dev/null | wc -l) || NB_FILES=0
    
    
    esc_echo "\t\e[4m$NB_FILES $TITLE\e[0m in ${!DIR}"
    
done < $ESCALADE_EXPECTED_FILES

[ "$RUN" != '.*' ] && echo -e "\t${ORANGEBKG}Remember that RUN is set to \"$RUN\"${NC} (therefore displayed data are filtered based on it)"
