#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

export ESCALADE_READLINK="readlink -m"

DIR=$(dirname $0)
LS_CLUSTER=$(ls $DIR/../pilots/clusters/)

for CLUSTER in $LS_CLUSTER; do
    
    for HOSTNAME_KNOWN in $(cat "$DIR/../pilots/clusters/$CLUSTER/hostname" 2> /dev/null); do
        
        if [ ! -z "$(hostname | grep -E "^$HOSTNAME_KNOWN$")" ]; then
            
            echo $CLUSTER
            exit 0
        fi
    done
done

echo "unknown"
exit 0
