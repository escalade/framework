#! /bin/bash

echo "Copying ${ESCALADE_NODEDIR} TO ${ESCALADE_OUTPUT}"
date
cp -r $ESCALADE_NODEDIR $ESCALADE_OUTPUT
echo "Moving logfiles to ${ESCALADE_LOGDIR} and moving output subdirectories up"
date
export DIR=$(basename $ESCALADE_NODEDIR)
for FOLDER in $(ls $ESCALADE_OUTPUT/$DIR); do
    mkdir $ESCALADE_LOGDIR/$FOLDER
    mv $ESCALADE_OUTPUT/$DIR/$FOLDER/input.txt $ESCALADE_LOGDIR/$FOLDER/
    mv $ESCALADE_OUTPUT/$DIR/$FOLDER/*.stdout $ESCALADE_LOGDIR/$FOLDER/
    mv $ESCALADE_OUTPUT/$DIR/$FOLDER/*.stderr $ESCALADE_LOGDIR/$FOLDER/
    mv $ESCALADE_OUTPUT/$DIR/$FOLDER/*.xml $ESCALADE_LOGDIR/$FOLDER/
    mv $ESCALADE_OUTPUT/$DIR/$FOLDER $ESCALADE_OUTPUT
done
echo "Deleting (empty) node directory copy"
date
rm -r $ESCALADE_OUTPUT/$DIR
echo "Transfer done"
date
