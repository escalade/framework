#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_CLEANLOG "" $# 0
[ $? -eq 1 ] && return 1

# Check the safethread lock
source $ESCALADE_SAFETHREAD > /dev/null
[ $? -eq 1 ] && exit 1

source $ESCALADE_LSDIR "1111" "0000" "2333"
[ $? -eq 1 ] && exit 1

#
# Environment checks
#

source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && return 1

#
# Pipe error messages from stdout to stderr
#
esc_echo -e "${GREEN}--> Start analyzing logfiles from $ESCALADE_TMPDIR${NC}"
if [ ! -z "$ESCALADE_DISABLE_LOGS" ]; then
    
    echo -e "\t${RED}* Skip log checking procedure.. on request (--disable-logs)${NC}"
    sleep 2
    exit 0
fi

#
# Remove harmless error messages from stderr, only in the temporary logdirectory
#
ESCALADE_TESTFILE="$ESCALADE_TESTFILE_ARGUMENT $ESCALADE_TESTFILE0"
ESCALADE_TESTFILE_AUTOFOUND=$(find $PWD -maxdepth 1 -name '*.extra' 2> /dev/null| tr '\n' ' ')
ESCALADE_TESTFILE="$ESCALADE_TESTFILE $ESCALADE_TESTFILE_AUTOFOUND"
ESCALADE_TESTFILE_AUTOFOUND=$(find $ESCALADE/pilots/software/escalade -maxdepth 1 -name '*.extra' 2> /dev/null | tr '\n' ' ')
ESCALADE_TESTFILE="$ESCALADE_TESTFILE $ESCALADE_TESTFILE_AUTOFOUND"
#ESCALADE_TESTFILE_AUTOFOUND=$(find $ESCALADE/pilots/clusters/$ESCALADE_CLUSTER -maxdepth 1 -name '*.extra' 2> /dev/null | tr '\n' ' ')
#ESCALADE_TESTFILE="$ESCALADE_TESTFILE $ESCALADE_TESTFILE_AUTOFOUND"
if [ "$ESCALADE_SOFTWARE" != "escalade" ]; then
    ESCALADE_TESTFILE_AUTOFOUND=$(find $ESCALADE/pilots/software/$ESCALADE_SOFTWARE -maxdepth 1 -name '*.extra' 2> /dev/null | tr '\n' ' ')
    ESCALADE_TESTFILE="$ESCALADE_TESTFILE $ESCALADE_TESTFILE_AUTOFOUND"
fi

tput el 2> /dev/null
echo -e "${GREEN}--> Looking for extra testfiles..${NC} $(echo $ESCALADE_TESTFILE | wc -w) found(s)"
function extratest
{
    local FILELIST=$1
    for FILE in $FILELIST; do
        
        local EXTRAFILE=$FILE
        
        if [ ! -f "$EXTRAFILE" ]; then
            
            esc_echo "${ORANGE}--> Extra test file not found..${NC} ($EXTRAFILE)" >&2
            continue;
        fi
        
        esc_echo "${GREEN}--> Applying extra test..${NC} $EXTRAFILE"
        
        local IRUN=0
        local NRUN=$(cat "$LS_LOGDIR0_TMP" | grep -E "stdout$" | wc -l)
        
        for STDOUT in $(cat "$LS_LOGDIR0_TMP" | grep -E "stdout$" 2> /dev/null); do
            
            IRUN=$(($IRUN+1))
            [[ ! -z "$RUN" && -z "$(echo "$STDOUT" | grep -E "$RUN")" ]] && continue;
            STDERR=$(dirname $STDOUT)/$(basename $STDOUT .stdout).stderr
            
            # Check the safethread lock
            source $ESCALADE_SAFETHREAD > /dev/null
            [ $? -eq 1 ] && exit 1
            
            source $ESCALADE_JOBINFO $STDERR
            export ESCALADE_JOBWORKDIR=$ESCALADE_TMPDIR/logs/slot-$ESCALADE_JOBSLOT/$ESCALADE_JOBID
            mkdir -p $ESCALADE_JOBWORKDIR
            
            ## HERE...
            EXTRATEST_OUTPUT="$(source $EXTRAFILE 2>&1)"
            if [ $? -eq 1 ]; then
                
                ESCALADE_BATCH_ID=$(cat $ESCALADE_LOGDIR0/.pid | grep -E $'\t'"$ESCALADE_JOBID"$'\t' | awk '{print $1}' | uniq)
                echo -e "\t${ORANGE} [ESCALADE TEST] Extra test run ID #$(basename $STDOUT .stdout) : FAILED ($IRUN/$NRUN)${NC}"
                if [[ ! -z "$ESCALADE_BATCH_ID" && ! -z "$(cat "$LS_QSTAT" | grep "$ESCALADE_BATCH_ID")" ]]; then
                    
                    EXTRATEST_OUTPUT=$(echo "$EXTRATEST_OUTPUT" | sed "s,^,\t\[ESCALADE TEST\] Extra test \($(basename $EXTRAFILE)\) : ,g")
                    echo -e "${ORANGE}\t (Nothing added into the temporary logfiles.. job still running mode detected)${NC}"
                    echo -e "${ORANGE}\t Outgoing message:${NC}"
                    echo -e "$EXTRATEST_OUTPUT"
                    
                    elif [[ ! -z "$ESCALADE_DEBUG" ]]; then
                    
                    EXTRATEST_OUTPUT=$(echo "$EXTRATEST_OUTPUT" | sed "s,^,\t\[ESCALADE TEST\] Extra test \($(basename $EXTRAFILE)\) : ,g")
                    echo -e "${ORANGE}\t (Nothing added into the temporary logfiles.. debug mode detected)${NC}"
                    echo -e "${ORANGE}\t Outgoing message:${NC}"
                    echo -e "$EXTRATEST_OUTPUT"
                else
                    if [ -z "$(grep ".ESCALADE TEST. Extra test .$(basename $EXTRAFILE). : FAILED" $STDERR)" ]; then
                        
                        EXTRATEST_OUTPUT=$(echo "$EXTRATEST_OUTPUT" | sed "s,^,\[ESCALADE TEST\] Extra test \($(basename $EXTRAFILE)\) : ,g")
                        echo "[ESCALADE TEST] Extra test ($(basename $EXTRAFILE)) : FAILED" >> $STDERR
                        echo "$EXTRATEST_OUTPUT" >> $STDERR
                    fi
                fi
            else
                tput el 2> /dev/null
                if [ ! -z "$ESCALADE_DEBUG" ]; then
                    
                    echo -e "\t${ORANGE} [ESCALADE TEST] Extra test run ID #$(basename $STDOUT .stdout) : SUCCEEDED ($IRUN/$NRUN)${NC}"
                    echo -e "${ORANGE}$EXTRATEST_OUTPUT${NC}"
                else
                    echo -ne "\t- Processing file ID #$ESCALADE_JOBID.. ($IRUN/$NRUN)\r"
                    if [ -z "$(grep ".ESCALADE TEST. Extra test .$(basename $EXTRAFILE). : SUCCEEDED" $STDOUT)" ]; then
                        
                        EXTRATEST_OUTPUT=$(echo "$EXTRATEST_OUTPUT" | sed "s,^,\[ESCALADE TEST\] Extra test \($(basename $EXTRAFILE)\) : ,g")
                        echo "[ESCALADE TEST] Extra test ($(basename $EXTRAFILE)) : SUCCEEDED" >> $STDOUT
                        echo -e "$EXTRATEST_OUTPUT" >> $STDOUT
                    fi
                fi
            fi
        done
        
        [ "$RUN" != '.*' ] && echo -e "\t${ORANGEBKG}Remember that RUN is set to \"$RUN\"${NC} (therefore displayed data are filtered based on it)"
    done
}

if [ -z "$ESCALADE_DISABLE_EXTRATEST" ]; then
    extratest "$ESCALADE_EXTRATEST"
else
    echo -e "\t${RED}* Skip logs extratest procedure.. on request (--disable-logs-extratest)${NC}"
fi

ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE_ARGUMENT $ESCALADE_PIPEFILE0"
ESCALADE_PIPEFILE_AUTOFOUND=$(find $PWD -maxdepth 1 -name '*.pipe' 2> /dev/null| tr '\n' ' ')
ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE $ESCALADE_PIPEFILE_AUTOFOUND"
ESCALADE_PIPEFILE_AUTOFOUND=$(find $ESCALADE/pilots/software/escalade -maxdepth 1 -name '*.pipe' 2> /dev/null | tr '\n' ' ')
ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE $ESCALADE_PIPEFILE_AUTOFOUND"
ESCALADE_PIPEFILE_AUTOFOUND=$(find $ESCALADE/pilots/batch/$ESCALADE_BATCH -maxdepth 1 -name '*.pipe' 2> /dev/null | tr '\n' ' ')
ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE $ESCALADE_PIPEFILE_AUTOFOUND"
ESCALADE_PIPEFILE_AUTOFOUND=$(find $ESCALADE/pilots/clusters/$ESCALADE_CLUSTER -maxdepth 1 -name '*.pipe' 2> /dev/null | tr '\n' ' ')
ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE $ESCALADE_PIPEFILE_AUTOFOUND"
if [ "$ESCALADE_SOFTWARE" != "escalade" ]; then
    ESCALADE_PIPEFILE_AUTOFOUND=$(find $ESCALADE/pilots/software/$ESCALADE_SOFTWARE -maxdepth 1 -name '*.pipe' 2> /dev/null | tr '\n' ' ')
    ESCALADE_PIPEFILE="$ESCALADE_PIPEFILE $ESCALADE_PIPEFILE_AUTOFOUND"
fi

tput el 2> /dev/null
echo -e "${GREEN}--> Looking for the pipe files..${NC} $(echo $ESCALADE_PIPEFILE | wc -w) found(s)"

function pipe
{
    local FILELIST=$1
    
    local IPIPE=0
    local NPIPE=20
    local IFILE=0
    local NFILE=$(echo "$FILELIST" | wc -w)
    
    local FINAL_GREP=""
    for FILE in $FILELIST; do
        
        IFILE=$((IFILE+1))
        local PIPEFILE=$FILE
        if [ ! -f "$PIPEFILE" ]; then
            
            esc_echo "${ORANGE}--> Pipe file not found..${NC} ($PIPEFILE)" >&2
            continue
        fi
        
        esc_echo "${GREEN}--> Reading pipe file..${NC} $PIPEFILE"
        
        NLINE=0
        while read GREP
        do
            GREP="$(echo "$GREP" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$GREP" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            
            NLINE=$((NLINE+1))
        done < "$PIPEFILE"
        
        [ $NLINE -eq 0 ] && continue;
        
        ILINE=0
        while read GREP
        do
            GREP="$(echo "$GREP" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$GREP" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            
            IPIPE=$((IPIPE+1))
            ILINE=$((ILINE+1))
            FINAL_GREP="-e \"$GREP\" $FINAL_GREP"
            echo -e "\t- Piping \"$GREP\" from stdout to stderr.. ($ILINE out of $NLINE)"
            
            if [[ $(($IPIPE % $NPIPE)) -eq 0 || ($IFILE -eq $NFILE && $NLINE -eq $ILINE) || ! -z "$ESCALADE_DEBUG" ]]; then
                
                tput el 2> /dev/null
                echo -e "${ORANGE}\t\t* Processing buffer..${NC} Please wait.."
                
                # Apply the piping informations
                local IRUN=1
                local NRUN=$(cat "$LS_LOGDIR0_TMP" | grep -E "stdout$" | wc -l)
                
                MATCHING_LINES=0
                for STDOUT in $(cat "$LS_LOGDIR0_TMP" | grep -E "stdout$" 2> /dev/null); do
                    
                    [[ ! -z "$RUN" && -z "$(echo "$STDOUT" | grep -E "$RUN")" ]] && continue;
                    
                    # Check the safethread lock
                    source $ESCALADE_SAFETHREAD > /dev/null
                    [ $? -eq 1 ] && exit 1
                    
                    source $ESCALADE_JOBINFO $STDOUT
                    export ESCALADE_JOBWORKDIR=$ESCALADE_TMPDIR/logs/slot-$ESCALADE_JOBSLOT/$ESCALADE_JOBID
                    mkdir -p $ESCALADE_JOBWORKDIR
                    rm -f "$ESCALADE_JOBWORKDIR/$ESCALADE_JOBID.tmp"
                    
                    tput el 2> /dev/null
                    echo -ne "${ORANGE}\t\t  File ID #$ESCALADE_JOBID..${NC} ($IRUN/$NRUN)\r"
                    if [ -s "$ESCALADE_JOBWORKDIR/$ESCALADE_JOBID.stdout" ]; then
                        
                        MATCH=$(eval "LC_ALL=C fgrep --binary-files=text -hi  $FINAL_GREP $ESCALADE_JOBWORKDIR/$ESCALADE_JOBID.stdout")
                        [ ! -z "$MATCH" ] && MATCHING_LINES=$(($MATCHING_LINES+$(echo "$MATCH" | wc -l)))
                        
                        [ ! -z "$MATCH" ] && echo "$MATCH" >> $ESCALADE_JOBWORKDIR/$ESCALADE_JOBID.stderr || touch $ESCALADE_JOBWORKDIR/$ESCALADE_JOBID.stderr
                        eval "LC_ALL=C fgrep --binary-files=text -hiv $FINAL_GREP $ESCALADE_JOBWORKDIR/$ESCALADE_JOBID.stdout" >> $ESCALADE_JOBWORKDIR/$ESCALADE_JOBID.tmp
                        mv $ESCALADE_JOBWORKDIR/$ESCALADE_JOBID.tmp $ESCALADE_JOBWORKDIR/$ESCALADE_JOBID.stdout
                    fi
                    
                    IRUN=$(($IRUN+1))
                done
                
                if [[ ! -z "$ESCALADE_DEBUG" && $IRUN != 1 ]]; then
                    tput el 2> /dev/null
                    echo -e "${ORANGE}\t\t* $MATCHING_LINES line(s) matching within $((IRUN-1))/$NRUN file(s)${NC}"
                fi
            fi
            [ "$RUN" != '.*' ] && echo -e "\t${ORANGEBKG}Remember that RUN is set to \"$RUN\"${NC} (therefore displayed data are filtered based on it)"
        done < "$PIPEFILE"
    done
}

if [ -z "$ESCALADE_DISABLE_PIPEFILE" ]; then
    pipe "$ESCALADE_PIPEFILE"
else
    echo -e "\t${RED}* Skip logs piping procedure.. on request (--disable-logs-pipe)${NC}"
fi

#
# Remove harmless error messages from stderr, only in the temporary logdirectory
#
ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE_ARGUMENT $ESCALADE_SUPPRFILE0"
ESCALADE_SUPPRFILE_AUTOFOUND=$(find $PWD -maxdepth 1 -name '*.suppr' 2> /dev/null| tr '\n' ' ')
ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE $ESCALADE_SUPPRFILE_AUTOFOUND"
ESCALADE_SUPPRFILE_AUTOFOUND=$(find $ESCALADE/pilots/software/escalade -maxdepth 1 -name '*.suppr' 2> /dev/null | tr '\n' ' ')
ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE $ESCALADE_SUPPRFILE_AUTOFOUND"

ESCALADE_SUPPRFILE_AUTOFOUND=$(find $ESCALADE/pilots/batch/$ESCALADE_BATCH -maxdepth 1 -name '*.suppr' 2> /dev/null | tr '\n' ' ')
ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE $ESCALADE_SUPPRFILE_AUTOFOUND"
ESCALADE_SUPPRFILE_AUTOFOUND=$(find $ESCALADE/pilots/clusters/$ESCALADE_CLUSTER -maxdepth 1 -name '*.suppr' 2> /dev/null | tr '\n' ' ')
ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE $ESCALADE_SUPPRFILE_AUTOFOUND"
if [ "$ESCALADE_SOFTWARE" != "escalade" ]; then
    ESCALADE_SUPPRFILE_AUTOFOUND=$(find $ESCALADE/pilots/software/$ESCALADE_SOFTWARE -maxdepth 1 -name '*.suppr' 2> /dev/null | tr '\n' ' ')
    ESCALADE_SUPPRFILE="$ESCALADE_SUPPRFILE $ESCALADE_SUPPRFILE_AUTOFOUND"
fi

tput el 2> /dev/null
echo -e "${GREEN}--> Looking for suppression files..${NC} $(echo $ESCALADE_SUPPRFILE | wc -w) found(s)"

function suppress
{
    tput el 2> /dev/null
    echo -ne "\t\t${ORANGE}* Fetching non-zero temporary logfiles (stderr files only).. ${NC}"
    
    LS_TMP_SUPPR=$(mktemp --suffix -ls-suppr)
    find $ESCALADE_TMPDIR/logs -type f -name "*.stderr" ! -size 0 > "$LS_TMP_SUPPR"
    
    local IRUN=1
    local NRUN=$(cat "$LS_TMP_SUPPR" | grep -E "\.stderr$" | wc -l)
    echo -ne "$NRUN file(s) found..\r"
    
    [ -z "$(cat "$LS_TMP_SUPPR" | grep -E stderr$ 2> /dev/null)" ] && return 0
    
    local FILELIST=$1
    
    local ISUPPR=0
    local NSUPPR=20
    local IFILE=0
    local NFILE=$(echo "$FILELIST" | wc -w)
    
    for FILE in $FILELIST; do
        
        IFILE=$((IFILE+1))
        
        local SUPPRFILE=$FILE
        
        if [ ! -f "$SUPPRFILE" ]; then
            esc_echo "${ORANGE}--> Suppression file not found..${NC} ($SUPPRFILE)" >&2
            continue;
        fi
        
        tput el 2> /dev/null
        esc_echo "${GREEN}--> Suppress harmless errors using..${NC} $SUPPRFILE"
        NLINE=0
        while read CODE
        do
            CODE="$(echo "$CODE" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$CODE" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            NLINE=$((NLINE+1))
        done < "$SUPPRFILE"
        [ $NLINE -eq 0 ] && continue;
        
        local ILINE=0
        while read CODE
        do
            
            CODE="$(echo "$CODE" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$CODE" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            
            ILINE=$(($ILINE+1))
            ISUPPR=$(($ISUPPR+1))
            
            echo -e "\t- Removing \"$CODE\" (sed,regex) warning messages.. ($ILINE out $NLINE)"
            local SEDCODE="-e \"$CODE\" $SEDCODE"
            if [[ $(( $ISUPPR % $NSUPPR )) -eq 0 || ($IFILE -eq $NFILE && $NLINE -eq $ILINE) || ! -z "$ESCALADE_DEBUG" ]]; then
                
                tput el 2> /dev/null
                echo -e "${ORANGE}\t\t* Processing buffer..${NC} Please wait.."
                
                for STDERR in $(cat "$LS_TMP_SUPPR" | grep -E "stderr$" 2> /dev/null); do
                    
                    [[ ! -z "$RUN" && -z "$(echo "$STDERR" | grep -E "$RUN")" ]] && continue;
                    
                    # Check the safethread lock
                    source $ESCALADE_SAFETHREAD > /dev/null
                    [ $? -eq 1 ] && exit 1
                    
                    source $ESCALADE_JOBINFO $STDERR
                    export ESCALADE_JOBWORKDIR=$ESCALADE_TMPDIR/logs/slot-$ESCALADE_JOBSLOT/$ESCALADE_JOBID
                    mkdir -p $ESCALADE_JOBWORKDIR
                    
                    tput el 2> /dev/null
                    echo -ne "${ORANGE}\t\t  File ID #$ESCALADE_JOBID..${NC} ($IRUN/$NRUN)\r"
                    IRUN=$(($IRUN+1))
                    
                    eval "sed $SEDCODE -i $ESCALADE_JOBWORKDIR/$ESCALADE_JOBID.stderr"
                done
                
                SEDCODE=""
                
                tput el 2> /dev/null
                echo -ne "\t\t${ORANGE}* Fetching non-zero temporary logfiles (stderr files only).. ${NC}"
                find $ESCALADE_TMPDIR/logs -type f -name "*.stderr" ! -size 0 > "$LS_TMP_SUPPR"
                
                local IRUN=1
                local NRUN=$(cat "$LS_TMP_SUPPR" | grep -E "\.stderr$" | wc -l)
                echo -e "$NRUN file(s) found.."
                
            fi
        done < "$SUPPRFILE"
        
        [ "$RUN" != '.*' ] && echo -e "\t${ORANGEBKG}Remember that RUN is set to \"$RUN\"${NC} (therefore displayed data are filtered based on it)"
    done
    
    rm -f "$LS_TMP_SUPPR"
    unset LS_TMP_SUPPR
}

if [ -z "$ESCALADE_DISABLE_SUPPRFILE" ]; then
    suppress "$ESCALADE_SUPPRFILE"
else
    echo -e "\t${RED}* Skip logs suppression procedure.. on request (--disable-logs-suppr)${NC}"
fi

return 0
