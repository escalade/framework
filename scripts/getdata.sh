#! /bin/bash

source $ESCALADE_USAGE $0 $ESCALADE_GETDATA "" $# 0
[ $? -eq 1 ] && exit 1

echo -e "${GREEN}--> Getting data from the following path(s).. (Please wait)${NC}"
if [[ ! -z "$ESCALADE_TARPATH" ]]; then
    
    echo -e "\tArchive as input expected.. A tar archive path has been specified: \"$ESCALADE_TARPATH\""
fi


ESCALADE_INPUT_TXT=$(mktemp --suffix -input-txt)

I=1
for INPUT in $ESCALADE_DATA; do
    
    ISTEXTFILE=$(echo "$INPUT" | grep -E "\.txt$")
    if [[ -f ${INPUT} && ! -z "$ISTEXTFILE" ]]; then
        echo -e "\tText file found ${INPUT}"
        cat ${INPUT} >> ${ESCALADE_INPUT_TXT}
    else
        esc_echo "\tInput directory provided.. \"$INPUT\" ($I/$(echo $ESCALADE_DATA | wc -w))"
        $ESCALADE_GETLIST $INPUT >> $ESCALADE_INPUT_TXT
    fi
    
    [ ! -z "$ESCALADE_DEBUG" ] && echo -e "\t${ORANGE}--> Input textfile stored here : $ESCALADE_INPUT_TXT${NC}"
    I=$((I+1))
done

#
# Looking for matching
#
if [ -z "${ESCALADE_DATAPATTERN}" ]; then
    
    echo -e "${RED}--> No extraction pattern found.." >&2
    echo -e "Please check your XML file : <step data-pattern='XXX'>" >&2
    exit 1
fi

echo -e "${GREEN}--> Looking for data matching with the following pattern \"${ESCALADE_DATAPATTERN}\"${NC}"
echo -e "\tAll file(s) found (whatever the pattern) : $(cat ${ESCALADE_INPUT_TXT} | wc -l) file(s)"

let ESCALADE_JOBSLOT=0
mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}

if [[ -f "$ESCALADE_DATARUNLIST" ]]; then
    echo -e "\tData runlist found here: $ESCALADE_DATARUNLIST"
    sleep 1
    
    elif [ ! -z "$ESCALADE_DATARUNLIST" ]; then
    echo -e "${RED}\tData runlist provided, but file not found: $ESCALADE_DATARUNLIST"
    sleep 2
    exit 1
else
    echo -e "${ORANGE}\tNo data runlist provided${NC}"
fi

ESCALADE_GETDATA_PY=$ESCALADE/scripts/getdata.py
ESCALADE_INPUT_VALID=$(mktemp --suffix -input-valid)
$ESCALADE_GETDATA_PY "$ESCALADE_INPUT_TXT" "${ESCALADE_DATAPATTERN}" -r "$ESCALADE_DATARUNLIST" "${ESCALADE_INPUT_VALID}"
#[ $? -eq 0 ] && exit 1

IFILE=1
NVFILE=$(cat ${ESCALADE_INPUT_VALID} | wc -l)
echo -e "\tFile(s) matching with the pattern + runlist: $NVFILE file(s)"
[ "$NFILE" == "0" ] && rm -rf "${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}"

IDLIST=$(cat ${ESCALADE_INPUT_VALID} | awk '{print $2}' | sort | uniq)
NID=$(echo "$IDLIST" | wc -l)
IID=0
IID_MODULO=0

OLD_IFS=$IFS
IFS=$'\n'
for DATA_ID in $IDLIST; do
    
    if [[ $(ls ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT} | wc -l) -ge $ESCALADE_SLOTSIZE ]]; then
        
        tput el 2> /dev/null
        echo -e "\tSlot #$ESCALADE_JOBSLOT has been created.. (Slot size: $ESCALADE_SLOTSIZE)"
        export ESCALADE_JOBSLOT=$((ESCALADE_JOBSLOT+1))
        mkdir -p ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}
    fi
    
    tput el 2> /dev/null
    echo -ne "\tPreparing run #$DATA_ID (#$IID/$NID)\r"
    
    IFS=$OLD_IFS
    FILE=$(cat ${ESCALADE_INPUT_VALID} | awk -v ID="$DATA_ID" '($2 == ID) { print $1 }')
    [ ! -z "$FILE" ] && echo "$FILE" >> ${ESCALADE_TMPDIR}/input/slot-${ESCALADE_JOBSLOT}/${DATA_ID}.txt
    IFS=$'\n'
    
    IID=$(($IID+1))
done
IFS=$OLD_IFS
