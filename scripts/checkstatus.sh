#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_CHECKSTATUS "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
$ESCALADE_ISARCHIVED $ESCALADE_OUTPUT0
[ $? -eq 1 ] && exit 1

source $ESCALADE_LSDIR "1001" "0000" "2003"
[ $? -eq 1 ] && exit 1


rm -f "$ESCALADE_PRODUCTION/error-report.log" "$ESCALADE_PRODUCTION/status-report.log" 2> /dev/null

# Get some numbers of the production (inputfiles, processed inputfiles, histograms, etc..)
source $ESCALADE_GETINFO 2>&1 | tee -a $ESCALADE_PRODUCTION/status-report.log
[ ! -z "$ESCALADE_PRODUCTION" ] && echo -e "\t${BLUE}ESCALADE_PRODUCTION=${NC}$ESCALADE_PRODUCTION"
esc_echo "\t${GREEN}Status report also saved here :${NC} $ESCALADE_PRODUCTION/status-report.log"

# Looking for non-zero size stderr logfiles
FATAL=$(find $ESCALADE_TMPDIR/logs -type f -name "*.stderr" ! -size 0 2> /dev/null | grep -E "$RUN" | sort -V)
IFATAL=0
NFATAL=$(echo "$FATAL" | wc -l)
if [[ ! $NFATAL -eq 0 && ! -z "$FATAL" ]]; then
    
    # Overview of the fatal errors detected
    if [ ! $ESCALADE_IJOBS -eq 0 ]; then
        echo -e "${ORANGE}--> $(echo "$FATAL" | wc -l) log(s) detected with potentially fatal errors among the completed jobs${NC}"
    else
        echo -e "${ORANGE}--> $(echo "$FATAL" | wc -l) log(s) detected with potentially fatal errors${NC}"
    fi
    
    [ ! $ESCALADE_IJOBS -eq 0 ] && echo -e "\t${ORANGE}Some jobs ($ESCALADE_IJOBS) are still running, some log files might not be parsed, because they appeared after the beginning of the parsing procedure..${NC}"
    
    echo -e "\tSome log files might not be parsed, if they appeared after the last check log procedure."
    for FILE in $FATAL; do
        
        [[ ! -z "$RUN" && -z "$(echo "$FILE" | grep -E "$RUN")" ]] && continue;
        echo "$FILE $(ls -lh $FILE | awk '{print $5}')B" >> $ESCALADE_PRODUCTION/error-report.log
        
        [ -z "$ESCALADE_DEBUG" ] && IFATAL=$(($IFATAL+1))
        [ $IFATAL -lt 8 ] && esc_echo -e "\t${ORANGE}Error(s) detected in: $FILE${NC} ($(ls -lh $FILE | awk '{print $5}')B)${NC}"
        [ $IFATAL -eq 8 ] && esc_echo -e "\t${ORANGE}[..]${NC} Please wait (+ Some $(($NFATAL-9)) other error(s))"
    done
    
    if [ $IFATAL -ge 8 ]; then
        
        esc_echo -e "\t${ORANGE}Error(s) detected in: $FILE${NC} ($(ls -lh $FILE | awk '{print $5}')B)${NC}"
    fi
    
    esc_echo "\t${ORANGE}Error report also saved here :${NC} $ESCALADE_PRODUCTION/error-report.log${NC}"
    [ "$RUN" != '.*' ] && echo -e "\t${ORANGEBKG}Remember that RUN is set to \"$RUN\"${NC} (therefore displayed data are filtered based on it)"
    [ ! $ESCALADE_IJOBS -eq 0 ] && echo -e "\t${ORANGE}Some jobs ($ESCALADE_IJOBS) are still running, some log files might not be parsed because they appeared after the beginning of the parsing procedure..${NC}"
    [ ! -z "$ESCALADE_TMPDIR" ] && echo -e "\t${BLUE}ESCALADE_TMPDIR=${NC}$ESCALADE_TMPDIR"
    [ ! -z "$ESCALADE_PRODUCTION" ] && echo -e "\t${BLUE}ESCALADE_PRODUCTION=${NC}$ESCALADE_PRODUCTION"
    
    echo -e "${REDBKG}--> Manual action required..${NC}"
    
    # To prevent mistakes, ask for manual check
    if [ ! $ESCALADE_IJOBS -eq 0 ]; then
        echo -e "${REDBKG}    $(echo "$FATAL" | wc -l) log(s) detected with potentially fatal errors among the completed jobs${NC}"
    else
        echo -e "${REDBKG}    $(echo "$FATAL" | wc -l) log(s) detected with potentially fatal errors${NC}"
    fi
    exit 1
fi

#
# If jobs are running or/and no error have been found.
#
[ "$RUN" != '.*' ] && echo -e "\t${ORANGEBKG}Remember that RUN is set to \"$RUN\"${NC} (therefore displayed data is filtered based on this)"
if [[ ! -d "$ESCALADE_INPUT0" || -z "$(ls -A $ESCALADE_INPUT0)" ]]; then
    
    echo -e "${RED}--> No input directory found.. please first run \"${ESCALADE_SOFTWARE}-data\"..${NC}"
    exit 1
    
    elif [[ ! -d "$ESCALADE_TMPDIR/logs" || -z "$(ls -A $ESCALADE_TMPDIR/logs)" ]]; then
    
    echo -e "${RED}--> No temporary directory found.. please first run \"${ESCALADE_SOFTWARE}-logs\"..${NC}"
    exit 1
else
    
    if [ ! $ESCALADE_IJOBS -eq 0 ]; then
        
        echo -e "${GREEN}--> No error detected among the completed jobs.${NC}"
        echo -e "${ORANGE}    Keep the temporary logdirectory in order to wait for the end of all jobs"
        echo -e "    (temporary logdirectory $ESCALADE_TMPDIR/logs)${NC}"
    fi
    
    echo -e "${GREEN}--> No error detected in \"$(basename $ESCALADE_OUTPUT0)\"${NC}"
    echo -e "    ($ESCALADE_TMPDIR/logs/)"
    
    #SUCCEEDED=$(find $ESCALADE_TMPDIR/logs -type f -name "*.stderr" -size 0 2> /dev/null | grep -E "$RUN" | sort -V)
    #[ "$RUN" != '.*' ] && echo -e "\t${ORANGEBKG}Remember that RUN is set to \"$RUN\"${NC} (therefore data is filtered based on this)"
    #for FILE in $SUCCEEDED; do
    
    #	[[ ! -z "$RUN" && -z "$(echo "$FILE" | grep -E "$RUN")" ]] && continue;
    #        echo -e "\tRun #$(basename $FILE .stderr) successfully processed"
    #done
fi
exit 0
