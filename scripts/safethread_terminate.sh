#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_SAFETHREAD_TERMINATE "" $# 0
[ $? -eq 1 ] && return 1


if [ -z "$ESCALADE_FORCEACTION" ]; then
    echo -e "${GREEN}--> Thread safe mode disabled${NC}"
else
    return 0
fi

set --
#
# Environment checks
#

if [ -f "$ESCALADE_PRODUCTION/escalade.lock" ]; then
    
    ESCALADE_PID_REFERENCE=$(cat $ESCALADE_PRODUCTION/escalade.lock)
    if [[ ! -z "$ESCALADE_PID_REFERENCE" && "$ESCALADE_PID" == "$ESCALADE_PID_REFERENCE" ]]; then
        
        echo -e "\t* [PID=$ESCALADE_PID] The mutex has been unlocked"
        rm $ESCALADE_PRODUCTION/escalade.lock
        
        if [ ! -z "$ESCALADE_QJOB_PID" ]; then
            
            echo -e "\t${RED}* [PID=$ESCALADE_PID] Job #$ESCALADE_QJOB_PID submission seemed to be on going..${NC}"
            echo -e "\t${RED}* [PID=$ESCALADE_PID] Submission properly aborted.. please use recovering procedure${NC}"
            $ESCALADE_QDEL $ESCALADE_QJOB_PID 2> /dev/null
        fi
    else
        echo -e "\t${ORANGE}* [PID=$ESCALADE_PID] It seems you were not the master ! skip..${NC}"
    fi
fi

return 0
