#! /bin/bash
# This getdata is useful for TGEANT, check if it might be remove later ! @meyerma

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_GETDATA_DUMMY "" $# 0
[ $? -eq 1 ] && exit 1

# Environment checks
#
source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && exit 1

#
# Check if already archived
#
$ESCALADE_ISARCHIVED $ESCALADE_OUTPUT0
[ $? -eq 1 ] && exit 1

#
# Getting data
#
if [ ! -z "$(find ${ESCALADE_INPUT0} -type f 2> /dev/null)" ]; then
    echo -e "${RED}--> Data input directory ${ESCALADE_INPUT0} already exists.${NC}"
    echo "    Please remove manually if not updated and re-run the previous command"
    exit 1
fi

if [ ! -d "$ESCALADE_PRODUCTION" ]; then
    
    echo -e "\tCreating production directory.."
    mkdir -p $ESCALADE_PRODUCTION
fi

#
# Preparing data
#
rm -rf $ESCALADE_TMPDIR/input-*.txt ${ESCALADE_TMPDIR}/input

#
# Include here..
#
if [ -f "$ESCALADE_SOFTWARE_DIR/getdata.sh" ]; then
    source $ESCALADE_SOFTWARE_DIR/getdata.sh
else
    source $ESCALADE_GETDATA
fi

tput el 2> /dev/null
if [ ! -d "${ESCALADE_TMPDIR}/input" ]; then
    esc_echo "\"${ESCALADE_TMPDIR}/input\" expected, but not found.. abort"
    exit 1
fi
mv ${ESCALADE_TMPDIR}/input/* ${ESCALADE_INPUT0} 2> /dev/null

#
# Cleaning temporary files and empty files
#
echo -ne "${GREEN}--> Cleaning empty files${NC}.. "
find ${ESCALADE_INPUT0} -size 0 -exec rm {} \;

echo "DONE"

echo -e "${GREEN}--> Total : $(find ${ESCALADE_INPUT0} -type f -name '*.txt' | wc -l) input file(s) prepared${NC} into ${ESCALADE_INPUT0}"
exit 0
