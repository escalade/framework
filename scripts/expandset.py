#! /usr/bin/env python
import os
import re
import glob

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-s", "--separator", default=" ", help="<separator>")
parser.add_argument("set", help="<set>")
parser.add_argument('arg', nargs='*', help='<argsX>')
args = parser.parse_args()

if(args.separator == '_' or args.separator == '/'):
    print(-3)
    exit(1)

def natural_sort(l): 

    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 

    lsort = sorted(l, key = alphanum_key)
    return lsort

def getlist(str0, delimiter = ' '):

    str0 = os.path.expandvars(str0)
    array = str0.split(delimiter)
    list = []

    if(str0 == ''): return []
    for s in array:
        m = re.search('([0-9]*)\.\.([0-9]*)', s)
        if(m != None and m.group(1) and m.group(2)):
            list += range(int(m.group(1)), int(m.group(2))+1)
        else:
            list += [s]

    return list

def mergeline(setlist, arglist, delimiter = ' '):

    str0 = str(len(setlist)) + " " + str(len(arglist))

    for i in range(len(setlist)):

        str0 += "\n" + str(setlist[i])
        for a in arglist:

            if i >= len(a):
                print(-1)
                exit(1)

            str0 += delimiter + str(a[i])

    return str0

def expand(setlist, arglist, delimiter = ' '):

    index = 0
    Nprod = -1
    Nargs = -1

    setlist = getlist(setlist, delimiter)
    N = len(setlist)

    for i in range(len(arglist)):
        arglist[i] = getlist(arglist[i], delimiter)
        if(len(arglist[i]) != N):
            print(-1)
            exit(1)

    # Empty string case..
    if(N == 0): return "1 0\nunnamed"

    #Possibly single set, expanded numbers, or textfile expected
    if(N == 1):

     if(len(arglist) == 0):
        if(os.path.isfile(setlist[0]) and setlist[0].endswith(".txt")):

            f = open(setlist[0])
            line = f.readline().strip()

            i = 0
            nargs = len(line.split(delimiter))
            str0 = ""
            while line:

                line = line.strip().split("#")[0].strip()
                i = i + 1
                if(nargs != len(line.split(delimiter))):
                    print(-1)
                    exit(1)

                str0 += "\n" + line.split(delimiter)[0] + " " + line
                line = f.readline()

            f.close()
            return str(i) + " " + str(nargs) + str0

        else:

            str0 = os.path.expandvars(setlist[0])
            str1 = str0.replace('.*', '*')
            str1 = str1.replace('(', '')
            str1 = str1.replace(')', '')

            paths = natural_sort(glob.glob(str1))
            if len(paths) == 0 :
               paths = [ str0 ]

            regex = re.compile(str0)

            str0 = ""
            N1,N2 = 0,0
            for p in paths:

               m = regex.search(p)
               if(m != None):
                 l = m.groups()

                 if(str0): str0 += "\n"
                 str0 += p

                 for i in range(len(l)):
                     str0 += " " + l[i]

                 N1 += 1
                 if(N2 < len(l)): N2 = len(l)

            if(str0):
                print(N1,N2)
                print(str0)
                exit(0)

    # If more than one, then multiple filepath to expand or multiple set
    return mergeline(setlist, arglist, delimiter)

if __name__ == "__main__":
    print(expand(args.set, args.arg, args.separator))
