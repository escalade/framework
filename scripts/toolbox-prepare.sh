#! /bin/bash

#
# Usage checks
#

if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_TOOLBOX_PREPARE "" $# 2
[ $? -eq 1 ] && exit 1

INPUT="$1"
if [[ -z "$INPUT" || "$INPUT" == "." || "$INPUT" == ".." ]]; then
    
    echo "INPUT variable not defined.."
    exit 1
fi
PATTERN="$2"
[ -z "$PATTERN" ] && PATTERN='*.root'

function get {
    
    INPUT0=$1
    if [ -d "$INPUT0" ]; then
        
        TMPFILE=/tmp/$(basename $INPUT0 .txt)-runlist-$RANDOM$RANDOM.txt
        find $(readlink -f $INPUT0) -name "$PATTERN" > $TMPFILE
        
        cat $TMPFILE | sort -V > $(basename $INPUT0 .txt).txt
        esc_echo -e "${GREEN}\t- Directory found \"$INPUT0\"..${NC} using \"$PATTERN\". It contains $(cat $(basename $INPUT0 .txt).txt | wc -l) file(s).. stored into $(basename $INPUT0 .txt).txt"
        
        elif [ -f "$INPUT0" ]; then
        
        esc_echo -e "${GREEN}\t- Runlist found \"$INPUT0\"..${NC} It contains $(cat $INPUT0 | wc -l) file(s).. stored into $(basename $INPUT0 .txt).txt"
        TMPFILE=/tmp/$(basename $INPUT0 .txt)-runlist-$RANDOM$RANDOM.txt
        for INPUT1 in $(cat $INPUT0); do
            
            echo "$PATTERN" | sed "s,{},$INPUT1,g" | sed "s,*,$INPUT1,g" >> $TMPFILE # {} or * can be used as replacement char
        done
        
        cat $TMPFILE | sort -V > $(basename ${INPUT0})
        
    else
        
        esc_echo -e "${ORANGE}\t- Skip \"$INPUT0\"..${NC} Nothing found"
    fi
}

esc_echo "${PURPLE}--> Prepare toolbox input, looking into \"$INPUT\"${NC}"
for INPUT0 in $INPUT; do
    
    get $INPUT0 &
    while [ $(jobs | grep 'get $INPUT0' | wc -l) -ge 10 ] ; do
        
        echo -e "\t${ORANGE}--- Please wait - Too many tasks (10 tasks) are running in parallel (check every 5s)${NC}"
        sleep 5
    done
done

sleep 5
echo -ne "${GREEN}--> Please wait.. ${NC}"
wait

echo -e "DONE."
exit 0
