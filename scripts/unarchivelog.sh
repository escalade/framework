#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_UNARCHIVELOG "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && exit 1

esc_echo "${GREEN}--> Looking for data stored on $FS, please wait.. ($ESCALADE_OUTPUT0)${NC}"
LS_OUTPUT0=$(mktemp --suffix -ls-output0)
$ESCALADE_GETLIST $ESCALADE_OUTPUT0 > $LS_OUTPUT0

#
# Check if archived
#
if [ -z "$(cat "$LS_OUTPUT0" | grep escalade-logfiles.tar.gz 2> /dev/null)" ]; then
    
    echo -e "${RED}--> No logfiles archived${NC} (expected here : $ESCALADE_OUTPUT0/escalade-logfiles.tar.gz)"
    exit 1
fi

#
# Check if a production directory already exist
#

if [[ ! -z "$ESCALADE_FORCEACTION" && "$ESCALADE_FORCEACTION" != "0" ]]; then
    
    echo -e "${ORANGE}--> Force to archive option is enabled.. You are allowed to unarchive !${NC}"
    
else
    
    if [[ ! -z "$(find $ESCALADE_PRODUCTION -type f -name 'escalade-key*')" ]]; then
        
        echo -e "${RED}--> $ESCALADE_PRODUCTION already exists.. Cannot overwrite it!${NC}"
        exit 1
    fi
fi

if [[ ! "$ESCALADE_OUTPUT0" -ef "$ESCALADE_PRODUCTION"  ]]; then
    
    esc_echo "\t- Get back logfiles on disk (from $ESCALADE_OUTPUT0/escalade-logfiles.tar.gz)"
    if [ ! -z "$(echo $FS_GET | grep -E ^xrdcp)" ]; then
        
        ${FS_GET} ${FS_ADDR}${ESCALADE_OUTPUT0#$FS_ADDR}/escalade-logfiles.tar.gz $ESCALADE_PRODUCTION/escalade-logfiles.tar.gz
    else
        ${FS_GET} ${ESCALADE_OUTPUT0#$FS_ADDR}/escalade-logfiles.tar.gz $ESCALADE_PRODUCTION/escalade-logfiles.tar.gz
    fi
fi

esc_echo "\t- Extract logfiles into $ESCALADE_PRODUCTION/logs"
cd $ESCALADE_PRODUCTION
tar -xzf ./escalade-logfiles.tar.gz
mkdir -p $ESCALADE_TMPDIR/logs/

eval ${FS_RM} "${ESCALADE_OUTPUT0#$FS_ADDR}/escalade-logfiles.tar.gz"
[ "$ESCALADE_PRODUCTION" != "$ESCALADE_OUTPUT0" ] && rm -f "$ESCALADE_PRODUCTION/escalade-logfiles.tar.gz"

cd - > /dev/null 2> /dev/null
exit 0
