#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_LOGHEADER "" $# 0
[ $? -eq 1 ] && return 1

if [ ! -z "$ESCALADE_BATCHMODE" ]; then
    
    echo -e "--- ESCALADE ON BATCH ------ ---" >> $ESCALADE_HISTORY
    echo -e "--- The following command will be sent on BATCH !" >> $ESCALADE_HISTORY
    echo -e "--- New call using $ESCALADE/bin/escalade done." >> $ESCALADE_HISTORY
    echo -e "--- User: $USER" >> $ESCALADE_HISTORY
    echo -e "--- Date: $(date)" >> $ESCALADE_HISTORY
    echo -e "--- Command: $ESCALADE_ORIGIN" >> $ESCALADE_HISTORY
    echo -e "--- ------------------------ ---\n" >> $ESCALADE_HISTORY
else
    echo -e "--- ESCALADE------ ---" >> $ESCALADE_HISTORY
    echo -e "--- New call using $ESCALADE/bin/escalade done." >> $ESCALADE_HISTORY
    echo -e "--- User: $USER" >> $ESCALADE_HISTORY
    echo -e "--- Date: $(date)" >> $ESCALADE_HISTORY
    echo -e "--- XML Config: $ESCALADE_CONFIG" >> $ESCALADE_HISTORY
    echo -e "--- Submission directory: $ESCALADE_PWD" >> $ESCALADE_HISTORY
    echo -e "--- Working directory: $ESCALADE_CD" >> $ESCALADE_HISTORY
    echo -e "--- Production directory: $ESCALADE_PRODUCTION" >> $ESCALADE_HISTORY
    echo -e "--- Output directory: $ESCALADE_OUTPUT0" >> $ESCALADE_HISTORY
    echo -e "--- Command: $ESCALADE_ORIGIN" >> $ESCALADE_HISTORY
    echo -e	"--- Makefile command: ESCALADE_STANDALONE=1 make -i -f $ESCALADE_SOFTWARE_DIR/Makefile $ESCALADE_CMD;" >> $ESCALADE_HISTORY
    echo -e "--- -------------- ---\n" >> $ESCALADE_HISTORY
fi

return 0
