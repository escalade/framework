#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

[ $# -eq 3 ] && NARGS=3 || NARGS=2

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_COPYFROMTO "<input directory> <output directory> [<id>]" $# $NARGS
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
DIR1=$1
DIR2=$2
ID=$(echo "$3" | sed 's,\W,_,g')

[ -z "$ID" ] && SUFFIX="" || SUFFIX="-$ID" # In case you specify a non-empty id, you will copy all files by including an id as suffix

[ -z "$ESCALADE_EXCLUDED_FILES" ] && ESCALADE_EXCLUDED_FILES="$ESCALADE/pilots/batch/$ESCALADE_BATCH/excluded_files $ESCALADE/pilots/software/$ESCALADE_SOFTWARE/excluded_files $ESCALADE/pilots/clusters/$ESCALADE_CLUSTER/excluded_files"
EXCLUDED_FILES_STR=$(cat $ESCALADE_EXCLUDED_FILES 2> /dev/null | uniq | sed -e :a -e '$!N;s/\n/|/;ta')

echo "----- Exclusion patterns found -----"
cat $ESCALADE_EXCLUDED_FILES 2> /dev/null | uniq
echo "----- -----"

LS_DIR1=$(ls -a $DIR1 | uniq | awk '{ if($1 != "." && $1 != ".." && $1 != "") print }')

if [ -z "$LS_DIR1" ]; then
    
    echo "----- Nothing to copy from: $DIR1 -----"
else
    echo "----- Directory to copy: $DIR1 -----"
    for FILE in $LS_DIR1; do
        
        if [ -z "$(echo $(basename $FILE) | grep -vE $EXCLUDED_FILES_STR)" ]; then
            
            echo -e "$RED$(ls -la $DIR1/$(basename $FILE)) (file excluded)$NC"
        else
            echo -e "$GREEN$(ls -la $DIR1/$(basename $FILE)) (file accepted)${NC}"
        fi
    done
    echo "----- -----"
fi

source $ESCALADE_SETFS $DIR2 > /dev/null
[ $? -eq 1 ] && exit 1
eval $FS_MKDIR $DIR2 2> /dev/null

for FILE in $(ls -a $DIR1 | uniq | grep -vE $EXCLUDED_FILES_STR)
do
    [ "$FILE" == "." ] && continue;
    [ "$FILE" == ".." ] && continue;
    
    NAME=$(echo $FILE | rev | cut -d"." -f2-  | rev | sed 's,\W,_,g')
    EXT=$(echo $FILE | rev | cut -d"." -f1  | rev)
    [ ! -z "$EXT" ] && EXT=".$EXT"
    
    i=0
    echo -e "[ESCALADE COPY] Move $FILE here: $DIR2/$NAME$SUFFIX$EXT"
    
    if [ ! -z "$(echo $FS_WRITE | grep -E ^xrdcp)" ]; then
        
        echo $FS_WRITE $DIR1/$FILE $FS_ADDR${DIR2#$FS_ADDR}/$NAME$SUFFIX$EXT
        eval $FS_WRITE $DIR1/$FILE $FS_ADDR${DIR2#$FS_ADDR}/$NAME$SUFFIX$EXT
    else
        echo $FS_WRITE $DIR1/$FILE ${DIR2#$FS_ADDR}/$NAME$SUFFIX$EXT
        eval $FS_WRITE $DIR1/$FILE ${DIR2#$FS_ADDR}/$NAME$SUFFIX$EXT
    fi
done

[ ! -z "$LS_DIR1" ] &&echo -e "[ESCALADE COPY] Transfert DONE."

exit 0
