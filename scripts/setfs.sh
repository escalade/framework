#! /bin/bash

#
# Usage check
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$BASH_SOURCE: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

if [[ ! $# -eq 1 && ! $# -eq 0 ]]; then
    
    echo -ne "${ORANGE}[Usage] $BASH_SOURCE ${NC}" >&2
    [ "$BASH_SOURCE" != "$ESCALADE_SETFS" ] && echo -ne "${ORANGE}: $ESCALADE_SETFS ${NC}" >&2
    echo -e "${ORANGE}<path>${NC}" >&2
    return 1
fi


#
# Check filesystem to use (for storage system)
#
if [ $# -eq 1 ]; then
    
    # Configuration by default
    ONTAPE=0
    FS="DISK"
    FS_PROTOCOL="STD"
    
    FS_LS="ls -la"
    FS_MKDIR="mkdir -p"
    FS_RM="rm -rf"
    FS_GET="cp -R"
    FS_WRITE="cp -R"
    FS_MV="mv"
    unset FS_ADDR
    
    # If cluster detected
    if [[ ! -z "$ESCALADE_CLUSTER" && "$ESCALADE_CLUSTER" != "unknown" ]]; then
        
        source $ESCALADE/pilots/clusters/$ESCALADE_CLUSTER/setfs.sh $1
    fi
    
    return 0
fi

return 1
