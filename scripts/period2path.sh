#! /bin/bash
# e.g. dy15W10t1 => 2015/W10/dyt1 at lyon

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE/scripts/period2path.sh "<txtfile>" $# 1
[ $? -eq 1 ] && exit 1

#
# Global variables
#
PERIOD=$1

for SED in $(cat $ESCALADE/scripts/$ESCALADE_CLUSTER/period2path.sed | grep -v "^#"); do
    
    if [ ! -z "$SED" ]; then
        
        echo $PERIOD | sed -ne "$SED" 2> /dev/null
    fi
done

exit 1
