#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_TESTSUPPR "<suppression file> <log file>" $# 2
[ $? -eq 1 ] && return 1

PIPEFILE=$1
LOGFILE=$2

#
# Pipe error messages from stdout to stderr
#
rm -f "/tmp/testpipe-*"
function pipe
{
    local FILELIST=$1
    local LOGFILE_ORIGINAL=$2
    
    local LOGFILE=""
    for STDERR in $LOGFILE_ORIGINAL; do
        
        STDFILE_TMP=/tmp/testsuppr-$(basename $STDERR)-$RANDOM$RANDOM
        echo "Copy to \"$STDFILE_TMP\""
        cp $STDERR $STDFILE_TMP
        
        LOGFILE="$LOGFILE $STDFILE_TMP"
    done
    
    for FILE in $FILELIST; do
        
        local PIPEFILE=$FILE
        if [ ! -f "$PIPEFILE" ]; then
            
            esc_echo "${ORANGE}--> Pipe file not found..${NC} ($PIPEFILE)" >&2
            continue
        fi
        
        esc_echo "${GREEN}--> Reading pipe file..${NC} $PIPEFILE"
        # Just display the regex to pipe
        local FINAL_REGEXCODE=""
        while read REGEXCODE
        do
            REGEXCODE="$(echo "$REGEXCODE" | cut -d'#' -f1)" # Remove comments
            [ -z "$(echo "$REGEXCODE" | grep -vE '^\s*$')" ] && continue # Continue if empty line (remove whitespace as well)
            
            [ -z "$FINAL_REGEXCODE" ] && FINAL_REGEXCODE="$REGEXCODE" || FINAL_REGEXCODE="$REGEXCODE|$FINAL_REGEXCODE"
            echo -e "\t- Piping \"$REGEXCODE\" (Regex code) from stdout to stderr.."
            
        done < "$PIPEFILE"
        
        [ -z "$FINAL_REGEXCODE" ] && continue;
        
        # Apply the piping informations
        IRUN=1
        NRUN=$(echo $LOGFILE | wc -w)
        for STDOUT in $LOGFILE; do
            
            tput el 2> /dev/null
            echo -ne "\t  Processing file $(basename $STDOUT).. ($IRUN/$NRUN)\r"
            IRUN=$(($IRUN+1))
            
            touch $STDOUT.grep
            if [ -s "$STDOUT" ]; then
                
                grep --binary-files=text -hiE  "$FINAL_REGEXCODE" $STDOUT > $STDOUT.grep
            fi
        done
        echo ""
    done
    
    for STDOUT in $LOGFILE; do
        
        touch $STDOUT.grep
        echo -e "${RED}--> Summary report for \"$STDOUT\"${NC}"
        echo -e "${RED}    There is/are $(cat "$STDOUT.grep" | wc -l) piped error line(s) ${NC}"
        if [ -z "$STDOUT" ]; then
            
            echo -e "   ${ORANGE}No error detected inside !${NC}"
        else
            cat $STDOUT.grep
        fi
    done
}

echo -e "${GREEN}--> Start analyzing.. \"$LOGFILE\"${NC}"
pipe "$PIPEFILE" "$LOGFILE"
echo -e "${GREEN}--> Finished.. :-)${NC}"
exit 0
