#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE_BATCH_DIR/prepjob.sh "" $# 0
[ $? -eq 1 ] && exit 1

if [ -z "$ESCALADE_JOBID" ]; then
    
    echo "ESCALADE_JOBID is not defined." >&2
    exit 1
fi

if [ -z "$ESCALADE_JOBSLOT" ]; then
    
    echo "ESCALADE_JOBSLOT is not defined." >&2
    exit 1
fi

if [ -z "$ESCALADE_JOBWORKDIR" ]; then
    echo "ESCALADE_JOBWORKDIR is not defined" >&2
    exit 1
fi

if [ -z "$ESCALADE_JOBFILE" ]; then
    
    echo "ESCALADE_JOBFILE is not defined" >&2
    exit 1
fi

if [ ! -s "$ESCALADE_JOBFILE" ]; then
    
    echo -e "Nothing to process.. $ESCALADE_JOBFILE is empty" >&2
    exit 1
fi

exit 0
