#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_BATCHSTATUS "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
echo -ne "${GREEN}--> Check status of the declared batch system(s)..${NC} "

STATUS=0
for BATCH in $(cat "$ESCALADE_CLUSTER_DIR/valid_batch"); do
    
    [ "$BATCH" == "fakebatch" ] && continue
    source "$ESCALADE/pilots/batch/$BATCH/setenv.sh"
    [ $? -eq 1 ] && exit 1
    
    echo -ne "$BATCH "
    RESPONSE=$(timeout 20 $ESCALADE_QSTAT 2> /dev/null)
    
    if [ ! $? -eq 0 ]; then
        echo -ne "${RED}(not responding)${NC}.. "
        STATUS=1
    else
        echo -ne "${GREEN}(ready)${NC}.. "
    fi
done

echo ""
exit 0
