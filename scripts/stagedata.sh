#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

[ $# -eq 3 ] && EXPECTED_ARGS=3 || EXPECTED_ARGS=2
source $ESCALADE_USAGE $0 $ESCALADE_STAGEDATA "<text file/staged file> <idpattern> [<force waiting>]" $# $EXPECTED_ARGS
[ $? -eq 1 ] && exit 1

#
# Global variables
#
INPUT0_FILE="$1"
INPUT0_ID="$2"

if [ -d "$INPUT0_FILE" ]; then
    INPUT0_FILE=$(find $1 -name '*.txt' -exec cat {} \;)
fi

TYPE=$(file -i "$INPUT0_FILE" | cut -d' ' -f2)
if [ ! -z "$(echo "$TYPE" | grep 'text/plain')" ]; then
    INPUT0LIST=$(cat $INPUT0_FILE | grep -E "$INPUT0_ID")
else
    INPUT0LIST=$(echo "$INPUT0_FILE" | grep -E "$INPUT0_ID")
fi

[ $# -eq 3 ] && FORCE=$3 || FORCE=0

set --

#
# Environment checks
#
if [[ -z "$ESCALADE_CLUSTER" || "$ESCALADE_CLUSTER" == "unknown" ]]; then
    
    echo -e "${ORANGE}No staging command known.. Unknown cluster${NC}"
    exit 0
fi

for DATA in $INPUT0LIST; do
    $ESCALADE_CLUSTER_DIR/staging.sh "$DATA" > /dev/null 2> /dev/null
done

if [[ "$FORCE" != 0 && ! -z "$FORCE" ]]; then
    
    echo -e "${GREEN}--> Staging data in process.. Wait for each file on tape to be staged${NC}"
    for DATA in $INPUT0LIST; do
        
        source $ESCALADE_SETFS $DATA > /dev/null
        [ $? -eq 1 ] && exit 1
        
        $ESCALADE/bin/root-accesstry "${FS_ADDR}${DATA#$FS_ADDR}" 2> /dev/null
    done
fi

exit 0
