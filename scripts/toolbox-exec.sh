#! /bin/bash

#
# Usage checks
#

if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_TOOLBOX_PREPARE "" $# 0
[ $? -eq 1 ] && exit 1

if [ -z "$EVAL" ]; then
    
    echo "EVAL variable not defined.."
    exit 1
fi

if [ -z "$FILE" ]; then
    
    echo "FILE variable not defined.."
    exit 1
fi

esc_echo "${PURPLE}--> Internal toolbox command called..${NC} $EVAL"
esc_echo "${BLUE}    (Use \"{}\" to get the filename without extension)${NC}"
esc_echo "${GREEN} --> Looking for input files \"$FILE\"${NC}"
DIR=$(dirname "$FILE")
FILE=$(basename "$FILE")

for FILE0 in $(find $DIR -name "$FILE"); do
    
    FILE0=$(basename "$FILE0")
    FILE0="${FILE0%.*}"
    
    COMMAND=$(echo "$EVAL" | sed "s,{},$FILE0,g")
    esc_echo "${GREEN}\t- Processing ${FILE0}..${NC} $COMMAND"
    
    eval "$COMMAND" &
    while [ $(jobs | grep "Running" | grep '$COMMAND' | wc -l) -ge 10 ] ; do
        
        echo -e "\t${ORANGE}--- Please wait - Too many tasks (10 tasks) are running in parallel (check every 5s)${NC}"
        sleep 5
    done
done
sleep 5

echo -ne "${GREEN}--> Please wait.. ${NC}"
wait

echo -e "DONE."
exit 0
