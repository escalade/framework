#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_TESTEXTRA "<extra file>" $# 1
[ $? -eq 1 ] && return 1

EXTRAFILE=$1

#
# Pipe error messages from STDERR to STDERR
#
rm -f "/tmp/testextra-*"
function extratest
{
    local FILELIST=$1
    local LOGFILE_ORIGINAL=$2
    
    for FILE in $FILELIST; do
        
        local EXTRAFILE=$FILE
        
        if [ ! -f "$EXTRAFILE" ]; then
            
            esc_echo "${ORANGE}--> Extra file not found.. skip${NC} ($EXTRAFILE)" >&2
            continue;
        fi
        
        EXTRATEST_OUTPUT="$(source $EXTRAFILE 2>&1)"
        if [ $? -eq 1 ]; then
            
            EXTRATEST_OUTPUT=$(echo "$EXTRATEST_OUTPUT" | sed "s,^,\t,g")
            echo -e "${RED}\t[ESCALADE TEST] $(basename $EXTRAFILE) : FAILED${NC}"
            echo -e "${ORANGE}$EXTRATEST_OUTPUT${NC}"
        else
            EXTRATEST_OUTPUT=$(echo "$EXTRATEST_OUTPUT" | sed "s,^,\t,g")
            echo -e "\t[ESCALADE TEST] $(basename $EXTRAFILE) : ${GREEN}SUCCEEDED${NC}"
            echo -e "$EXTRATEST_OUTPUT"
        fi
    done
}

echo -e "${GREEN}--> Start running extratest.. ${NC}"
extratest "$EXTRAFILE"
echo -e "${GREEN}--> Finished.. :-)${NC}"
exit 0
