#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_MERGEDATA "<inputdir> <outputdir>" $# 2
[ $? -eq 1 ] && exit 1

#
# Global variables
#
INPUT0=$1
OUTPUT0=$2
set --

#
# Environment checks
#
if [ -z "$(ls -A $INPUT0)" ]; then
    echo -e "${RED}--> Input directory not found..${NC} ($INPUT0)"
    exit 1
fi

if [ "$INPUT0" == "$OUTPUT0" ]; then
    
    echo -e "${ORANGE} Same output and input directory.. Input will be removed after the procedure..${NC}"
fi

# Check if there is a auth key, that mean that jobs have already been produced..
source $ESCALADE_CHECKKEY $(dirname $INPUT0) $(dirname $OUTPUT0) > /dev/null 2> /dev/null
if [ ! $? -eq 2 ]; then
    echo -e "${RED}--> Key already detected, you cannot change anything about the input files at this moment..${NC}"
    exit 1
else
    echo -e "${GREEN}--> No key found, that's ok. You can still modify the input files at this moment..${NC}"
fi

#
# Preparing for merging input txtfiles
#
WORKDIR=$(mktemp -d)

let SLOT=0
mkdir -p $OUTPUT0 ${WORKDIR}/slot-$SLOT/

for DATA in $(find $INPUT0 -type f -name "*.txt" 2> /dev/null); do
    
    if [ $(ls ${WORKDIR}/slot-${SLOT} | wc -l) -gt 1022 ]; then
        
        SLOT=$((SLOT+1))
        mkdir -p ${WORKDIR}/slot-$SLOT
        echo "[INFO] New slot created.. (slot:$SLOT)"
    fi
    
    tput el 2> /dev/null
    echo -ne "${GREEN}--> Merging run $(basename $DATA)${NC}\r"
    DATA_ID=$(basename $DATA .txt | rev | cut -f2- -d'_' | rev)
    cat $DATA >> ${WORKDIR}/slot-$SLOT/$DATA_ID.txt
done

# Move splitted files
[ "$INPUT0" == "$OUTPUT0" ] && rm -rf $INPUT0/*

mv ${WORKDIR}/* $OUTPUT0/

# Remove empty files and remove old txtfiles
find $OUTPUT0 -size 0 -exec rm {} \;

rm -r "${WORKDIR}"
exit 0
