#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_BRING_ONDISK "<file> <outputdir>" $# 2
[ $? -eq 1 ] && exit 1

FILE=$1
OUTPUT0DIR=$2
set --

#
# Read content of directory
#

source $ESCALADE_SETFS $FILE > /dev/null
[ $? -eq 1 ] && exit 1

# If already on disk
if [ "$FS" == "DISK" ]; then
    
    READLINK_FILE=$(readlink -f $FILE 2> /dev/null)
    [ -z "$READLINK_FILE" ] && exit 1
    echo $READLINK_FILE
    exit 0
fi

# If on tape.. first stage and wait for it..
$ESCALADE_STAGEDATA $FILE '*' 1
if [ $? -eq 1 ]; then
    
    echo "${RED}[ERROR] Failed to transfert $FILE on disk.. abort${NC}" >&2
    exit 1
fi

# BRing on disk..
FILENAME=$(basename $FILE)
mkdir -p $OUTPUT0DIR

if [ ! -f $OUTPUT0DIR/$FILENAME ]; then
    
    if [ ! -z "$(echo $FS_GET | grep -E ^xrdcp)" ]; then
        eval $FS_GET ${FS_ADDR}${FILE#$FS_ADDR} $OUTPUT0DIR/$FILENAME
    else
        eval $FS_GET ${FILE#$FS_ADDR} $OUTPUT0DIR/$FILENAME #> /dev/null 2> /dev/null
    fi
fi

if [ $? -eq 0 ]; then
    
    echo $OUTPUT0DIR/$FILENAME
    exit 0
else
    echo "${RED}[ERROR] Failed to transfert $FILE on disk.. abort${NC}" >&2
    exit 1
fi
