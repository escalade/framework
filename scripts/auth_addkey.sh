#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_ADDKEY "<reference_dir> <dir1> <dir2>" $# 3
[ $? -eq 1 ] && return 1

#
# Global variables
#
REF_DIR=$1 # The key is generated using the content of a directory
DIR1=$2
DIR2=$3

#
# Environment checks
#

if [ -z "$(ls -A $REF_DIR)" ]; then
    
    echo -e "${RED}* Cannot create a key of authenticity, no input found${NC}" >&2
    return 1
fi

set --

# Check the safethread lock
source $ESCALADE_SAFETHREAD > /dev/null
[ $? -eq 1 ] && exit 1

# Generate the key
MD5_KEY=`echo -n "$ESCALADE $(ls $REF_DIR 2> /dev/null)" | md5sum | awk '{print $1}'`
echo "* The generated key for this configuration is : \"$MD5_KEY\""

UNIQ_KEYNAME=$(echo "$REF_DIR" | md5sum | awk '{print $1}')
echo $ESCALADE > $ESCALADE_TMPDIR/$UNIQ_KEYNAME

source $ESCALADE_SETFS $DIR1 > /dev/null
[ $? -eq 1 ] && return 1

esc_echo -ne "\t* Creation of the key #1 for \"${DIR1#FS_ADDR}\".. "
if [ ! -z "$(echo $FS_WRITE | grep -E ^xrdcp)" ]; then
    
    # Check the safethread lock
    source $ESCALADE_SAFETHREAD > /dev/null
    [ $? -eq 1 ] && exit 1
    
    
    
    $FS_WRITE $ESCALADE_TMPDIR/$UNIQ_KEYNAME ${FS_ADDR}${DIR1#FS_ADDR}/escalade-key-$MD5_KEY &
else
    
    # Check the safethread lock
    source $ESCALADE_SAFETHREAD > /dev/null
    [ $? -eq 1 ] && exit 1
    
    
    $FS_WRITE $ESCALADE_TMPDIR/$UNIQ_KEYNAME ${DIR1#$FS_ADDR}/escalade-key-$MD5_KEY &
fi
wait
echo "DONE."

source $ESCALADE_SETFS $DIR2 > /dev/null
[ $? -eq 1 ] && return 1

esc_echo -ne "\t* Creation of the key #2 for \"${DIR2#FS_ADDR}\".. "
if [ ! -z "$(echo $FS_WRITE | grep -E ^xrdcp)" ]; then
    
    # Check the safethread lock
    source $ESCALADE_SAFETHREAD > /dev/null
    [ $? -eq 1 ] && exit 1
    
    $FS_WRITE $ESCALADE_TMPDIR/$UNIQ_KEYNAME ${FS_ADDR}${DIR2#FS_ADDR}/escalade-key-$MD5_KEY &
else
    # Check the safethread lock
    source $ESCALADE_SAFETHREAD > /dev/null
    [ $? -eq 1 ] && exit 1
    
    
    $FS_WRITE $ESCALADE_TMPDIR/$UNIQ_KEYNAME ${DIR2#$FS_ADDR}/escalade-key-$MD5_KEY &
fi
wait
echo "DONE."

# Check the safethread lock
source $ESCALADE_SAFETHREAD > /dev/null
[ $? -eq 1 ] && exit 1


rm $ESCALADE_TMPDIR/$UNIQ_KEYNAME
