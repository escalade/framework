#! /bin/bash

#
# Usage check
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$BASH_SOURCE: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_SYNCLOG "" $# 0
[ $? -eq 1 ] && return 1

#
# Global variables
#

#
# Environment checks
#
source $ESCALADE_LSDIR "0100" "0000" "0200"
[ $? -eq 1 ] && exit 1

#
# Update new files from Log directory to the temporary Log directory
#
echo -e "${GREEN}--> Synchronization procedure of logfiles on going..${NC}"
if [ ! -d $ESCALADE_TMPDIR/logs ]; then
    
    if [ ! -z "$ESCALADE_DISABLE_SYNC" ]; then
        
        echo -e "\t${RED}* Skip synchronization procedure.. on request (--disable-sync)${NC}"
        echo -e "\t  ${REDBKG}No temporary logfiles found.. procedure has to stop here..${NC}"
        sleep 2
        exit 0
    fi
    
    echo -e  "   * Copy the original logfiles ($ESCALADE_LOGDIR0)${NC}.."
    esc_echo -e "     .. into a temporary log directory ($ESCALADE_TMPDIR/logs) to analyze them.. please wait.."
else
    if [ ! -z "$ESCALADE_DISABLE_SYNC" ]; then
        
        echo -e "\t${RED}* Skip synchronization procedure.. on request (--disable-sync)${NC}"
        echo -e       "\t  Temporary log directory found.. the current one will be used"
        sleep 2
        return 0
    else
        esc_echo -e "   * Temporary log directory ($ESCALADE_TMPDIR/logs) already exists..${NC}"
        echo     -e "     .. Update logfiles from the original log directory.. please wait.."
    fi
fi

# rsync -razu --exclude '.pid' $ESCALADE_LOGDIR0 $ESCALADE_TMPDIR --info=progress2
mkdir -p $ESCALADE_TMPDIR
cp -gRu $ESCALADE_LOGDIR0 $ESCALADE_TMPDIR
rm -f "$ESCALADE_TMPDIR/logs/.pid"

return 0
