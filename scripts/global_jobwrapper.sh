#!/bin/bash

ulimit -c 0 # remove core.* files
unset TERM

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE/scripts/usage.sh $0 $ESCALADE/scripts/global_jobwrapper.sh "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
[ -z "$ESCALADE_BATCH_WORKDIR" ] && export ESCALADE_BATCH_WORKDIR=$PWD
echo "Job wrapper working directory: \"$ESCALADE_BATCH_WORKDIR\""
export ESCALADE_JOBWORKDIR0=$ESCALADE_BATCH_WORKDIR
mkdir -p $ESCALADE_JOBWORKDIR0
cd $ESCALADE_JOBWORKDIR0

[[ -f "$ESCALADE_BATCH_SETUP" && ! -z "$ESCALADE_BATCH_SETUP" ]] && source $ESCALADE_BATCH_SETUP

if [ -z "$ESCALADE_JOBLIST" ]; then
    
    echo "ESCALADE_JOBLIST is not defined.. checkout your submitter code." >&2
    exit 1
fi

if [ "$ESCALADE_BATCH" != "fakebatch" ]; then
    
    [ ! -z "$ESCALADE_BATCH_ERR" ] && touch $ESCALADE_BATCH_ERR
    [ ! -z "$ESCALADE_BATCH_OUT" ] && touch $ESCALADE_BATCH_OUT
    [ ! -z "$ESCALADE_BATCH_ENV" ] && touch $ESCALADE_BATCH_ENV
fi

echo "[ESCALADE] Job did not finish properly.." >> $ESCALADE_BATCH_ERR # In case the run don't finish correctly, this will be erased at the end..
echo "Job started: $(date)"

#
# Call of the real wrapper here
#
if [ -s "$ESCALADE_SOFTWARE_DIR/jobwrapper.sh" ]; then
    
    echo "\"$ESCALADE_SOFTWARE\" software is using a specific batch job wrapper available in the software pilot directory"
    eval $ESCALADE_SOFTWARE_DIR/jobwrapper.sh
else
    echo "Use the default batch jobwrapper available in the batch \"$ESCALADE_BATCH\" pilot directory"
    eval $ESCALADE_BATCH_DIR/jobwrapper.sh
fi

#
# Claim the end of the job (remove the error message)
#
mv $ESCALADE_BATCH_ENV $ESCALADE_LOGDIR0 2> /dev/null

sed -i "/.ESCALADE. Job did not finish properly../d" $ESCALADE_BATCH_ERR # If job is stopped before the ned, the message should stay!
echo "[Batch] Job ended.. $(date)"

rm "$ESCALADE_JOBLIST"
