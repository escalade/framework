#! /bin/bash
#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_GETDATA "" $# 0
[ $? -eq 1 ] && exit 1

#
# Environment checks
#
source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && exit 1

# Get list of output files
esc_echo "${GREEN}--> Looking for data stored on $FS, please wait.. ($ESCALADE_OUTPUT0)${NC}"
LS_OUTPUT0=$($ESCALADE_GETLIST $ESCALADE_OUTPUT0 2> /dev/null)

#
# Check if already archived
#
if [ ! -z "$(cat "$LS_OUTPUT0" | grep escalade-logfiles.tar.gz 2> /dev/null)" ]; then
    
    echo -e "${RED}--> Logfiles already archived here : $ESCALADE_OUTPUT0/escalade-logfiles.tar.gz${NC}"
    echo -e "    You already processed this set of data"
    exit 1
fi

#
# Authenticity keys
#
source $ESCALADE_CHECKKEY ${ESCALADE_OUTPUT0} ${ESCALADE_PRODUCTION} # If created, it will check keys
[ $? -eq 1 ] && exit 1

if [ ! $ESCALADE_IJOBS -eq 0 ]; then
    
    esc_echo "${ORANGE}\tSome jobs ($ESCALADE_IJOBS) are still running, please wait until the end..${NC}"
    exit 1
fi

#
# Check output directory
#

source $ESCALADE_SETFS $ESCALADE_OUTPUT0 > /dev/null
[ $? -eq 1 ] && exit 1

echo -e "${ORANGE}--> Updating list of input data..${NC}"

echo -e "${ORANGE}--> Remove input directory${NC}"
rm -rf "${ESCALADE_PRODUCTION}/input"
mkdir -p ${ESCALADE_PRODUCTION}/input

echo -e "${ORANGE}--> Remove escalade keys${NC}"
cp ${ESCALADE_PRODUCTION}/escalade-key* ${ESCALADE_PRODUCTION}/.tmp/ 2> /dev/null
rm -rf "${ESCALADE_PRODUCTION}/escalade-key*"
eval "${FS_RM} ${ESCALADE_OUTPUT0#$FS_ADDR}/escalade-key*" 2> /dev/null

$ESCALADE_GETDATA

echo -e "${ORANGE}--> Set back the keys${NC}"
mv ${ESCALADE_PRODUCTION}/.tmp/escalade-key* ${ESCALADE_PRODUCTION} 2> /dev/null

if [ ! -z "$(echo $FS_WRITE | grep -E ^xrdcp)" ]; then
    
    eval "${FS_WRITE} ${ESCALADE_PRODUCTION}/escalade-key* ${ESCALADE_OUTPUT0}/" 2> /dev/null
else
    eval "${FS_WRITE} ${ESCALADE_PRODUCTION}/escalade-key* ${ESCALADE_OUTPUT0#$FS_ADDR}/" 2> /dev/null
fi
exit 0
