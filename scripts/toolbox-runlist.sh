#! /bin/bash

#
# Usage checks
#

if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_TOOLBOX_PREPARE "" $# 2
[ $? -eq 1 ] && exit 1

DIR="$1"
if [[ -z "$DIR" || "$DIR" == "." || "$DIR" == ".." ]]; then
    
    echo "DIR variable not defined.."
    exit 1
fi
PATTERN="$2"
[ -z "$PATTERN" ] && PATTERN='.*-(.*).root'
PATTERN=".*$PATTERN" # in order to remove the path

function get {
    
    DIR0=$1
    
    rm -f $(basename $DIR0).txt
    
    PATTERN0=$(echo $PATTERN | sed 's,(,\\(,g' | sed 's,),\\),g')
    TMPFILE=/tmp/$(basename $INPUT0 .txt)-runlist-$RANDOM$RANDOM.txt
    for FILE in $(find $(readlink -f $DIR0) | grep -E "$PATTERN"); do
        
        echo $FILE | sed "s,$PATTERN0,\1,g" >> $TMPFILE
    done
    
    cat $TMPFILE | sort -V > $(basename $DIR0).txt
    
    esc_echo -e "${GREEN}\t- Directory found \"$DIR0\"..${NC} using \"$PATTERN\". It contains $(cat $(basename $DIR0).txt | wc -l) file(s)..${NC}"
    esc_echo -e "${GREEN}\t  To be converted into runlist using PATTERN=\"$PATTERN\" and stored into $(basename $DIR0).txt"
}

esc_echo "${PURPLE}--> Prepare toolbox input, looking into \"$DIR\"${NC}"
for DIR0 in $DIR; do
    
    [ ! -d "$DIR0" ] && continue;
    
    get $DIR0 &
    while [ $(jobs | grep 'get $DIR0' | wc -l) -ge 10 ] ; do
        
        echo -e "\t${ORANGE}--- Please wait - Too many tasks (10 tasks) are running in parallel (check every 5s)${NC}"
        sleep 5
    done
done
sleep 5

echo -ne "${GREEN}--> Please wait.. ${NC}"
wait

echo -e "DONE."
exit 0
