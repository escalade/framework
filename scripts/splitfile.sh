#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi


if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

source $ESCALADE_USAGE $0 $ESCALADE_SPLITFILE "<inputfile> <outputdir> <maxline>" $# 3
[ $? -eq 1 ] && exit 1

#
# Global variables
#

INPUT0=$1
OUTPUT0=$(eval "$ESCALADE_READLINK $2")
NLINE=$3

set --

if [ ! -s $INPUT0 ]; then
    
    echo -e "${RED}--> $INPUT0 not found..${NC}"
    exit 0
fi

if [ $NLINE -eq 0 ]; then
    echo -e "${GREEN}--> Nothing to do, NLINE < 1${NC}"
    exit 0
fi

#
# Splitting method
#
WORKDIR=$(mktemp -d)
echo -e "${GREEN}--> Temporary working directory used: $WORKDIR${NC}"
mkdir -p $WORKDIR

cp $INPUT0 $WORKDIR/input.txt
FILE=$(basename -- "$INPUT0")
EXT="${FILE##*.}"
FILE="${FILE%.*}"
[ ! -z "$EXT" ] && EXT=".$EXT"

echo -e "${GREEN}--> Files will be named like \"$FILE-XXX$EXT\"${NC}"
export INPUT0=$WORKDIR/input.txt

echo -e "${GREEN}--> Splitting input file:${NC} $INPUT0 (into $NLINE line(s) per file)"
cd $WORKDIR
split -l $NLINE $INPUT0 -a64

echo -e "\tThere is/are $(ls x* | wc -l) outgoing file(s)"

i=1
mkdir -p $OUTPUT0
for x in $(ls x* | sort -V); do
    
    mv $x $OUTPUT0/$FILE-$i$EXT
    i=$(($i+1))
done

cd - > /dev/null 2> /dev/null
rm -r "${WORKDIR}"
exit 0
