#! /bin/bash
# rfdir /castor/cern.ch/compass/data/2015/raw/ | awk '{print "make data-size DIR=/castor/cern.ch/compass/data/2015/raw/"$9}'

#
# Usage checks
#
if [[ "$BASH_SOURCE" != "$0" && "$BASH_SOURCE" == "bash" ]]; then
    echo "$0 : This script cannot be sourced !" >&2
    return 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    exit 1
fi

if [ ! $# -eq 1 ]; then
    
    echo "[Usage] $0 <directory>"
    exit 1
fi

#
# Global variables
#
DIRECTORY=$1

#
# Environment checks
#
source $ESCALADE_SETFS $DIRECTORY >  /dev/null
[ $? -eq 1 ] && exit 1

#
# Calculate the size of $DIRECTORY
#

if [ "$FS" == "DISK" ]; then
    DATA=$(find $DIRECTORY 2> /dev/null  | xargs ls -la | awk '{ SUM += $5 } END { print SUM }') # Bytes
else
    DATA=$(eval $FS_LS $DIRECTORY 2> /dev/null | awk '{ SUM += $5 } END { print SUM }') # Bytes
fi

if [[ "$DATA" == "0" || -z "$DATA" ]]; then
    GIB=0
    GB=0
else
    GIB=$(echo "scale=2; $DATA/1024/1024/1024" | bc -l)
    GB=$(echo "scale=2; $DATA/1000/1000/1000" | bc -l)
fi

echo "$(readlink -f $DIRECTORY) = $GB GB = $GIB GiB"
exit 0
