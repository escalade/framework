#! /bin/bash

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

source $ESCALADE_USAGE $BASH_SOURCE $ESCALADE_CHECKKEY "<dir1> <dir2>" $# 2
[ $? -eq 1 ] && return 1

#
# Global variables
#
DIR1=$1
DIR2=$2

esc_echo -e "${GREEN}--> Looking for an authenticity key.. ($DIR2)${NC}"

#
# Environment checks
#

if [ ! -s "$LS_DIR1" ]; then
    LS_DIR1=$(mktemp --suffix -ls-dir1)
    $ESCALADE_GETLIST "$DIR1" 1 2> /dev/null > $LS_DIR1
fi

if [ ! -s "$LS_DIR2" ]; then
    LS_DIR2=$(mktemp --suffix -ls-dir2)
    $ESCALADE_GETLIST "$DIR2" 1 2> /dev/null > $LS_DIR2
fi

KEY1=$(basename "$(cat "$LS_DIR1" | grep escalade-key)" | cut -d'-' -f3)
KEY2=$(basename "$(cat "$LS_DIR2" | grep escalade-key)" | cut -d'-' -f3)

# Already processed
[ -z "$KEY1" ] && esc_echo -e "\t* Key 1 not found for $DIR1" || esc_echo -e "\t* Key 1 found for $DIR1/escalade-key-$KEY1"
[ -z "$KEY2" ] && esc_echo -e "\t* Key 2 not found for $DIR2" || esc_echo -e "\t* Key 2 found for $DIR2/escalade-key-$KEY2"

if [ -z "$KEY1$KEY2" ]; then
    
    echo -e "\t* No key found neither #1 nor #2.."
    return 2
fi

if [ "$KEY1" == "$KEY2" ]; then
    
    echo -e "\t* Compatible key detected.. :-)"
    return 0
else
    echo -e "${RED}\t* Unexpected key detected between ${ORANGE}$DIR1${NC}${RED}"
    echo -e "\t  and ${ORANGE}$DIR2${RED}"
    echo -e "\t  This mean that the data contained within ${ORANGE}$DIR1${RED}"
    echo -e "\t  seems to do not correspond to ${ORANGE}$DIR2${NC}"
    return 1
fi
