#! /bin/bash

trap ctrl_c INT

function esc_kaomoji
{
    declare -a ESCALADE_KAOMOJI_ARRAY=( "(ง •̀ω•́)ง✧" "༼つಠ益ಠ༽つ" "(ﾉಥ益ಥ)ﾉ" "٩(.•́_ʖ•̀.)۶" "(҂\`ﾛ´)凸" "(•)(•)ԅ(≖‿≖ԅ)"  "٩(๑･ิᴗ･ิ)۶٩(･ิᴗ･ิ๑)۶ﾞ" " |ʘ‿ʘ)╯" "┬┴┬┴┤･ω･)ﾉ" "(¬_¬'')ԅ(-ε-ԅ)" "(╯°益°)╯彡┻━┻")
    ESCALADE_KAOMOJI=${ESCALADE_KAOMOJI_ARRAY[$(($RANDOM$RANDOM%${#ESCALADE_KAOMOJI_ARRAY[@]}))]}
    
    export PS1='${debian_chroot:+($debian_chroot)}\033[0;35m\e[01m$ESCALADE_KAOMOJI < \[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    echo $ESCALADE_KAOMOJI
}


function ctrl_c() {
    
    trap '' INT
    echo -e "${RED}** You pressed CTRL-C => Termination ongoing..${NC}"
    
    set --
    source $ESCALADE_SAFETHREAD_TERMINATE
    [ $? -eq 1 ] && exit 1
    
    for PID in $ESCALADE_NOHUP_PIDLIST; do
        
        echo $PID
        kill -9 $PID 2> /dev/null
    done
    
    echo -e "\a"
    echo -e "${PURPLE}$(esc_kaomoji) < Bye Bye! ($(date))${NC}"
    exit 0
}

#
# Usage checks
#
if [[ "$BASH_SOURCE" == "$0" ]]; then
    
    echo "$0 : This script has to be sourced !" >&2
    exit 1
fi

if [ -z "$ESCALADE" ]; then
    echo "$0: ESCALADE is not defined. Please source setup.[c]sh" >&2
    return 1
fi

if [ $# == 0 ]; then
    
    source $ESCALADE_USAGE $0 $ESCALADE_SAFETHREAD "[<take over the master>]" $# 0
    [ $? -eq 1 ] && return 1
    
else
    source $ESCALADE_USAGE $0 $ESCALADE_SAFETHREAD "[<take over the master>]" $# 1
    [ $? -eq 1 ] && return 1
fi

ESCALADE_TAKEOVERTHREAD=0
[[ ! -z "$1" && $1 != 0 ]] && ESCALADE_TAKEOVERTHREAD=1

set --

if [ -z "$ESCALADE_FORCEACTION" ]; then
    echo -e "${GREEN}--> Thread safe mode enabled${NC}"
else
    echo -e "${ORANGE}--> Safethread disabled because you used --force-action. Hope you know what you are doing :)${NC}"
    return 0
fi

#
# Environment checks
#
if [ -z "$ESCALADE_PID" ]; then
    
    export ESCALADE_PID="$(hostname)-$$"
    echo -e "\t* [PID=$ESCALADE_PID] Escalade PID $ESCALADE_PID registered for production #${ESCALADE_ID}"
fi

if [ -f "$ESCALADE_PRODUCTION/escalade.lock" ]; then
    
    ESCALADE_PID_REFERENCE=$(cat $ESCALADE_PRODUCTION/escalade.lock)
    if [[ $ESCALADE_PID_REFERENCE != $ESCALADE_PID && $ESCALADE_TAKEOVERTHREAD == 1 ]]; then
        
        echo -e "\t${ORANGE}* [PID=$ESCALADE_PID] A mutex (PID=$ESCALADE_PID_REFERENCE) was already set, but you took over it and locked the session !${NC}"
        echo $ESCALADE_PID > $ESCALADE_PRODUCTION/escalade.lock
        
        elif [[ $ESCALADE_PID_REFERENCE != $ESCALADE_PID && $ESCALADE_TAKEOVERTHREAD == 0 ]]; then
        
        echo -e "\t${ORANGE}* [PID=$ESCALADE_PID] A master takes over you.. (PID=$ESCALADE_PID_REFERENCE)\${NC}"
        echo -e "\t${ORANGE}* [PID=$ESCALADE_PID] Your process is going to be killed${NC} (Use --take-over to force)" >&2
        kill -9 $(echo $ESCALADE_PID | cut -d '-' -f2)
        return 1
    else
        ( 2> /dev/null touch $ESCALADE_PRODUCTION/escalade.lock ) 2> /dev/null
        if [ $? -eq 0 ]; then
            echo -e "\t* [PID=$ESCALADE_PID] You are the master ! You can pass.."
        else
            echo -e "${RED}\t* [PID=$ESCALADE_PID] An issue happened while creating the lock file (permission denied?).. abort${NC}"
            return 1
        fi
    fi
else
    
    ( > $ESCALADE_PRODUCTION/escalade.lock 2> /dev/null echo $ESCALADE_PID ) 2>/dev/null
    if [ $? -eq 0 ]; then
        echo -e "\t* [PID=$ESCALADE_PID] You are the master ! You can pass.."
    else
        echo -e "${RED}\t* [PID=$ESCALADE_PID] An issue happened while creating the lock file (permission denied?).. abort${NC}"
        return 1
    fi
fi

return 0
